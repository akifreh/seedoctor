<?php
class Refactor extends CI_Controller 
{
    function process($id)
    {
        ini_set('max_execution_time', 30000);
        print '<pre>';

        $start = time();
        print 'Process Start - ' . date('H:i:s', $start);
        print '<br /><br />';        

        switch ($id) {
            case '1':
                $this->updateSingleDayTimings();
                break;
            case '2':
                $this->updateMultipleDayTimings();
                break;
            case '3':
                $this->changeNutritionistToDietition();
                break;
            case '4':
                $this->addSessionIdAndTokenInSessionTable();
                break;
            case '5':
                $this->insertHowItWorksPage();
                break;
            case '6':
                $this->addPostalCountryandZip();
                break;
            case '7':
                $this->createCardHolder();
                break;
            case '8':
                $this->makeProfileAdditions();
                break;
             case '9':
                $this->deleteExtraRecords();
                break;
            case '10':
                $this->createUserSession();
                break;
            default:
                # code...
                break;
        }

        $end = time();
        print 'Process End - ' . date('H:i:s', $end);

        print '<br /><br />';
        print $end - $start . ' Seconds';

        print '<br />';
        print 'Exiting';
        print '</pre>';
        ini_set('max_execution_time', 300);

    }  

    function createUserSession()
    {
        $sql = "CREATE TABLE IF NOT EXISTS `user_session`(`id` int(11) NOT NULL AUTO_INCREMENT,`user_id` int(11) NOT NULL,`role` tinyint(1) NOT NULL,`email` varchar(255) DEFAULT NULL,`code` varchar(255) NOT NULL,`name` varchar(100) DEFAULT NULL,`created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,PRIMARY KEY (`id`)) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1";
        $this->db->query($sql);
        print 'Table user_session Created Successfully<br /><br />';
    }

    function deleteExtraRecords()
    {
        $sql = "DELETE FROM `notification`";
        $this->db->query($sql);
        print 'Notifications Cleared<br /><br />';

        $sql = "DELETE FROM `appointment_now`";
        $this->db->query($sql);
        print 'Appointment Now Cleared<br /><br />';
    }

    function makeProfileAdditions()
    {
        $sql = "ALTER TABLE `users` ADD `next_of_kin_contact` VARCHAR(50) NULL AFTER `next_of_kin`;";
        $this->db->query($sql);
        print 'next_of_kin_contact field Created Successfully<br /><br />';

        $sql = "ALTER TABLE `pre_existing_condition` ADD `desc` VARCHAR(100) NULL ;";
        $this->db->query($sql);
        print 'Other description field created for pre_existing_condition successfully<br /><br />';        
    }
    
    function createCardHolder()
    {
        $sql = "ALTER TABLE `user_registration` ADD `card_holder` VARCHAR(100) NULL AFTER `card_brand`;";
        $this->db->query($sql);
        print 'Card Holder field Created Successfully<br /><br />';
    }
    
    function addPostalCountryandZip()
    {
        $sql = "ALTER TABLE `users` ADD `p_country` VARCHAR(50) NULL AFTER `p_province`, ADD `p_zip` VARCHAR(50) NULL AFTER `p_country`;";
        $this->db->query($sql);
        print 'Country and Zip fields added for Postal Address.<br /><br />';

        $sql = "ALTER TABLE `users` ADD `employer_number` VARCHAR(20) NULL AFTER `company_employed`, ADD `employer_address` VARCHAR(100) NULL AFTER `employer_number`;";
        $this->db->query($sql);
        print 'Employer Number and Address fields added for Job.<br /><br />';

        $sql = "ALTER TABLE `doctor_cv` ADD `id_card` VARCHAR(150) NULL AFTER `profile_pic`;";
        $this->db->query($sql);
        print 'Id Card field added for Doctor.<br /><br />';        
    }
     
    function insertHowItWorksPage()
    {
        $sql = "INSERT INTO `pages` (`id`, `type`, `title`, `content`, `status`) VALUES (NULL, 'how_it_works', 'How It Works', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec commodo sapien felis, ut sollicitudin dolor pharetra at. Maecenas at hendrerit neque. Integer porttitor felis in ligula tempus consequat. Donec fermentum, ipsum ornare vehicula ultricies, sem nunc porttitor justo, sed consequat lectus ante id risus. Suspendisse potenti. Morbi consequat lectus neque, et rhoncus lorem finibus sed. Sed cursus risus id mi tempor, non bibendum ex lacinia. Aenean pellentesque eu massa a semper. Integer varius luctus nibh, nec condimentum orci sagittis quis. Duis venenatis vulputate enim nec euismod. Vestibulum et ipsum eleifend, faucibus elit sed, convallis lorem. Curabitur mollis hendrerit ornare.', '1');";
        $this->db->query($sql);
        print 'How it works page added<br /><br />';
    }

    function addSessionIdAndTokenInSessionTable()
    {
        $sql = "ALTER TABLE `sessionkey` ADD `sessionId` VARCHAR(200) NULL AFTER `appointment_id`, ADD `token` TEXT NULL AFTER `sessionId`;";
        $this->db->query($sql);
        print 'sessionId and token fields added in sessionkey Table.<br /><br />';
    }

    function changeNutritionistToDietition()
    {
        $this->db->where('id', 1);
        $data['type_of_doctor'] = 'dietitian';
        $this->db->update('doctor_type', $data);

        $this->db->where('id', 1);
        $data1['doctor_type'] = 'Dietitian';
        $this->db->update('charges', $data1);

        print 'Nutritionist Updated to Dietition.<br /><br />';
    }
    
    function updateSingleDayTimings()
    {
        $count = 0;
        $this->db->where('day_id', NULL);
        $this->db->where('status', 0);
        $query = $this->db->get('timing');

        $result = $query->result();
        foreach ($result as $key => $value) 
        {
            $this->db->where('user_id', $value->user_id);
            $query = $this->db->get('available_doctor_days');
            $count = $query->num_rows();
            
            if($count == 1)
            {
                $count++;
                $row = $query->row();
                $data['day_id'] = $row->day_id;
                $data['status'] = 1;
                $this->db->where('id', $value->id);
                $this->db->update('timing', $data);
            }
        }
        print 'Records Updated for timings : ' . $count . '<br /><br />';
                
    }

    function updateMultipleDayTimings()
    {
        $record_count = 0;
        $this->db->where('day_id', NULL);
        $this->db->where('status', 0);
        $query = $this->db->get('timing');

        $result = $query->result();
        foreach ($result as $key => $value) 
        {
            $this->db->where('user_id', $value->user_id);
            $query = $this->db->get('available_doctor_days');
            $count = $query->num_rows();
            $check_first_record = 1;

            if($count > 1)
            {
                $rows = $query->result();
                foreach ($rows as $k => $row) 
                {
                    $record_count++;
                    if($check_first_record == 1)
                    {
                        $data['day_id'] = $row->day_id;
                        $data['status'] = 1;
                        $this->db->where('id', $value->id);
                        $this->db->update('timing', $data);
                        $check_first_record++;
                    }
                    else
                    {
                        $data['day_id'] = $row->day_id;
                        $data['status'] = 1;
                        $data['start_time'] = $value->start_time;
                        $data['end_time'] = $value->end_time;
                        $data['user_id'] = $value->user_id;
                        $this->db->insert('timing', $data);
                    }
                }
            }
            elseif($count == 0)
            {
                $data['status'] = 1;
                $this->db->where('id', $value->id);
                $this->db->update('timing', $data);
            }
        }
        print 'Records Updated for timings : ' . $record_count . '<br /><br />';
                
    }
}