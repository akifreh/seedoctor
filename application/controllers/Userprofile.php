<?php
class Userprofile extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->library(array('session', 'form_validation', 'email'));
        $this->load->helper(array('form','url'));
        $this->load->database();
        $this->load->model('user_profile');
        $this->load->helper('security');
    }
    
    function index()
    {
        if(empty ($this->session->userdata['logged_in']['role']))
        {
            $url = uri_string();
            if(isset($_SERVER['QUERY_STRING']) && $_SERVER['QUERY_STRING'])
                $url .= '?' . $_SERVER['QUERY_STRING'];
            $this->session->set_flashdata('verify_msg','<div class="alert alert-danger text-center">Please login before you visit the site!</div>');
            $this->session->set_userdata('redirectUrl', $url);
            redirect();
        }

        $role = $this->session->userdata['logged_in']['role'];
        $userId = $this->session->userdata['logged_in']['id'];
        if($role == 2)
        {
            $result = $this->user_profile->checkUserProfileExist($userId); 
            if($result == FALSE)
            {   
                // surpassing add medical profile to update medical profile
                redirect('userprofile/updateMedicalHistory');
            }
            else
            {  
                $result['userinfo'] = $this->user_profile->readUserInfo($userId);
                $result['userprofile'] = $this->user_profile->readUserProfiles($userId);
                $result['userallergies'] = $this->user_profile->readAllergies($userId);
                $result['userpecs'] = $this->user_profile->readPreExistingCondition($userId); 
                $result['userpersonalupdates'] = $this->user_profile->readPersonalUpdate($userId);  
                $result['userfamilyhistory'] = $this->user_profile->readFamilyHistory($userId);
                $result['usermydoctor'] = $this->user_profile->readMyDoctor($userId);
                $result['previousHistory'] = $this->user_profile->readPreviousHistory($userId);
                $header_data['breadcrumbs']['medical_profile'] = TRUE;
                $this->load->view('header', $header_data);
                $this->load->view('user/display_medical_history', $result);
                $this->load->model('page_model');
                $footer['footer'] = $this->page_model->get_content('footer');
                $this->load->view('footer', $footer);
            }
        }
        else
        {
            
            $this->session->set_flashdata('verify_msg','<div class="alert alert-danger text-center">You are not authorized to visit this page. Sorry!</div>');
            redirect();
        }
       
    }
     
    function addMedicalHistory()
    {
        if(empty ($this->session->userdata['logged_in']['role']))
        {
            $url = uri_string();
            if(isset($_SERVER['QUERY_STRING']) && $_SERVER['QUERY_STRING'])
                $url .= '?' . $_SERVER['QUERY_STRING'];
            $this->session->set_flashdata('verify_msg','<div class="alert alert-danger text-center">Please login before you visit the site!</div>');
            $this->session->set_userdata('redirectUrl', $url);
            redirect();
        }

        $role = $this->session->userdata['logged_in']['role'];
        $user_id = $this->session->userdata['logged_in']['id'];
        $result = $this->user_profile->checkUserProfileExist($user_id);

        if($role == 2)
        { 
            // Surpassing add with update
            redirect('Userprofile/updateMedicalHistory');
            if(!$result)
            {
                $data['getallergies'] = $this->user_profile->readAllergies($user_id);
                $data['getPreExisting'] = $this->user_profile->readPreExistingCondition($user_id);
                $data['getPersonalUpdate'] = $this->user_profile->readPersonalUpdate($user_id);
                $data['familyHistory'] = $this->user_profile->readFamilyHistory($user_id);
                $data['myDoctor'] = $this->user_profile->readMyDoctor($user_id);
                $data['profilepic'] = $this->user_profile->readUserInfo($user_id);
                
                $header_data['breadcrumbs']['medical_profile'] = TRUE;
                $this->load->view('header', $header_data);
                $this->load->view('user/add_medical_history', $data);
                $this->load->model('page_model');
                $footer['footer'] = $this->page_model->get_content('footer');
                $this->load->view('footer', $footer);
            }
            else
                redirect('Userprofile/updateMedicalHistory');
        }
        else
        {
           $this->session->set_flashdata('verify_msg','<div class="alert alert-danger text-center">You are not authorized to visit this page. Sorry!</div>');
            redirect();
        }
    }
       
    function updateMedicalHistory()
    {
        $role = isset($this->session->userdata['logged_in']['role']) ? $this->session->userdata['logged_in']['role'] : NULL;
        if($role == 2)
        {   
            $user_id = $this->session->userdata['logged_in']['id'];
            $data['getallergies'] = $this->user_profile->readAllergies($user_id);
            $data['getPreExisting'] = $this->user_profile->readPreExistingCondition($user_id);
            $data['getPersonalUpdate'] = $this->user_profile->readPersonalUpdate($user_id);
            $data['familyHistory'] = $this->user_profile->readFamilyHistory($user_id);
            $data['myDoctor'] = $this->user_profile->readMyDoctor($user_id);
            $data['userInfo'] = $this->user_profile->readUserInfo($user_id);
            $data['profileInfo'] = $this->user_profile->readUserProfiles($user_id);
            $data['previousHistory'] = $this->user_profile->readPreviousHistory($user_id);
            $data['preExisting'] = array();
            
            if($data['getPreExisting'])
            {
                foreach ($data['getPreExisting'] as $key => $value) {
                    $data['preExisting'][] = $value->feeling;
                    if($value->desc)
                        $data['preExisting']['desc'] = $value->desc;
                }
            }

            $data['oldTab'] = $this->session->flashdata('tab') ? $this->session->flashdata('tab') : 'my-allergies';  
            $header_data['breadcrumbs']['medical_profile'] = TRUE;
            $this->load->view('header', $header_data);
            $this->load->view('user/edit_medical_history', $data);
            $this->load->model('page_model');
            $footer['footer'] = $this->page_model->get_content('footer');
            $this->load->view('footer', $footer);
        }
        else
            redirect();
   }
      
       
    function addUserInfo()
    {
        $userId = $this->session->userdata['logged_in']['id'];
        $this->form_validation->set_rules('diet', 'Diet', 'trim|required');
        $this->form_validation->set_rules('height', 'Height', 'trim|required');
        $this->form_validation->set_rules('weight', 'Weight', 'trim|required');
        $this->form_validation->set_rules('sport_activity', 'Sport', 'trim|required');
        $this->form_validation->set_rules('alcohol', 'Alcohol', 'trim|required');
        $this->form_validation->set_rules('smoke', 'Smoke', 'trim|required');
        //$this->form_validation->set_rules('other', 'Other', 'trim|required');
        
       /* $this->form_validation->set_rules('resistant[]', 'resistant', 'trim|required');
        $this->form_validation->set_rules('reaction[]', 'reaction', 'trim|required');
        
        $this->form_validation->set_rules('feeling[]', 'Feeling', 'trim|required');
        
        $this->form_validation->set_rules('condition[]', 'Condition', 'trim|required');
        $this->form_validation->set_rules('fh_relationship[]', 'Relationship', 'trim|required');
        
        $this->form_validation->set_rules('md_name[]', 'Smoke', 'trim|required');
        $this->form_validation->set_rules('md_sname[]', 'Other', 'trim|required');
        $this->form_validation->set_rules('md_email[]', 'Smoke', 'trim|required');
        $this->form_validation->set_rules('md_number[]', 'Other', 'trim|required');
        
        $this->form_validation->set_rules('pu_feeling[]', 'Smoke', 'trim|required');
        $this->form_validation->set_rules('symptom[]', 'Other', 'trim|required');
        $this->form_validation->set_rules('med_taken[]', 'Smoke', 'trim|required');
        $this->form_validation->set_rules('describe_med[]', 'Other', 'trim|required');*/
        
        if ($this->form_validation->run() == FALSE) 
        {
            $this->session->set_flashdata('msg', '<div class="alert alert-danger text-center">Oops! Error.  Please try again later!!!</div>');
            redirect('userprofile/addmedicalhistory');    
        } 
        else
        {
            $data = array(
                'user_id' => $userId,
                'diet' => $this->input->post('diet'),
                'height' => $this->input->post('height'),
                'weight' => $this->input->post('weight'),
                //'age' => $this->input->post('age'),
                'sport_activity' => $this->input->post('sport_activity'),
                'alcohol' => $this->input->post('alcohol'),
                'smoke' => $this->input->post('smoke'),
                'other' => $this->input->post('other')
                
            );
            
            $this->user_profile->insertUserPersonalInfo($data);

            $resistant = $this->input->post('resistant');
            $reaction = $this->input->post('reaction');
            $loop = count($resistant);
            if($loop)
            {
                for ( $index =0; $index < $loop; $index++)
                {           
                    $data1 = array(     
                        'resistant' =>  $resistant[$index],
                        'reaction' =>  $reaction[$index],
                        'user_id' => $userId  
                    );
                    $this->user_profile->insertAllergies($data1);
                }
            }
              
            $feeling = $this->input->post('feeling');
            $loop = count($feeling);
            if($loop)
            {
                for ( $index =0; $index < $loop; $index++)
                {    
                    $data2 = array(     
                        'feeling' =>  $feeling[$index],
                        'user_id' => $userId 
                    );
                $this->user_profile->insertPreExistingCondition($data2);
                }
            }
            
            $b_dd = $this->input->post('pu_dd');
            $b_mm = $this->input->post('pu_mm');
            $b_yy = $this->input->post('pu_yy');
            $dob = $b_yy .'-'. $b_mm .'-'. $b_dd;
        
            if($this->input->post('pu_feeling'))
            {
                $data3 = array(
                    'user_id' => $userId,
                    'feeling' => $this->input->post('pu_feeling'),
                    'symptom' => $this->input->post('symptom'),
                    'med_taken' => $this->input->post('med_taken'),
                    'describe_med' => $this->input->post('describe_med'),
                    'date' => $dob,
                );
            
                $this->user_profile->insertPersonalUpdates($data3);
            }

            $condition = $this->input->post('condition');
            $fh_relationship = $this->input->post('fh_relationship');
            $loop = count($condition);
            if($loop)
            {
                for ( $index =0; $index < $loop; $index++)
                {
                    $data4 = array(     
                        'condition' =>  $condition[$index],
                        'relationship' =>  $fh_relationship[$index],
                        'user_id' => $userId
                    );
                    $this->user_profile->insertFamilyHistory($data4);
                }
            }

            $mdName = $this->input->post('md_name');
            $mdSname = $this->input->post('md_sname');
            $mdNumber = $this->input->post('md_number');
            $mdEmail = $this->input->post('md_email');
            $loop = count($mdName);
            if($loop)
            {
                for ( $index =0; $index < $loop; $index++)
                {    
                    $data5 = array(     
                        'email' =>  $mdEmail[$index],
                        'contact_number' =>  $mdNumber[$index],
                        'name' =>  $mdName[$index],
                        'sname' =>  $mdSname[$index],
                        'user_id' => $userId
                    );
                    $this->user_profile->insertMyDoctor($data5);
                }
            }
            
            redirect('userprofile');
        }
 
    }

    function editAllergies()
    {
        $userId = $this->session->userdata['logged_in']['id'];
        $id = $this->input->post('allergyid');
        
        $this->form_validation->set_rules('resistant', 'resistant', 'trim|required');
        $this->form_validation->set_rules('reaction', 'reaction', 'trim|required');
        
        if($this->form_validation->run() == FALSE)
        {
            $this->session->set_flashdata('msg', '<div class="alert alert-danger text-center">Oops! Error.  Please try again later!!!</div>');
            $this->session->set_flashdata('tab','my-allergies');
            redirect('userprofile/updatemedicalhistory');  
        }
        else
        {
            $data = array(  
                'resistant' =>  $this->input->post('resistant'),
                'reaction' =>   $this->input->post('reaction')
            );
        
            $this->user_profile->updateAllergies($id,$data);
            $this->session->set_flashdata('tab','my-allergies');
            redirect('userprofile/updatemedicalhistory'); 
        }    
    }    
    
    function editPreExistingCondition()
    {
        $userId = $this->session->userdata['logged_in']['id'];
        $id = $this->input->post('pecid');
        $this->form_validation->set_rules('pec_feeling', 'feeling', 'trim|required');
        
        if($this->form_validation->run() == FALSE)
        {
            $this->session->set_flashdata('msg', '<div class="alert alert-danger text-center">Oops! Error.  Please try again later!!!</div>');
            $this->session->set_flashdata('tab','pec');
            redirect('userprofile/updatemedicalhistory'); 
        }
        else
        {
            $data = array('feeling' =>  $this->input->post('pec_feeling'));
        
            $this->user_profile->updatePreExistingCondition($id, $data);
            $this->session->set_flashdata('tab','pec');
            redirect('userprofile/updatemedicalhistory');
        }
    }

    function editPersonalUpdate()
    {
        $userId = $this->session->userdata['logged_in']['id'];
        $id = $this->input->post('puid');
        
        $this->form_validation->set_rules('pu_feeling', 'feeling', 'trim|required');
        $this->form_validation->set_rules('pu_symptom', 'symptom', 'trim|required');
        $this->form_validation->set_rules('pu_med', 'med_taken', 'trim|required');
        $this->form_validation->set_rules('pu_desc_med', 'describe_med', 'trim|required');

        if($this->form_validation->run() == FALSE)
        {
            $this->session->set_flashdata('msg', '<div class="alert alert-danger text-center">Oops! Error.  Please try again later!!!</div>');
            $this->session->set_flashdata('tab','health-updates');
            redirect('userprofile/updatemedicalhistory');
        }
        else
        {
            //$bdd = $this->input->post('b_dd');
            //$bmm = $this->input->post('b_mm');
            //$byy = $this->input->post('b_yy');
            $dob = date('Y-m-d'); //$byy . $bmm . $bdd;
            $data = array(    
                'feeling' =>  $this->input->post('pu_feeling'),
                'symptom' =>  $this->input->post('pu_symptom'),
                'med_taken' =>  $this->input->post('pu_med'),
                'describe_med' =>  $this->input->post('pu_desc_med'),
                'date' =>  $dob
            );

            $this->user_profile->updatePersonalUpdate($id,$data);
            $this->session->set_flashdata('tab','health-updates');
            redirect('userprofile/updatemedicalhistory');
        }
    }

    function editFamilyHistory()
    {
        $userId = $this->session->userdata['logged_in']['id'];
        $id = $this->input->post('fhid');
        
        $this->form_validation->set_rules('fh_condition', 'condition', 'trim|required');
        $this->form_validation->set_rules('fh_relationship', 'relationship', 'trim|required');
          
        if($this->form_validation->run() == FALSE)
        {
            $this->session->set_flashdata('msg', '<div class="alert alert-danger text-center">Oops! Error.  Please try again later!!!</div>');
            $this->session->set_flashdata('tab','family-history');
            redirect('userprofile/updatemedicalhistory');  
        }
        else
        {
            $data = array(        
                'condition' =>  $this->input->post('fh_condition'),
                'relationship' =>  $this->input->post('fh_relationship')
            );
            
            $this->user_profile->updateFamilyHistory($id,$data);
            $this->session->set_flashdata('tab','family-history');
            redirect('userprofile/updatemedicalhistory');  
        }
    }

    function editMyDoctor()
    {
        $userId = $this->session->userdata['logged_in']['id'];
        $id = $this->input->post('mdid');
        
        $this->form_validation->set_rules('md_name', 'name', 'trim|required');
        $this->form_validation->set_rules('md_sname', 'sname', 'trim|required');
        $this->form_validation->set_rules('md_number', 'contact_number', 'trim|required');
        $this->form_validation->set_rules('md_email', 'email', 'trim|required');
         
        if($this->form_validation->run() == FALSE)
        {
            $this->session->set_flashdata('msg', '<div class="alert alert-danger text-center">Oops! Error.  Please try again later!!!</div>');
            $this->session->set_flashdata('tab','my-doctors');
            redirect('userprofile/updatemedicalhistory');
        }
        else
        {
            $data = array(        
                'name' =>  $this->input->post('md_name'),
                'sname' =>  $this->input->post('md_sname'),
                'contact_number' =>  $this->input->post('md_number'),
                'email' =>  $this->input->post('md_email')
            );
            
            $this->user_profile->updateMyDoctor($id, $data);
            $this->session->set_flashdata('tab','my-doctors');
            redirect('userprofile/updatemedicalhistory');   
        }
    }   
    
    function addNewAllergy()
    {    
        $userId = $this->session->userdata['logged_in']['id'];
        $this->form_validation->set_rules('add_resistant', 'resistant', 'trim|required');
        $this->form_validation->set_rules('add_reaction', 'reaction', 'trim|required');
     
        if($this->form_validation->run() == FALSE)
        {
            $this->session->set_flashdata('msg', '<div class="alert alert-danger text-center">Oops! Error.  Please try again later!!!</div>');
            $this->session->set_flashdata('tab','my-allergies');
            redirect('userprofile/updatemedicalhistory');
        }
        else
        {
            $data = array(
                'user_id' => $userId,
                'resistant' => $this->input->post('add_resistant'),
                'reaction' => $this->input->post('add_reaction')
            );

            $this->user_profile->insertAllergies($data);
            $this->session->set_flashdata('tab','my-allergies');
            redirect('userprofile/updatemedicalhistory');
        }
      
    }
    
    function addNewPec()
    {
        $userId = $this->session->userdata['logged_in']['id'];
        $this->form_validation->set_rules('add_feeling[]', 'feeling', 'trim|required');
        
        if($this->form_validation->run() == FALSE)
        {
            $this->session->set_flashdata('msg', '<div class="alert alert-danger text-center">Oops! Error.  Please try again later!!!</div>');
            $this->session->set_flashdata('tab','pec');
            redirect('userprofile/updatemedicalhistory'); 
        }
        else
        {   
            $feelings = $this->input->post('add_feeling');
            $getPreExisting = $this->user_profile->readPreExistingCondition($userId);
            
            $array = array();
            if($getPreExisting)
            {
                foreach ($getPreExisting as $key => $value) 
                {
                    $array[$value->id] = $value->feeling;
                }          
            }

            $to_insert = array_diff($feelings, $array);
            $to_delete = array_diff($array, $feelings);

            foreach($to_insert as $value)
            {
                $data = array(
                    'user_id' => $userId,
                    'feeling' => $value,
                );
                $this->user_profile->insertPreExistingCondition($data);
            }

            if($desc = $this->input->post('other_desc'))
            {
                $data_other = array(
                    'user_id' => $userId,
                    'feeling' => 'Other',
                    'desc'  => $desc
                );

                $this->user_profile->savePECOther($userId, $data_other);
            }

            foreach ($to_delete as $key => $value)
              $this->user_profile->deletePec($key);  

            $this->session->set_flashdata('tab','pec');
            redirect('userprofile/updatemedicalhistory');
        }
        
    }
    
    function addNewPersonalUpdate()
    {    
        $userId = $this->session->userdata['logged_in']['id'];
        $this->form_validation->set_rules('add_pu_feeling', 'feeling', 'trim|required');
        $this->form_validation->set_rules('add_symptom', 'symptom', 'trim|required');
        $this->form_validation->set_rules('add_med_taken', 'med_taken', 'trim|required');
        $this->form_validation->set_rules('add_desc_med', 'describe_med', 'trim|required');
          
        if($this->form_validation->run() == FALSE)
        {
            $this->session->set_flashdata('msg', '<div class="alert alert-danger text-center">Oops! Error.  Please try again later!!!</div>');
            $this->session->set_flashdata('tab','health-updates');
            redirect('userprofile/updatemedicalhistory'); 
        }
        else
        {
            //$dd = $this->input->post('dd');
            //$mm = $this->input->post('mm');
            //$yy = $this->input->post('yy');
            
            $date = date('Y-m-d');
            $data = array(
                'user_id' => $userId,
                'feeling' => $this->input->post('add_pu_feeling'),
                'symptom' => $this->input->post('add_symptom'),
                'med_taken' => $this->input->post('add_med_taken'),
                'describe_med' => $this->input->post('add_desc_med'),
                'date' => $date
            );
            
            $this->user_profile->insertPersonalUpdates($data);
            $this->session->set_flashdata('tab','health-updates');
            redirect('userprofile/updatemedicalhistory'); 
        }
    }
    
    function addNewFamilyHistory()
    {
        $userId = $this->session->userdata['logged_in']['id'];
        $this->form_validation->set_rules('add_condition', 'condition', 'trim|required');
        $this->form_validation->set_rules('add_relation', 'relationship', 'trim|required');
      
        if($this->form_validation->run() == FALSE)
        {
            $this->session->set_flashdata('msg', '<div class="alert alert-danger text-center">Oops! Error.  Please try again later!!!</div>');
            $this->session->set_flashdata('tab','family-history');
            redirect('userprofile/updatemedicalhistory'); 
        }
        else
        {
            $data = array(
                'user_id' => $userId,
                'condition' => $this->input->post('add_condition'),
                'relationship' => $this->input->post('add_relation')
            );
            
            $this->user_profile->insertFamilyHistory($data);
            $this->session->set_flashdata('tab','family-history');
            redirect('userprofile/updatemedicalhistory');
        }
    }   
    
    function addNewMyDoctor()
    {
        $userId = $this->session->userdata['logged_in']['id'];
        $this->form_validation->set_rules('add_name', 'name', 'trim|required');
        $this->form_validation->set_rules('add_sname', 'sname', 'trim|required');
        $this->form_validation->set_rules('add_number', 'contact_number', 'trim|required');
        $this->form_validation->set_rules('add_email', 'email', 'trim|required');

        if($this->form_validation->run() == FALSE)
        {
            $this->session->set_flashdata('msg', '<div class="alert alert-danger text-center">Oops! Error.  Please try again later!!!</div>');
            $this->session->set_flashdata('tab','my-doctors');
            redirect('userprofile/updatemedicalhistory');
        }
        else
        {
            $data = array(
                'user_id' => $userId,
                'name' => $this->input->post('add_name'),
                'sname' => $this->input->post('add_sname'),
                'contact_number' => $this->input->post('add_number'),
                'email' => $this->input->post('add_email')
            );
            
            $this->user_profile->insertMyDoctor($data);
            $this->session->set_flashdata('tab','my-doctors');
            redirect('userprofile/updatemedicalhistory');   
        }
    } 
 
    function deleteAllergy()
    {
       $id = $this->input->post('allergy_id');

       $this->user_profile->deleteMyAllergy($id);
       $this->session->set_flashdata('tab','my-allergies');
       redirect('userprofile/updatemedicalhistory');   
    }
    
    function deletePreExistingCondition()
    {
        $id = $this->input->post('pec_id');
        
        $this->user_profile->deletePec($id);
        redirect('userprofile/updatemedicalhistory');    
    }
    
    function deletePersonalUpdate()
    {
        $id = $this->input->post('pu_id');
        
        $this->user_profile->deletePu($id);
        $this->session->set_flashdata('tab','health-updates');
        redirect('userprofile/updatemedicalhistory');   
    }
    
    function deleteFamilyHistory()
    {
        $id = $this->input->post('fh_id');
        
        $this->user_profile->deleteFh($id);
        $this->session->set_flashdata('tab','family-history');
        redirect('userprofile/updatemedicalhistory');     
    }
    
    function deleteMyDoctor()
    {
        $id = $this->input->post('md_id');
        
        $this->user_profile->deleteMd($id);
        $this->session->set_flashdata('tab','my-doctors');
        redirect('userprofile/updatemedicalhistory');     
    }
    
    function updateProfileInfo()
    {
        $userId = $this->session->userdata['logged_in']['id'];
        
        $this->form_validation->set_rules('diet', 'name', 'trim|required');
        $this->form_validation->set_rules('height', 'sname', 'trim|required');
        $this->form_validation->set_rules('weight', 'contact_number', 'trim|required');
        $this->form_validation->set_rules('sport_activity', 'email', 'trim|required');
        
        if($this->form_validation->run() == FALSE)
        {
            $this->session->set_flashdata('msg', '<div class="alert alert-danger text-center">Oops! Error.  Please try again later!!!</div>');
            redirect('userprofile/updatemedicalhistory'); 
        }
        else
        {
            $data = array(
                'diet' => $this->input->post('diet'),
                'height' => $this->input->post('height'),
                'weight' => $this->input->post('weight'),
                'sport_activity' => $this->input->post('sport_activity'),
                'alcohol' => $this->input->post('alcohol'),
                'smoke' => $this->input->post('smoke'),
                'activity_level' => $this->input->post('activity_level'),
                'bmi' => $this->input->post('bmi'),
                'bmr' => $this->input->post('bmr'),
                'other' => $this->input->post('other')
            ); 
            
            $this->user_profile->updateUserProfileInfo($userId, $data);
            redirect('userprofile/updatemedicalhistory');
        }
    }

    function viewPendingPayments()
    {
        $userId = $this->session->userdata['logged_in']['id'];
        $data['records'] = $this->user_profile->view_pending_amount($userId);
        $this->load->view('header', $header_data);
        $this->load->view('user/payment',$data);
        $this->load->model('page_model');
        $footer['footer'] = $this->page_model->get_content('footer');
        $this->load->view('footer', $footer);
    }

    function payPendingAmount($id = NULL)
    {
        if(!$id)
        redirect('Userprofile/viewPendingPayments');

        $this->load->model('call_model');
        $record = $this->call_model->getAppointmentById($id);
        $payment = $this->call_model->getPaymentByAppointmentId($id);
        if($record && $payment && $payment->status != 1)
        {
            $user_registration = $this->call_model->get_user_registration($record->p_id);
            $patient = $this->call_model->getUserById($record->p_id);
            $doctor = $this->call_model->getUserById($record->d_id);

            $response = $this->takePayment($user_registration, $payment->price, $patient, $id);
            if($response['response'] == 'success')
            {    
                $this->call_model->updatePayment($payment->id, $response['data']);
                $my_payment['payment_id'] = $payment->id;
                $this->call_model->sendPatientInvoiceEmail($patient->email, $patient, $doctor, $my_payment);
                
                $this->session->set_flashdata('msg','<div class="alert alert-success text-center">Thank you for settling the pending payment. You may now proceed to consult with a medical practitioner immediately or book an appointment.</div>');
                redirect('Userprofile/viewPendingPayments');
            }

            $this->session->set_flashdata('msg','<div class="alert alert-danger text-center">An error occured during payment. Please try again. ' . $response['error_msg'] . '</div>');
            redirect('Userprofile/viewPendingPayments');
        }

        $this->session->set_flashdata('msg','<div class="alert alert-danger text-center">No Record Found</div>');
        redirect('Userprofile/viewPendingPayments');

    }

    function takePayment($id = NULL, $amount = NULL, $patient, $appointment_id) 
    {
        if(!$id || !$amount)
            return array('response' => 'error', 'error_msg' => 'No registration id or amount');
        
        $success_codes = array('000.000.000', '000.400.000', '000.400.010', '000.400.020', '000.400.040', '000.400.060', '000.400.090','000.100.110', '000.100.111', '000.100.112');

        $site = $this->config->item('IS_LIVE') ? 'live_' : 'local_';
        $peachUrl = $this->config->item('IS_LIVE') ? 'https://oppwa.com/' : 'https://test.oppwa.com/';
        $url = $peachUrl . "v1/registrations/" . $id . "/payments";
        $data = "authentication.userId=" . $this->config->item($site . 'peach_userId') . 
           "&authentication.password=" . $this->config->item($site . 'peach_password') . 
            "&authentication.entityId=" . $this->config->item($site . 'peach_entityId_onceOff') .
            "&amount=" . number_format($amount, 2, '.', '') .
            "&currency=ZAR" .
            "&paymentType=DB" .
            "&recurringType=REPEATED" .
            "&customer.email=" . $patient->email . 
            "&customer.givenName=" . $patient->fname . 
            "&customer.surname=" . $patient->sname . 
            "&merchantTransactionId=" . $appointment_id;
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, $this->config->item('IS_LIVE'));
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $responseData = curl_exec($ch);
       
        if(curl_errno($ch)) {
            return array('response' => 'error', 'error_msg' => curl_error($ch));
        }
        curl_close($ch);
        $response = json_decode($responseData);

        if(!$response)
            return array('response' => 'error', 'error_msg' => 'No data found');

        if(in_array($response->result->code, $success_codes))
            return array('response' => 'success', 'data' => array('transaction_id' => $response->id, 'created_at' => $response->timestamp, 'status' => 1));
        return array('response' => 'error', 'error_msg' => $response->result->description);
    }
}