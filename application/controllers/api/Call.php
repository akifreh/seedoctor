<?php
require(APPPATH . 'libraries/REST_Controller.php');

class Call extends REST_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->helper(array('form','url'));
        $this->load->library(array('session', 'form_validation', 'email'));
        $this->load->database();
        $this->load->model('user_model');
        $this->load->model('appointment_model');
        $this->load->helper('security');
        $this->load->model('call_model');
        
        if($this->post() && !$_POST)
            $_POST = $this->post();
    }
    
    function index_get()
    {
        $code = $this->get('code');
        $userData = $this->user_model->getSession($code);
        
        if($userData)
        {
            $sessionKey = $this->get('sessionKey');
            if(!$sessionKey)
            {
                //$this->session->unset_userdata('call_url');
                $data['msg'] = 'You are not authorized to visit this page. Sorry!';
                $this->response($data, 404);
            } 
                
            $data['sesskey'] = $this->call_model->readSessionKey($sessionKey);
            if(!$data['sesskey'])
            {
                //$this->session->unset_userdata('call_url');
                $data['msg'] = 'You are not authorized to visit this page. Sorry!';
                $this->response($data, 404);
            } 

            $pId = $data['sesskey']->p_id;
            $dId = $data['sesskey']->d_id;
            $sessionId = $data['sesskey']->sessionId;
            $token = $data['sesskey']->token;

            if($userData->role == 2)
            {
                if($pId != $userData->user_id) 
                {
                    //$this->session->unset_userdata('call_url');  
                    $data['msg'] = 'You are not authorized to visit this page. Sorry!';
                    $this->response($data, 404);
                }
                /******************** START OF - CLOSING THIS FOR NO-CHARGE TEMPORARY *************
                elseif($this->appointment_model->has_card_expired($pId))
                {
                    $data['msg'] = 'Your card has expired. To retain call service please edit your card details!';
                    $this->response($data, 400);
                }
                elseif($this->appointment_model->has_pending_amount($pId))
                {
                    $data['msg'] = 'You have pending amount remaining. Please pay amount to resume services!';
                    $this->response($data, 400);
                }
                /********************** END OF - CLOSING THIS FOR NO-CHARGE TEMPORARY *************/      
            }
            elseif($userData->role == 3 && $dId != $userData->user_id)
            {
                //$this->session->unset_userdata('call_url');
                $data['msg'] = 'You are not authorized to visit this page. Sorry!';
                $this->response($data, 404);
            } 

            $data['patientInfo'] = $this->call_model->getUserById($pId);
            $data['doctorInfo'] = $this->call_model->getUserById($dId);
            $data['session'] = $userData;
            $data['username'] = $data['session']->name;
            $data['room_name'] = $dId . '-' . $pId .'-'. date('d-M');
            $data['db_name'] = 'Chat-' . date('d-M-y');
            $data['session_key'] = $sessionKey;
            $data['mapKey'] = $this->config->item('map_key');
            if(!$sessionId || !$token)
                list($sessionId, $token) = $this->getSessionId($sessionKey);
            
            $data['openTok_sessionId'] = $sessionId;
            $data['openTok_token'] = $token;
            $data['openTok_apiKey'] = $this->config->item('OPENTOK_API_KEY');

            if($userData->role == 2) // && empty($this->session->userdata('call_url')))
            {    
                $doctor_message = 'Patient '.$data['session']->name. ' has started the call.';
                $notification_array =  array();
                $notification_array['notification_type'] = "call";
                $notification_array['appointment_type'] = "now";
                $notification_array['message'] = $doctor_message;
                $notification_array['user_id'] = $dId;
                $notification_array['p_id'] = $pId;
                $notification_array['d_id'] = $dId;
                
                $notification_array['appointment_id'] = $data['sesskey']->appointment_id;
                $this->appointment_model->insertNotification($notification_array);
            }
            
          
            if($userData->role == 3) // && empty($this->session->userdata('call_url')))
            {  
                $patient_message = 'Doctor '.$data['session']->name. ' has started the call.';
                $notification_array =  array();
                $notification_array['notification_type'] = "call";
                $notification_array['appointment_type'] = "now";
                $notification_array['message'] = $patient_message;
                $notification_array['user_id'] = $pId;
                $notification_array['p_id'] = $pId;
                $notification_array['d_id'] = $dId;

                $notification_array['appointment_id'] = $data['sesskey']->appointment_id;
                $this->appointment_model->insertNotification($notification_array);    
            }
            
            //if(empty($this->session->userdata('call_url')))
              //  $this->session->set_userdata('call_url', 'call?sessionkey=' . $sessionKey);
            
            $data['call_url'] = 'call?sessionkey=' . $sessionKey;

            $data['breadcrumbs']['call'] = TRUE;
            $this->load->model('page_model');
            $data['footer'] = $this->page_model->get_content('footer');
            
            $this->response($data, 200);

        }
        else
        {
            $data['url'] = uri_string();
            if(isset($_SERVER['QUERY_STRING']) && $_SERVER['QUERY_STRING'])
                $data['url'] .= '?' . $_SERVER['QUERY_STRING'];
            
            $data['msg'] = 'Please login before you visit the site!';
            $this->response($data, 401);
        }
    }
    
    function callEnd_post()
    {
        $session_key = $this->post('sessionKey');
        $start_time = $this->post('startTime');

        $end_time = time();

        //$start_time = $this->session->userdata('start_time');
        //$this->session->unset_userdata('start_time');
        //$this->session->unset_userdata('call_url');
        $data = array();

        $record = $this->call_model->readSessionKey($session_key);
        if(!$record)
        {
            $data['msg'] = 'Call had been ended earlier';
            $this->response($data, 400);
        }

        // Update session key to 0 for redirection of patient
        $row =  array('id' => $record->id, 'status' => 0);
        $this->call_model->updateSessionKey($row);

        /******************** START OF - CLOSING THIS FOR NO-CHARGE TEMPORARY *************
        // First Call for all patients are free. Check for First Call.
        $isFirstCall = $this->call_model->checkFirstCall($record->p_id); ******************/
        $isFirstCall = TRUE;
        /********************** END OF - CLOSING THIS FOR NO-CHARGE TEMPORARY *************/
        
        // Calculate duration & record to db
        $duration = 0;
        if($start_time)
            $duration = $end_time - $start_time;
        $payment = $this->recordPayment($duration, $record, $isFirstCall);
        $patient = $this->call_model->getUserById($record->p_id);
        $doctor = $this->call_model->getUserById($record->d_id);

        /*************** DO PAYMENT THINGS HERE *********************/
        if(!$isFirstCall)
        {
            $user_registration = $this->call_model->get_user_registration($record->p_id);
            $response = $this->takePayment($user_registration, $payment['amount'], $patient, $record->appointment_id);
        
            if($response['response'] == 'success')
            {
                $this->call_model->updatePayment($payment['payment_id'], $response['data']);
                $this->call_model->sendPatientInvoiceEmail($patient->email, $patient, $doctor, $payment);
            }
        }
        else
            $this->call_model->sendPatientNoInvoiceEmail($patient->email, $patient, $doctor, $payment);
        /*************** END OF PAYMENTS ****************************/

        // Update patient served for doctor
        $this->call_model->updatePatientServed($record->d_id);

        // Update meeting_approval to 3 meaning finished
        $data1 = array(
            'id' => $record->appointment_id,
            'meeting_approval' => 3
        );
        
        $this->call_model->updateAppointmentStatus($data1);
    
        $data['redirect'] = 'Redirect doctor to add prescription page';
        $data['url'] = 'api/call/addPrescription/';
        $data['appointmentId'] = $record->appointment_id;
        $data['msg'] = 'Call ended successfully.';
        
        $this->response($data, 200);
    }

    function addPrescription_post()
    {
        $prescription = $this->post('prescription');
        $appointment_id = $this->post('appointmentId');

        if(!$prescription || !$appointment_id)
        {
            $data['msg'] = 'No prescription or appointment id given.';
            $this->response($data, 400);
        }

        $session_key = $this->post('sessionKey');
        $record = $this->call_model->readSessionKey($session_key, FALSE);
        
        if(!$record)
        {
            $data['msg'] = 'Session key does not exist.';
            $this->response($data, 400);
        }

        //delete session key from db
        $this->call_model->deleteSessionKey($session_key);
        $record = $this->call_model->getAppointmentById($appointment_id);
        
        if(!$record)
        {
            $data['msg'] = 'appointment does not exist.';
            $this->response($data, 400);
        }

        $patient = $this->call_model->getUserById($record->p_id);
        $this->load->model('admin_model');

        $doctor = $this->admin_model->readDoctorDetails($record->d_id);
        $data['appointment_id'] = $appointment_id;
        $data['pres_detail'] = $prescription;
        $data['comments'] = $this->post('comments');

        $this->call_model->addPrescription($data);
        $this->call_model->sendPrescriptionToPatient($patient->email, $patient, $doctor, $prescription);
        $this->call_model->sendPrescriptionToAdmin($this->config->item('site_email'), $patient, $doctor, $prescription);
        $this->response(array('msg'=>'Prescription and comments added successfully'), 200);
    }

    function getTimerSession()
    {   
        if(empty($this->session->userdata('start_time')))
            $this->session->set_userdata('start_time', time());

        print $this->session->userdata('start_time');
    }
    
    function checkCallEnd_get()
    {
        $token = $this->get('sessionKey');
        if(!$token)
        {
            //$this->session->unset_userdata('start_time');
            //$this->session->unset_userdata('call_url');
            $data['callEnd'] = TRUE;
            $this->response($data, 200);
        }    

        $record = $this->call_model->readSessionKey($token);
        if($record)
        {
            $data['callEnd'] = FALSE;
        }
        else
        {
            //$this->session->unset_userdata('start_time');
            //$this->session->unset_userdata('call_url');
            $data['callEnd'] = TRUE;
        }

        $this->response($data, 200);
    }

    function recordPayment($duration = 900, $record, $isFirstCall = FALSE)
    {
        $mins = floor(($duration / 60) % 60);
        $seconds = $duration % 60;
        if($seconds >= 30)
            $mins = $mins+1;
        
        if($mins < 15)
            $mins = 15;

        $rate = $this->call_model->getDoctorRate($record->d_id);
        $rate = $rate / 15;

        $data['rate'] = $rate;
        $data['appointment_id'] = $record->appointment_id;
        $data['actual_duration']  = gmdate('H:i:s', $duration);
        if(!$isFirstCall) :
            $data['charged_duration'] = gmdate('H:i:s', $mins * 60);
            $data['price'] = round($mins * $rate, 2);
        else :
            $data['charged_duration'] = gmdate('H:i:s', 0);
            $data['price'] = 0;
            $data['created_at'] = date('Y-m-d H:i:s');
            $data['status'] = 1;
        endif;
        
        $payment_id = $this->call_model->insertPayment($data);
        return array('payment_id' => $payment_id, 'amount' => $data['price']);
    }

    function takePayment($id = NULL, $amount = NULL, $patient, $appointment_id) 
    {
        if(!$id || !$amount)
            return array('response' => 'error', 'error_msg' => 'No registration id or amount');

        $success_codes = array('000.000.000', '000.400.000', '000.400.010', '000.400.020', '000.400.040', '000.400.060', '000.400.090','000.100.110', '000.100.111', '000.100.112');
        
        $site = $this->config->item('IS_LIVE') ? 'live_' : 'local_';
        $peachUrl = $this->config->item('IS_LIVE') ? 'https://oppwa.com/' : 'https://test.oppwa.com/';
        $url = $peachUrl . "v1/registrations/" . $id . "/payments";
        $data = "authentication.userId=" . $this->config->item($site . 'peach_userId') . 
           "&authentication.password=" . $this->config->item($site . 'peach_password') . 
            "&authentication.entityId=" . $this->config->item($site . 'peach_entityId_onceOff') .
            "&amount=" . number_format($amount, 2, '.', '') .
            "&currency=ZAR" .
            "&paymentType=DB" .
            "&recurringType=REPEATED" .
            "&customer.email=" . $patient->email . 
            "&customer.givenName=" . $patient->fname . 
            "&customer.surname=" . $patient->sname . 
            "&merchantTransactionId=" . $appointment_id;
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, $this->config->item('IS_LIVE'));
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $responseData = curl_exec($ch);
       
        if(curl_errno($ch)) {
            return array('response' => 'error', 'error_msg' => curl_error($ch));
        }
        curl_close($ch);
        $response = json_decode($responseData);
        
        if(!$response)
            return array('response' => 'error', 'error_msg' => 'No data found');

        if(in_array($response->result->code, $success_codes))
            return array('response' => 'success', 'data' => array('transaction_id' => $response->id, 'created_at' => $response->timestamp, 'status' => 1));
        return array('response' => 'error', 'error_msg' => $response->result->description);
    }

    function saveUserLatLng_post()
    {
        $appointment_id = $this->post('appointmentId');
        $lat = $this->post('lat');
        $lng = $this->post('long');

        if($appointment_id && $lat & $lng)
        {    
            $data1 = array(
                'id' => $appointment_id,
                'latitude' => $lat,
                'longitude' => $lng
            );
            
            $this->call_model->updateAppointmentStatus($data1);
            
            $this->response('User lat-long saved in appointment', 200);
        }
        else
            $this->response('Arguments not complete. appointmentId, lat & long required', 400);

    }

    function getUserLatLng_get()
    {
        $appointment_id = $this->get('appointmentId');
        if($appointment_id)
        {    
            $result = $this->call_model->getLatLong($appointment_id);
            $this->response($result, 200);
        }
    }
    
    function getSessionId($session_key = NULL) 
    {
        require APPPATH . "/libraries/OpenTok/opentok.phar";

        if(!$session_key)
            return array(NULL,NULL);

        $API_KEY = $this->config->item('OPENTOK_API_KEY'); 
        $API_SECRET = $this->config->item('OPENTOK_API_SECRET');
        
        $OT = new OpenTok\OpenTok($API_KEY, $API_SECRET);
        
        $session = $OT->createSession();
        $data['sessionId'] = $session->getSessionId();
        $data['token'] = $OT->generateToken($data['sessionId']);

        $this->call_model->updateSessionKeyForSessionId($session_key, $data);
        return array($data['sessionId'], $data['token']);
    }


    function checkPayment($amount = 2) 
    {
        d($this->config, 0);
        $id = $this->call_model->get_user_registration($this->session->userdata('logged_in')['id']);

        if(!$id)
            d(array('response' => 'error', 'error_msg' => 'No registration id'));
        
        $site = $this->config->item('IS_LIVE') ? 'live_' : 'local_';
        $peachUrl = $this->config->item('IS_LIVE') ? 'https://oppwa.com/' : 'https://test.oppwa.com/';
        $url = $peachUrl . "v1/registrations/" . $id . "/payments";
        $data = "authentication.userId=" . $this->config->item($site . 'peach_userId') . 
           "&authentication.password=" . $this->config->item($site . 'peach_password') . 
            "&authentication.entityId=" . $this->config->item($site . 'peach_entityId_onceOff') .
            "&amount=" . number_format($amount, 2, '.', '') .
            "&currency=ZAR" .
            "&paymentType=DB" .
            "&recurringType=REPEATED" .
            "&customer.email=" . 'akif00@yahoo.com' . 
            "&customer.givenName=" . 'Akif' . 
            "&customer.surname=" . 'Rehmani' . 
            "&merchantTransactionId=" . 20;

        d('This determine LIVE or LOCAL', 0);
        d($_SERVER['HTTP_HOST'], 0);
        d('This is peach url', 0);
        d($url, 0);
        d('These are the data parameters', 0);
        d($data , 0);

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, $this->config->item('IS_LIVE'));
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $responseData = curl_exec($ch);
       
        if(curl_errno($ch)) {
            d(array('response' => 'error', 'error_msg' => curl_error($ch)));
        }
        curl_close($ch);
        $response = json_decode($responseData);
        
        d('This is the response', 0);
        d($response);

        if(!$response)
            return array('response' => 'error', 'error_msg' => 'No data found');

        if($response->result->code = '000.100.110')
            return array('response' => 'success', 'data' => array('transaction_id' => $response->id, 'created_at' => $response->timestamp, 'status' => 1));
        return array('response' => 'error', 'error_msg' => $response->result->description);
    }
}