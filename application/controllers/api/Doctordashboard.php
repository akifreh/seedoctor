<?php
require(APPPATH . 'libraries/REST_Controller.php');

class Doctordashboard extends REST_Controller
{
    // Local Variables
    private $doctorId;

    public function __construct()
    {
        parent::__construct();
        $this->load->helper(array('form','url'));
        $this->load->library(array('session', 'form_validation', 'email'));
        $this->load->database();
        $this->load->model('user_model');
        $this->load->model('appointment_model');
        $this->load->model('doctor_model');
        $this->load->helper('security');
        $code = $this->get('code') ? $this->get('code') : $this->post('code');

        $session = $this->user_model->getSession($code);
        if(empty($session))
        {
            $data['url'] = uri_string();
            
            if(isset($_SERVER['QUERY_STRING']) && $_SERVER['QUERY_STRING'])
                $data['url'] .= '?' . $_SERVER['QUERY_STRING'];
            
            $data['msg'] = 'Please login before you visit the site!';
            $this->response($data, 401);          
        }
        elseif($session->role != 3)
        {
            $data['msg'] = 'You are not authorized to visit this page. Sorry!';
            $this->response($data, 404);          
        }

        $this->doctorId = $session->user_id;

        if($this->post() && !$_POST)
            $_POST = $this->post();
    }
    
    function index_get()
    {
        $doctorId = $this->doctorId;    
        $data = $this->doctor_model->readDoctorNowAppointment($doctorId);
        
        $data['now_appointment_detail'] = isset($data[0]) ? $data[0] : NULL;
        $patientId = isset($data[0]) ? $data[0]->p_id : NULL;
        $data['apppoinment_now_id'] = isset($data[0]) ? $data[0]->id : NULL;
        $data['appointment_link'] = $this->doctor_model->getLastestAppointmentLink($doctorId);
        $data['doctorrating'] = $this->doctor_model->readDoctorRating($doctorId);
        $data['patientsInfo'] = $patientId ? $this->doctor_model->readPatient($patientId) : NULL;
        $data['allappointments'] = $this->doctor_model->readAllScheduledAppointments($doctorId);
        $data['finished_appointments'] = $this->doctor_model->readAllPreviousAppointments($doctorId);
        $data['missed_appointments'] = $this->doctor_model->readAllPreviousAppointments($doctorId, 1);

        $data['breadcrumbs']['doctor_dashboard'] = TRUE;
        $data['showChat'] = TRUE;
        $this->load->model('page_model');
        $data['footer'] = $this->page_model->get_content('footer');
        
        $this->response($data, 200);
    }
    
  function approveAppointmentRequest_post()
  {
    $this->form_validation->set_rules('code', 'code', 'trim|required');
    $this->form_validation->set_rules('appointmentId', 'appointmentId', 'trim|required');
    $this->form_validation->set_rules('button', 'button', 'trim|required');

    if($this->form_validation->run() === FALSE)
    {
       // error
      $code = '400';
      $msg['error'] = 'Error! Please Fill the form & try again!!!';
      $msg['error_description'] = validation_errors();
      $this->response($msg, $code);
    }
      
    $doctorId = $this->doctorId;
    $appointment_id = $this->post('appointment_id');

    $result = $this->doctor_model->DoctorBookAppointment($doctorId, $appointment_id); // Doctor's Approved patient request of appointment
    if($result)
    {
       
      $patientId = $result[0]->p_id;
      $patientInfo = $this->doctor_model->readPatient($patientId);    
      $appointmentId = $result[0]->id;

      $action = $this->post('button');
      $meeting_approval = ($action == 'deny') ? 2 : 1;

      $data = array(
       'id' => $appointmentId,
       'meeting_approval' => $meeting_approval
      );
      
      $this->doctor_model->updateApproveStatus($data); 
      
      $patientName = $patientInfo[0]->fname .' ' . $patientInfo[0]->sname;
      $patientEmail = $patientInfo[0]->email;
      $toEmail = $this->session->userdata['logged_in']['email'];
      $doctorName = $this->session->userdata['logged_in']['fname'];
      $randomString = '';

      if($action == 'approve')
      {
        
        $dId = $this->session->userdata['logged_in']['id'];
        $pId = $result[0]->p_id;
        $confirmSendEmail = $this->doctor_model->readApprovedNowAppointment($dId,$pId);  
        $length = 10;
        $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $charactersLength = strlen($characters);
        
        for ($i = 0; $i < $length; $i++)
            $randomString .= $characters[rand(0, $charactersLength - 1)];
       
        if($confirmSendEmail)
        {
          $time = new DateTime('+5 mins');
          $appointment = array(
            'p_id' => $confirmSendEmail[0]->p_id,
            'd_id' => $confirmSendEmail[0]->d_id,
            'meeting_approval' => $confirmSendEmail[0]->meeting_approval,
            'brief_description' => $confirmSendEmail[0]->brief_description,
            'med_taken' => $confirmSendEmail[0]->med_taken,
            'patient' => $confirmSendEmail[0]->patient,
            'type' => 'now',
            'start_time' => $time->format('Y-m-d H:i:s'),
            'end_time' => $time->modify('+ 15 mins')->format('Y-m-d H:i:s')
          );

          $appointmentId = $this->doctor_model->addAppointment($appointment);  
          $sessInfo = array(
              'p_id' => $pId,
              'd_id' => $dId,
              'session_key' => $randomString,
              'appointment_id' => $appointmentId
           );
          
          $doctor_message = 'Dr ' . $this->session->userdata['logged_in']['fname'] . ' has approved your appointment now request.';
          $notification_array =  array();
          $notification_array['notification_type'] = "approved";
          $notification_array['appointment_type'] = "now";
          $notification_array['message'] = $doctor_message;
          $notification_array['user_id'] = $pId;
          $notification_array['p_id'] = $pId;
          $notification_array['d_id'] = $dId;
          $notification_array['appointment_id'] = $appointmentId;
          $this->appointment_model->insertNotification($notification_array);
          
          $this->doctor_model->updateSymptoms(array('appointment_id' => $appointmentId), $pId); 
          $this->doctor_model->insertSessionKey($sessInfo);
          $this->doctor_model->deleteAllAppointmentNow($pId, $confirmSendEmail[0]->created_at);
          $this->appointment_model->sendEmailToDoctor($toEmail, $doctorName, $randomString, $action);
          $this->appointment_model->sendEmailToPatient($patientEmail, $patientName, $randomString, $action, $doctorName);
        }
        
      }
      $code = 200;
      if($action == 'approve')
        $message = 'Approved successful.';
      else
        $message = 'Deny successful.';
    }  
    else
    {
      $message = 'Kindly note that the patient has already been attended to.';
      $code = 400;
    }

    $this->response($message, $code);
  }

  function payment_get()
  {
    $doctorId = $this->doctorId;

    list($data['finished_payments'],$data['finished_total']) = $this->doctor_model->readAllPayments($doctorId, 1);
    list($data['pending_payments'],$data['pending_total']) = $this->doctor_model->readAllPayments($doctorId, 0);
    
    $data['breadcrumbs']['doctor_payment'] = TRUE;
    $this->load->model('page_model');
    $data['footer'] = $this->page_model->get_content('footer');
    
    $this->response($data, 200);
  }

  function profile_get()
  {
    $doctorId = $this->doctorId;
    $this->load->model('admin_model');
    $data['row'] = $this->admin_model->readDoctorDetails($doctorId);
    $data['doctorAvailableDays'] = $this->admin_model->readDoctorAvailableDays($doctorId);
    $data['doctorTimings'] = $this->admin_model->readNewDoctorTimings($doctorId);
    $data['doctorStatus'] = 1;
    $data['editProfile'] = TRUE;
    
    $data['breadcrumbs']['doctor_profile'] = TRUE;
    
    $this->load->model('page_model');
    $data['footer'] = $this->page_model->get_content('footer');
    
    $this->response($data, 200);
  }
    
    function editProfile_post()
    {
      $this->form_validation->set_rules('code', 'code', 'required');
      $this->form_validation->set_rules('type', 'type', 'required');

      if($this->form_validation->run() === FALSE)
      {
         // error
        $code = '400';
        $msg['error'] = 'Error! Please Fill the form & try again!!!';
        $msg['error_description'] = validation_errors();
        $msg['type_options'] = 'personal, biography, daytime, type, academic, experience';
        $this->response($msg, $code);
      }
      
      $doctorId = $this->doctorId;
      $this->load->model('admin_model');
      $type = $this->post('type');
      
      if($type == 'personal')
      {
        $data = array(
           // 'fname'   => $this->post('fname'),
           //  'sname'  => $this->post('sname'),
           //  'email'  => $this->post('email'),
            'phone'  => $this->post('phone'),
            'address'=> $this->post('address'),
            'city'=> $this->post('city'),
            'province'=> $this->post('province'),
            'role'   => 3,
            'gender' => $this->post('gender')
        );
        $this->doctor_model->updateDoctor($data, $doctorId); 
      }
      elseif($type == 'biography')
      {
        $data1 = array(  
            'description' => $this->post('description'),
            'profession_number' => $this->post('profession_number'),
            'practice_number' => $this->post('practice_number'),
            'speciality' => $this->post('speciality'),
        );
        $this->doctor_model->updateDoctorInfo($data1, $doctorId);
      }
      elseif($type == 'type')
      {
        $data1 = array(  
            'type_of_doctor' => $this->post('type_of_doctor'),
        );

        $this->doctor_model->updateDoctorInfo($data1, $doctorId);
      }

      if(isset($_FILES))
      {
        // Upload Doctor Profile
        $config['upload_path']          = './uploads/profile/';
        $config['file_name']            = 'pic-' . $doctorId;
        $config['allowed_types']        = 'gif|jpg|png';
        
        //$config['allowed_types']        = 'gif|jpg|png|docx|doc';
        
        $this->load->library('upload', $config);
        $this->upload->initialize($config);

        $thumb_path = realpath(getcwd()) . '/uploads/' ;
        if (!is_dir($thumb_path))
        {
            mkdir($thumb_path);
            chmod($thumb_path, 0777);
        }

        if(isset($_FILES['profile_pic']['tmp_name']))
        {
            $thumb_path = realpath(getcwd()) . '/uploads/profile/' ;
            if (!is_dir($thumb_path))
            {
                mkdir($thumb_path);
                chmod($thumb_path, 0777);
            }

           if($this->upload->do_upload('profile_pic'))
           {
              $data_file = array('upload_data' => $this->upload->data());
              $file_data['profile_pic'] = $data_file['upload_data']['file_name'];
              generate_preset($data_file['upload_data']['full_path']);
           }
        }

        // Upload Doctor Profile
        $config['upload_path']          = './uploads/id_card/';
        $config['file_name']            = 'pic-' . $doctorId;
        $config['allowed_types']        = 'gif|jpg|png';
        
        //$config['allowed_types']        = 'gif|jpg|png|docx|doc';
        
        $this->load->library('upload', $config);
        $this->upload->initialize($config);

        $thumb_path = realpath(getcwd()) . '/uploads/' ;
        if (!is_dir($thumb_path))
        {
            mkdir($thumb_path);
            chmod($thumb_path, 0777);
        }

        if(isset($_FILES['id_card']['tmp_name']))
        {
            $thumb_path = realpath(getcwd()) . '/uploads/id_card/' ;
            if (!is_dir($thumb_path))
            {
                mkdir($thumb_path);
                chmod($thumb_path, 0777);
            }

           if($this->upload->do_upload('id_card'))
           {
              $data_file = array('upload_data' => $this->upload->data());
              $file_data['id_card'] = $data_file['upload_data']['file_name'];
              generate_preset($data_file['upload_data']['full_path']);
           }
        }

         // Upload Doctor Profile
        $config['upload_path']          = './uploads/cv/';
        $config['file_name']            = 'cv-' . $doctorId;
        $config['allowed_types']        = 'docx|doc|pdf';
        
        $this->load->library('upload', $config);
        $this->upload->initialize($config);

        if(isset($_FILES['cv']['tmp_name']))
        {
          $thumb_path = realpath(getcwd()) . '/uploads/cv/' ;
          if (!is_dir($thumb_path))
          {
              mkdir($thumb_path);
              chmod($thumb_path, 0777);
          }

          if ($this->upload->do_upload('cv'))
          {
            $data_file = array('upload_data' => $this->upload->data(), '');
            $$file_data['cv'] = $data_file['upload_data']['file_name'];
          }
        }

        if(isset($file_data))
          $this->doctor_model->updateDoctorInfo($file_data, $doctorId);
      }
      
      $response['msg'] = 'Your Profile has been updated successfully!';
      $this->response($response, 200);
    }

    function userProfile_get()
    {
      $userId = $this->get('userId');
      if(!$userId)
        $this->response('No user id provided', 400);

      $this->load->model('user_profile');
      $result['userinfo'] = $this->user_profile->readUserInfo($userId);
      $result['userprofile'] = $this->user_profile->readUserProfiles($userId);
      $result['userallergies'] = $this->user_profile->readAllergies($userId);
      $result['userpecs'] = $this->user_profile->readPreExistingCondition($userId); 
      $result['userpersonalupdates'] = $this->user_profile->readPersonalUpdate($userId);  
      $result['userfamilyhistory'] = $this->user_profile->readFamilyHistory($userId);
      $result['usermydoctor'] = $this->user_profile->readMyDoctor($userId);
      $result['previousHistory'] = $this->user_profile->readPreviousHistory($userId);
      $result['dontShowUpdate'] = TRUE;
      
      $this->load->model('page_model');
      $result['footer'] = $this->page_model->get_content('footer');
      
      $this->response($result, 200);
    }

    function addAcademics_post()
    {    
      $this->form_validation->set_rules('qualification', 'qualification', 'trim|required');
      $this->form_validation->set_rules('institution', 'institution', 'trim|required');
      $this->form_validation->set_rules('year', 'year', 'trim|required');
      $this->form_validation->set_rules('code', 'code', 'trim|required');
   
      if($this->form_validation->run() === FALSE)
      {
        $code = '400';
        $msg['error'] = 'Error! Please Fill the form & try again!!!';
        $msg['error_description'] = validation_errors();
        $this->response($msg, $code);
      }
      else
      {
        $code = $this->post('code');
        $session = $this->user_model->getSession($code);

        if(!$session || $session->role != 3)
        {
          $msg = 'You are not authorized to visit this page. Sorry!';
          $this->response($msg, 404);
        }

        $doctorId = $session->user_id;

        $qualification = $this->post('qualification');
        $institution = $this->post('institution');
        $year = $this->post('year');

        $data5 = array(
                    'qualification' => $qualification,
                    'institution' => $institution,
                    'year' => $year,
                    'user_id' => $doctorId
                  );
        $this->doctor_model->insertAcademics($data5);
  
        $this->response('Academic Added successfully', 200);
      }
    }

    function editAcademics_post()
    {        
      $this->form_validation->set_rules('qualification', 'qualification', 'trim|required');
      $this->form_validation->set_rules('institution', 'institution', 'trim|required');
      $this->form_validation->set_rules('year', 'year', 'trim|required');
      $this->form_validation->set_rules('academicId', 'academicId', 'trim|required');
      $this->form_validation->set_rules('code', 'code', 'trim|required');
   
      if($this->form_validation->run() === FALSE)
      {
        $code = '400';
        $msg['error'] = 'Error! Please Fill the form & try again!!!';
        $msg['error_description'] = validation_errors();
        $this->response($msg, $code);
      }
      else
      {
        $code = $this->post('code');
        $session = $this->user_model->getSession($code);

        if(!$session || $session->role != 3)
        {
          $msg = 'You are not authorized to visit this page. Sorry!';
          $this->response($msg, 404);
        }

        $doctorId = $session->user_id;
        $id = $this->post('academicId');

        $data = array(  
            'qualification' =>  $this->post('qualification'),
            'institution'   =>  $this->post('institution'),
            'year'          =>  $this->post('year')
        );

        $this->doctor_model->updateAcademic($data, $id);
        $this->response('Academic Record updated successfully', 200);   
      }    
    }

    function deleteAcademics_post()
    {
      $this->form_validation->set_rules('code', 'code', 'trim|required');
      $this->form_validation->set_rules('academicId', 'academicId', 'trim|required');
        
      if($this->form_validation->run() === FALSE)
      {
        $code = '400';
        $msg['error'] = 'Error! Please Fill the form & try again!!!';
        $msg['error_description'] = validation_errors();
        $this->response($msg, $code);
      }
      else
      {          
        $code = $this->post('code');
        $session = $this->user_model->getSession($code);

        if(!$session || $session->role != 3)
        {
          $msg = 'You are not authorized to visit this page. Sorry!';
          $this->response($msg, 404);
        }
        
        $id = $this->post('academicId');
        $this->doctor_model->apiDeleteAcademic($id);
        $this->response('Academic deleted successfully', 200);
      }
    }

    function addExperience_post()
    {    
      $this->form_validation->set_rules('company', 'company', 'trim|required');
      $this->form_validation->set_rules('jobTitle', 'jobTitle', 'trim|required');
      $this->form_validation->set_rules('startYear', 'startYear', 'trim|required');
      $this->form_validation->set_rules('startMonth', 'startMonth', 'trim|required');
      $this->form_validation->set_rules('endYear', 'endYear', 'trim|required');
      $this->form_validation->set_rules('endMonth', 'endMonth', 'trim|required');
      $this->form_validation->set_rules('code', 'code', 'trim|required');
   
      if($this->form_validation->run() === FALSE)
      {
        $code = '400';
        $msg['error'] = 'Error! Please Fill the form & try again!!!';
        $msg['error_description'] = validation_errors();
        $this->response($msg, $code);
      }
      else
      {
        $code = $this->post('code');
        $session = $this->user_model->getSession($code);

        if(!$session || $session->role != 3)
        {
          $msg = 'You are not authorized to visit this page. Sorry!';
          $this->response($msg, 404);
        }

        $doctorId      = $session->user_id;
        $company        = $this->post('company');
        $job_title      = $this->post('jobTitle');
        $start_yy       = $this->post('startYear');
        $start_mm       = $this->post('startMonth');
        $end_yy         = $this->post('endYear');
        $end_mm         = $this->post('endMonth');
        $present_here   = $this->post('present');

        $data6 = array(
                  'company'       => $company,
                  'job_title'     => $job_title,
                  'start_date'    => date('Y-m-d', strtotime($start_yy . '-' . $start_mm)),
                  'end_date'      => date('Y-m-d', strtotime($end_yy . '-' . $end_mm)),
                  'present_here'  => $present_here,
                  'user_id'      => $doctorId
                );

        $this->doctor_model->insertExperience($data6);
        $this->response('Experience Added successfully', 200);
      }
    }

    function editExperience_post()
    {        
      $this->form_validation->set_rules('company', 'company', 'trim|required');
      $this->form_validation->set_rules('jobTitle', 'jobTitle', 'trim|required');
      $this->form_validation->set_rules('startYear', 'startYear', 'trim|required');
      $this->form_validation->set_rules('startMonth', 'startMonth', 'trim|required');
      $this->form_validation->set_rules('endYear', 'endYear', 'trim|required');
      $this->form_validation->set_rules('endMonth', 'endMonth', 'trim|required');
      $this->form_validation->set_rules('experienceId', 'experienceId', 'trim|required');
      $this->form_validation->set_rules('code', 'code', 'trim|required');
   
      if($this->form_validation->run() === FALSE)
      {
        $code = '400';
        $msg['error'] = 'Error! Please Fill the form & try again!!!';
        $msg['error_description'] = validation_errors();
        $this->response($msg, $code);
      }
      else
      {
        $code = $this->post('code');
        $session = $this->user_model->getSession($code);

        if(!$session || $session->role != 3)
        {
          $msg = 'You are not authorized to visit this page. Sorry!';
          $this->response($msg, 404);
        }

        $doctorId = $session->user_id;
        $id = $this->post('experienceId');

        $data = array(  
            'company' =>  $this->post('company'),
            'job_title'   =>  $this->post('jobTitle'),
            'start_date' =>   date('Y-m-d', strtotime($startYear . '-' . $startMonth)),
            'end_date'   =>   date('Y-m-d', strtotime($endYear . '-' . $endMonth)),
            'user_id'   =>  $doctorId
        );
        
        if($this->post('present'))
          $data['present_here'] = $this->post('present');

        $this->doctor_model->updateExperience($data, $id);
        $this->response('Experience updated successfully', 200);   
      }    
    }

    function deleteExperience_post()
    {
      $this->form_validation->set_rules('code', 'code', 'trim|required');
      $this->form_validation->set_rules('experienceId', 'experienceId', 'trim|required');
        
      if($this->form_validation->run() === FALSE)
      {
        $code = '400';
        $msg['error'] = 'Error! Please Fill the form & try again!!!';
        $msg['error_description'] = validation_errors();
        $this->response($msg, $code);
      }
      else
      {          
        $code = $this->post('code');
        $session = $this->user_model->getSession($code);

        if(!$session || $session->role != 3)
        {
          $msg = 'You are not authorized to visit this page. Sorry!';
          $this->response($msg, 404);
        }
        
        $id = $this->post('experienceId');
        $this->doctor_model->apiDeleteExperience($id);
        $this->response('Academic deleted successfully', 200);
      }
    }

    function addTiming_post()
    {    
      $this->form_validation->set_rules('dayId', 'dayId', 'trim|required');
      $this->form_validation->set_rules('toTime', 'toTime', 'trim|required');
      $this->form_validation->set_rules('fromTime', 'fromTime', 'trim|required');
      $this->form_validation->set_rules('code', 'code', 'trim|required');
   
      if($this->form_validation->run() === FALSE)
      {
        $code = '400';
        $msg['error'] = 'Error! Please Fill the form & try again!!!';
        $msg['error_description'] = validation_errors();
        $this->response($msg, $code);
      }
      else
      {
        $code = $this->post('code');
        $session = $this->user_model->getSession($code);

        if(!$session || $session->role != 3)
        {
          $msg = 'You are not authorized to visit this page. Sorry!';
          $this->response($msg, 404);
        }

        $doctorId = $session->user_id;

        $dayId = $this->post('dayId');
        $toTime = $this->post('toTime');
        $fromTime = $this->post('fromTime');

        $timing[] = array(
                     'user_id' => $doctorId,
                     'day_id' => $dayId,
                     'start_time' => date('H:i:s', strtotime($fromTime)),
                     'end_time' => date('H:i:s', strtotime($toTime)),
                  );
        $this->doctor_model->insertDoctorTiming($timing);

        if(!$this->doctor_model->alreadyDayPresent($doctorId, $dayId))
        {
          $day[] = array(
             'user_id' => $doctorId,
             'day_id' => $dayId
          );
          $this->doctor_model->apiInsertDoctorDays($day);
        }               

        $this->response('Timing Added successfully', 200);
      }
    }

    function editTiming_post()
    {        
      $this->form_validation->set_rules('dayId', 'dayId', 'trim|required');
      $this->form_validation->set_rules('toTime', 'toTime', 'trim|required');
      $this->form_validation->set_rules('fromTime', 'fromTime', 'trim|required');
      $this->form_validation->set_rules('code', 'code', 'trim|required');
      $this->form_validation->set_rules('timingId', 'timingId', 'trim|required');
   
      if($this->form_validation->run() === FALSE)
      {
        $code = '400';
        $msg['error'] = 'Error! Please Fill the form & try again!!!';
        $msg['error_description'] = validation_errors();
        $this->response($msg, $code);
      }
      else
      {
        $code = $this->post('code');
        $session = $this->user_model->getSession($code);

        if(!$session || $session->role != 3)
        {
          $msg = 'You are not authorized to visit this page. Sorry!';
          $this->response($msg, 404);
        }

        $doctorId = $session->user_id;
        $id = $this->post('timingId');

        $data = array(  
            'day_id'     =>  $this->post('dayId'),
            'start_time' =>  date('H:i:s', strtotime($this->post('fromTime'))),
            'end_time'   =>  date('H:i:s', strtotime($this->post('toTime'))),
            'user_id'    =>  $doctorId
        );
        
        $this->doctor_model->updateTiming($data, $id);
        $this->response('Timing updated successfully', 200);   
      }    
    }

    function deleteTiming_post()
    {
      $this->form_validation->set_rules('code', 'code', 'trim|required');
      $this->form_validation->set_rules('timingId', 'timingId', 'trim|required');
        
      if($this->form_validation->run() === FALSE)
      {
        $code = '400';
        $msg['error'] = 'Error! Please Fill the form & try again!!!';
        $msg['error_description'] = validation_errors();
        $this->response($msg, $code);
      }
      else
      {          
        $code = $this->post('code');
        $session = $this->user_model->getSession($code);

        if(!$session || $session->role != 3)
        {
          $msg = 'You are not authorized to visit this page. Sorry!';
          $this->response($msg, 404);
        }
        
        $id = $this->post('timingId');
        $this->doctor_model->apiDeleteTiming($id);
        $this->response('Timing deleted successfully', 200);
      }
    }
}