<?php
require(APPPATH . 'libraries/REST_Controller.php');

class Userprofile extends REST_Controller
{
    // Local Variables
    private $userId;

    public function __construct()
    {
        parent::__construct();
        $this->load->library(array('session', 'form_validation', 'email'));
        $this->load->helper(array('form','url'));
        $this->load->database();
        $this->load->model('user_profile');
        $this->load->model('user_model');
        $this->load->helper('security');

        $code = $this->get('code') ? $this->get('code') : $this->post('code');
        $session = $this->user_model->getSession($code);

        if(empty($session))
        {
            $data['url'] = uri_string();
            
            if(isset($_SERVER['QUERY_STRING']) && $_SERVER['QUERY_STRING'])
                $data['url'] .= '?' . $_SERVER['QUERY_STRING'];
            
            $data['msg'] = 'Please login before you visit the site!';
            $this->response($data, 401);          
        }
        elseif($session->role != 2)
        {
            $data['msg'] = 'You are not authorized to visit this page. Sorry!';
            $this->response($data, 404);          
        }

        $this->userId = $session->user_id;

        if($this->post() && !$_POST)
            $_POST = $this->post();
    }
    
    function index_get()
    {
       
        $userId = $this->userId;  
        $result['userinfo'] = $this->user_profile->readUserInfo($userId);
        $result['userprofile'] = $this->user_profile->readUserProfiles($userId);
        $result['userallergies'] = $this->user_profile->readAllergies($userId);
        $result['userpecs'] = $this->user_profile->readPreExistingCondition($userId); 
        $result['userpersonalupdates'] = $this->user_profile->readPersonalUpdate($userId);
        $result['userfamilyhistory'] = $this->user_profile->readFamilyHistory($userId);
        $result['usermydoctor'] = $this->user_profile->readMyDoctor($userId);
        $result['previousHistory'] = $this->user_profile->readPreviousHistory($userId);

        $result['breadcrumbs']['medical_profile'] = TRUE;
        
        $this->load->model('page_model');
        $result['footer'] = $this->page_model->get_content('footer');
        
        $this->response($result, 200);       
    }
     
    function viewMedicalHistory_get()
    {
        $user_id = $this->userId; 
        $type = $this->get('type');

        if($type == 'allergy') : 
            $data['allergies'] = $this->user_profile->readAllergies($user_id);
        elseif($type == 'preExisting') : 
            $data['preExisting'] = $this->user_profile->readPreExistingCondition($user_id);
        elseif($type == 'personalUpdate') : 
            $data['personalUpdate'] = $this->user_profile->readPersonalUpdate($user_id);
        elseif($type == 'familyHistory') : 
            $data['familyHistory'] = $this->user_profile->readFamilyHistory($user_id);
        elseif($type == 'myDoctor') : 
            $data['myDoctor'] = $this->user_profile->readMyDoctor($user_id);
        elseif($type == 'userInfo') : 
            $data['userInfo'] = $this->user_profile->readUserInfo($user_id);
        elseif($type == 'profileInfo') : 
            $data['profileInfo'] = $this->user_profile->readUserProfiles($user_id);
        elseif($type == 'previousHistory') : 
            $data['previousHistory'] = $this->user_profile->readPreviousHistory($user_id);
        else :
            $data['getallergies'] = $this->user_profile->readAllergies($user_id);
            $data['getPreExisting'] = $this->user_profile->readPreExistingCondition($user_id);
            $data['getPersonalUpdate'] = $this->user_profile->readPersonalUpdate($user_id);
            $data['familyHistory'] = $this->user_profile->readFamilyHistory($user_id);
            $data['myDoctor'] = $this->user_profile->readMyDoctor($user_id);
            $data['userInfo'] = $this->user_profile->readUserInfo($user_id);
            $data['profileInfo'] = $this->user_profile->readUserProfiles($user_id);
            $data['previousHistory'] = $this->user_profile->readPreviousHistory($user_id);
            $data['activityLevel'] = $this->config->item('active_level');
        endif;

        $data['activityLevel'] = $this->config->item('active_level');
        $data['preExisting'] = array();
            
        if(isset($data['getPreExisting']) && $data['getPreExisting'])
        {
            foreach ($data['getPreExisting'] as $key => $value) 
            {
                $data['preExisting'][] = $value->feeling;
                if($value->desc)
                    $data['preExisting']['desc'] = $value->desc;
            }
        }

        $data['breadcrumbs']['medical_profile'] = TRUE;
        $this->load->model('page_model');
        $data['footer'] = $this->page_model->get_content('footer');

        $this->response($data, 200);   
    }      
       
    function addMedicalProfile_post()
    {
        $this->form_validation->set_rules('diet', 'Diet', 'trim|required');
        $this->form_validation->set_rules('height', 'Height', 'trim|required');
        $this->form_validation->set_rules('weight', 'Weight', 'trim|required');
        $this->form_validation->set_rules('sportActivity', 'sportActivity', 'trim|required');
        $this->form_validation->set_rules('alcohol', 'Alcohol', 'trim|required');
        $this->form_validation->set_rules('smoke', 'Smoke', 'trim|required');
        $this->form_validation->set_rules('code', 'Code', 'trim|required');
        
        if($this->form_validation->run() === FALSE)
        {
            // error
            $code = '400';
            $msg['error'] = 'Error! Please Fill the form & try again!!!';
            $msg['error_description'] = validation_errors();
            $this->response($msg, $code);
        } 
        else
        {
            $userId = $this->userId;
            $data = array(
                'user_id' => $userId,
                'diet' => $this->post('diet'),
                'height' => $this->post('height'),
                'weight' => $this->post('weight'),
                'sport_activity' => $this->post('sportActivity'),
                'alcohol' => $this->post('alcohol'),
                'activity_level' => $this->post('activityLevel'),
                'bmi' => $this->post('bmi'),
                'bmr' => $this->post('bmr'),
                'other' => $this->post('other')
            );

            $this->user_profile->insertUserPersonalInfo($data);
            $this->response('User Medical Profile Added successfully', 200);
        }
    }

    function addAllergy_post()
    {    
        $this->form_validation->set_rules('resistant', 'resistant', 'trim|required');
        $this->form_validation->set_rules('reaction', 'reaction', 'trim|required');
        $this->form_validation->set_rules('code', 'code', 'trim|required');
     
        if($this->form_validation->run() === FALSE)
        {
            $code = '400';
            $msg['error'] = 'Error! Please Fill the form & try again!!!';
            $msg['error_description'] = validation_errors();
            $this->response($msg, $code);
        }
        else
        {
            $resistant = $this->post('resistant');
            $reaction = $this->post('reaction');
            $data = array(
                'user_id' => $session->user_id,
                'resistant' => $resistant,
                'reaction' => $reaction
            );

            $this->response('Allergy Added successfully', 200);
        }
      
    }
    
    function addPreExistingCondition_post()
    {
        $this->form_validation->set_rules('feeling[]', 'feeling', 'trim|required');
        $this->form_validation->set_rules('code', 'code', 'trim|required');

        if($this->form_validation->run() === FALSE)
        {
            $code = '400';
            $msg['error'] = 'Error! Please Fill the form & try again!!!';
            $msg['error_description'] = validation_errors();
            $this->response($msg, $code);
        }
        else
        {   
            $userId = $this->userId;
            $feelings = $this->post('feeling');
            /*
            $getPreExisting = $this->user_profile->readPreExistingCondition($userId);
            
            $array = array();
            if($getPreExisting)
            {
                foreach ($getPreExisting as $key => $value) 
                {
                    $array[$value->id] = $value->feeling;
                }          
            }

            $to_insert = array_diff($feelings, $array);
            $to_delete = array_diff($array, $feelings);

            foreach($to_insert as $value)
            {
                $data = array(
                    'user_id' => $userId,
                    'feeling' => $value,
                );
                $this->user_profile->insertPreExistingCondition($data);
            }
            */

            if($desc = $this->post('otherDesc'))
            {
                $data_other = array(
                    'user_id' => $userId,
                    'feeling' => 'Other',
                    'desc'  => $desc
                );

                $this->user_profile->savePECOther($userId, $data_other);
            }
            else
            {
                $data = array(
                    'user_id' => $userId,
                    'feeling' => $feelings,
                );
                $this->user_profile->insertPreExistingCondition($data);
            }

            /*foreach ($to_delete as $key => $value)
              $this->user_profile->deletePec($key);  */

            $this->response('Pre Existing Condition Added successfully', 200);
            
        }
        
    }
    
    function addPersonalUpdate_post()
    {    
        $this->form_validation->set_rules('feeling', 'feeling', 'trim|required');
        $this->form_validation->set_rules('symptom', 'symptom', 'trim|required');
        $this->form_validation->set_rules('medicineTaken', 'medicineTaken', 'trim|required');
        $this->form_validation->set_rules('describeMedicine', 'describeMedicine', 'trim|required');
        $this->form_validation->set_rules('code', 'code', 'trim|required');
          
        if($this->form_validation->run() === FALSE)
        {
            $code = '400';
            $msg['error'] = 'Error! Please Fill the form & try again!!!';
            $msg['error_description'] = validation_errors();
            $this->response($msg, $code);
        }
        else
        {          
            $userId = $this->userId;
            $date = date('Y-m-d');
            
            $feeling = $this->post('feeling');
            $symptom = $this->post('symptom');
            $med_taken = $this->post('medicineTaken');
            $describe_med = $this->post('describeMedicine');

            $data = array(
                'user_id' => $userId,
                'feeling' => $feeling,
                'symptom' => $symptom,
                'med_taken' => $med_taken,
                'describe_med' => $describe_med,
                'date' => $date
            );
                
            $this->user_profile->insertPersonalUpdates($data);
            $this->response('Personal Update Added successfully', 200);
        }
    }
    
    function addFamilyHistory_post()
    {
        $this->form_validation->set_rules('condition', 'condition', 'trim|required');
        $this->form_validation->set_rules('relationship', 'relationship', 'trim|required');
        $this->form_validation->set_rules('code', 'code', 'trim|required');

        if($this->form_validation->run() === FALSE)
        {
            $code = '400';
            $msg['error'] = 'Error! Please Fill the form & try again!!!';
            $msg['error_description'] = validation_errors();
            $this->response($msg, $code);
        }
        else
        {
            $userId = $this->userId;
            $condition = $this->post('condition');
            $relationship = $this->post('relationship');

            $data = array(
                'user_id' => $userId,
                'condition' => $condition,
                'relationship' => $relationship
            );
                
            $this->user_profile->insertFamilyHistory($data); 
            $this->response('Family History Added successfully', 200);
        }
    }   
    
    function addMyDoctor_post()
    {
        
        $this->form_validation->set_rules('name', 'name', 'trim|required');
        $this->form_validation->set_rules('sname', 'sname', 'trim|required');
        $this->form_validation->set_rules('number', 'number', 'trim|required');
        $this->form_validation->set_rules('email', 'email', 'trim|required');
        $this->form_validation->set_rules('code', 'code', 'trim|required');

        if($this->form_validation->run() === FALSE)
        {
            $code = '400';
            $msg['error'] = 'Error! Please Fill the form & try again!!!';
            $msg['error_description'] = validation_errors();
            $this->response($msg, $code);
        }
        else
        {
            $userId = $this->userId;
            $name = $this->post('name');
            $sname = $this->post('sname');
            $contact_number = $this->post('number');
            $email = $this->post('email');

            $data = array(
                        'user_id' => $userId,
                        'name' => $name,
                        'sname' => $sname,
                        'contact_number' => $contact_number,
                        'email' => $email
                    );
                
            $this->user_profile->insertMyDoctor($data);
            $this->response('My Doctor Added successfully', 200);
        }
    }

    function editAllergy_post()
    {        
        $this->form_validation->set_rules('resistant', 'resistant', 'trim|required');
        $this->form_validation->set_rules('reaction', 'reaction', 'trim|required');
        $this->form_validation->set_rules('code', 'code', 'trim|required');
        $this->form_validation->set_rules('allergyId', 'allergyId', 'trim|required');

        if($this->form_validation->run() === FALSE)
        {
            $code = '400';
            $msg['error'] = 'Error! Please Fill the form & try again!!!';
            $msg['error_description'] = validation_errors();
            $this->response($msg, $code);
        }
        else
        {
            $userId = $this->userId;
            $id = $this->post('allergyId');

            $data = array(  
                'resistant' =>  $this->post('resistant'),
                'reaction' =>   $this->post('reaction')
            );
        
            $this->user_profile->updateAllergies($id,$data);

            $this->response('Allergy Updated successfully', 200);   
        }    
    }    
    
    function editPreExistingCondition_post()
    {
        $this->form_validation->set_rules('feeling', 'feeling', 'trim|required');
        $this->form_validation->set_rules('code', 'code', 'trim|required');
        $this->form_validation->set_rules('pecId', 'pecId', 'trim|required');

        if($this->form_validation->run() === FALSE)
        {
            $code = '400';
            $msg['error'] = 'Error! Please Fill the form & try again!!!';
            $msg['error_description'] = validation_errors();
            $this->response($msg, $code);
        }
        else
        {
            $userId = $this->userId;
            $id = $this->post('pecId');
            
            $data = array('feeling' =>  $this->post('feeling'));
            if($desc = $this->post('otherDesc'))
            {
                $data['feeling'] = 'Other';
                $data['desc'] = $desc;
            }

            $this->user_profile->updatePreExistingCondition($id, $data);
            $this->response('Pre Existing Condition Updated successfully', 200);
        }
    }

    function editPersonalUpdate_post()
    {
        $this->form_validation->set_rules('feeling', 'feeling', 'trim|required');
        $this->form_validation->set_rules('symptom', 'symptom', 'trim|required');
        $this->form_validation->set_rules('medicineTaken', 'medicineTaken', 'trim|required');
        $this->form_validation->set_rules('describeMedicine', 'describeMedicine', 'trim|required');
        $this->form_validation->set_rules('code', 'code', 'trim|required');
        $this->form_validation->set_rules('puId', 'puId', 'trim|required');
          
        if($this->form_validation->run() === FALSE)
        {
            $code = '400';
            $msg['error'] = 'Error! Please Fill the form & try again!!!';
            $msg['error_description'] = validation_errors();
            $this->response($msg, $code);
        }
        else
        {          
            $userId = $this->userId;
            $id = $this->post('puId');
            $date = date('Y-m-d');

            $data = array(    
                'feeling' =>  $this->post('feeling'),
                'symptom' =>  $this->post('symptom'),
                'med_taken' =>  $this->post('medicineTaken'),
                'describe_med' =>  $this->post('describeMedicine'),
                'date' =>  $date
            );

            $this->user_profile->updatePersonalUpdate($id,$data);

            $this->response('Personal Update updated successfully', 200);
        }
    }

    function editFamilyHistory_post()
    {               
        $this->form_validation->set_rules('condition', 'condition', 'trim|required');
        $this->form_validation->set_rules('relationship', 'relationship', 'trim|required');
        $this->form_validation->set_rules('code', 'code', 'trim|required');
        $this->form_validation->set_rules('fhId', 'fhId', 'trim|required');
          
        if($this->form_validation->run() === FALSE)
        {
            $code = '400';
            $msg['error'] = 'Error! Please Fill the form & try again!!!';
            $msg['error_description'] = validation_errors();
            $this->response($msg, $code);
        }
        else
        {          
            $id = $this->post('fhId');

            $data = array(        
                'condition' =>  $this->post('condition'),
                'relationship' =>  $this->post('relationship')
            );
            
            $this->user_profile->updateFamilyHistory($id, $data);
            $this->response('Family History updated successfully', 200);
        }
    }

    function editMyDoctor_post()
    {
        $this->form_validation->set_rules('name', 'name', 'trim|required');
        $this->form_validation->set_rules('sname', 'sname', 'trim|required');
        $this->form_validation->set_rules('number', 'contact_number', 'trim|required');
        $this->form_validation->set_rules('email', 'email', 'trim|required');
        $this->form_validation->set_rules('code', 'code', 'trim|required');
        $this->form_validation->set_rules('mdId', 'mdId', 'trim|required');
          
        if($this->form_validation->run() === FALSE)
        {
            $code = '400';
            $msg['error'] = 'Error! Please Fill the form & try again!!!';
            $msg['error_description'] = validation_errors();
            $this->response($msg, $code);
        }
        else
        {          
            $id = $this->post('mdId');
            $data = array(        
                'name' =>  $this->post('name'),
                'sname' =>  $this->post('sname'),
                'contact_number' =>  $this->post('number'),
                'email' =>  $this->post('email')
            );
            
            $this->user_profile->updateMyDoctor($id, $data);
            $this->response('Family History updated successfully', 200);
        }
    }
 
    function deleteAllergy_post()
    {
        $this->form_validation->set_rules('code', 'code', 'trim|required');
        $this->form_validation->set_rules('allergyId', 'allergyId', 'trim|required');
          
        if($this->form_validation->run() === FALSE)
        {
            $code = '400';
            $msg['error'] = 'Error! Please Fill the form & try again!!!';
            $msg['error_description'] = validation_errors();
            $this->response($msg, $code);
        }
        else
        {          
            $id = $this->post('allergyId');
            $this->user_profile->deleteMyAllergy($id);
            $this->response('Allergy deleted successfully', 200);
        }
    }
    
    function deletePreExistingCondition_post()
    {
        $this->form_validation->set_rules('code', 'code', 'trim|required');
        $this->form_validation->set_rules('pecId', 'pecId', 'trim|required');
          
        if($this->form_validation->run() === FALSE)
        {
            $code = '400';
            $msg['error'] = 'Error! Please Fill the form & try again!!!';
            $msg['error_description'] = validation_errors();
            $this->response($msg, $code);
        }
        else
        {
            $id = $this->post('pecId');
            $this->user_profile->deletePec($id);
            $this->response('Pre Existing Condition deleted successfully', 200);
        }
    }
    
    function deletePersonalUpdate_post()
    {
        $this->form_validation->set_rules('code', 'code', 'trim|required');
        $this->form_validation->set_rules('puId', 'puId', 'trim|required');
          
        if($this->form_validation->run() === FALSE)
        {
            $code = '400';
            $msg['error'] = 'Error! Please Fill the form & try again!!!';
            $msg['error_description'] = validation_errors();
            $this->response($msg, $code);
        }
        else
        {
            $id = $this->post('puId');
            $this->user_profile->deletePu($id);
            $this->response('Personal Update deleted successfully', 200);
        }
    }
    
    function deleteFamilyHistory_post()
    {
        $this->form_validation->set_rules('code', 'code', 'trim|required');
        $this->form_validation->set_rules('fhId', 'fhId', 'trim|required');
          
        if($this->form_validation->run() === FALSE)
        {
            $code = '400';
            $msg['error'] = 'Error! Please Fill the form & try again!!!';
            $msg['error_description'] = validation_errors();
            $this->response($msg, $code);
        }
        else
        {
            $id = $this->post('fhId');
            $this->user_profile->deleteFh($id);
            $this->response('Family History deleted successfully', 200);       
        }
    }
    
    function deleteMyDoctor_post()
    {
        $this->form_validation->set_rules('code', 'code', 'trim|required');
        $this->form_validation->set_rules('mdId', 'mdId', 'trim|required');
          
        if($this->form_validation->run() === FALSE)
        {
            $code = '400';
            $msg['error'] = 'Error! Please Fill the form & try again!!!';
            $msg['error_description'] = validation_errors();
            $this->response($msg, $code);
        }
        else
        {
            $id = $this->post('mdId');
            $this->user_profile->deleteMd($id);
            $this->response('My Doctor deleted successfully', 200);       
        }
    }
    
    function updateMedicalProfile_post()
    {
        $this->form_validation->set_rules('diet', 'Diet', 'trim|required');
        $this->form_validation->set_rules('height', 'Height', 'trim|required');
        $this->form_validation->set_rules('weight', 'Weight', 'trim|required');
        $this->form_validation->set_rules('sportActivity', 'sportActivity', 'trim|required');
        $this->form_validation->set_rules('alcohol', 'Alcohol', 'trim|required');
        $this->form_validation->set_rules('smoke', 'Smoke', 'trim|required');
        $this->form_validation->set_rules('code', 'Code', 'trim|required');
        
        if($this->form_validation->run() === FALSE)
        {
            // error
            $code = '400';
            $msg['error'] = 'Error! Please Fill the form & try again!!!';
            $msg['error_description'] = validation_errors();
            $this->response($msg, $code);
        } 
        else
        {
            $userId = $this->userId;
            $data = array(
                'diet' => $this->post('diet'),
                'height' => $this->post('height'),
                'weight' => $this->post('weight'),
                'sport_activity' => $this->post('sportActivity'),
                'alcohol' => $this->post('alcohol'),
                'smoke' => $this->post('smoke'),
                'activity_level' => $this->post('activityLevel'),
                'bmi' => $this->post('bmi'),
                'bmr' => $this->post('bmr'),
                'other' => $this->post('other')
            ); 
            
            $this->user_profile->updateUserProfileInfo($userId, $data);
            $this->response('Medical Profile updated successfully', 200);
        }
    }

    function getActivityLevels_get()
    {
        $data['activityLevel'] = $this->config->item('active_level');
        $this->response($data, 200);
    }

    function viewPendingPayments_get()
    {
        $userId = $this->userId;
        $data['records'] = $this->user_profile->view_pending_amount($userId);
        $this->load->model('page_model');
        $data['footer'] = $this->page_model->get_content('footer');
        $this->response($data, 200);        
    }

    function payPendingAmount_post()
    {
        $this->form_validation->set_rules('appointmentId', 'appointmentId', 'trim|required');
        $this->form_validation->set_rules('code', 'code', 'trim|required');
       
        if ($this->form_validation->run() == FALSE) 
        {
            $data['msg'] = 'Error. Please try again later!!!';
            $data['errors'] = validation_errors();

            $this->response($data, 404);
        }

        $id = $this->post('appointmentId');
        $this->load->model('call_model');
        $record = $this->call_model->getAppointmentById($id);
        $payment = $this->call_model->getPaymentByAppointmentId($id);
        if($record && $payment && $payment->status != 1)
        {
            $user_registration = $this->call_model->get_user_registration($record->p_id);
            $patient = $this->call_model->getUserById($record->p_id);
            $doctor = $this->call_model->getUserById($record->d_id);

            $response = $this->takePayment($user_registration, $payment->price, $patient, $id);
            if($response['response'] == 'success')
            {    
                $this->call_model->updatePayment($payment->id, $response['data']);
                $my_payment['payment_id'] = $payment->id;
                $this->call_model->sendPatientInvoiceEmail($patient->email, $patient, $doctor, $my_payment);
                
                $data['msg']= 'Payment received successfully.';
                $this->response($data, 200);
            }

            $data['msg']= 'An error occured during payment. Please try again.';
            $data['error_description'] = $response['error_msg'];
            $this->response($data, 400);
        }

        $data['msg']= 'No Record Found!';
        $this->response($data, 400);

    }

    function takePayment($id = NULL, $amount = NULL, $patient, $appointment_id) 
    {
        if(!$id || !$amount)
            return array('response' => 'error', 'error_msg' => 'No registration id or amount');
        
        $success_codes = array('000.000.000', '000.400.000', '000.400.010', '000.400.020', '000.400.040', '000.400.060', '000.400.090','000.100.110', '000.100.111', '000.100.112');

        $site = $this->config->item('IS_LIVE') ? 'live_' : 'local_';
        $peachUrl = $this->config->item('IS_LIVE') ? 'https://oppwa.com/' : 'https://test.oppwa.com/';
        $url = $peachUrl . "v1/registrations/" . $id . "/payments";
        $data = "authentication.userId=" . $this->config->item($site . 'peach_userId') . 
           "&authentication.password=" . $this->config->item($site . 'peach_password') . 
            "&authentication.entityId=" . $this->config->item($site . 'peach_entityId_onceOff') .
            "&amount=" . number_format($amount, 2, '.', '') .
            "&currency=ZAR" .
            "&paymentType=DB" .
            "&recurringType=REPEATED" .
            "&customer.email=" . $patient->email . 
            "&customer.givenName=" . $patient->fname . 
            "&customer.surname=" . $patient->sname . 
            "&merchantTransactionId=" . $appointment_id;
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, $this->config->item('IS_LIVE'));
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $responseData = curl_exec($ch);
       
        if(curl_errno($ch)) {
            return array('response' => 'error', 'error_msg' => curl_error($ch));
        }
        curl_close($ch);
        $response = json_decode($responseData);

        if(!$response)
            return array('response' => 'error', 'error_msg' => 'No data found');

        if(in_array($response->result->code, $success_codes))
            return array('response' => 'success', 'data' => array('transaction_id' => $response->id, 'created_at' => $response->timestamp, 'status' => 1));
        return array('response' => 'error', 'error_msg' => $response->result->description);
    }
}