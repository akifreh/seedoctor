<?php
require(APPPATH . 'libraries/REST_Controller.php');

class Home extends REST_Controller
{    
    public function __construct() 
    {
        parent::__construct();
        $this->load->library(array('session', 'form_validation', 'email', 'upload'));
        $this->load->helper(array('form', 'url'));
        $this->load->database();
        $this->load->model('home_model');
        $this->load->model('user_model');
        $this->load->helper('security');
        if($this->post() && !$_POST)
            $_POST = $this->post();
    }

    function index_get() 
    {          
        $data['doctor_count'] = $this->home_model->getUserCount(3);
        //$data['patient_count'] = $this->home_model->getUserCount(2);
        $data['patient_count'] = $this->home_model->getServedPatientCount();
        $data['best_doctors'] = $this->home_model->getBestDoctors();
        $data['our_clients'] = $this->home_model->get_clients();

        $this->load->model('page_model');
        $data['fee_section'] = $this->page_model->get_content('fees');
        $data['footer'] = $this->page_model->get_content('footer');
        
        $this->response($data, 200);
    }

    function register_post() 
    {        
        $this->form_validation->set_rules('b_dd', 'b_dd', 'trim|required');
        $this->form_validation->set_rules('b_mm', 'b_mm', 'trim|required');
        $this->form_validation->set_rules('b_yy', 'b_yy', 'trim|required');
        $this->form_validation->set_rules('fname', 'fname', 'trim|required');
        $this->form_validation->set_rules('sname', 'sname', 'trim|required');
        $this->form_validation->set_rules('email', 'email', 'trim|required');
        $this->form_validation->set_rules('password', 'password', 'trim|required');
        $this->form_validation->set_rules('gender', 'gender', 'trim|required');
        $this->form_validation->set_rules('employer_number', 'employer_number', 'trim|required');

        if($this->form_validation->run() === FALSE)
        {
           // error
          $code = '400';
          $msg['error'] = 'Error! Please Fill the form & try again!!!\n';
          $msg['error_description'] = validation_errors();
          $this->response($msg, $code);
        }
        
        $b_dd = $this->post('b_dd');
        $b_mm = $this->post('b_mm');
        $b_yy = $this->post('b_yy');
        $dob = $b_yy .'-'. $b_mm .'-'. $b_dd;

        $data = array(
            'fname' => $this->post('fname'),
            'sname' => $this->post('sname'),
            'email' => $this->post('email'),
            'password' => md5($this->post('password')),
            'employer_number' => $this->post('employer_number'),
            'gender' => $this->post('gender'),
            'dob' => $dob,
            'role' => 2       
        );
        
        // AS ACTIVATION LINK PROCESS IS CLOSED SO MAKING STATUS TO 1 BY DEFAULT.
        $data['status'] = 1;

        /*
        $cc_number = $this->post('cc_number');
        $cc_number = str_replace(' ', '', $cc_number);

        $exp_dd = $this->post('ex_dd');
        $exp_mm = $this->post('ex_mm');
        $exp_yy = $this->post('ex_yy'); */

        $userEmail = $this->post('email');
        $userExist = $this->user_model->readAlreadyExistUser($userEmail);
        if($userExist)
        {
            $code = '405';
            $msg = 'The Email is already exists. Please choose another one.';
            $this->response($msg, $code);
        }
        else
        {
            // insert form data into database
            if ($id = $this->user_model->insertUser($data)) // OMITTED CARD DETAILS, $data1)) 
            {
                // send email
                $email = $this->post('email');
                $password = $this->post('password');
                $name = $this->post('fname');
                $this->user_model->sendAfterVerifyEmail($data);

                $code = '200';
                $data['id'] = $id;
                $data['msg'] = 'Congratulations! You have successfully registered. You now have free access to SeeADoctor.africa at any time!';
                $this->response($data, $code);
            } 
            else 
            {
                // error
                $this->session->set_flashdata('msg', '<div class="alert alert-danger text-center">Oops! Error.  Please try again later!!!</div>');
                redirect();
            }
        }  
        
    }

    function verify_get()
    {
        $hash = $this->get('hash');
        if($this->user_model->verifyEmailID($hash)) 
        {
            $user = $this->user_model->getUserByHashEmail($hash);
            $data = json_decode(json_encode($user), true);
            $data['msg'] = 'Your Email Address is successfully verified! Please login to access your account!';
            $code = '200';
            $this->response($data, $code);
        } 
        else 
        {
            // error
            $code = '401';
            $msg = 'Sorry! There is error verifying your Email Address!';
            $this->response($msg, $code);
        }
    }

    // Check for user login process
    public function login_post() 
    {
        $emailPass = array('email' => $this->post('email'),'password' => $this->post('password'));
        $userResult = $this->user_model->checkUserPass($emailPass);

        if (!$userResult) 
        {
            // error
            $code = '401';
            $msg = 'Error! Please enter valid username or password';
            $this->response($msg, $code);         
        } 
        else 
        {     
            $data = array(
                'email' => $this->post('email'),
                'password' => $this->post('password')
            );

            $admin = $this->user_model->getAdmin($data);
            if($admin == TRUE )
            {     
                $email = $this->post('email');
                $admin = $this->user_model->read_user_information($email);
                
                if($admin) 
                {
                    $session_data = array(
                        'fname' => $admin[0]->fname,
                        'email' => $admin[0]->email,
                        'id' => $admin[0]->id,
                        'role' => $admin[0]->role
                    );

                                        session_regenerate_id();
                    $session = session_id();
                    $userSession = array(
                        'name' => $admin[0]->fname, 
                        'email' => $admin[0]->email,
                        'user_id' => $admin[0]->id,
                        'role' => $admin[0]->role,
                        'code' => $session
                    );
                    $this->user_model->addSession($userSession);

                    // Add user data in session
                    $response_data['logged_in'] = $session_data;
                    $response_data['code'] = $session;
                    $response_data['msg'] = 'You have been successfully logged into system';
                    $code = '200';
                    $this->response($response_data, $code);
                }

            }

            // Patient Login Process
            $doctor = $this->user_model->loginUser($data);
            if ($doctor == TRUE) 
            {
                $email = $this->post('email');
                $doctor = $this->user_model->read_user_information($email);
                
                if ($doctor) 
                {
                    $session_data = array(
                        'fname' => $doctor[0]->fname,
                        'email' => $doctor[0]->email,
                        'id' => $doctor[0]->id,
                        'role' => $doctor[0]->role
                    );

                    session_regenerate_id();
                    $session = session_id();
                    $userSession = array(
                        'name' => $doctor[0]->fname, 
                        'email' => $doctor[0]->email,
                        'user_id' => $doctor[0]->id,
                        'role' => $doctor[0]->role,
                        'code' => $session
                    );
                    $this->user_model->addSession($userSession);

                    // Add user data in session
                    $response_data['logged_in'] = $session_data;
                    $response_data['code'] = $session;
                    $response_data['msg'] = 'Hello ' . ucfirst($doctor[0]->fname) .', ' .  ucfirst($doctor[0]->sname) . ' welcome, we look forward to assisting you';
                    $code = '200';
                    $this->response($response_data, $code);                    
                }
            }

            // Doctor Login Process
            $result = $this->user_model->loginDoctor($data);
            if ($result) 
            {                
                $email = $this->post('email');
                $result = $this->user_model->read_user_information($email);
                
                if ($result) 
                {
                    session_regenerate_id();
                    $session = session_id();
                    $user_id = $result[0]->id;
                    $time = time();
                    
                    $session_data = array(
                        'fname' => $result[0]->fname,
                        'email' => $result[0]->email,
                        'id' => $user_id,
                        'role' => $result[0]->role,
                        'session' => $session
                    );

                    $sessionData = array(
                        'session' => $session,
                        'time' => $time,
                        'user_id' => $user_id
                    );

                    $userSession = array(
                        'name' => $result[0]->fname, 
                        'email' => $result[0]->email,
                        'user_id' => $user_id,
                        'role' => $result[0]->role,
                        'code' => $session
                    );
                    
                    // Add user data in session
                    $response_data['logged_in'] = $session_data;
                    $response_data['code'] = $session;
                    $response_data['msg'] = 'Login successful';
                    $code = '200';

                    // Add user data in session
                    $this->user_model->addSession($userSession);
                    $onlineDoctor = $this->user_model->readDoctorSession($user_id);
                    
                    if($onlineDoctor)
                        $this->user_model->updateDoctorSession($sessionData);
                    else
                        $this->user_model->insertDoctorSession($sessionData);  
                                        
                    $this->response($response_data, $code); 
                }
            } 
            else
            {
                // error
                $code = '401';
                $msg = 'Please type correct email and password or contact the Administrator!';
                $this->response($msg, $code);
            }   
        }
    }
    
    public function logout_get() 
    {
        $code = $this->get('code');
        
        $result = $this->user_model->getSession($code);
        if($result)
        {
            $this->user_model->deleteSession($code);
        
            $userId = $result->user_id;     
            $result = $this->user_model->readSessionInfo($userId);
            if($result)
                $this->user_model->deleteDoctorSession($userId);                      
        
            $code = '200';
            $msg = 'Thank you for visiting our site. Hope you come again soon!';
            $this->response($msg, $code);
        }
        else
        {
            // error
            $code = '401';
            $msg = 'No such user is logged in. Either user is logged out or wrong session code is given.';
            $this->response($msg, $code);
        }
    }
    
    function secondSignup_get()
    {
        $code = $this->get('code');
        $result = $this->user_model->getSession($code);
        if($result && $result->role == 2)
        {
            $email = $result->email;
            $id = $result->user_id;
            $data['userinfo'] = $this->user_model->read_user_information($email);
            //$data['academics'] = $this->user_model->readAcademics($id);
            $data['otherprofile'] = $this->user_model->readOtherProfile($id);
            $data['cardInfo'] = $this->user_model->readCardInfo($id);
            $date = $data['cardInfo']->card_expiry;

            if(isset($data['userinfo'][0]) && $data['userinfo'][0])
            {
                $data['userinfo'][0]->profile_pic = get_image_url($data['userinfo'][0]->profile_pic);
            }

            $code = '200';
            $this->response($data, $code);
        }
        else
        {
            // error
            $code = '404';
            $msg = 'You don\'t have access to view this content.';
            $this->response($msg, $code);
        }
    }

    function secondSignup_post()
    {         
        $post = $this->post();
        if($post)
        {   
            $code = $post['code'];
            $session = $this->user_model->getSession($code);
            if(!$session)
            {
                // error
                $code = '404';
                $msg = 'You don\'t have permission to access this service';
                $this->response($msg, $code);
            }

            $type = $this->post('submit');
            $error = FALSE;
            if($type == 'personal')
            {
                $b_dd = $this->post('b_dd');
                $b_mm = $this->post('b_mm');
                $b_yy = $this->post('b_yy');
                $dob = $b_yy .'-'. $b_mm .'-'. $b_dd;  

                $data = array(
                    'id' => $session->user_id,
                    'dob' => $dob,
                    'phone' => $this->post('phone')
                );
            }
            elseif($type == 'contact')
            {
                $data = array(
                    'id' => $session->user_id,
                    'address' => $this->post('address'),
                    'city' => $this->post('city'),
                    'province' => $this->post('province'),
                    'country' => $this->post('country'),
                    'zip' => $this->post('zip'),   
                    'p_address' => $this->post('p_address'),
                    'p_city' => $this->post('p_city'),
                    'p_province' => $this->post('p_province'),
                    'p_country' => $this->post('p_country'),
                    'p_zip' => $this->post('p_zip'),
                    'sameasres' => $this->post('sameasres'),
                );
            }
            /******************** START OF - CLOSING THIS FOR NO-CHARGE TEMPORARY *************
            elseif($type == 'card')
            {
                $cc_number = str_replace(' ', '', $this->post('cc_number')); 
                $data1 = array(
                    'cc_type' => $this->post('cc_type'),
                    'cc_holder' => $this->post('cc_holder'),
                    'cc_number' => $cc_number,
                    'cc_cvv' => $this->post('cc_csv_number'),
                    'cc_exp_yy' => $this->post('ex_yy'),
                    'cc_exp_mm' => $this->post('ex_mm')
                );

                $result = $this->getUserToken($data1);

                if($result['response'] == 'error')
                {
                    $data['error'] = $result['error_msg'];
                    $error = TRUE;
                }
                else
                {
                    $data1 = array(
                                'registration_id ' => $result['id'],
                                'card_brand' => $this->post('cc_type'),
                                'card_holder' => $this->post('cc_holder'),
                                'card_expiry' => date('Y-m-d', strtotime($this->post('ex_yy').'-'.$this->post('ex_mm').'-01')),
                                'card_number' =>  substr($cc_number, -4)
                            );
                    $this->user_model->updateCardInfo($data1, $session->user_id);                         
                    
                    $data['msg'] = 'Your card information has been updated successfully';
                    $this->respose($data, 200);
                }
                
            }
            /********************** END OF - CLOSING THIS FOR NO-CHARGE TEMPORARY *************/
            elseif($type == 'job')
            {
                $hasjob = $this->post('has_job');
                $hasaid = $this->post('has_aid');
                if($hasjob == 'no')
                {
                    $job_title = 'No';
                    $company_employed = 'None';
                    $employer_number = 'None';
                    $employer_address = 'None';
                }
                else
                {
                    $job_title = $this->post('job_title');
                    $company_employed = $this->post('company_employed');
                    $employer_number = $this->post('employer_number');
                    $employer_address = $this->post('employer_address');
                }

                if($hasaid == 'no')
                    $medical_aid = 'No';
                else
                    $medical_aid = $this->post('medical_aid');

                $data = array(
                    'id' => $session->user_id,
                    'job_title' => $job_title,
                    'company_employed' => $company_employed,
                    'employer_number' => $employer_number,
                    'employer_address' => $employer_address,
                    'next_of_kin' => $this->post('next_of_kin'),
                    'next_of_kin_contact' => $this->post('next_of_kin_contact'),
                    'medical_aid' => $medical_aid,
                );
            }

            if($_FILES)
            {    
                $config['upload_path']          = './uploads/profile';
                $config['allowed_types']        = 'gif|jpg|png';

                $this->load->library('upload', $config);
                $this->upload->initialize($config);

                if(isset($_FILES['profile_pic']['tmp_name']) && $_FILES['profile_pic']['tmp_name'])
                {
                    $thumb_path = realpath(getcwd()) . '/uploads/profile/' ;
                    if (!is_dir($thumb_path))
                    {
                        mkdir($thumb_path);
                        chmod($thumb_path, 0777);
                    }

                    if ( ! $this->upload->do_upload('profile_pic'))
                    {
                            $error = TRUE;
                            $data['error'] = $this->upload->display_errors();
                    }
                    else
                    {
                            $data_file = array('upload_data' => $this->upload->data());
                            $data['profile_pic'] = $data_file['upload_data']['file_name'];
                            generate_preset($data_file['upload_data']['full_path']);
                    }
                }  

            }

            if(!$error)
            {
                $data['msg'] = 'Your profile has been updated successfully.';
                $this->response($data, 200);
            }
        }
        else
        {
            // error
            $code = '401';
            $msg = 'Error updating profile. Please try again!';
            $this->response($msg, $code);
        }
    }

    function getUserToken($data = array()) 
    {
        $success_codes = array('000.000.000', '000.400.000', '000.400.010', '000.400.020', '000.400.040', '000.400.060', '000.400.090','000.100.110', '000.100.111', '000.100.112', '000.200.000');
        
        $site = $this->config->item('IS_LIVE') ? 'live_' : 'local_';
        $peachUrl = $this->config->item('IS_LIVE') ? 'https://oppwa.com/' : 'https://test.oppwa.com/';
        $url = $peachUrl . "v1/payments";
        $data = "authentication.userId=" . $this->config->item($site . 'peach_userId') . 
                "&authentication.password=" . $this->config->item($site . 'peach_password') . 
                "&authentication.entityId=" . $this->config->item($site . 'peach_entityId_onceOff') .
                "&amount=1.00" .
                "&currency=ZAR" .
                "&paymentBrand=" . $data['cc_type'] .
                "&paymentType=PA" .
                "&card.number=" . $data['cc_number'] .
                "&createRegistration=true" .
                "&card.holder=" . $data['cc_holder'] .
                "&card.expiryMonth=" . $data['cc_exp_mm'] .
                "&card.expiryYear=" . $data['cc_exp_yy'] .
                "&card.cvv=" . $data['cc_cvv'] . 
                "&shopperResultUrl=". base_url() . "home/complete-profile";

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, $this->config->item('IS_LIVE'));
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $responseData = curl_exec($ch);

        if(curl_errno($ch)) {
            return array('response' => 'error', 'error_msg' => curl_error($ch));
        }
        curl_close($ch);
        $response = json_decode($responseData);
        
        if(!$response)
            return array('response' => 'error', 'error_msg' => 'Card registration error.');
        
        if(in_array($response->result->code, $success_codes))
            return array('response' => 'success', 'id' => $response->registrationId);
            
        return array('response' => 'error', 'error_msg' => 'Card registration error. ' . $response->result->description);
    }
	
	function viewDoctorDetail_get()
    {       
        $id = $this->get('id');
        $this->load->model('admin_model');
        $this->load->model('doctor_model');
        $this->load->model('page_model');

        $data['row'] = $this->admin_model->readDoctorDetails($id);
        $data['doctorAvailableDays'] = $this->admin_model->readDoctorAvailableDays($id);
        $data['doctorTimings'] = $this->admin_model->readDoctorTimings($id);
        $data['doctorStatus'] = $this->admin_model->doctorStatus($id);
        $data['doctorrating'] = $this->doctor_model->readDoctorRating($id);

        $data['footer'] = $this->page_model->get_content('footer');
        $code = '200';
        $this->response($data, $code);
    }
    
    function ourClients_get()
    {       
        $data['our_clients'] = $this->home_model->get_clients();
        $this->load->model('page_model');
        $data['footer'] = $this->page_model->get_content('footer');
        
        $this->response($data, 200);
    }

    function page_get() 
    {  
        $page = $this->get('page');
        $this->load->model('page_model');
        $data = NULL;

        if($page == 'about-us') :
            $content = $this->page_model->get_content('about_us');
            $data['title'] = $content ? $content->title : NULL;
            $data['content'] = $content ? $content->content : NULL;
        endif;

        $data['footer'] = $this->page_model->get_content('footer');
        $code = '200';
        $this->response($data, $code);
        
    }

    function addClient_post()
     {
        $this->form_validation->set_rules('name', 'name', 'trim|required');
        $this->form_validation->set_rules('surname', 'surname', 'trim|required');
        $this->form_validation->set_rules('companyName', 'companyName', 'trim|required');
        $this->form_validation->set_rules('message', 'message', 'trim|required');
        $this->form_validation->set_rules('email', 'email', 'trim|required');
        $this->form_validation->set_rules('companySize', 'companySize', 'trim|required');
        
        if($this->form_validation->run() === FALSE)
        {
           // error
          $code = '400';
          $msg['error'] = 'Error! Please Fill the form & try again!!!';
          $msg['error_description'] = validation_errors();
          $this->response($msg, $code);
        }

        $data['name'] = $this->post('name');
        $data['surname'] = $this->post('surname');
        $data['email'] = $this->post('email');
        $data['company_name'] = $this->post('companyName');
        $data['message'] = $this->post('message');
        $data['company_size'] = $this->post('companySize');

        $this->home_model->sendClientEmailToAdmin($data);
        $data['msg'] = 'Your information has been send successfully! Please wait till you here from us shortly';
        
        $this->response($data, 200); 
     }
}