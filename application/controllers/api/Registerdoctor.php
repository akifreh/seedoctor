<?php
require(APPPATH . 'libraries/REST_Controller.php');

class Registerdoctor extends REST_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->helper(array('form','url'));
        $this->load->library(array('session', 'form_validation', 'email'));
        $this->load->database();
        $this->load->model('doctor_model');
        $this->load->model('user_model');
        $this->load->helper('security');

        if($this->post() && !$_POST)
            $_POST = $this->post();
    }
    
    function index_get()
    {
        $this->load->model('page_model');
        $data['page'] = $this->page_model->get_content('apply_doctor_page');
        $data['breadcrumbs']['apply_doctor'] = TRUE;
        $data['footer'] = $this->page_model->get_content('footer');
        
        $this->response($data, 200);
    }
    
    function applyAsDoctor_post()
    {
      $this->form_validation->set_rules('email', 'email', 'trim|required');
      $this->form_validation->set_rules('fname', 'First Name', 'trim|required');
      $this->form_validation->set_rules('sname', 'Surname', 'trim|required');
      $this->form_validation->set_rules('phone', 'phone', 'trim|required');
      $this->form_validation->set_rules('qualification[]', 'qualification', 'required');
      $this->form_validation->set_rules('institution[]', 'institution', 'required');
      $this->form_validation->set_rules('year[]', 'year', 'required');
      $this->form_validation->set_rules('type_of_doctor', 'type_of_doctor', 'required');
      $this->form_validation->set_rules('company[]', 'company', 'required');
      $this->form_validation->set_rules('job_title[]', 'job_title', 'required');
      $this->form_validation->set_rules('start_mm[]', 'start_mm', 'required');
      $this->form_validation->set_rules('start_yy[]', 'start_yy', 'required');
      $this->form_validation->set_rules('dayoftheweek[]', 'dayoftheweek', 'required');
      $this->form_validation->set_rules('to-timing[]', 'to-timing', 'required');
      $this->form_validation->set_rules('from-timing[]', 'from-timing', 'required');

      if($this->form_validation->run() === FALSE)
      {
         // error
        $code = '400';
        $msg['error'] = 'Error! Please Fill the form & try again!!!';
        $msg['error_description'] = validation_errors();
        $this->response($msg, $code);
      }
      $docDay = $this->post('dayoftheweek');
      $docEmail = $this->post('email');
      $doctorExist = $this->user_model->readAlreadyExistUser($docEmail);
      $toTime = $this->post('to-timing');
      $fromTime = $this->post('from-timing');

      $err = FALSE;
      if(empty($docDay))
      {
           $errorMsg = 'Please Choose your day(s)';
           $err = TRUE;
      }
      
      foreach ($docDay as $key => $value) 
      {
        if(empty($toTime[$value][0]) || empty($fromTime[$value][0]))
        {
          $errorMsg = 'Please Choose your timing(s)';
          $err = TRUE;
        }
      }
      
      if($err == FALSE)
      {
        if($doctorExist == TRUE)
        {
          $errorMsg = 'User Already Exists! Try another email to signup';
          $err = TRUE;
        }
      }
      // Pictures removed for now     
      //$picture = $_FILES['profile_pic'];
      $file = $_FILES['cv'];
      
      /*if(!$file)
      {
          $errorMsg = 'Cv not included. Please attach a cv';
          $err = TRUE;
      }*/

      if($err === TRUE)
      {     
         $this->response($errorMsg, 400);
      }
      else
      {
        $data = array(
           'fname' => $this->post('fname'),
            'sname' => $this->post('sname'),
            'email' => $this->post('email'),
            'phone' => $this->post('phone'),
            //'address' => $this->post('address'),
            'role' => 3,
            //'gender' => $this->post('gender'),    
        );
        
        $data1 = array(
            //'degree' => $this->post('degree'),
            'description' => $this->post('description'),
            'profession_number' => $this->post('profession_number'),
            'practice_number' => $this->post('practice_number'),
            //'experience' => $this->post('experience'),
            //'speciality' => $this->post('speciality'),
            'type_of_doctor' => $this->post('type_of_doctor'),
        );

        $result = $this->doctor_model->insertDoctor($data); 
        $data1['user_id'] = $result;
        $this->doctor_model->insertDoctorInfo($data1);
        
        $data2 = array('day'=> $this->post('dayoftheweek'));
        $data2['user_id'] = $result;
        $this->doctor_model->insertDoctorDays($data2);
            
        foreach ($docDay as $key => $value) 
        {
          foreach($toTime[$value] as $k => $v) :
            if($v && $fromTime[$value][$k]) 
            {
              $timing[] = array(
                 'user_id' => $result,
                 'day_id' => $value,
                 'end_time' => date('H:i:s', strtotime($v)),
                 'start_time' => date('H:i:s', strtotime($fromTime[$value][$k]))
              );
            }
          endforeach;
        }
        $this->doctor_model->insertDoctorTiming($timing);

        $qualification = $this->post('qualification');
        $institution = $this->post('institution');
        $year = $this->post('year');

        foreach($qualification as $key => $value)
        {
          if($value)
          {
            $data5 = array(
              'qualification' => $value,
              'institution' => $institution[$key],
              'year' => $year[$key],
              'user_id' => $result
            );
            $this->doctor_model->insertAcademics($data5);
          }   
        }

        $company = $this->post('company');
        $job_title = $this->post('job_title');
        $start_yy = $this->post('start_yy');
        $start_mm = $this->post('start_mm');
        $end_yy = $this->post('end_yy');
        $end_mm = $this->post('end_mm');
         $present_here   = $this->post('stay_here');
        $get_present_here  =   $present_here[0];
        
        $checkCounter = 1;

        foreach($company as $key => $value)
        {
            
          if($value)
          {
              $present_here = ($checkCounter == $get_present_here) ? '1' : '0' ;
              
            $data6 = array(
              'company' => $value,
              'job_title' => $job_title[$key],
              'start_date' => date('Y-m-d', strtotime($start_yy[$key] . '-' . $start_mm[$key])),
              'end_date' => date('Y-m-d', strtotime($end_yy[$key] . '-' . $end_mm[$key])),
              'present_here'  => $present_here,
              'user_id' => $result
            );
            $this->doctor_model->insertExperience($data6);   
          }
           $checkCounter++;
          
        }

        $doctorId = $result;
        $data2 = NULL;
        
         // Upload Doctor Id Card
        $config['upload_path']          = './uploads/id_card/';
        $config['file_name']            = 'pic-' . $doctorId;
        $config['allowed_types']        = 'gif|jpg|png';
        
        $this->load->library('upload', $config);
        $this->upload->initialize($config);

        if(isset($_FILES['id_card']['tmp_name']))
        {
          $thumb_path = realpath(getcwd()) . '/uploads/id_card/' ;
          if (!is_dir($thumb_path))
          {
              mkdir($thumb_path);
              chmod($thumb_path, 0777);
          }

          if ( ! $this->upload->do_upload('id_card'))
          {
            $error = array('error' => $this->upload->display_errors());
          }
          else
          {
              
              $data_file = array('upload_data' => $this->upload->data());
              $data2['id_card'] = $data_file['upload_data']['file_name'];
              //generate_preset($data_file['upload_data']['full_path']);    
          }
        }
        
           // Upload Doctor Profile
        $config['upload_path']          = './uploads/cv/';
        $config['file_name']            = 'cv-' . $doctorId;
        $config['allowed_types']        = 'docx|doc|pdf';
        
        $this->load->library('upload', $config);
        $this->upload->initialize($config);

        if(isset($_FILES['cv']['tmp_name']))
        {
          $thumb_path = realpath(getcwd()) . '/uploads/cv/' ;
          if (!is_dir($thumb_path))
          {
              mkdir($thumb_path);
              chmod($thumb_path, 0777);
          }

          if ( ! $this->upload->do_upload('cv'))
          {
            $error = array('error' => $this->upload->display_errors());         
          }
          else
          {
            $data_file = array('upload_data' => $this->upload->data(), '');
            $data2['cv'] = $data_file['upload_data']['file_name'];     
          }
        }
            
        if($result == TRUE)
        {
          if($data2)
            $this->doctor_model->updateDoctorInfo($data2, $doctorId);

          $this->doctor_model->DoctorSignupEmailSendToAdmin($data);
          $this->doctor_model->DoctorSignupEmailSendToDoctor($data);
          
          $msg = 'You have successfully applied as Doctor. Please wait for administrator approval Email.';
          $this->response($msg, 200);
        }
        else
        {
          $msg = 'Ops! Error occured. Please fill the form properly.';
          $this->response($msg, 400);        
        }
      }        
    }
}