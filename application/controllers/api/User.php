<?php
require(APPPATH . 'libraries/REST_Controller.php');

class User extends REST_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->helper(array('form','url'));
        $this->load->library(array('session', 'form_validation', 'email'));
        $this->load->database();
        $this->load->model('user_model');
        $this->load->helper('security');

        if($this->post() && !$_POST)
            $_POST = $this->post();
    }
    
    function addProfile_post()
    {
        $this->form_validation->set_rules('name', 'Name', 'trim|required|min_length[3]|max_length[30]');
        $this->form_validation->set_rules('sname', 'Sname', 'trim|required|min_length[3]|max_length[30]');
        $this->form_validation->set_rules('relationship', 'Relationship', 'trim|required|min_length[3]|max_length[30]|xss_clean');
        $this->form_validation->set_rules('b_dd', 'b_dd', 'trim|required');
        $this->form_validation->set_rules('b_mm', 'b_mm', 'trim|required');
        $this->form_validation->set_rules('b_yy', 'b_yy', 'trim|required');
        $this->form_validation->set_rules('code', 'code', 'trim|required');
       
        if ($this->form_validation->run() == FALSE) 
        {
            $data['msg'] = 'Error. Please try again later!!!';
            $data['errors'] = validation_errors();

            $this->response($data, 404);
        }
        else 
        {    
            $code = $this->post('code');
            $session = $this->user_model->getSession($code);

            if(!$session || $session->role != 2)
            {
                $data['msg'] = 'You are not authorized to visit this page. Sorry!';
                $this->response($data, 404);          
            }

            $b_dd = $this->post('b_dd');
            $b_mm = $this->post('b_mm');
            $b_yy = $this->post('b_yy');
            $name = $this->post('name');
            $sname = $this->post('sname');
            $relationShip = $this->post('relationship');
        
            $dob = $b_yy .'-'. $b_mm .'-'. $b_dd; 
            $insertData = array(     
                    'name' =>  $name[$index],
                    'sname' => $sname[$index],
                    'relationship' =>  $relationShip,
                    'dob' => $dob,
                    'user_id' => $session->user_id
                );
            $this->user_model->insertProfile($insertData);

            $msg = 'Your other profile record has been added successfully.';
            $this->response($msg, 200);
        }
    }

    function viewOtherProfile_get()
    {
        $code = $this->get('code');
        $session = $this->user_model->getSession($code);

        if(!$session || $session->role != 2)
        {
            $data['msg'] = 'You are not authorized to visit this page. Sorry!';
            $this->response($data, 404);          
        }

        $this->load->model('page_model');
        $data['otherProfileDetail'] = $this->user_model->readOtherProfile($session->user_id);
        $data['footer'] = $this->page_model->get_content('footer');
        
        $this->response($data, 200);
    }

    function deleteOtherProfile_post()
    {
        $id = $this->post('id');
        $this->user_model->deleteProfile($id);

        $msg = 'Your other profile record has been deleted successfully.';
        $this->response($msg, 200);
    }

    function editOtherProfile()
    {
        $this->form_validation->set_rules('name', 'Name', 'trim|required|min_length[3]|max_length[30]');
        $this->form_validation->set_rules('sname', 'Sname', 'trim|required|min_length[3]|max_length[30]');
        $this->form_validation->set_rules('relationship', 'Relationship', 'trim|required|min_length[3]|max_length[30]|xss_clean');
        $this->form_validation->set_rules('b_dd', 'b_dd', 'trim|required');
        $this->form_validation->set_rules('b_mm', 'b_mm', 'trim|required');
        $this->form_validation->set_rules('b_yy', 'b_yy', 'trim|required');
        $this->form_validation->set_rules('id', 'id', 'trim|required');
        $this->form_validation->set_rules('code', 'code', 'trim|required');
        
        if ($this->form_validation->run() == FALSE) 
        {
            $data['msg'] = 'Error. Please try again later!!!';
            $data['errors'] = validation_errors();

            $this->response($data, 404);
        }
        else
        {
            $code = $this->post('code');
            $session = $this->user_model->getSession($code);

            if(!$session || $session->role != 2)
            {
                $data['msg'] = 'You are not authorized to visit this page. Sorry!';
                $this->response($data, 404);          
            }

            $b_dd = $this->post('b_dd');
            $b_mm = $this->post('b_mm');
            $b_yy = $this->post('b_yy');
            
            $dob = $b_yy .'-'. $b_mm .'-'. $b_dd;  
            $data = array(
                'id' => $this->post('id'),
                'name' => $this->post('name'), 
                'sname' => $this->post('sname'), 
                'relationship' => $this->post('relationship'),
                'dob' => $dob
            );

            $this->user_model->updateOtherProfile($data);

            $msg = 'Your other profile record has been updated successfully.';
            $this->response($msg, 200);
        } 
    }

    function forgetPasswordToken_get()
    {
        $email = $this->get('email');
        if($email)
        {
            if($record = $this->user_model->checkUserByEmail($email))
            {
                $token = $this->user_model->insertToken($record->id, $email);
                $this->user_model->sendForgetEmail($email, $token, $record->fname);
                $link = base_url() .'api/user/checkPasswordToken?token=' . $token;
                $data['link'] = $link;
                $data['name'] = $record->fname;
                $data['email'] = $email;
                $data['token'] = $token;
                $data['msg'] = 'Token generated successfully.';
                $this->response($data, 200);
            }
            else
                $this->response('Email address '. $email .' is not registered on this platform.', 400); 
        }
        else
        {
           $this->response('Valid Email address is required.', 400);
        }
    }

    function checkPasswordToken_get()
    {
        $token = $this->get('token');
        if(!$token)
        {
            $data['isValidToken'] = FALSE;
            $data['msg'] = 'No or invalid token. Please re-submit forget password form';
            $this->response($data, 400);
        }
        
        $record = $this->user_model->getTokenDetails($token);
        if(!$record)
        {
            $data['isValidToken'] = FALSE;
            $data['msg'] = 'Invalid token. Please re-submit forget password form';
            $this->response($data, 400);
        } 
        
            $data['isValidToken'] = TRUE;
            $link = base_url() .'api/user/newPassword';
            $data['link'] = $link;
            $data['userId'] = $record->user_id;
            $data['msg'] = 'Token is valid.';
            $data['developer_msg'] = 'Please include provided userId in new password request';
            $this->response($data, 400);
    }

    function newPassword_post()
    {
        $password = $this->post('newPassword');
        $confirm_password = $this->post('confirmPassword');

        $this->form_validation->set_rules('newPassword', 'Password', 'trim|required|matches[confirmPassword]');
        $this->form_validation->set_rules('confirmPassword', 'Confirm Password', 'trim|required');
        $this->form_validation->set_rules('userId', 'User Id', 'trim|required');

        //validate form input
        if ($this->form_validation->run() == FALSE)
        {
            $data['msg'] = 'Error. Please try again later!!!';
            $data['errors'] = validation_errors();
            $this->response($data, 400);
        }
        else
        {
            $user_id = $this->post('userId');
            if($this->user_model->changePassword($password, $user_id))
                $this->response('Password changed successfully.', 200);
            else
                $this->response('An error occurred. Please type new password again.', 400);
        }       
    }

    function rateDoctorAndSystem_post()
    {
        $this->form_validation->set_rules('code', 'Code', 'trim|required');
        $this->form_validation->set_rules('doctorId', 'doctorId', 'trim|required');
        $this->form_validation->set_rules('doctorRating', 'Doctor Rating', 'trim|required');
        $this->form_validation->set_rules('appointmentId', 'Appointment Id', 'trim|required');
        $this->form_validation->set_rules('systemRating', 'system Rating', 'trim|required');
        $this->form_validation->set_rules('comments', 'comments', 'trim|required');

        if ($this->form_validation->run() == FALSE) 
        {
            $data['msg'] = 'Error. Please try again later!!!';
            $data['errors'] = validation_errors();

            $this->response($data, 404);
        }
        
        $code = $this->post('code');
        $session = $this->user_model->getSession($code);

        if(!$session || $session->role != 2)
        {
            $data['msg'] = 'You are not authorized to visit this page. Sorry!';
            $this->response($data, 404);          
        }
        
        $doctorId = $this->post('doctorId');
        $record = array(
            'p_id' => $session->user_id,
            'd_id' => $doctorId,
            'rating' => $this->post('doctorRating')
        );
        
        $this->user_model->saveDoctorRating($record);
        $this->user_model->updateDoctorRating($doctorId);
        
        $record = array(
            'p_id' => $session->user_id,
            'appointment_id' => $this->post('appointmentId'),
            'system_rating' => $this->post('systemRating'),
            'comments' => $this->post('comments')
        );
        $this->user_model->saveUserFeedback($record);
        $this->user_model->sendCommentsToAdmin($record);
        
        $data['msg'] = 'System & Doctor rating & comments saved successfully.';
        $this->response($data, 200);
    }

    function doctorVerify_get()
    {
        $hash = $this->get('hash');
        if ($this->user_model->verifyEmailID($hash))
        {
            $this->load->model('admin_model');  
            
            $record = $this->user_model->getUserByHashEmail($hash);
            
            $password = $this->admin_model->randomPassword();
            $this->admin_model->updateDoctorPassword($record->id, $password);
       
            // send login detail email to doctor
            $this->admin_model->sendDoctorEmail($record, $password);

            // Set doctor status to 1 
            $this->admin_model->verfiyDoctorEmail($record->id, 1);
            
            $data['msg'] = 'Your Email Address is successfully verified. Please check your email for login credentials and login to access your account!';
            $this->response($data, 200);
        }
        else
        {
            $data['msg'] = 'Sorry! There is error verifying your Email Address! Please contact the administrator';
            $this->response($data, 400);   
        }
    }

    function changePassword_post()
    {
        $password = $this->post('newPassword');
        $current_password = $this->post('currentPassword');
        $confirm_password = $this->post('confirmPassword');

        $this->form_validation->set_rules('newPassword', 'Password', 'trim|required|matches[confirmPassword]');
        $this->form_validation->set_rules('confirmPassword', 'Confirm Password', 'trim|required');
        $this->form_validation->set_rules('currentPassword', 'Current Password', 'trim|required');
        $this->form_validation->set_rules('code', 'code', 'trim|required');
        
        if($this->form_validation->run() == FALSE)
        {
            $data['msg'] = 'Error. Please try again later!!!';
            $data['errors'] = validation_errors();

            $this->response($data, 404);
        }

        $code = $this->post('code');
        $session = $this->user_model->getSession($code);
        if(!$session)
        {
            $data['msg'] = 'You are not authorized to visit this page. Sorry!';
            $this->response($data, 404);          
        }

        $user_id = $session->user_id;
        
        if(!$this->user_model->checkPassword($current_password, $user_id))
        {
            $data['msg'] = 'Current Password is not correct. Please try again.';
            $this->response($data, 404);
        }
                      
        $this->user_model->changePassword($password, $user_id);
        $data['msg'] = 'Your password has been changed successfully';
        $this->response($data, 200);
    }
}