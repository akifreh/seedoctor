<?php
require(APPPATH . 'libraries/REST_Controller.php');

class Appointment extends REST_Controller
{
  public function __construct()
  {
      parent::__construct();
      $this->load->helper(array('form','url'));
      $this->load->library(array('session', 'form_validation', 'email'));
      $this->load->database();
      $this->load->model('user_model');
      $this->load->model('appointment_model');
      $this->load->helper('security');
      $this->load->model('doctor_model');
      
      if($this->post() && !$_POST)
        $_POST = $this->post();
  }
  
  function index_get()
  {
    $code = $this->get('code');
    $session = $this->user_model->getSession($code);
    
    if($session) 
    {
      if($session->role != 2)
      {
        $data['msg'] = 'You are not authorized to visit this page. Sorry!';
        $this->response($data, 404); 
      }

      $userId = $session->user_id;
      
      $data['breadcrumbs']['appointment'] = TRUE;
      $data['showChat'] = TRUE;

      $data['nutritionists'] = $this->appointment_model->readNutritionists();
      $data['psychologists'] = $this->appointment_model->readPsychologists();
      $data['medicalDoctors'] = $this->appointment_model->readMedicalDoctor();
      $data['allpsychologists'] = $this->appointment_model->readAllPsychologists();
      $data['allmedicaldoctors'] = $this->appointment_model->readAllMedicalDoctor();
      $data['myappointments'] = $this->appointment_model->readAllMyAppointments($userId);
      $data['is_profile_incomplete'] = $this->appointment_model->is_profile_incomplete($userId);

      /******************** START OF - CLOSING THIS FOR NO-CHARGE TEMPORARY *************
      $data['has_card_expired'] = $this->appointment_model->has_card_expired($userId);
      $data['has_pending_amount'] = $this->appointment_model->has_pending_amount($userId); */
      $data['has_card_expired'] = FALSE;
      $data['has_pending_amount'] = FALSE;
      /********************** END OF - CLOSING THIS FOR NO-CHARGE TEMPORARY *************/

      $data['appointment_link'] = $this->appointment_model->getLastestAppointmentLink($userId);
      
      list($request, $response) = $this->appointment_model->checkDeleteAppointmentNow($userId);
      
      $data['pending_appointment_now_request'] = $request;

      if($request && $response)
      {
        $time = date('Y-m-d H:i:s', time());
        $request_time = date_diff(date_create($time), date_create($response));
        $data['passed_seconds'] = $request_time->i * 60 + $request_time->s;
      }
      elseif($response)
      {
        $data['msg'] = 'All doctors are currently occupied. Please try again. Alternatively you may book an appointment';
      }

      $this->response($data, 200);
    }
    else
    {
      $data['url'] = uri_string();
      if(isset($_SERVER['QUERY_STRING']) && $_SERVER['QUERY_STRING'])
          $data['url'] .= '?' . $_SERVER['QUERY_STRING'];
      
      $data['msg'] = 'Please login before you visit the site!';
      $this->response($data, 401);
    }
  }
  
  
  function getCallUrl_get()
  {
      $id = $this->get('id');
      $code = $this->get('code');

      if(!$id)
        $this->response('No appointment id specified', 401);
    
      $this->db->where('status', 1);
      $this->db->where('appointment_id', $id);
      $query = $this->db->get('sessionkey');
      $result = $query->row();
        
      if($result)
      {   
        $data['call_link'] = base_url() . 'call?sessionKey='.$result->session_key.'&code='.$code;
        $this->response($data, 200);
      }
      else
      {
        $msg = 'It is too early to start the call now, we will send you a reminder closer to scheduled time.';
        $this->response($msg, 401);
      }       
  }
    
  function doctorDays_post()
  {
    $doctorId = $this->post('userid');
    $data = $this->appointment_model->readOneMedicalDoctor($doctorId);

    // Set timezone
    date_default_timezone_set('UTC');

    // Start date
    $date = date('Y-m-d');

    // End date
    $end_date = strtotime ( '+14 days' , strtotime ( $date ) ) ;
    $end_date = date ( 'Y-m-d' , $end_date );

    $display_data['days'][0] = 'Select Appointment Day';
    while (strtotime($date) <= strtotime($end_date)) 
    {
      //echo "$date\n";
      for($i=0; $i< count($data); $i++)
      {
        if(ucfirst($data[$i]->day) == date('l', strtotime($date)))
        {
          $display_data['days'][] = $date;
        }
      }
      
      date('l', strtotime($date));
      $date = date ("Y-m-d", strtotime("+1 day", strtotime($date)));                       
    }

    $this->response($display_data, 200);
  }
  
  function doctorAvailableTiming_post()
  {
    $today = FALSE;
    $doctorId = $this->post('userid');
    $appointmentDate  = $this->post('appointmentDate');
    if($appointmentDate == date('Y-m-d'))
    {
      $today = TRUE;
      $timeNow = new DateTime();
      $timeNow->modify('+ 30 mins');
      $hour = $timeNow->format('H');
      $min = $timeNow->format('i');
      if($min < 15)
        $min = '15';
      elseif($min < 30)
        $min = '30';
      elseif($min < 45)
        $min = '45';
      else
      {
        $min = '00';
        $hour += 1;
      }
      $currentTime = $appointmentDate . ' ' . $hour . ':' . $min . ':00';
    }

    $day = date('D', strtotime($appointmentDate));
    $result = $this->appointment_model->readNutriTime($doctorId, $day);
    $result2 = $this->appointment_model->readNutriAvailableTime($doctorId);
    $display_data['timings'][0] = 'Select an Appointment Time';
    
    for ($i=0; $i<count($result); $i++)
    {
      $startTime = strtotime($appointmentDate. ' ' . $result[$i]->start_time);
      $endTime = strtotime($result[$i]->end_time);
      $timeDiffrence = (strtotime($result[$i]->end_time) - strtotime($result[$i]->start_time))/3600;
      $timeDiffrenceMin = $timeDiffrence * 60;
        
      for($j=0; $j< $timeDiffrenceMin; $j = $j + 15)
      {      
        $k = $j+15;
        $found = false;

        if($today)
        {
          if($currentTime > date("Y-m-d H:i:s", strtotime("+{$j} minutes", $startTime)))
            $found = true;
        }
        
        if($result2 && !$found)
        {
          foreach ($result2 as $row)
          {
            if( $row->start_time == date("Y-m-d H:i:s", strtotime("+{$j} minutes", $startTime)))
            {
              $found = true;
              break;
            } 
          }
        }
        
        if($found) 
          continue;
        
        $display_data['timings'][] = date("H:i:s", strtotime("+{$j} minutes", $startTime)) .' - '. date("H:i:s", strtotime("+{$k} minutes", $startTime));
      }
    }
    $this->response($display_data, 200);
  }

  function setAppointmentWithDoctor_post()
  {      
    $this->form_validation->set_rules('patient', 'Patient', 'trim|required');
    $this->form_validation->set_rules('doctorId', 'doctorId', 'trim|required');
    $this->form_validation->set_rules('availableDays', 'availableDays', 'trim|required');
    $this->form_validation->set_rules('description', 'description', 'trim|required');
    $this->form_validation->set_rules('availableTime', 'availableTime', 'trim|required');
    $this->form_validation->set_rules('code', 'code', 'trim|required');

    if($this->form_validation->run() === FALSE)
    {
      // error
      $code = '400';
      $msg['error'] = 'Error! Please Fill the form & try again!!!';
      $msg['error_description'] = validation_errors();
      $this->response($msg, $code);
    }
    else
    {        
      $code = $this->post('code');
      $result = $this->user_model->getSession($code);
      if(!$result)
      {
          // error
          $code = '404';
          $msg = 'You don\'t have permission to access this service';
          $this->response($msg, $code);
      }

      $patients = $this->post('patient');
      $patientId = $result->user_id;
      $availableDay = $this->post('availableDays');
      $availableTime = explode('-', $this->post('availableTime'));
      
      $data = array(
           'p_id' => $patientId,
           'd_id' => $this->post('doctorId'),
           'brief_description' => $this->post('description'),
           'med_taken' => $this->post('medication'),
           'patient' => $patients,
           'start_time' => $availableDay.' '.$availableTime[0],
           'end_time' => $availableDay.' '.$availableTime[1]
      );  
         
      $doctorId = $this->post('doctorId');    
      $appId = $this->appointment_model->insertMedicalDoctorAppointment($data); 
      $symptom = $this->post('symptom');
      
      if($symptom)
      {
        foreach ($symptom as $key => $value) 
        {
          foreach ($value as $val) 
          {
            $data_symp = array(
                'appointment_id' => $appId,
                'p_id' => $patientId,
                'category' => $key,
                'symptom' => $val
            );
            $this->appointment_model->insertSymptoms($data_symp); 
          }
        }
      }  
      $data1 = $this->appointment_model->readPatientScheduledAppointment($appId);
      
      $patientInfo = $this->doctor_model->readPatient($patientId);
      $doctorInfo = $this->doctor_model->readPatient($doctorId);
      if($data1 && $patientInfo && $doctorInfo)
      {
        $patientEmail = $patientInfo[0]->email;
        $patientName = $patientInfo[0]->fname .' '. $patientInfo[0]->sname;
        $doctorEmail = $doctorInfo[0]->email;
        $doctorName = $doctorInfo[0]->fname .' '. $doctorInfo[0]->sname;
        $appDate = date("d-m-Y",strtotime($data1[0]->start_time));
        $appTime = date("H:i:s",strtotime($data1[0]->start_time));
        
        $this->appointment_model->sendEmailToPatientForAppointment($patientEmail, $patientName, $appDate, $appTime);
        
        $doctor_message = 'You have a request for appointment on <b>'. $appDate .'</b> at <b>' . $appTime . '</b> You will recieve a meeting room url 30 minutes before your meeting time.';
        $notification_array =  array();
        $notification_array['notification_type'] = "new";
        $notification_array['appointment_type'] = "schedule";
        $notification_array['message'] = $doctor_message;
        $notification_array['user_id'] = $doctorId;
        $notification_array['p_id'] = $patientId;
        $notification_array['d_id'] = $doctorId;
        $notification_array['appointment_id'] = $appId;
        $this->appointment_model->insertNotification($notification_array);
        
        $this->appointment_model->sendEmailToDoctorForAppointment($doctorEmail, $doctorName, $appDate, $appTime);
      }

      $data['msg'] = 'Your Appointment has been successfully scheduled';
      $code = '200';
      $this->response($data, $code);
    }    
  } 
  
  function setAppointmentWithDoctorNow_post()
  {    
    $this->form_validation->set_rules('patient', 'Patient', 'trim|required');
    $this->form_validation->set_rules('description', 'description', 'trim|required');
    $this->form_validation->set_rules('doctor', 'Doctor', 'trim|required');
    $this->form_validation->set_rules('code', 'Code', 'trim|required');

    if($this->form_validation->run() === FALSE)
    {
       // error
      $code = '400';
      $msg['error'] = 'Error! Please Fill the form & try again!!!';
      $msg['error_description'] = validation_errors();
      $this->response($msg, $code);
    }
    else
    {        
      $code = $this->post('code');
      $result = $this->user_model->getSession($code);
      if(!$result)
      {
          // error
          $code = '404';
          $msg = 'You don\'t have permission to access this service';
          $this->response($msg, $code);
      }
      
      $patientId = $result->user_id;

      $doctor = $this->post('doctor');
      if($doctor == 'psychologist')
        $data = $this->appointment_model->readPsychologists();
      else
        $data = $this->appointment_model->readMedicalDoctor();
      
      $patients = $this->post('patient');
      
      if($data)
      {
        $time = date('Y-m-d H:i:s', time());
        foreach($data as $value)
        {
          
          $data1 = array(
          'p_id' => $patientId,
          'd_id' => $value->user_id,
          'brief_description' => $this->post('description'),
          'med_taken' => $this->post('medtaken'),
          'patient' => $patients,
          'created_at' => $time
          );

          $appointment_id = $this->appointment_model->insertNowAppointments($data1);
          
          //send notification to doctor 
          $notification_array =  array();
          $notification_array['notification_type'] = "new";
          $notification_array['appointment_type'] = "now";
          $notification_array['message'] = str_replace('{pname}', $result->name, NOW_APPOINTMENT_NOTIFY_DOCTOR);
          $notification_array['user_id'] = $value->user_id;
          $notification_array['p_id'] = $patientId;
          $notification_array['d_id'] = $value->user_id;
          
          $notification_array['appointment_id'] = $appointment_id;
          $this->appointment_model->insertNotification($notification_array);

          $this->appointment_model->sendAppointmentNowNotificationEmail($value->email, $value->fname);
        }
        
        /* Symptoms don't show on appointment now
        $symptom = $this->post('symptom');
        if($symptom)
        {
          foreach ($symptom as $key => $value) 
          {
            foreach ($value as $val) 
            {
              $data = array(
                  'appointment_id' => NULL,
                  'p_id' => $patientId,
                  'category' => $key,
                  'symptom' => $val
              );
              $this->appointment_model->insertSymptoms($data); 
            }
          }
        }*/

        $response_data['msg'] = 'Your request has been successfully sent to our healthcare consultant. Please be patient a qualified Doctor will be with you shortly.';
      }     
      else
      {
        if($doctor == 'psychologist')
          $time = $this->appointment_model->getNextAvailableTime(2);
        else
          $time = $this->appointment_model->getNextAvailableTime(3);

        $msg = 'Hello '. $result->name . ',  kindly note that there is no ' .ucfirst($doctor). ' available at this moment. ';
        $msg .= $time ? 'A ' .ucfirst($doctor). ' will be available at ' . $time .'.' : 'Please try again tomorrow.';
        $msg .= ' Alternatively you may book an appointment.';
        $response_data['msg'] = $msg; 
      }      
      
      $code = '200';
      $this->response($response_data, $code);
    }
  }

  function cancelAppointment_post()
  {
    $id = $this->post('appointmentId');
    $code = $this->post('code');
    $reason = $this->post('reason');

    if(!$id || !$code || !$reason)
    {
      // error
      $code = '400';
      $msg = 'No appointment id or code or reason given.';
      $this->response($msg, $code);
    }

    $result = $this->user_model->getSession($code);
    if(!$result)
    {
      // error
      $code = '404';
      $msg = 'You don\'t have access to view this content.';
      $this->response($msg, $code);
    }

    $record = $this->appointment_model->getAppointment($id);
    if(!$record)
    {
      // error
      $code = '400';
      $msg = 'Appointment does not exist';
      $this->response($msg, $code);
    }

    $patient = $this->appointment_model->getUserById($record->p_id);
    $doctor = $this->appointment_model->getUserById($record->d_id);
    
    $this->appointment_model->deleteAppoinment($id);
        
    if($result->role == 2)
    {   
      //send notification to doctor 
      $doctor_message = $patient->fname. ' has cancelled following appointment with you Date : '. date('d M Y', strtotime($record->start_time)) . '&nbsp;&nbsp;&nbsp;Time : '. date('h:i a', strtotime($record->start_time));
      $doctor_message .= '<br />' . 'This is the reason given : <br />' . formulate_multiline_text($reason);
      $notification_array =  array();
      $notification_array['notification_type'] = "cancel";
      $notification_array['appointment_type'] = "schedule";
      $notification_array['message'] = $doctor_message;
      $notification_array['user_id'] = $record->d_id;
      $notification_array['p_id'] = $record->p_id;
      $notification_array['d_id'] = $record->d_id;
      
      $notification_array['appointment_id'] = $id;
      $this->appointment_model->insertNotification($notification_array);
    }
    elseif($result->role == 3)
    {    
      //send notification to patient 
      $doctor_message = 'Doctor ' . $doctor->fname. ' has cancelled following appointment with you Date : '. date('d M Y', strtotime($record->start_time)) . '&nbsp;&nbsp;&nbsp;Time : '. date('h:i a', strtotime($record->start_time));
      $doctor_message .= '<br />' . 'This is the reason given : <br />' . formulate_multiline_text($reason);
      $notification_array =  array();
      $notification_array['notification_type'] = "cancel";
      $notification_array['appointment_type'] = "schedule";
      $notification_array['message'] = $doctor_message;
      $notification_array['user_id'] = $record->p_id;
      $notification_array['p_id'] = $record->p_id;
      $notification_array['d_id'] = $record->d_id;
      
      $notification_array['appointment_id'] = $id;
      $this->appointment_model->insertNotification($notification_array);
    }   
        
    // Send Email to Doctor
    $this->appointment_model->sendCancelAppoinmentEmail($doctor->email, $doctor->fname, $patient->fname, $record->start_time, 'doctor', $result->role, $reason);
    
    // Send Email to Patient
    $this->appointment_model->sendCancelAppoinmentEmail($patient->email, $doctor->fname, $patient->fname, $record->start_time, 'patient', $result->role, $reason);

    $response_data['msg'] = 'Your Appointment has been cancelled successfully.'; 
    $code = '200';
    
    $this->response($response_data, $code);
  }

  function detail_get()
  {
    $id = $this->get('appointmentId');      
    $record = $this->appointment_model->getAppointment($id);

    if(!$record)
    {
      // error
      $code = '400';
      $data['msg'] = 'Appointment does not exist';
      $this->response($data, $code);
    }

    $data['patient'] = $this->appointment_model->getUserById($record->p_id);
    $data['record'] = $record;
    if($data['record']->latitude && $data['record']->longitude)
    {
      $url = 'https://maps.googleapis.com/maps/api/geocode/json?latlng=' . $data['record']->latitude . ',' . $data['record']->longitude . '&key=' . $this->config->item('map_key');
      $location = json_decode(file_get_contents($url));
      if(isset($location->results[0]->formatted_address))
          $data['record']->user_address = $location->results[0]->formatted_address;
    }

    $this->response($data, 200);
  }
    
    public function getcount_get()
    {
        $where = '';
        if($this->get('last_notification_id'))
        {
            $last_notification_id = $this->get('last_notification_id');
            $where = " AND notification_id > $last_notification_id";
        }    

        $result = $this->user_model->getSession($this->get('code'));
        if(!$result)
          $this->response('No such user', 400);

        $notificationshow = '';
        $query = $this->db->query("SELECT * FROM notification WHERE is_view = 0 AND user_id ='".$result->user_id."'$where ORDER BY notification_id DESC");
        $notificationdetail = $query->result_array();
        
        if($notificationdetail)
        {
           $notificationshow = count($notificationdetail);
        }

        $response = array();
        $response['notification_count'] = $notificationshow; 
        $response['last_notification_id'] = $this->get('last_notification_id');
        $response['detail'] = $notificationdetail;
        
        $this->response($response, 200);          
    }
    
    public function viewed_get()
    {
      $result = $this->user_model->getSession($this->get('code'));
      if($result)
      {    
          $data=array('is_view'=>1);
          $this->db->where('user_id',$result->user_id);
          $this->db->update('notification',$data);
      }     
      echo "ok";
      die;
    }

    function notify_expiry_of_now()
    {
        $userId = $this->session->userdata['logged_in']['id'];
        $result = $this->appointment_model->getAppointmentNow($userId);

        if($result)
        {        
          foreach ($result as $key => $value) 
          {
            //send notification to doctor 
            $doctor_message = 'Appointment now request sent to you by ' . $this->session->userdata['logged_in']['fname']. ' has expired.';
            $notification_array =  array();
            $notification_array['notification_type'] = "cancel";
            $notification_array['appointment_type'] = "now";
            $notification_array['message'] = $doctor_message;
            $notification_array['user_id'] = $value->d_id;
            $notification_array['p_id'] = $value->p_id;
            $notification_array['d_id'] = $value->d_id;
            
            $notification_array['appointment_id'] = $value->id;
            $this->appointment_model->insertNotification($notification_array);           
          }
        }

        redirect('appointment');
    }
}