<?php
class Doctorsession extends CI_Controller
{
  public function __construct() 
  {
        parent::__construct();
        $this->load->helper(array('form', 'url'));
        $this->load->library(array('session', 'form_validation', 'email', 'upload'));
        $this->load->database();
        $this->load->model('user_model');
        $this->load->helper('security');     
  }
    
  public function index()
  {
    echo "Hello, World" . PHP_EOL;
  }
 
  public function checkSession()
  {
    $role = isset($this->session->userdata['logged_in']['role']) ? $this->session->userdata['logged_in']['role'] : 0;
    if($role == 3)
    {
      $userId = $this->session->userdata['logged_in']['id'];
      $result = $this->user_model->readSessionInfo($userId);
      if($result)
      {
        $session = $result->session;
        $time = time();
        $time_check = $time-30; //SET TIME 20 Minute 
        $sessionData = array(
            'session' => $session,
            'time' => $time,
            'user_id' => $userId
        );
               
        $result2 = $this->user_model->updateDoctorSession($sessionData);
        if($result2 == FALSE)
        {
          $sess_array = array('email' => '');
          $this->session->unset_userdata('logged_in', $sess_array);
          redirect('home/logout');
        }
      }
      else
        redirect('home/logout');
    }
  }
}