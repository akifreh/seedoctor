<?php
class Firebase extends CI_Controller {
	function index($id = NULL) 
 	{
 		$data['username'] = $id ? 'Akif' : 'Waqar';
		$data['room_name'] = '162-28-Sep';
		$data['db_name'] = 'Chat-' . date('d-M-y');
		$data['mapKey'] = $this->config->item('map_key');
		$this->load->view('firebase_room', $data);	
	}

	function showChat($id = null)
	{
		$data['username'] = $id ? 'Akif' : 'Waqar';
		$this->load->view('firebase_room', $data);
	}

}