<div class="medical-pro-section">
<div class="container">   
<div class="row med-detail-wrapp">
<h3>Received Payments <?php echo $finished_total ? '(R '. $finished_total .')' : ''; ?></h3>
 <div class="medical-history-table table-design">
				<div class="table-header">
					<div class="table-head-col col-lg-2 col-md-2 col-sm-2 col-xs-2">Name</div>
            <div class="table-head-col col-lg-2 col-md-2 col-sm-2 col-xs-2">Date</div>
            <div class="table-head-col col-lg-2 col-md-2 col-sm-2 col-xs-2">Timing</div>
	          <div class="table-head-col col-lg-1 col-md-1 col-sm-1 col-xs-1">Duration</div>
            <div class="table-head-col col-lg-1 col-md-1 col-sm-1 col-xs-1">Charged</div>
            <div class="table-head-col col-lg-2 col-md-2 col-sm-2 col-xs-2">Amount</div>
            <div class="table-head-col col-lg-2 col-md-2 col-sm-2 col-xs-2">&nbsp; </div>
				</div>
                              
                              
				<?php if($finished_payments) : 
        for($i=0; $i<count($finished_payments); $i++) : ?>
				<div class="table-body" id="row ">
          <div class="table-col col-lg-2 col-md-2 col-sm-2 col-xs-2"><?php echo $finished_payments[$i]->patient_name;?></div>
          <div  class="table-col col-lg-2 col-md-2 col-sm-2 col-xs-2"><?php echo date('d M Y', strtotime($finished_payments[$i]->start_time)); ?></div>
           <div class="table-col col-lg-2 col-md-2 col-sm-2 col-xs-2"><?php echo date('h:i a', strtotime($finished_payments[$i]->start_time)) . ' - ' . date('h:i a', strtotime($finished_payments[$i]->end_time)); ?></div>
          <div  class="table-col col-lg-1 col-md-1 col-sm-1 col-xs-1"><?php echo $finished_payments[$i]->actual_duration ? date('i:s ' , strtotime($finished_payments[$i]->actual_duration)) : ''; ?></div>
          <div  class="table-col col-lg-1 col-md-1 col-sm-1 col-xs-1"><?php echo $finished_payments[$i]->actual_duration ? date('i:s ' , strtotime($finished_payments[$i]->charged_duration)) : ''; ?></div>
          <div  class="table-col col-lg-2 col-md-2 col-sm-2 col-xs-2"><?php echo $finished_payments[$i]->price; ?></div>
          <div  class="table-col col-lg-2 col-md-2 col-sm-2 col-xs-2"><a href="<?php echo base_url() . 'appointment/detail/'. $finished_payments[$i]->appointment_id; ?>">view details</a></div>        
      </div> 
    <?php endfor;
    endif; ?>
			</div>
  </div>
 
<div class="row med-detail-wrapp">
<h3>Pending Payments <?php echo $pending_total ? '(R '. $pending_total .')' : ''; ?></h3>
 <div class="medical-history-table table-design">
        <div class="table-header">
          <div class="table-head-col col-lg-2 col-md-2 col-sm-2 col-xs-2">Name</div>
            <div class="table-head-col col-lg-2 col-md-2 col-sm-2 col-xs-2">Date</div>
            <div class="table-head-col col-lg-2 col-md-2 col-sm-2 col-xs-2">Timing</div>
            <div class="table-head-col col-lg-1 col-md-1 col-sm-1 col-xs-1">Duration</div>
            <div class="table-head-col col-lg-1 col-md-1 col-sm-1 col-xs-1">Charged</div>
            <div class="table-head-col col-lg-2 col-md-2 col-sm-2 col-xs-2">Amount</div>
            <div class="table-head-col col-lg-2 col-md-2 col-sm-2 col-xs-2">&nbsp; </div>
        </div>
                              
                              
        <?php if($pending_payments) : 
        for($i=0; $i<count($pending_payments); $i++) : ?>
        <div class="table-body" id="row ">
          <div class="table-col col-lg-2 col-md-2 col-sm-2 col-xs-2"><?php echo $pending_payments[$i]->patient_name;?></div>
          <div  class="table-col col-lg-2 col-md-2 col-sm-2 col-xs-2"><?php echo date('d M Y', strtotime($pending_payments[$i]->start_time)); ?></div>
           <div class="table-col col-lg-2 col-md-2 col-sm-2 col-xs-2"><?php echo date('h:i a', strtotime($pending_payments[$i]->start_time)) . ' - ' . date('h:i a', strtotime($pending_payments[$i]->end_time)); ?></div>
          <div  class="table-col col-lg-1 col-md-1 col-sm-1 col-xs-1"><?php echo $pending_payments[$i]->actual_duration ? date('i:s ' , strtotime($pending_payments[$i]->actual_duration)) : ''; ?></div>
          <div  class="table-col col-lg-1 col-md-1 col-sm-1 col-xs-1"><?php echo $pending_payments[$i]->actual_duration ? date('i:s ' , strtotime($pending_payments[$i]->charged_duration)) : ''; ?></div>
          <div  class="table-col col-lg-2 col-md-2 col-sm-2 col-xs-2"><?php echo $pending_payments[$i]->price; ?></div>
          <div  class="table-col col-lg-2 col-md-2 col-sm-2 col-xs-2"><a href="<?php echo base_url() . 'appointment/detail/'. $pending_payments[$i]->appointment_id; ?>">view details</a></div>        
      </div> 
    <?php endfor;
    endif; ?>              
       </div>
  </div>
</div>
</div>
</div>