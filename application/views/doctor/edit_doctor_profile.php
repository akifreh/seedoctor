   <div class="form-register-doctor">
    <div class="container">
        <div class="col-lg-8 col-md-offset-2">
            <h1 style="margin-bottom:30px;">Edit Profile</h1>
<?php $attributes = array("name" => "doctorinfo" , "id" => "doctorinfo", "enctype" => 'multipart/form-data');
       echo form_open("doctordashboard/editProfile", $attributes);?>

<?php echo $this->session->flashdata('msg'); ?>
<?php echo $this->session->flashdata('fail'); ?>
    
    <div class="personal-detail">
        <div class="row">
        <div class="col-lg-3 col-md-3 col-sm-4 col-xs-4  padding-right-0">
                             <div class="user-pro-pic-main">
                      <img src="<?php echo isset($record->profile_pic) ? $record->profile_pic : ''; ?>" id="pro-pic-id" alt="Profile-Image"  >
                            <input type="file" name="profile_pic" id="add-pro-pic" class="add-profile-pic"> 
                            <span class="add-pro-pic-link"><i class="fa fa-camera" aria-hidden="true"></i> Add Photo</span>
                        
                         <!--<div class="prfoile-pic-update" style="display:none;" >Please Click Update Button to Change your Profile Picture</div>-->
                         </div>
                         </div>
                         <div class="col-lg-8 pull-right col-sm-12 col-xs-12">
                             <div class="row">
                              <div class="col-lg-12">
            <label for="inputEmail" class="  control-label">*Name </label>
            <div class=" ">
                <input disabled="disabled" name="fname" type="text" value="<?php echo isset($record->fname) ? $record->fname : ''; ?>" class="form-control" required>
            </div>
    </div>
    
    <div class="col-lg-12">
        <label for="inputEmail" class="  control-label">*Surname </label>
        <div class=" ">
            <input disabled="disabled" name="sname" type="text" value="<?php echo isset($record->sname) ? $record->sname : ''; ?>" class="form-control" required>
        </div>
    </div>
    
    <div class="col-lg-12">
    <label for="inputEmail" class="  control-label">*Email </label>
    <div class=" ">                   
        <input disabled="disabled" name="email" type="email" value="<?php echo isset($record->email) ? $record->email : ''; ?>" class="form-control" required>
    </div>
</div>
                                    </div>
                         </div>
                         </div>
                         <div class="clearfix"></div>
    <h3 style="border-bottom:#0484cf solid 2px; padding-bottom:5px;">Personal Detail</h3>
    <div class="row">
       
    <div class="col-lg-6">
    <label for="inputEmail" class="  control-label">*Phone </label>
    <div class=" ">                                        
        <input name="phone" type="number" value="<?php echo isset($record->phone) ? $record->phone : ''; ?>" class="form-control" required>
    </div>
</div>

<div class="col-lg-6">
    <label for="inputEmail" class="  control-label">*Address </label>
    <div class=" ">                                        
        <input type="text" name="address" class="form-control" value="<?php echo isset($record->address) ? $record->address : ''; ?>">
    </div>
</div>
    
<div class="col-lg-6">
    <label for="inputCity" class="  control-label">City </label>
    <div class=" ">                                        
        <input type="text" name="city" class="form-control" value="<?php echo isset($record->city) ? $record->city : ''; ?>">
    </div>
</div>
        
<div class="col-lg-6">
    <label for="inputProvince" class="  control-label">Province </label>
    <div class=" ">                                        
        <input type="text" name="province" class="form-control" value="<?php echo isset($record->province) ? $record->province : ''; ?>">
    </div>
</div>        
        
<div class="col-lg-12">
    <label for="inputEmail" class="  control-label">*Gender </label>
    <div class=" ">
        <input type="radio" name="gender" value="male" <?php echo isset($record->gender) && ($record->gender == 'male') ? 'checked' : ''; ?>> Male&nbsp;&nbsp;
        <input type="radio" name="gender" value="female" <?php echo isset($record->gender) && ($record->gender == 'female') ? 'checked' : ''; ?>> Female<br><br>
    </div>
</div>

</div>
</div>
 
             <!--<div class="form-group">
                    <label for="inputEmail" class="  control-label">Upload Profile Picture</label>
                    <div class=" ">
                                            
                                            <input name="profile_pic" type="file" value="" class="form-control">
                    </div>
                </div>-->

                <div class="form-group">
                    <label for="inputEmail" class="  control-label">Upload CV</label>
                    <div class=" ">
                                            
                    
                    <input name="cv" type="file" value="" id="cv" class="form-control">
                    </div>
                </div>

<div class="form-group">
    <label for="inputEmail" class="  control-label">*Upload Id Card</label>
    <div class=" ">                            
        <input name="id_card" type="file" value="" id="id_card" class="form-control">
    </div>
</div>


 
   <div class="personal-detail">
    <h3 style="border-bottom:#0484cf solid 2px; padding-bottom:5px;">Academic qualifications</h3>
    <div class="row main-qualification-section">
    <?php 
    
    
    if($record->academics) : 
    $count = 1;
    
      
        
      
    foreach($record->academics as $key => $row) :?>
    <input type="hidden" name="aqcount" id="aqcount" value="<?php echo count($record->academics) +1; ?>" /> 
    <div class="count-ac col-lg-12"><span><?php echo $count; $count++;?></span></div>
    <div class="acadmeic-quali-repeater" id="repeatID1">
        <div class="col-lg-4">
    
            <label for="inputEmail" class="  control-label"><?php echo ($key == 0) ? '*Qualification' : '*Qualification';?> </label>
            <div class=" ">
                <input name="qualification[]" value="<?php echo isset($row->qualification) ? $row->qualification : ''; ?>" class="form-control" required>
            </div>
 
        </div>
        <div class="col-lg-4">
    
            <label for="inputEmail" class="  control-label"><?php echo ($key == 0) ? '*Institution' : '*Institution ';?> </label>
            <div class=" ">
                <input name="institution[]" type="text" value="<?php echo isset($row->institution) ? $row->institution : ''; ?>" class="form-control" required>
            </div>
 
        </div>
        <div class="col-lg-4">
    
            <label for="inputEmail" class="  control-label"><?php echo ($key == 0) ? '*Year' : ' *Year';?> </label>
            <div class=" ">
                <input name="year[]" type="text" value="<?php echo isset($row->year) ? $row->year : ''; ?>" class="form-control" required>
            </div>
 
        </div>
         
        </div>
    <?php endforeach; ?>
    <?php else : ?>
    <input type="hidden" name="aqcount" id="aqcount" value="2" /> 
    <div class="count-ac col-lg-12"><span>1</span></div>
    <div class="acadmeic-quali-repeater" id="repeatID1">
        <div class="col-lg-4">
    
            <label for="inputEmail" class="  control-label">*Qualification </label>
            <div class=" ">
                <input name="qualification[]" value="<?php echo isset($row->qualification) ? $row->qualification : ''; ?>" class="form-control" required>
            </div>
 
        </div>
        <div class="col-lg-4">
    
            <label for="inputEmail" class="  control-label">*Institution </label>
            <div class=" ">
                <input name="institution[]" type="text" value="<?php echo isset($row->institution) ? $row->institution : ''; ?>" class="form-control" required>
            </div>
 
        </div>
        <div class="col-lg-4">
    
            <label for="inputEmail" class="  control-label">*Year  </label>
            <div class=" ">
                <input name="year[]" type="text" value="<?php echo isset($row->year) ? $row->year : ''; ?>" class="form-control" required>
            </div>
 
        </div>
         
        </div> 
    <?php endif; ?>

    <div id="aq-repeter"></div>
        <div class="add-academic col-lg-12"><a href="javascript:void(0)" class="add-more-academic-link submit-button pull-right">Add More Academic</a></div>
        
     </div>
    </div>
       <div class="personal-detail">
    <h3 style="border-bottom:#0484cf solid 2px; padding-bottom:5px;">Biography</h3>
    <div class="row">
  
        <div class="col-lg-12">
 
    <label for="inputEmail" class="  control-label">Description <span style="font-size:10px;">(Maximum 150 Words)</span> </label>
    <textarea class="form-control" id="content" name="description" ><?php echo isset($record->description) ? $record->description : ''; ?></textarea>
     
</div>


</div></div>


                <div class="personal-detail">
    <h3 style="border-bottom:#0484cf solid 2px; padding-bottom:5px;">Type</h3>
    <div class="row">
  <div class="col-lg-12">
    <label for="inputEmail" class="  control-label">*Area of training </label>
    <div class=" ">
        <input disabled="disabled" id="type-of-doctor-2" type="radio" name="type_of_doctor" value="2" <?php echo isset($record->type_of_doctor) && ($record->type_of_doctor == 'psychologist') ? 'checked' : '' ;?>> Psychologist &nbsp;&nbsp;
        <input disabled="disabled" id="type-of-doctor-1" type="radio" name="type_of_doctor" value="1" <?php echo isset($record->type_of_doctor) && ($record->type_of_doctor == 'dietitian') ? 'checked' : '' ;?>> Dietitian &nbsp;&nbsp;
        <input disabled="disabled" id="type-of-doctor-3" type="radio" name="type_of_doctor" value="3" <?php echo isset($record->type_of_doctor) && ($record->type_of_doctor == 'medical-doctor') ? 'checked' : '' ;?>> Medical Doctor &nbsp;&nbsp;
    </div>
</div>
  <div class="clearfix"></div>
<br /> 
      <div class="clearfix"></div>

<div class="col-lg-6" id="only-medical-doctor-1">
    <label for="inputEmail" class="  control-label">Profession Number </label>
    <div class=" ">                                        
        <input name="profession_number" type="text" value="<?php echo isset($record->profession_number) ? $record->profession_number : ''; ?>" class="form-control">
    </div>
</div>

<div class="col-lg-6" id="only-medical-doctor-2">
    <label for="inputEmail" class="  control-label">Practice Number </label>
    <div class=" ">                                        
        <input name="practice_number" type="text" value="<?php echo isset($record->practice_number) ? $record->practice_number : ''; ?>" class="form-control">
    </div>
</div>     

<div  class="col-lg-6">
<label for="inputEmail" class="  control-label"> Speciality </label>
    <div class=" ">
        <input name="speciality" type="text" value="<?php echo isset($record->speciality) ? $record->speciality : ''; ?>" class="form-control">
    </div>
    </div>
</div></div>

<div class="personal-detail">
    <h3 style="border-bottom:#0484cf solid 2px; padding-bottom:5px;">Working Experience</h3>
    <div class="row">
  <?php 
  
  //echo '<pre>'; print_r($record->experience); die;
  
  $result_for_exp_check_present = array();
  
    if($record->experience) :
        
        
        $condition = 'user_id='.$_SESSION['logged_in']['id'].' AND present_here = 1';
        $this->db->from('doctor_experience');
        $this->db->where($condition);
        $query = $this->db->get();
        
        $result_for_exp_check_present = $query->result();
        
        
    $count=1;
    $checkUniq = 1;
    $linkButtonStatus = '';
    foreach($record->experience as $key => $row) : ?>
    <input type="hidden" name="wecount" id="wecount" value="<?php echo count($record->experience) + 1;?>" /> 
    <div class="count-we col-lg-12"><span><?php echo $count; $count += 1;?></span></div>
  <div class="exp-repeat" id="repeatID2">
        <div class="col-lg-3">
 <label for="inputEmail" class="  control-label"><?php echo ($key == 0) ? '*Company' : '*Company';?> </label>
    <div class=" ">
        <input name="company[]" type="text" value="<?php echo isset($row->company) ? $row->company : ''; ?>" class="form-control" required>
    </div>
</div>
 <div class="col-lg-3">
 <label for="inputEmail" class="  control-label"><?php echo ($key == 0) ? '*Job Title' : '*Job Title';?> </label>
    <div class=" ">
        <input name="job_title[]" type="text" value="<?php echo isset($row->job_title) ? $row->job_title : ''; ?>" class="form-control" required>
    </div>
</div>
 <div class="col-lg-2 select-half-on">
 <label for="inputEmail" class="  control-label"><?php echo ($key == 0) ? '*Start Date' : '*Start Date ';?> </label>
    <div class=" ">
        <select name="start_mm[]" class="form-control" id="select">
            <option value="">Month</option>
            <option value="01" <?php echo isset($row->start_date) && date('m', strtotime($row->start_date)) == '01' ? 'selected' : ''; ?>>Jan</option>
            <option value="02" <?php echo isset($row->start_date) && date('m', strtotime($row->start_date)) == '02' ? 'selected' : ''; ?>>Feb</option>
            <option value="03" <?php echo isset($row->start_date) && date('m', strtotime($row->start_date)) == '03' ? 'selected' : ''; ?>>Mar</option>
            <option value="04" <?php echo isset($row->start_date) && date('m', strtotime($row->start_date)) == '04' ? 'selected' : ''; ?>>Apr</option>
            <option value="05" <?php echo isset($row->start_date) && date('m', strtotime($row->start_date)) == '05' ? 'selected' : ''; ?>>May</option>
            <option value="06" <?php echo isset($row->start_date) && date('m', strtotime($row->start_date)) == '06' ? 'selected' : ''; ?>>June</option>
            <option value="07" <?php echo isset($row->start_date) && date('m', strtotime($row->start_date)) == '07' ? 'selected' : ''; ?>>July</option>
            <option value="08" <?php echo isset($row->start_date) && date('m', strtotime($row->start_date)) == '08' ? 'selected' : ''; ?>>Aug</option>
            <option value="09" <?php echo isset($row->start_date) && date('m', strtotime($row->start_date)) == '09' ? 'selected' : ''; ?>>Sep</option>
            <option value="10" <?php echo isset($row->start_date) && date('m', strtotime($row->start_date)) == '10' ? 'selected' : ''; ?>>Oct</option>
            <option value="11" <?php echo isset($row->start_date) && date('m', strtotime($row->start_date)) == '11' ? 'selected' : ''; ?>>Nov</option>
            <option value="12" <?php echo isset($row->start_date) && date('m', strtotime($row->start_date)) == '12' ? 'selected' : ''; ?>>Dec</option>
        </select>
        <select name="start_yy[]" class="form-control" id="select">
            <option value="">Year</option>
            <?php for($i=date('Y')-80; $i<=date('Y'); $i++): ?>
                <option value="<?php echo $i; ?>" <?php echo isset($row->start_date) && date('Y', strtotime($row->start_date)) == $i ? 'selected' : ''; ?>><?php echo $i; ?></option>
            <?php endfor; ?>
        </select>
        
        <!--input name="start_date[]" type="text" value="?php echo isset($row->start_date) ? $row->start_date : ''; ?>" class="form-control" required-->
    </div>
</div>

<div class="col-lg-2 select-half-on" id="end_date_wrapper_<?php echo $checkUniq; ?>" style="display:<?php echo ($row->present_here == 1) ? 'none' : 'block'; ?>">
 <label for="inputEmail" class="  control-label"><?php echo ($key == 0) ? '*End Date' : '*End Date';?> </label>
    <div class=" " >
        <select name="end_mm[]" class="form-control" id="select">
            <option value="">Month</option>
            <option value="01" <?php echo isset($row->end_date) && date('m', strtotime($row->end_date)) == '01' ? 'selected' : ''; ?>>Jan</option>
            <option value="02" <?php echo isset($row->end_date) && date('m', strtotime($row->end_date)) == '02' ? 'selected' : ''; ?>>Feb</option>
            <option value="03" <?php echo isset($row->end_date) && date('m', strtotime($row->end_date)) == '03' ? 'selected' : ''; ?>>Mar</option>
            <option value="04" <?php echo isset($row->end_date) && date('m', strtotime($row->end_date)) == '04' ? 'selected' : ''; ?>>Apr</option>
            <option value="05" <?php echo isset($row->end_date) && date('m', strtotime($row->end_date)) == '05' ? 'selected' : ''; ?>>May</option>
            <option value="06" <?php echo isset($row->end_date) && date('m', strtotime($row->end_date)) == '06' ? 'selected' : ''; ?>>June</option>
            <option value="07" <?php echo isset($row->end_date) && date('m', strtotime($row->end_date)) == '07' ? 'selected' : ''; ?>>July</option>
            <option value="08" <?php echo isset($row->end_date) && date('m', strtotime($row->end_date)) == '08' ? 'selected' : ''; ?>>Aug</option>
            <option value="09" <?php echo isset($row->end_date) && date('m', strtotime($row->end_date)) == '09' ? 'selected' : ''; ?>>Sep</option>
            <option value="10" <?php echo isset($row->end_date) && date('m', strtotime($row->end_date)) == '10' ? 'selected' : ''; ?>>Oct</option>
            <option value="11" <?php echo isset($row->end_date) && date('m', strtotime($row->end_date)) == '11' ? 'selected' : ''; ?>>Nov</option>
            <option value="12" <?php echo isset($row->end_date) && date('m', strtotime($row->end_date)) == '12' ? 'selected' : ''; ?>>Dec</option>
        </select>
        <select name="end_yy[]" class="form-control" id="select">
            <option value="">Year</option>
            <?php for($i=date('Y')-80; $i<=date('Y'); $i++): ?>
                <option value="<?php echo $i; ?>" <?php echo isset($row->end_date) && date('Y', strtotime($row->end_date)) == $i ? 'selected' : ''; ?>><?php echo $i; ?></option>
            <?php endfor; ?>
        </select>
        <!--input name="end_date[]" type="text" value="?php echo isset($row->end_date) ? $row->end_date : ''; ?>" class="form-control" required-->
    </div>
</div>
      <?php  
      $allChekBoxStatus = '';
      if($result_for_exp_check_present)
      {
          if($row->present_here)
          {
              $allChekBoxStatus = "";
          }
          else
          {
              $allChekBoxStatus = "disabled='disabled'";
          }    
      }    
        //$allChekBoxStatus  = ($row->present_here == 1) ? 'show' : 'none';
        //$linkButtonStatus  = ($row->present_here == 1) ? 'none' : 'block';
      ?>
<div class="col-lg-2 select-half-on">
 <label for="inputEmail" class="  control-label"> Present </label>
    <div class=" "> 
            <input name="stay_here[]" type="checkbox" value="<?php echo $checkUniq; ?>" class="pull-left present_here" id="present_here_<?php echo $checkUniq; ?>" <?php echo ($row->present_here == 1) ? 'checked' : ''; ?>  
                data-indexof="<?php echo $checkUniq; ?>" onclick="present_here(this.id);" <?php echo $allChekBoxStatus;?> />
    </div>
 
</div>      
    <?php  
    
   
    
    $checkUniq++; //update counter
    //echo '<h1>'.$row->present_here.'</h1><br/>'; 
    ?>  
</div>
    
<?php endforeach; ?>
<?php else : ?>
    <input type="hidden" name="wecount" id="wecount" value="2" /> 
    <div class="count-we col-lg-12"><span>1</span></div>
    <div class="exp-repeat" id="repeatID2">
        <div class="col-lg-3">
 <label for="inputEmail" class="  control-label">*Company </label>
    <div class=" ">
        <input name="company[]" type="text" value="<?php echo isset($row->company) ? $row->company : ''; ?>" class="form-control" required>
    </div>
</div>
 <div class="col-lg-3">
 <label for="inputEmail" class="  control-label">*Job Title </label>
    <div class=" ">
        <input name="job_title[]" type="text" value="<?php echo isset($row->job_title) ? $row->job_title : ''; ?>" class="form-control" required>
    </div>
</div>
 <div class="col-lg-2 select-half-on">
 <label for="inputEmail" class="  control-label">*Start Date </label>
    <div class=" ">
        <select name="start_mm[]" class="form-control" id="select">
            <option value="">Month</option>
            <option value="01" <?php echo isset($row->start_date) && date('m', strtotime($row->start_date)) == '01' ? 'selected' : ''; ?>>Jan</option>
            <option value="02" <?php echo isset($row->start_date) && date('m', strtotime($row->start_date)) == '02' ? 'selected' : ''; ?>>Feb</option>
            <option value="03" <?php echo isset($row->start_date) && date('m', strtotime($row->start_date)) == '03' ? 'selected' : ''; ?>>Mar</option>
            <option value="04" <?php echo isset($row->start_date) && date('m', strtotime($row->start_date)) == '04' ? 'selected' : ''; ?>>Apr</option>
            <option value="05" <?php echo isset($row->start_date) && date('m', strtotime($row->start_date)) == '05' ? 'selected' : ''; ?>>May</option>
            <option value="06" <?php echo isset($row->start_date) && date('m', strtotime($row->start_date)) == '06' ? 'selected' : ''; ?>>June</option>
            <option value="07" <?php echo isset($row->start_date) && date('m', strtotime($row->start_date)) == '07' ? 'selected' : ''; ?>>July</option>
            <option value="08" <?php echo isset($row->start_date) && date('m', strtotime($row->start_date)) == '08' ? 'selected' : ''; ?>>Aug</option>
            <option value="09" <?php echo isset($row->start_date) && date('m', strtotime($row->start_date)) == '09' ? 'selected' : ''; ?>>Sep</option>
            <option value="10" <?php echo isset($row->start_date) && date('m', strtotime($row->start_date)) == '10' ? 'selected' : ''; ?>>Oct</option>
            <option value="11" <?php echo isset($row->start_date) && date('m', strtotime($row->start_date)) == '11' ? 'selected' : ''; ?>>Nov</option>
            <option value="12" <?php echo isset($row->start_date) && date('m', strtotime($row->start_date)) == '12' ? 'selected' : ''; ?>>Dec</option>
        </select>
        <select name="start_yy[]" class="form-control" id="select">
            <option value="">Year</option>
            <?php for($i=date('Y')-80; $i<=date('Y'); $i++): ?>
                <option value="<?php echo $i; ?>" <?php echo isset($row->start_date) && date('Y', strtotime($row->start_date)) == $i ? 'selected' : ''; ?>><?php echo $i; ?></option>
            <?php endfor; ?>
        </select>
        <!--input name="start_date[]" type="text" placeholder="DD-MM-YYYY" value="?php echo isset($row->start_date) ? $row->start_date : ''; ?>" class="form-control" required-->
    </div>
</div>

<div class="col-lg-2 select-half-on">
 <label for="inputEmail" class="  control-label">*End Date</label>
    <div class=" ">
        <select name="end_mm[]" class="form-control" id="select">
            <option value="">Month</option>
            <option value="01" <?php echo isset($row->end_date) && date('m', strtotime($row->end_date)) == '01' ? 'selected' : ''; ?>>Jan</option>
            <option value="02" <?php echo isset($row->end_date) && date('m', strtotime($row->end_date)) == '02' ? 'selected' : ''; ?>>Feb</option>
            <option value="03" <?php echo isset($row->end_date) && date('m', strtotime($row->end_date)) == '03' ? 'selected' : ''; ?>>Mar</option>
            <option value="04" <?php echo isset($row->end_date) && date('m', strtotime($row->end_date)) == '04' ? 'selected' : ''; ?>>Apr</option>
            <option value="05" <?php echo isset($row->end_date) && date('m', strtotime($row->end_date)) == '05' ? 'selected' : ''; ?>>May</option>
            <option value="06" <?php echo isset($row->end_date) && date('m', strtotime($row->end_date)) == '06' ? 'selected' : ''; ?>>June</option>
            <option value="07" <?php echo isset($row->end_date) && date('m', strtotime($row->end_date)) == '07' ? 'selected' : ''; ?>>July</option>
            <option value="08" <?php echo isset($row->end_date) && date('m', strtotime($row->end_date)) == '08' ? 'selected' : ''; ?>>Aug</option>
            <option value="09" <?php echo isset($row->end_date) && date('m', strtotime($row->end_date)) == '09' ? 'selected' : ''; ?>>Sep</option>
            <option value="10" <?php echo isset($row->end_date) && date('m', strtotime($row->end_date)) == '10' ? 'selected' : ''; ?>>Oct</option>
            <option value="11" <?php echo isset($row->end_date) && date('m', strtotime($row->end_date)) == '11' ? 'selected' : ''; ?>>Nov</option>
            <option value="12" <?php echo isset($row->end_date) && date('m', strtotime($row->end_date)) == '12' ? 'selected' : ''; ?>>Dec</option>
        </select>
        <select name="end_yy[]" class="form-control" id="select">
            <option value="">Year</option>
            <?php for($i=date('Y')-80; $i<=date('Y'); $i++): ?>
                <option value="<?php echo $i; ?>" <?php echo isset($row->end_date) && date('Y', strtotime($row->end_date)) == $i ? 'selected' : ''; ?>><?php echo $i; ?></option>
            <?php endfor; ?>
        </select>
        
        
        <!--input name="end_date[]" type="text" placeholder="DD-MM-YYYY" value="?php echo isset($row->end_date) ? $row->end_date : ''; ?>" class="form-control" required-->
    </div>
</div>
        
 <div class="col-lg-2 select-half-on">
    <label for="inputEmail" class="control-label"> Present here </label>
    <div class=" "> 
            <input 
                name="stay_here[]" 
                type="checkbox" 
                value="1" 
                class="present_here pull-left"
                id="present_here_1"   
                data-indexof="1" 
                onclick="present_here(this.id);" 
             />
    </div>
 </div> 
</div>
<?php endif; ?>

 
    
    
<div id="we-repeter"></div>
<div class="add-academic col-lg-12" ><a href="javascript:void(0)" class="add-more-exp-link submit-button pull-right" style="display:<?php echo $linkButtonStatus; ?>">Add More Experience</a></div>
</div></div> 
                             
<div class="personal-detail">
    <h3 style="border-bottom:#0484cf solid 2px; padding-bottom:5px;">Days & Timings</h3>
    
    <div class="row">
        <?php $hasTimings = FALSE; if(isset($doctorTimings[1])) $hasTimings = TRUE; ?>
        <div><input type="checkbox" name="dayoftheweek[]" value="1" <?php echo $hasTimings ? 'checked' : ''; ?>/><label>&nbsp;Monday</label></div>
        <div id="timings-1" style="<?php echo $hasTimings ? '' :  'display:none;'; ?>">
        <?php if(!$hasTimings) : ?>
            <?php $doctorTimings[1] = array('abc'); $value = NULL; ?>
        <?php endif; ?>
        <?php $count=0 ; foreach ($doctorTimings[1] as $key => $value) : ?>
        <div class="time-repeat" id="repeatID2">
            <?php $count++; ?>
            <?php if($count == 1) : ?>
            <div class="col-lg-6 select-half-on"></div>
            <?php else : ?>
            <div class="col-lg-2 select-half-on"></div>
            <?php endif; ?>                        
            <div class="col-lg-2 select-half-on">
                <label for="inputEmail" class="  control-label">*From </label>
                <div class=" ">
                <select name="from-timimg[1][]" class="form-control" id="select" style=" height: 2.2em !important;">
                    <option value="">-From-</option>
                    <option value="1 AM"<?php echo ($value && $value->start_time == '01:00:00') ? 'selected' : ''; ?>>1:00 AM</option>
                    <option value="2 AM"<?php echo ($value && $value->start_time == '02:00:00') ? 'selected' : ''; ?>>2:00 AM</option>
                    <option value="3 AM"<?php echo ($value && $value->start_time == '03:00:00') ? 'selected' : ''; ?>>3:00 AM</option>
                    <option value="4 AM"<?php echo ($value && $value->start_time == '04:00:00') ? 'selected' : ''; ?>>4:00 AM</option>
                    <option value="5 AM"<?php echo ($value && $value->start_time == '05:00:00') ? 'selected' : ''; ?>>5:00 AM</option>
                    <option value="6 AM"<?php echo ($value && $value->start_time == '06:00:00') ? 'selected' : ''; ?>>6:00 AM</option>
                    <option value="7 AM"<?php echo ($value && $value->start_time == '07:00:00') ? 'selected' : ''; ?>>7:00 AM</option>
                    <option value="8 AM"<?php echo ($value && $value->start_time == '08:00:00') ? 'selected' : ''; ?>>8:00 AM</option>
                    <option value="9 AM"<?php echo ($value && $value->start_time == '09:00:00') ? 'selected' : ''; ?>>9:00 AM</option>
                    <option value="10 AM"<?php echo ($value && $value->start_time == '10:00:00') ? 'selected' : ''; ?>>10:00 AM</option>
                    <option value="11 AM"<?php echo ($value && $value->start_time == '11:00:00') ? 'selected' : ''; ?>>11:00 AM</option>
                    <option value="12 PM"<?php echo ($value && $value->start_time == '12:00:00') ? 'selected' : ''; ?>>12:00 PM</option>
                    <option value="1 PM"<?php echo ($value && $value->start_time == '13:00:00') ? 'selected' : ''; ?>>1:00 PM</option>
                    <option value="2 PM"<?php echo ($value && $value->start_time == '14:00:00') ? 'selected' : ''; ?>>2:00 PM</option>
                    <option value="3 PM"<?php echo ($value && $value->start_time == '15:00:00') ? 'selected' : ''; ?>>3:00 PM</option>
                    <option value="4 PM"<?php echo ($value && $value->start_time == '16:00:00') ? 'selected' : ''; ?>>4:00 PM</option>
                    <option value="5 PM"<?php echo ($value && $value->start_time == '17:00:00') ? 'selected' : ''; ?>>5:00 PM</option>
                    <option value="6 PM"<?php echo ($value && $value->start_time == '18:00:00') ? 'selected' : ''; ?>>6:00 PM</option>
                    <option value="7 PM"<?php echo ($value && $value->start_time == '19:00:00') ? 'selected' : ''; ?>>7:00 PM</option>
                    <option value="8 PM"<?php echo ($value && $value->start_time == '20:00:00') ? 'selected' : ''; ?>>8:00 PM</option>
                    <option value="9 PM"<?php echo ($value && $value->start_time == '21:00:00') ? 'selected' : ''; ?>>9:00 PM</option>
                    <option value="10 PM"<?php echo ($value && $value->start_time == '22:00:00') ? 'selected' : ''; ?>>10:00 PM</option>
                    <option value="11 PM"<?php echo ($value && $value->start_time == '23:00:00') ? 'selected' : ''; ?>>11:00 PM</option>
                    <option value="12 AM"<?php echo ($value && ($value->start_time == '24:00:00' || $value->start_time=='00:00:00')) ? 'selected' : ''; ?>>12:00 PM</option>
                </select>
                </div>
            </div>

            <div class="col-lg-2 select-half-on" id="end_date_wrapper_1">
            <label for="inputEmail" class="  control-label">*To </label>
            <div class=" ">
                <select name="to-timing[1][]" class="form-control" id="select" style=" height: 2.2em !important;">
                    <option value="">-To-</option>
                    <option value="1 AM"<?php echo ($value && $value->end_time =='01:00:00') ? 'selected' : ''; ?>>1:00 AM</option>
                    <option value="2 AM"<?php echo ($value && $value->end_time =='02:00:00') ? 'selected' : ''; ?>>2:00 AM</option>
                    <option value="3 AM"<?php echo ($value && $value->end_time =='03:00:00') ? 'selected' : ''; ?>>3:00 AM</option>
                    <option value="4 AM"<?php echo ($value && $value->end_time =='04:00:00') ? 'selected' : ''; ?>>4:00 AM</option>
                    <option value="5 AM"<?php echo ($value && $value->end_time =='05:00:00') ? 'selected' : ''; ?>>5:00 AM</option>
                    <option value="6 AM"<?php echo ($value && $value->end_time =='06:00:00') ? 'selected' : ''; ?>>6:00 AM</option>
                    <option value="7 AM"<?php echo ($value && $value->end_time =='07:00:00') ? 'selected' : ''; ?>>7:00 AM</option>
                    <option value="8 AM"<?php echo ($value && $value->end_time =='08:00:00') ? 'selected' : ''; ?>>8:00 AM</option>
                    <option value="9 AM"<?php echo ($value && $value->end_time =='09:00:00') ? 'selected' : ''; ?>>9:00 AM</option>
                    <option value="10 AM"<?php echo ($value && $value->end_time =='10:00:00') ? 'selected' : ''; ?>>10:00 AM</option>
                    <option value="11 AM"<?php echo ($value && $value->end_time =='11:00:00') ? 'selected' : ''; ?>>11:00 AM</option>
                    <option value="12 PM"<?php echo ($value && $value->end_time =='12:00:00') ? 'selected' : ''; ?>>12:00 PM</option>
                    <option value="1 PM"<?php echo ($value && $value->end_time =='13:00:00') ? 'selected' : ''; ?>>1:00 PM</option>
                    <option value="2 PM"<?php echo ($value && $value->end_time =='14:00:00') ? 'selected' : ''; ?>>2:00 PM</option>
                    <option value="3 PM"<?php echo ($value && $value->end_time =='15:00:00') ? 'selected' : ''; ?>>3:00 PM</option>
                    <option value="4 PM"<?php echo ($value && $value->end_time =='16:00:00') ? 'selected' : ''; ?>>4:00 PM</option>
                    <option value="5 PM"<?php echo ($value && $value->end_time =='17:00:00') ? 'selected' : ''; ?>>5:00 PM</option>
                    <option value="6 PM"<?php echo ($value && $value->end_time =='18:00:00') ? 'selected' : ''; ?>>6:00 PM</option>
                    <option value="7 PM"<?php echo ($value && $value->end_time =='19:00:00') ? 'selected' : ''; ?>>7:00 PM</option>
                    <option value="8 PM"<?php echo ($value && $value->end_time =='20:00:00') ? 'selected' : ''; ?>>8:00 PM</option>
                    <option value="9 PM"<?php echo ($value && $value->end_time =='21:00:00') ? 'selected' : ''; ?>>9:00 PM</option>
                    <option value="10 PM"<?php echo ($value && $value->end_time =='22:00:00') ? 'selected' : ''; ?>>10:00 PM</option>
                    <option value="11 PM"<?php echo ($value && $value->end_time =='23:00:00') ? 'selected' : ''; ?>>11:00 PM</option>
                    <option value="12 AM"<?php echo ($value && ($value->end_time == '24:00:00' || $value->end_time=='00:00:00')) ? 'selected' : ''; ?>>12:00 PM</option>
                </select>
            </div>
            </div>
            <div class="col-lg-4 select-half-on" id="end_date_wrapper_1"></div>  
        </div>
    <?php endforeach; ?>
        <div id="time-repeter-1"></div>
        <div class="add-academic col-lg-12"><a href="javascript:void(0);" class="add-more-time-link submit-button pull-right" onClick="repeatTime(1);">Add More Time</a></div>
    </div>
    </div>

    <div class="row">
    <?php $hasTimings = FALSE; if(isset($doctorTimings[2])) $hasTimings = TRUE; ?>
        <div><input type="checkbox" name="dayoftheweek[]" value="2" <?php echo $hasTimings ? 'checked' : ''; ?>/><label>&nbsp;Tuesday</label></div>
        <div id="timings-2" style="<?php echo $hasTimings ? '' :  'display:none;'; ?>">
    <?php if(!$hasTimings) : ?>
        <?php $doctorTimings[2] = array('abc'); $value = NULL; ?>
        <?php endif; ?>
        <?php $count=0 ; foreach ($doctorTimings[2] as $key => $value) : ?>
        <div class="time-repeat" id="repeatID2">
            <?php $count++; ?>
            <?php if($count == 1) : ?>
            <div class="col-lg-6 select-half-on"></div>
            <?php else : ?>
            <div class="col-lg-2 select-half-on"></div>
            <?php endif; ?>                        
            <div class="col-lg-2 select-half-on">
                <label for="inputEmail" class="  control-label">*From </label>
                <div class=" ">
                <select name="from-timimg[2][]" class="form-control" id="select" style=" height: 2.2em !important;">
                    <option value="">-From-</option>
                    <option value="1 AM"<?php echo ($value && $value->start_time == '01:00:00') ? 'selected' : ''; ?>>1:00 AM</option>
                    <option value="2 AM"<?php echo ($value && $value->start_time == '02:00:00') ? 'selected' : ''; ?>>2:00 AM</option>
                    <option value="3 AM"<?php echo ($value && $value->start_time == '03:00:00') ? 'selected' : ''; ?>>3:00 AM</option>
                    <option value="4 AM"<?php echo ($value && $value->start_time == '04:00:00') ? 'selected' : ''; ?>>4:00 AM</option>
                    <option value="5 AM"<?php echo ($value && $value->start_time == '05:00:00') ? 'selected' : ''; ?>>5:00 AM</option>
                    <option value="6 AM"<?php echo ($value && $value->start_time == '06:00:00') ? 'selected' : ''; ?>>6:00 AM</option>
                    <option value="7 AM"<?php echo ($value && $value->start_time == '07:00:00') ? 'selected' : ''; ?>>7:00 AM</option>
                    <option value="8 AM"<?php echo ($value && $value->start_time == '08:00:00') ? 'selected' : ''; ?>>8:00 AM</option>
                    <option value="9 AM"<?php echo ($value && $value->start_time == '09:00:00') ? 'selected' : ''; ?>>9:00 AM</option>
                    <option value="10 AM"<?php echo ($value && $value->start_time == '10:00:00') ? 'selected' : ''; ?>>10:00 AM</option>
                    <option value="11 AM"<?php echo ($value && $value->start_time == '11:00:00') ? 'selected' : ''; ?>>11:00 AM</option>
                    <option value="12 PM"<?php echo ($value && $value->start_time == '12:00:00') ? 'selected' : ''; ?>>12:00 PM</option>
                    <option value="1 PM"<?php echo ($value && $value->start_time == '13:00:00') ? 'selected' : ''; ?>>1:00 PM</option>
                    <option value="2 PM"<?php echo ($value && $value->start_time == '14:00:00') ? 'selected' : ''; ?>>2:00 PM</option>
                    <option value="3 PM"<?php echo ($value && $value->start_time == '15:00:00') ? 'selected' : ''; ?>>3:00 PM</option>
                    <option value="4 PM"<?php echo ($value && $value->start_time == '16:00:00') ? 'selected' : ''; ?>>4:00 PM</option>
                    <option value="5 PM"<?php echo ($value && $value->start_time == '17:00:00') ? 'selected' : ''; ?>>5:00 PM</option>
                    <option value="6 PM"<?php echo ($value && $value->start_time == '18:00:00') ? 'selected' : ''; ?>>6:00 PM</option>
                    <option value="7 PM"<?php echo ($value && $value->start_time == '19:00:00') ? 'selected' : ''; ?>>7:00 PM</option>
                    <option value="8 PM"<?php echo ($value && $value->start_time == '20:00:00') ? 'selected' : ''; ?>>8:00 PM</option>
                    <option value="9 PM"<?php echo ($value && $value->start_time == '21:00:00') ? 'selected' : ''; ?>>9:00 PM</option>
                    <option value="10 PM"<?php echo ($value && $value->start_time == '22:00:00') ? 'selected' : ''; ?>>10:00 PM</option>
                    <option value="11 PM"<?php echo ($value && $value->start_time == '23:00:00') ? 'selected' : ''; ?>>11:00 PM</option>
                    <option value="12 AM"<?php echo ($value && ($value->start_time == '24:00:00' || $value->start_time=='00:00:00')) ? 'selected' : ''; ?>>12:00 PM</option>
                </select>
                </div>
            </div>

            <div class="col-lg-2 select-half-on" id="end_date_wrapper_1">
            <label for="inputEmail" class="  control-label">*To </label>
            <div class=" ">
                <select name="to-timing[2][]" class="form-control" id="select" style=" height: 2.2em !important;">
                    <option value="">-To-</option>
                    <option value="1 AM"<?php echo ($value && $value->end_time =='01:00:00') ? 'selected' : ''; ?>>1:00 AM</option>
                    <option value="2 AM"<?php echo ($value && $value->end_time =='02:00:00') ? 'selected' : ''; ?>>2:00 AM</option>
                    <option value="3 AM"<?php echo ($value && $value->end_time =='03:00:00') ? 'selected' : ''; ?>>3:00 AM</option>
                    <option value="4 AM"<?php echo ($value && $value->end_time =='04:00:00') ? 'selected' : ''; ?>>4:00 AM</option>
                    <option value="5 AM"<?php echo ($value && $value->end_time =='05:00:00') ? 'selected' : ''; ?>>5:00 AM</option>
                    <option value="6 AM"<?php echo ($value && $value->end_time =='06:00:00') ? 'selected' : ''; ?>>6:00 AM</option>
                    <option value="7 AM"<?php echo ($value && $value->end_time =='07:00:00') ? 'selected' : ''; ?>>7:00 AM</option>
                    <option value="8 AM"<?php echo ($value && $value->end_time =='08:00:00') ? 'selected' : ''; ?>>8:00 AM</option>
                    <option value="9 AM"<?php echo ($value && $value->end_time =='09:00:00') ? 'selected' : ''; ?>>9:00 AM</option>
                    <option value="10 AM"<?php echo ($value && $value->end_time =='10:00:00') ? 'selected' : ''; ?>>10:00 AM</option>
                    <option value="11 AM"<?php echo ($value && $value->end_time =='11:00:00') ? 'selected' : ''; ?>>11:00 AM</option>
                    <option value="12 PM"<?php echo ($value && $value->end_time =='12:00:00') ? 'selected' : ''; ?>>12:00 PM</option>
                    <option value="1 PM"<?php echo ($value && $value->end_time =='13:00:00') ? 'selected' : ''; ?>>1:00 PM</option>
                    <option value="2 PM"<?php echo ($value && $value->end_time =='14:00:00') ? 'selected' : ''; ?>>2:00 PM</option>
                    <option value="3 PM"<?php echo ($value && $value->end_time =='15:00:00') ? 'selected' : ''; ?>>3:00 PM</option>
                    <option value="4 PM"<?php echo ($value && $value->end_time =='16:00:00') ? 'selected' : ''; ?>>4:00 PM</option>
                    <option value="5 PM"<?php echo ($value && $value->end_time =='17:00:00') ? 'selected' : ''; ?>>5:00 PM</option>
                    <option value="6 PM"<?php echo ($value && $value->end_time =='18:00:00') ? 'selected' : ''; ?>>6:00 PM</option>
                    <option value="7 PM"<?php echo ($value && $value->end_time =='19:00:00') ? 'selected' : ''; ?>>7:00 PM</option>
                    <option value="8 PM"<?php echo ($value && $value->end_time =='20:00:00') ? 'selected' : ''; ?>>8:00 PM</option>
                    <option value="9 PM"<?php echo ($value && $value->end_time =='21:00:00') ? 'selected' : ''; ?>>9:00 PM</option>
                    <option value="10 PM"<?php echo ($value && $value->end_time =='22:00:00') ? 'selected' : ''; ?>>10:00 PM</option>
                    <option value="11 PM"<?php echo ($value && $value->end_time =='23:00:00') ? 'selected' : ''; ?>>11:00 PM</option>
                    <option value="12 AM"<?php echo ($value && ($value->end_time == '24:00:00' || $value->end_time=='00:00:00')) ? 'selected' : ''; ?>>12:00 PM</option>
                </select>
            </div>
            </div>
            <div class="col-lg-4 select-half-on" id="end_date_wrapper_1"></div>  
        </div>
        <?php endforeach; ?>
        <div id="time-repeter-2"></div>
        <div class="add-academic col-lg-12"><a href="javascript:void(0);" class="add-more-time-link submit-button pull-right" onClick="repeatTime(2);">Add More Time</a></div>
    </div>
    </div>

    <div class="row">
        <?php $hasTimings = FALSE; if(isset($doctorTimings[3])) $hasTimings = TRUE; ?>
        <div><input type="checkbox" name="dayoftheweek[]" value="3" <?php echo $hasTimings ? 'checked' : ''; ?>/><label>&nbsp;Wednesday</label></div>
        <div id="timings-3" style="<?php echo $hasTimings ? '' :  'display:none;'; ?>">
        <?php if(!$hasTimings) : ?>
            <?php $doctorTimings[3] = array('abc'); $value = NULL; ?>
        <?php endif; ?>
        <?php $count=0 ; foreach ($doctorTimings[3] as $key => $value) : ?>
        <div class="time-repeat" id="repeatID2">
            <?php $count++; ?>
            <?php if($count == 1) : ?>
            <div class="col-lg-6 select-half-on"></div>
            <?php else : ?>
            <div class="col-lg-2 select-half-on"></div>
            <?php endif; ?>                        
            <div class="col-lg-2 select-half-on">
                <label for="inputEmail" class="  control-label">*From </label>
                <div class=" ">
                <select name="from-timimg[3][]" class="form-control" id="select" style=" height: 2.2em !important;">
                    <option value="">-From-</option>
                    <option value="1 AM"<?php echo ($value && $value->start_time == '01:00:00') ? 'selected' : ''; ?>>1:00 AM</option>
                    <option value="2 AM"<?php echo ($value && $value->start_time == '02:00:00') ? 'selected' : ''; ?>>2:00 AM</option>
                    <option value="3 AM"<?php echo ($value && $value->start_time == '03:00:00') ? 'selected' : ''; ?>>3:00 AM</option>
                    <option value="4 AM"<?php echo ($value && $value->start_time == '04:00:00') ? 'selected' : ''; ?>>4:00 AM</option>
                    <option value="5 AM"<?php echo ($value && $value->start_time == '05:00:00') ? 'selected' : ''; ?>>5:00 AM</option>
                    <option value="6 AM"<?php echo ($value && $value->start_time == '06:00:00') ? 'selected' : ''; ?>>6:00 AM</option>
                    <option value="7 AM"<?php echo ($value && $value->start_time == '07:00:00') ? 'selected' : ''; ?>>7:00 AM</option>
                    <option value="8 AM"<?php echo ($value && $value->start_time == '08:00:00') ? 'selected' : ''; ?>>8:00 AM</option>
                    <option value="9 AM"<?php echo ($value && $value->start_time == '09:00:00') ? 'selected' : ''; ?>>9:00 AM</option>
                    <option value="10 AM"<?php echo ($value && $value->start_time == '10:00:00') ? 'selected' : ''; ?>>10:00 AM</option>
                    <option value="11 AM"<?php echo ($value && $value->start_time == '11:00:00') ? 'selected' : ''; ?>>11:00 AM</option>
                    <option value="12 PM"<?php echo ($value && $value->start_time == '12:00:00') ? 'selected' : ''; ?>>12:00 PM</option>
                    <option value="1 PM"<?php echo ($value && $value->start_time == '13:00:00') ? 'selected' : ''; ?>>1:00 PM</option>
                    <option value="2 PM"<?php echo ($value && $value->start_time == '14:00:00') ? 'selected' : ''; ?>>2:00 PM</option>
                    <option value="3 PM"<?php echo ($value && $value->start_time == '15:00:00') ? 'selected' : ''; ?>>3:00 PM</option>
                    <option value="4 PM"<?php echo ($value && $value->start_time == '16:00:00') ? 'selected' : ''; ?>>4:00 PM</option>
                    <option value="5 PM"<?php echo ($value && $value->start_time == '17:00:00') ? 'selected' : ''; ?>>5:00 PM</option>
                    <option value="6 PM"<?php echo ($value && $value->start_time == '18:00:00') ? 'selected' : ''; ?>>6:00 PM</option>
                    <option value="7 PM"<?php echo ($value && $value->start_time == '19:00:00') ? 'selected' : ''; ?>>7:00 PM</option>
                    <option value="8 PM"<?php echo ($value && $value->start_time == '20:00:00') ? 'selected' : ''; ?>>8:00 PM</option>
                    <option value="9 PM"<?php echo ($value && $value->start_time == '21:00:00') ? 'selected' : ''; ?>>9:00 PM</option>
                    <option value="10 PM"<?php echo ($value && $value->start_time == '22:00:00') ? 'selected' : ''; ?>>10:00 PM</option>
                    <option value="11 PM"<?php echo ($value && $value->start_time == '23:00:00') ? 'selected' : ''; ?>>11:00 PM</option>
                    <option value="12 AM"<?php echo ($value && ($value->start_time == '24:00:00' || $value->start_time=='00:00:00')) ? 'selected' : ''; ?>>12:00 PM</option>
                </select>
                </div>
            </div>

            <div class="col-lg-2 select-half-on" id="end_date_wrapper_1">
            <label for="inputEmail" class="  control-label">*To </label>
            <div class=" ">
                <select name="to-timing[3][]" class="form-control" id="select" style=" height: 2.2em !important;">
                    <option value="">-To-</option>
                    <option value="1 AM"<?php echo ($value && $value->end_time =='01:00:00') ? 'selected' : ''; ?>>1:00 AM</option>
                    <option value="2 AM"<?php echo ($value && $value->end_time =='02:00:00') ? 'selected' : ''; ?>>2:00 AM</option>
                    <option value="3 AM"<?php echo ($value && $value->end_time =='03:00:00') ? 'selected' : ''; ?>>3:00 AM</option>
                    <option value="4 AM"<?php echo ($value && $value->end_time =='04:00:00') ? 'selected' : ''; ?>>4:00 AM</option>
                    <option value="5 AM"<?php echo ($value && $value->end_time =='05:00:00') ? 'selected' : ''; ?>>5:00 AM</option>
                    <option value="6 AM"<?php echo ($value && $value->end_time =='06:00:00') ? 'selected' : ''; ?>>6:00 AM</option>
                    <option value="7 AM"<?php echo ($value && $value->end_time =='07:00:00') ? 'selected' : ''; ?>>7:00 AM</option>
                    <option value="8 AM"<?php echo ($value && $value->end_time =='08:00:00') ? 'selected' : ''; ?>>8:00 AM</option>
                    <option value="9 AM"<?php echo ($value && $value->end_time =='09:00:00') ? 'selected' : ''; ?>>9:00 AM</option>
                    <option value="10 AM"<?php echo ($value && $value->end_time =='10:00:00') ? 'selected' : ''; ?>>10:00 AM</option>
                    <option value="11 AM"<?php echo ($value && $value->end_time =='11:00:00') ? 'selected' : ''; ?>>11:00 AM</option>
                    <option value="12 PM"<?php echo ($value && $value->end_time =='12:00:00') ? 'selected' : ''; ?>>12:00 PM</option>
                    <option value="1 PM"<?php echo ($value && $value->end_time =='13:00:00') ? 'selected' : ''; ?>>1:00 PM</option>
                    <option value="2 PM"<?php echo ($value && $value->end_time =='14:00:00') ? 'selected' : ''; ?>>2:00 PM</option>
                    <option value="3 PM"<?php echo ($value && $value->end_time =='15:00:00') ? 'selected' : ''; ?>>3:00 PM</option>
                    <option value="4 PM"<?php echo ($value && $value->end_time =='16:00:00') ? 'selected' : ''; ?>>4:00 PM</option>
                    <option value="5 PM"<?php echo ($value && $value->end_time =='17:00:00') ? 'selected' : ''; ?>>5:00 PM</option>
                    <option value="6 PM"<?php echo ($value && $value->end_time =='18:00:00') ? 'selected' : ''; ?>>6:00 PM</option>
                    <option value="7 PM"<?php echo ($value && $value->end_time =='19:00:00') ? 'selected' : ''; ?>>7:00 PM</option>
                    <option value="8 PM"<?php echo ($value && $value->end_time =='20:00:00') ? 'selected' : ''; ?>>8:00 PM</option>
                    <option value="9 PM"<?php echo ($value && $value->end_time =='21:00:00') ? 'selected' : ''; ?>>9:00 PM</option>
                    <option value="10 PM"<?php echo ($value && $value->end_time =='22:00:00') ? 'selected' : ''; ?>>10:00 PM</option>
                    <option value="11 PM"<?php echo ($value && $value->end_time =='23:00:00') ? 'selected' : ''; ?>>11:00 PM</option>
                    <option value="12 AM"<?php echo ($value && ($value->end_time == '24:00:00' || $value->end_time=='00:00:00')) ? 'selected' : ''; ?>>12:00 PM</option>
                </select>
            </div>
            </div>
            <div class="col-lg-4 select-half-on" id="end_date_wrapper_1"></div>  
        </div>
        <?php endforeach; ?>
        <div id="time-repeter-3"></div>
        <div class="add-academic col-lg-12"><a href="javascript:void(0);" class="add-more-time-link submit-button pull-right" onClick="repeatTime(3);">Add More Time</a></div>
    </div>
    </div>

            <div class="row">
        <?php $hasTimings = FALSE; if(isset($doctorTimings[4])) $hasTimings = TRUE; ?>
        <div><input type="checkbox" name="dayoftheweek[]" value="4" <?php echo $hasTimings ? 'checked' : ''; ?>/><label>&nbsp;Thursday</label></div>
        <div id="timings-4" style="<?php echo $hasTimings ? '' :  'display:none;'; ?>">
        <?php if(!$hasTimings) : ?>
            <?php $doctorTimings[4] = array('abc'); $value = NULL; ?>
        <?php endif; ?>
        <?php $count=0 ; foreach ($doctorTimings[4] as $key => $value) : ?>
        <div class="time-repeat" id="repeatID2">
            <?php $count++; ?>
            <?php if($count == 1) : ?>
            <div class="col-lg-6 select-half-on"></div>
            <?php else : ?>
            <div class="col-lg-2 select-half-on"></div>
            <?php endif; ?>                        
            <div class="col-lg-2 select-half-on">
                <label for="inputEmail" class="  control-label">*From </label>
                <div class=" ">
                <select name="from-timimg[4][]" class="form-control" id="select" style=" height: 2.2em !important;">
                    <option value="">-From-</option>
                    <option value="1 AM"<?php echo ($value && $value->start_time == '01:00:00') ? 'selected' : ''; ?>>1:00 AM</option>
                    <option value="2 AM"<?php echo ($value && $value->start_time == '02:00:00') ? 'selected' : ''; ?>>2:00 AM</option>
                    <option value="3 AM"<?php echo ($value && $value->start_time == '03:00:00') ? 'selected' : ''; ?>>3:00 AM</option>
                    <option value="4 AM"<?php echo ($value && $value->start_time == '04:00:00') ? 'selected' : ''; ?>>4:00 AM</option>
                    <option value="5 AM"<?php echo ($value && $value->start_time == '05:00:00') ? 'selected' : ''; ?>>5:00 AM</option>
                    <option value="6 AM"<?php echo ($value && $value->start_time == '06:00:00') ? 'selected' : ''; ?>>6:00 AM</option>
                    <option value="7 AM"<?php echo ($value && $value->start_time == '07:00:00') ? 'selected' : ''; ?>>7:00 AM</option>
                    <option value="8 AM"<?php echo ($value && $value->start_time == '08:00:00') ? 'selected' : ''; ?>>8:00 AM</option>
                    <option value="9 AM"<?php echo ($value && $value->start_time == '09:00:00') ? 'selected' : ''; ?>>9:00 AM</option>
                    <option value="10 AM"<?php echo ($value && $value->start_time == '10:00:00') ? 'selected' : ''; ?>>10:00 AM</option>
                    <option value="11 AM"<?php echo ($value && $value->start_time == '11:00:00') ? 'selected' : ''; ?>>11:00 AM</option>
                    <option value="12 PM"<?php echo ($value && $value->start_time == '12:00:00') ? 'selected' : ''; ?>>12:00 PM</option>
                    <option value="1 PM"<?php echo ($value && $value->start_time == '13:00:00') ? 'selected' : ''; ?>>1:00 PM</option>
                    <option value="2 PM"<?php echo ($value && $value->start_time == '14:00:00') ? 'selected' : ''; ?>>2:00 PM</option>
                    <option value="3 PM"<?php echo ($value && $value->start_time == '15:00:00') ? 'selected' : ''; ?>>3:00 PM</option>
                    <option value="4 PM"<?php echo ($value && $value->start_time == '16:00:00') ? 'selected' : ''; ?>>4:00 PM</option>
                    <option value="5 PM"<?php echo ($value && $value->start_time == '17:00:00') ? 'selected' : ''; ?>>5:00 PM</option>
                    <option value="6 PM"<?php echo ($value && $value->start_time == '18:00:00') ? 'selected' : ''; ?>>6:00 PM</option>
                    <option value="7 PM"<?php echo ($value && $value->start_time == '19:00:00') ? 'selected' : ''; ?>>7:00 PM</option>
                    <option value="8 PM"<?php echo ($value && $value->start_time == '20:00:00') ? 'selected' : ''; ?>>8:00 PM</option>
                    <option value="9 PM"<?php echo ($value && $value->start_time == '21:00:00') ? 'selected' : ''; ?>>9:00 PM</option>
                    <option value="10 PM"<?php echo ($value && $value->start_time == '22:00:00') ? 'selected' : ''; ?>>10:00 PM</option>
                    <option value="11 PM"<?php echo ($value && $value->start_time == '23:00:00') ? 'selected' : ''; ?>>11:00 PM</option>
                    <option value="12 AM"<?php echo ($value && ($value->start_time == '24:00:00' || $value->start_time=='00:00:00')) ? 'selected' : ''; ?>>12:00 PM</option>
                </select>
                </div>
            </div>

            <div class="col-lg-2 select-half-on" id="end_date_wrapper_1">
            <label for="inputEmail" class="  control-label">*To </label>
            <div class=" ">
                <select name="to-timing[4][]" class="form-control" id="select" style=" height: 2.2em !important;">
                    <option value="">-To-</option>
                    <option value="1 AM"<?php echo ($value && $value->end_time =='01:00:00') ? 'selected' : ''; ?>>1:00 AM</option>
                    <option value="2 AM"<?php echo ($value && $value->end_time =='02:00:00') ? 'selected' : ''; ?>>2:00 AM</option>
                    <option value="3 AM"<?php echo ($value && $value->end_time =='03:00:00') ? 'selected' : ''; ?>>3:00 AM</option>
                    <option value="4 AM"<?php echo ($value && $value->end_time =='04:00:00') ? 'selected' : ''; ?>>4:00 AM</option>
                    <option value="5 AM"<?php echo ($value && $value->end_time =='05:00:00') ? 'selected' : ''; ?>>5:00 AM</option>
                    <option value="6 AM"<?php echo ($value && $value->end_time =='06:00:00') ? 'selected' : ''; ?>>6:00 AM</option>
                    <option value="7 AM"<?php echo ($value && $value->end_time =='07:00:00') ? 'selected' : ''; ?>>7:00 AM</option>
                    <option value="8 AM"<?php echo ($value && $value->end_time =='08:00:00') ? 'selected' : ''; ?>>8:00 AM</option>
                    <option value="9 AM"<?php echo ($value && $value->end_time =='09:00:00') ? 'selected' : ''; ?>>9:00 AM</option>
                    <option value="10 AM"<?php echo ($value && $value->end_time =='10:00:00') ? 'selected' : ''; ?>>10:00 AM</option>
                    <option value="11 AM"<?php echo ($value && $value->end_time =='11:00:00') ? 'selected' : ''; ?>>11:00 AM</option>
                    <option value="12 PM"<?php echo ($value && $value->end_time =='12:00:00') ? 'selected' : ''; ?>>12:00 PM</option>
                    <option value="1 PM"<?php echo ($value && $value->end_time =='13:00:00') ? 'selected' : ''; ?>>1:00 PM</option>
                    <option value="2 PM"<?php echo ($value && $value->end_time =='14:00:00') ? 'selected' : ''; ?>>2:00 PM</option>
                    <option value="3 PM"<?php echo ($value && $value->end_time =='15:00:00') ? 'selected' : ''; ?>>3:00 PM</option>
                    <option value="4 PM"<?php echo ($value && $value->end_time =='16:00:00') ? 'selected' : ''; ?>>4:00 PM</option>
                    <option value="5 PM"<?php echo ($value && $value->end_time =='17:00:00') ? 'selected' : ''; ?>>5:00 PM</option>
                    <option value="6 PM"<?php echo ($value && $value->end_time =='18:00:00') ? 'selected' : ''; ?>>6:00 PM</option>
                    <option value="7 PM"<?php echo ($value && $value->end_time =='19:00:00') ? 'selected' : ''; ?>>7:00 PM</option>
                    <option value="8 PM"<?php echo ($value && $value->end_time =='20:00:00') ? 'selected' : ''; ?>>8:00 PM</option>
                    <option value="9 PM"<?php echo ($value && $value->end_time =='21:00:00') ? 'selected' : ''; ?>>9:00 PM</option>
                    <option value="10 PM"<?php echo ($value && $value->end_time =='22:00:00') ? 'selected' : ''; ?>>10:00 PM</option>
                    <option value="11 PM"<?php echo ($value && $value->end_time =='23:00:00') ? 'selected' : ''; ?>>11:00 PM</option>
                    <option value="12 AM"<?php echo ($value && ($value->end_time == '24:00:00' || $value->end_time=='00:00:00')) ? 'selected' : ''; ?>>12:00 PM</option>
                </select>
            </div>
            </div>
            <div class="col-lg-4 select-half-on" id="end_date_wrapper_1"></div>  
        </div>
        <?php endforeach; ?>
        <div id="time-repeter-4"></div>
        <div class="add-academic col-lg-12"><a href="javascript:void(0);" class="add-more-time-link submit-button pull-right" onClick="repeatTime(4);">Add More Time</a></div>
    </div>
    </div>

            <div class="row">
        <?php $hasTimings = FALSE; if(isset($doctorTimings[5])) $hasTimings = TRUE; ?>
        <div><input type="checkbox" name="dayoftheweek[]" value="5" <?php echo $hasTimings ? 'checked' : ''; ?>/><label>&nbsp;Friday</label></div>
        <div id="timings-5" style="<?php echo $hasTimings ? '' :  'display:none;'; ?>">
        <?php if(!$hasTimings) : ?>
            <?php $doctorTimings[5] = array('abc'); $value = NULL; ?>
        <?php endif; ?>
        <?php $count=0 ; foreach ($doctorTimings[5] as $key => $value) : ?>
        <div class="time-repeat" id="repeatID2">
            <?php $count++; ?>
            <?php if($count == 1) : ?>
            <div class="col-lg-6 select-half-on"></div>
            <?php else : ?>
            <div class="col-lg-2 select-half-on"></div>
            <?php endif; ?>                        
            <div class="col-lg-2 select-half-on">
                <label for="inputEmail" class="  control-label">*From </label>
                <div class=" ">
                <select name="from-timimg[5][]" class="form-control" id="select" style=" height: 2.2em !important;">
                    <option value="">-From-</option>
                    <option value="1 AM"<?php echo ($value && $value->start_time == '01:00:00') ? 'selected' : ''; ?>>1:00 AM</option>
                    <option value="2 AM"<?php echo ($value && $value->start_time == '02:00:00') ? 'selected' : ''; ?>>2:00 AM</option>
                    <option value="3 AM"<?php echo ($value && $value->start_time == '03:00:00') ? 'selected' : ''; ?>>3:00 AM</option>
                    <option value="4 AM"<?php echo ($value && $value->start_time == '04:00:00') ? 'selected' : ''; ?>>4:00 AM</option>
                    <option value="5 AM"<?php echo ($value && $value->start_time == '05:00:00') ? 'selected' : ''; ?>>5:00 AM</option>
                    <option value="6 AM"<?php echo ($value && $value->start_time == '06:00:00') ? 'selected' : ''; ?>>6:00 AM</option>
                    <option value="7 AM"<?php echo ($value && $value->start_time == '07:00:00') ? 'selected' : ''; ?>>7:00 AM</option>
                    <option value="8 AM"<?php echo ($value && $value->start_time == '08:00:00') ? 'selected' : ''; ?>>8:00 AM</option>
                    <option value="9 AM"<?php echo ($value && $value->start_time == '09:00:00') ? 'selected' : ''; ?>>9:00 AM</option>
                    <option value="10 AM"<?php echo ($value && $value->start_time == '10:00:00') ? 'selected' : ''; ?>>10:00 AM</option>
                    <option value="11 AM"<?php echo ($value && $value->start_time == '11:00:00') ? 'selected' : ''; ?>>11:00 AM</option>
                    <option value="12 PM"<?php echo ($value && $value->start_time == '12:00:00') ? 'selected' : ''; ?>>12:00 PM</option>
                    <option value="1 PM"<?php echo ($value && $value->start_time == '13:00:00') ? 'selected' : ''; ?>>1:00 PM</option>
                    <option value="2 PM"<?php echo ($value && $value->start_time == '14:00:00') ? 'selected' : ''; ?>>2:00 PM</option>
                    <option value="3 PM"<?php echo ($value && $value->start_time == '15:00:00') ? 'selected' : ''; ?>>3:00 PM</option>
                    <option value="4 PM"<?php echo ($value && $value->start_time == '16:00:00') ? 'selected' : ''; ?>>4:00 PM</option>
                    <option value="5 PM"<?php echo ($value && $value->start_time == '17:00:00') ? 'selected' : ''; ?>>5:00 PM</option>
                    <option value="6 PM"<?php echo ($value && $value->start_time == '18:00:00') ? 'selected' : ''; ?>>6:00 PM</option>
                    <option value="7 PM"<?php echo ($value && $value->start_time == '19:00:00') ? 'selected' : ''; ?>>7:00 PM</option>
                    <option value="8 PM"<?php echo ($value && $value->start_time == '20:00:00') ? 'selected' : ''; ?>>8:00 PM</option>
                    <option value="9 PM"<?php echo ($value && $value->start_time == '21:00:00') ? 'selected' : ''; ?>>9:00 PM</option>
                    <option value="10 PM"<?php echo ($value && $value->start_time == '22:00:00') ? 'selected' : ''; ?>>10:00 PM</option>
                    <option value="11 PM"<?php echo ($value && $value->start_time == '23:00:00') ? 'selected' : ''; ?>>11:00 PM</option>
                    <option value="12 AM"<?php echo ($value && ($value->start_time == '24:00:00' || $value->start_time=='00:00:00')) ? 'selected' : ''; ?>>12:00 PM</option>
                </select>
                </div>
            </div>

            <div class="col-lg-2 select-half-on" id="end_date_wrapper_1">
            <label for="inputEmail" class="  control-label">*To </label>
            <div class=" ">
                <select name="to-timing[5][]" class="form-control" id="select" style=" height: 2.2em !important;">
                    <option value="">-To-</option>
                    <option value="1 AM"<?php echo ($value && $value->end_time =='01:00:00') ? 'selected' : ''; ?>>1:00 AM</option>
                    <option value="2 AM"<?php echo ($value && $value->end_time =='02:00:00') ? 'selected' : ''; ?>>2:00 AM</option>
                    <option value="3 AM"<?php echo ($value && $value->end_time =='03:00:00') ? 'selected' : ''; ?>>3:00 AM</option>
                    <option value="4 AM"<?php echo ($value && $value->end_time =='04:00:00') ? 'selected' : ''; ?>>4:00 AM</option>
                    <option value="5 AM"<?php echo ($value && $value->end_time =='05:00:00') ? 'selected' : ''; ?>>5:00 AM</option>
                    <option value="6 AM"<?php echo ($value && $value->end_time =='06:00:00') ? 'selected' : ''; ?>>6:00 AM</option>
                    <option value="7 AM"<?php echo ($value && $value->end_time =='07:00:00') ? 'selected' : ''; ?>>7:00 AM</option>
                    <option value="8 AM"<?php echo ($value && $value->end_time =='08:00:00') ? 'selected' : ''; ?>>8:00 AM</option>
                    <option value="9 AM"<?php echo ($value && $value->end_time =='09:00:00') ? 'selected' : ''; ?>>9:00 AM</option>
                    <option value="10 AM"<?php echo ($value && $value->end_time =='10:00:00') ? 'selected' : ''; ?>>10:00 AM</option>
                    <option value="11 AM"<?php echo ($value && $value->end_time =='11:00:00') ? 'selected' : ''; ?>>11:00 AM</option>
                    <option value="12 PM"<?php echo ($value && $value->end_time =='12:00:00') ? 'selected' : ''; ?>>12:00 PM</option>
                    <option value="1 PM"<?php echo ($value && $value->end_time =='13:00:00') ? 'selected' : ''; ?>>1:00 PM</option>
                    <option value="2 PM"<?php echo ($value && $value->end_time =='14:00:00') ? 'selected' : ''; ?>>2:00 PM</option>
                    <option value="3 PM"<?php echo ($value && $value->end_time =='15:00:00') ? 'selected' : ''; ?>>3:00 PM</option>
                    <option value="4 PM"<?php echo ($value && $value->end_time =='16:00:00') ? 'selected' : ''; ?>>4:00 PM</option>
                    <option value="5 PM"<?php echo ($value && $value->end_time =='17:00:00') ? 'selected' : ''; ?>>5:00 PM</option>
                    <option value="6 PM"<?php echo ($value && $value->end_time =='18:00:00') ? 'selected' : ''; ?>>6:00 PM</option>
                    <option value="7 PM"<?php echo ($value && $value->end_time =='19:00:00') ? 'selected' : ''; ?>>7:00 PM</option>
                    <option value="8 PM"<?php echo ($value && $value->end_time =='20:00:00') ? 'selected' : ''; ?>>8:00 PM</option>
                    <option value="9 PM"<?php echo ($value && $value->end_time =='21:00:00') ? 'selected' : ''; ?>>9:00 PM</option>
                    <option value="10 PM"<?php echo ($value && $value->end_time =='22:00:00') ? 'selected' : ''; ?>>10:00 PM</option>
                    <option value="11 PM"<?php echo ($value && $value->end_time =='23:00:00') ? 'selected' : ''; ?>>11:00 PM</option>
                    <option value="12 AM"<?php echo ($value && ($value->end_time == '24:00:00' || $value->end_time=='00:00:00')) ? 'selected' : ''; ?>>12:00 PM</option>
                </select>
            </div>
            </div>
            <div class="col-lg-4 select-half-on" id="end_date_wrapper_1"></div>  
        </div>
    <?php endforeach; ?>
        <div id="time-repeter-5"></div>
        <div class="add-academic col-lg-12"><a href="javascript:void(0);" class="add-more-time-link submit-button pull-right" onClick="repeatTime(5);">Add More Time</a></div>
    </div>
    </div>

        <div class="row">
        <?php $hasTimings = FALSE; if(isset($doctorTimings[6])) $hasTimings = TRUE; ?>
        <div><input type="checkbox" name="dayoftheweek[]" value="6" <?php echo $hasTimings ? 'checked' : ''; ?>/><label>&nbsp;Saturday</label></div>
        <div id="timings-6" style="<?php echo $hasTimings ? '' :  'display:none;'; ?>">
        <?php if(!$hasTimings) : ?>
            <?php $doctorTimings[6] = array('abc'); $value = NULL; ?>
        <?php endif; ?>
        <?php $count=0 ; foreach ($doctorTimings[6] as $key => $value) : ?>
        <div class="time-repeat" id="repeatID2">
            <?php $count++; ?>
            <?php if($count == 1) : ?>
            <div class="col-lg-6 select-half-on"></div>
            <?php else : ?>
            <div class="col-lg-2 select-half-on"></div>
            <?php endif; ?>                        
            <div class="col-lg-2 select-half-on">
                <label for="inputEmail" class="  control-label">*From </label>
                <div class=" ">
                <select name="from-timimg[6][]" class="form-control" id="select" style=" height: 2.2em !important;">
                    <option value="">-From-</option>
                    <option value="1 AM"<?php echo ($value && $value->start_time == '01:00:00') ? 'selected' : ''; ?>>1:00 AM</option>
                    <option value="2 AM"<?php echo ($value && $value->start_time == '02:00:00') ? 'selected' : ''; ?>>2:00 AM</option>
                    <option value="3 AM"<?php echo ($value && $value->start_time == '03:00:00') ? 'selected' : ''; ?>>3:00 AM</option>
                    <option value="4 AM"<?php echo ($value && $value->start_time == '04:00:00') ? 'selected' : ''; ?>>4:00 AM</option>
                    <option value="5 AM"<?php echo ($value && $value->start_time == '05:00:00') ? 'selected' : ''; ?>>5:00 AM</option>
                    <option value="6 AM"<?php echo ($value && $value->start_time == '06:00:00') ? 'selected' : ''; ?>>6:00 AM</option>
                    <option value="7 AM"<?php echo ($value && $value->start_time == '07:00:00') ? 'selected' : ''; ?>>7:00 AM</option>
                    <option value="8 AM"<?php echo ($value && $value->start_time == '08:00:00') ? 'selected' : ''; ?>>8:00 AM</option>
                    <option value="9 AM"<?php echo ($value && $value->start_time == '09:00:00') ? 'selected' : ''; ?>>9:00 AM</option>
                    <option value="10 AM"<?php echo ($value && $value->start_time == '10:00:00') ? 'selected' : ''; ?>>10:00 AM</option>
                    <option value="11 AM"<?php echo ($value && $value->start_time == '11:00:00') ? 'selected' : ''; ?>>11:00 AM</option>
                    <option value="12 PM"<?php echo ($value && $value->start_time == '12:00:00') ? 'selected' : ''; ?>>12:00 PM</option>
                    <option value="1 PM"<?php echo ($value && $value->start_time == '13:00:00') ? 'selected' : ''; ?>>1:00 PM</option>
                    <option value="2 PM"<?php echo ($value && $value->start_time == '14:00:00') ? 'selected' : ''; ?>>2:00 PM</option>
                    <option value="3 PM"<?php echo ($value && $value->start_time == '15:00:00') ? 'selected' : ''; ?>>3:00 PM</option>
                    <option value="4 PM"<?php echo ($value && $value->start_time == '16:00:00') ? 'selected' : ''; ?>>4:00 PM</option>
                    <option value="5 PM"<?php echo ($value && $value->start_time == '17:00:00') ? 'selected' : ''; ?>>5:00 PM</option>
                    <option value="6 PM"<?php echo ($value && $value->start_time == '18:00:00') ? 'selected' : ''; ?>>6:00 PM</option>
                    <option value="7 PM"<?php echo ($value && $value->start_time == '19:00:00') ? 'selected' : ''; ?>>7:00 PM</option>
                    <option value="8 PM"<?php echo ($value && $value->start_time == '20:00:00') ? 'selected' : ''; ?>>8:00 PM</option>
                    <option value="9 PM"<?php echo ($value && $value->start_time == '21:00:00') ? 'selected' : ''; ?>>9:00 PM</option>
                    <option value="10 PM"<?php echo ($value && $value->start_time == '22:00:00') ? 'selected' : ''; ?>>10:00 PM</option>
                    <option value="11 PM"<?php echo ($value && $value->start_time == '23:00:00') ? 'selected' : ''; ?>>11:00 PM</option>
                    <option value="12 AM"<?php echo ($value && ($value->start_time == '24:00:00' || $value->start_time=='00:00:00')) ? 'selected' : ''; ?>>12:00 PM</option>
                </select>
                </div>
            </div>

            <div class="col-lg-2 select-half-on" id="end_date_wrapper_1">
            <label for="inputEmail" class="  control-label">*To </label>
            <div class=" ">
                <select name="to-timing[6][]" class="form-control" id="select" style=" height: 2.2em !important;">
                    <option value="">-To-</option>
                    <option value="1 AM"<?php echo ($value && $value->end_time =='01:00:00') ? 'selected' : ''; ?>>1:00 AM</option>
                    <option value="2 AM"<?php echo ($value && $value->end_time =='02:00:00') ? 'selected' : ''; ?>>2:00 AM</option>
                    <option value="3 AM"<?php echo ($value && $value->end_time =='03:00:00') ? 'selected' : ''; ?>>3:00 AM</option>
                    <option value="4 AM"<?php echo ($value && $value->end_time =='04:00:00') ? 'selected' : ''; ?>>4:00 AM</option>
                    <option value="5 AM"<?php echo ($value && $value->end_time =='05:00:00') ? 'selected' : ''; ?>>5:00 AM</option>
                    <option value="6 AM"<?php echo ($value && $value->end_time =='06:00:00') ? 'selected' : ''; ?>>6:00 AM</option>
                    <option value="7 AM"<?php echo ($value && $value->end_time =='07:00:00') ? 'selected' : ''; ?>>7:00 AM</option>
                    <option value="8 AM"<?php echo ($value && $value->end_time =='08:00:00') ? 'selected' : ''; ?>>8:00 AM</option>
                    <option value="9 AM"<?php echo ($value && $value->end_time =='09:00:00') ? 'selected' : ''; ?>>9:00 AM</option>
                    <option value="10 AM"<?php echo ($value && $value->end_time =='10:00:00') ? 'selected' : ''; ?>>10:00 AM</option>
                    <option value="11 AM"<?php echo ($value && $value->end_time =='11:00:00') ? 'selected' : ''; ?>>11:00 AM</option>
                    <option value="12 PM"<?php echo ($value && $value->end_time =='12:00:00') ? 'selected' : ''; ?>>12:00 PM</option>
                    <option value="1 PM"<?php echo ($value && $value->end_time =='13:00:00') ? 'selected' : ''; ?>>1:00 PM</option>
                    <option value="2 PM"<?php echo ($value && $value->end_time =='14:00:00') ? 'selected' : ''; ?>>2:00 PM</option>
                    <option value="3 PM"<?php echo ($value && $value->end_time =='15:00:00') ? 'selected' : ''; ?>>3:00 PM</option>
                    <option value="4 PM"<?php echo ($value && $value->end_time =='16:00:00') ? 'selected' : ''; ?>>4:00 PM</option>
                    <option value="5 PM"<?php echo ($value && $value->end_time =='17:00:00') ? 'selected' : ''; ?>>5:00 PM</option>
                    <option value="6 PM"<?php echo ($value && $value->end_time =='18:00:00') ? 'selected' : ''; ?>>6:00 PM</option>
                    <option value="7 PM"<?php echo ($value && $value->end_time =='19:00:00') ? 'selected' : ''; ?>>7:00 PM</option>
                    <option value="8 PM"<?php echo ($value && $value->end_time =='20:00:00') ? 'selected' : ''; ?>>8:00 PM</option>
                    <option value="9 PM"<?php echo ($value && $value->end_time =='21:00:00') ? 'selected' : ''; ?>>9:00 PM</option>
                    <option value="10 PM"<?php echo ($value && $value->end_time =='22:00:00') ? 'selected' : ''; ?>>10:00 PM</option>
                    <option value="11 PM"<?php echo ($value && $value->end_time =='23:00:00') ? 'selected' : ''; ?>>11:00 PM</option>
                    <option value="12 AM"<?php echo ($value && ($value->end_time == '24:00:00' || $value->end_time=='00:00:00')) ? 'selected' : ''; ?>>12:00 PM</option>
                </select>
            </div>
            </div>
            <div class="col-lg-4 select-half-on" id="end_date_wrapper_1"></div>  
        </div>
        <?php endforeach; ?>
        <div id="time-repeter-6"></div>
        <div class="add-academic col-lg-12"><a href="javascript:void(0);" class="add-more-time-link submit-button pull-right" onClick="repeatTime(6);">Add More Time</a></div>
    </div>
    </div>

    <div class="row">
        <?php $hasTimings = FALSE; if(isset($doctorTimings[7])) $hasTimings = TRUE; ?>
        <div><input type="checkbox" name="dayoftheweek[]" value="7" <?php echo $hasTimings ? 'checked' : ''; ?>/><label>&nbsp;Sunday</label></div>
        <div id="timings-7" style="<?php echo $hasTimings ? '' :  'display:none;'; ?>">
        <?php if(!$hasTimings) : ?>
            <?php $doctorTimings[7] = array('abc'); $value = NULL; ?>
        <?php endif; ?>
        <?php $count=0 ; foreach ($doctorTimings[7] as $key => $value) : ?>
        <div class="time-repeat" id="repeatID2">
            <?php $count++; ?>
            <?php if($count == 1) : ?>
            <div class="col-lg-6 select-half-on"></div>
            <?php else : ?>
            <div class="col-lg-2 select-half-on"></div>
            <?php endif; ?>                        
            <div class="col-lg-2 select-half-on">
                <label for="inputEmail" class="  control-label">*From </label>
                <div class=" ">
                <select name="from-timimg[7][]" class="form-control" id="select" style=" height: 2.2em !important;">
                    <option value="">-From-</option>
                    <option value="1 AM"<?php echo ($value && $value->start_time == '01:00:00') ? 'selected' : ''; ?>>1:00 AM</option>
                    <option value="2 AM"<?php echo ($value && $value->start_time == '02:00:00') ? 'selected' : ''; ?>>2:00 AM</option>
                    <option value="3 AM"<?php echo ($value && $value->start_time == '03:00:00') ? 'selected' : ''; ?>>3:00 AM</option>
                    <option value="4 AM"<?php echo ($value && $value->start_time == '04:00:00') ? 'selected' : ''; ?>>4:00 AM</option>
                    <option value="5 AM"<?php echo ($value && $value->start_time == '05:00:00') ? 'selected' : ''; ?>>5:00 AM</option>
                    <option value="6 AM"<?php echo ($value && $value->start_time == '06:00:00') ? 'selected' : ''; ?>>6:00 AM</option>
                    <option value="7 AM"<?php echo ($value && $value->start_time == '07:00:00') ? 'selected' : ''; ?>>7:00 AM</option>
                    <option value="8 AM"<?php echo ($value && $value->start_time == '08:00:00') ? 'selected' : ''; ?>>8:00 AM</option>
                    <option value="9 AM"<?php echo ($value && $value->start_time == '09:00:00') ? 'selected' : ''; ?>>9:00 AM</option>
                    <option value="10 AM"<?php echo ($value && $value->start_time == '10:00:00') ? 'selected' : ''; ?>>10:00 AM</option>
                    <option value="11 AM"<?php echo ($value && $value->start_time == '11:00:00') ? 'selected' : ''; ?>>11:00 AM</option>
                    <option value="12 PM"<?php echo ($value && $value->start_time == '12:00:00') ? 'selected' : ''; ?>>12:00 PM</option>
                    <option value="1 PM"<?php echo ($value && $value->start_time == '13:00:00') ? 'selected' : ''; ?>>1:00 PM</option>
                    <option value="2 PM"<?php echo ($value && $value->start_time == '14:00:00') ? 'selected' : ''; ?>>2:00 PM</option>
                    <option value="3 PM"<?php echo ($value && $value->start_time == '15:00:00') ? 'selected' : ''; ?>>3:00 PM</option>
                    <option value="4 PM"<?php echo ($value && $value->start_time == '16:00:00') ? 'selected' : ''; ?>>4:00 PM</option>
                    <option value="5 PM"<?php echo ($value && $value->start_time == '17:00:00') ? 'selected' : ''; ?>>5:00 PM</option>
                    <option value="6 PM"<?php echo ($value && $value->start_time == '18:00:00') ? 'selected' : ''; ?>>6:00 PM</option>
                    <option value="7 PM"<?php echo ($value && $value->start_time == '19:00:00') ? 'selected' : ''; ?>>7:00 PM</option>
                    <option value="8 PM"<?php echo ($value && $value->start_time == '20:00:00') ? 'selected' : ''; ?>>8:00 PM</option>
                    <option value="9 PM"<?php echo ($value && $value->start_time == '21:00:00') ? 'selected' : ''; ?>>9:00 PM</option>
                    <option value="10 PM"<?php echo ($value && $value->start_time == '22:00:00') ? 'selected' : ''; ?>>10:00 PM</option>
                    <option value="11 PM"<?php echo ($value && $value->start_time == '23:00:00') ? 'selected' : ''; ?>>11:00 PM</option>
                    <option value="12 AM"<?php echo ($value && ($value->start_time == '24:00:00' || $value->start_time=='00:00:00')) ? 'selected' : ''; ?>>12:00 PM</option>
                </select>
                </div>
            </div>

            <div class="col-lg-2 select-half-on" id="end_date_wrapper_1">
            <label for="inputEmail" class="  control-label">*To </label>
            <div class=" ">
                <select name="to-timing[7][]" class="form-control" id="select" style=" height: 2.2em !important;">
                    <option value="">-To-</option>
                    <option value="1 AM"<?php echo ($value && $value->end_time =='01:00:00') ? 'selected' : ''; ?>>1:00 AM</option>
                    <option value="2 AM"<?php echo ($value && $value->end_time =='02:00:00') ? 'selected' : ''; ?>>2:00 AM</option>
                    <option value="3 AM"<?php echo ($value && $value->end_time =='03:00:00') ? 'selected' : ''; ?>>3:00 AM</option>
                    <option value="4 AM"<?php echo ($value && $value->end_time =='04:00:00') ? 'selected' : ''; ?>>4:00 AM</option>
                    <option value="5 AM"<?php echo ($value && $value->end_time =='05:00:00') ? 'selected' : ''; ?>>5:00 AM</option>
                    <option value="6 AM"<?php echo ($value && $value->end_time =='06:00:00') ? 'selected' : ''; ?>>6:00 AM</option>
                    <option value="7 AM"<?php echo ($value && $value->end_time =='07:00:00') ? 'selected' : ''; ?>>7:00 AM</option>
                    <option value="8 AM"<?php echo ($value && $value->end_time =='08:00:00') ? 'selected' : ''; ?>>8:00 AM</option>
                    <option value="9 AM"<?php echo ($value && $value->end_time =='09:00:00') ? 'selected' : ''; ?>>9:00 AM</option>
                    <option value="10 AM"<?php echo ($value && $value->end_time =='10:00:00') ? 'selected' : ''; ?>>10:00 AM</option>
                    <option value="11 AM"<?php echo ($value && $value->end_time =='11:00:00') ? 'selected' : ''; ?>>11:00 AM</option>
                    <option value="12 PM"<?php echo ($value && $value->end_time =='12:00:00') ? 'selected' : ''; ?>>12:00 PM</option>
                    <option value="1 PM"<?php echo ($value && $value->end_time =='13:00:00') ? 'selected' : ''; ?>>1:00 PM</option>
                    <option value="2 PM"<?php echo ($value && $value->end_time =='14:00:00') ? 'selected' : ''; ?>>2:00 PM</option>
                    <option value="3 PM"<?php echo ($value && $value->end_time =='15:00:00') ? 'selected' : ''; ?>>3:00 PM</option>
                    <option value="4 PM"<?php echo ($value && $value->end_time =='16:00:00') ? 'selected' : ''; ?>>4:00 PM</option>
                    <option value="5 PM"<?php echo ($value && $value->end_time =='17:00:00') ? 'selected' : ''; ?>>5:00 PM</option>
                    <option value="6 PM"<?php echo ($value && $value->end_time =='18:00:00') ? 'selected' : ''; ?>>6:00 PM</option>
                    <option value="7 PM"<?php echo ($value && $value->end_time =='19:00:00') ? 'selected' : ''; ?>>7:00 PM</option>
                    <option value="8 PM"<?php echo ($value && $value->end_time =='20:00:00') ? 'selected' : ''; ?>>8:00 PM</option>
                    <option value="9 PM"<?php echo ($value && $value->end_time =='21:00:00') ? 'selected' : ''; ?>>9:00 PM</option>
                    <option value="10 PM"<?php echo ($value && $value->end_time =='22:00:00') ? 'selected' : ''; ?>>10:00 PM</option>
                    <option value="11 PM"<?php echo ($value && $value->end_time =='23:00:00') ? 'selected' : ''; ?>>11:00 PM</option>
                    <option value="12 AM"<?php echo ($value && ($value->end_time == '24:00:00' || $value->end_time=='00:00:00')) ? 'selected' : ''; ?>>12:00 PM</option>
                </select>
            </div>
            </div>
            <div class="col-lg-4 select-half-on" id="end_date_wrapper_1"></div>  
        </div>
        <?php endforeach; ?>
        <div id="time-repeter-7"></div>
        <div class="add-academic col-lg-12"><a href="javascript:void(0);" class="add-more-time-link submit-button pull-right" onClick="repeatTime(7);">Add More Time</a></div>
    </div>
    </div>

</div>

    <button type="submit" class="btn btn-default submit-button">Submit</button>
    <?php echo form_close(); ?>
        </div>
    </div>
   </div>

<script type="text/javascript">


    $("#doctorinfo").validate({rules: {
            'dayoftheweek[]': {
                required: true,
                minlength: 1
            }
            
        },
        
        messages: {
            'dayoftheweek[]': {
                required: "You must check at least 1 box",
                maxlength: "Check no more than {0} boxes"
            }
        }
    });
</script>
 
<script type="text/javascript">
$(document).ready(function() {
    $(document).on('click change', '#doctorinfo,.exp-repeat, select', function(){
  var total_error = checkExprience();
   if(total_error > 0){
       return false;
   }
});



function checkExprience(){ // validate Experience 
   var count = 0;
   var total_error = 0;
   
   $( ".exp-repeat" ).each(function() {

        var startClass = 'start' + count; // classname define
        var endClass = 'end' + count;// classname define
        
        $('.'+endClass+'end_yy').remove(); // remove exist classname
        $('.'+startClass+'start_yy').remove(); // remove exist classname
        
        var error = 0;
        count++;  // use for classname unique define
        
        $(this).addClass(startClass); // add class
        
        var display_property = $('.'+ startClass +' select[name="end_mm[]"] ').parent().parent().css('display'); // check property of end date div for validation
        
        
        var start_month = $('.'+ startClass +' select[name="start_mm[]"] ').val();
        var start_year = $('.'+ startClass +' select[name="start_yy[]"] ').val();
        
        if(display_property == 'block'){ // start and end date compare
            var end_month = $('.'+ startClass +' select[name="end_mm[]"] ').val();
            var end_year = $('.'+ startClass +' select[name="end_yy[]"] ').val();


            if(end_year == '' || end_month == ''){
                $('.'+ startClass +' select[name="end_yy[]"] ').after('<label id="end_yy-error" class="error '+endClass+'end_yy" for="end_yy">Please select end date.</label>');
                error++;
            }
            
            if(start_year == '' || start_month == ''){
                $('.'+ startClass +' select[name="start_yy[]"] ').after('<label id="start_yy-error" class="error '+startClass+'start_yy" for="end_yy">Please select start date.</label>');
                error++;
            }
        
            if(error == 0){
                if(start_year > end_year || (start_year == end_year && start_month > end_month)){
                   $('.'+ startClass +' select[name="end_yy[]"] ').after('<label id="end_yy-error" class="error '+endClass+'end_yy" for="end_yy">End date Should be greater than Start date.</label>');
                   error++;
                }
            }
            
        }else if(start_year == '' || start_month == ''){ // check present data
            $('.'+ startClass +' select[name="start_yy[]"] ').after('<label id="start_yy-error" class="error '+startClass+'start_yy" for="end_yy">Please select start date.</label>');
            error++;
        }
    
        total_error = total_error + error; // count errors
    
   });
        return total_error;
}

   /* $('#Rank').bind('change', function() {
        var elements = $('div.day-container').children().hide(); // hide all the elements
        var value = $(this).val();

        if (value.length) { // if somethings' selected
            elements.filter('.' + value).show(); // show the ones we want
        }
    }).trigger('change');

     /*$('#timings').bind('change', function() {
        var elements = $('div.timing-container').children().hide(); // hide all the elements
        var value = $(this).val();

        if (value.length) { // if somethings' selected
            elements.filter('.' + value).show(); // show the ones we want
        }
    }).trigger('change');*/
});


  $("input[name='dayoftheweek[]']").on('click', function() {
  if($(this).is(':checked')){
    $('#timings-'+this.value).show();
  }else{
    $('#timings-'+this.value).hide();
  }
});

  function delete_md() {
    document.getElementById("del_md").submit();
  }
  
function repeatTime(day){
$('#time-repeter-'+day).append('<div class="time-repeat" id="repeatID2"><div class="col-lg-2 select-half-on"></div><div class="col-lg-2 select-half-on" id="timings-1"><label for="inputEmail" class="  control-label">*From </label><div class=" "><select name="from-timimg['+day+'][]" class="form-control" id="select" style=" height: 2.2em !important;"><option value="">-From-</option><option value="1 AM">1:00 AM</option><option value="2 AM">2:00 AM</option><option value="3 AM">3:00 AM</option><option value="4 AM">4:00 AM</option><option value="5 AM">5:00 AM</option><option value="6 AM">6:00 AM</option><option value="7 AM">7:00 AM</option><option value="8 AM">8:00 AM</option><option value="9 AM">9:00 AM</option><option value="10 AM">10:00 AM</option><option value="11 AM">11:00 AM</option><option value="12 PM">12:00 PM</option><option value="1 PM">1:00 PM</option><option value="2 PM">2:00 PM</option><option value="3 PM">3:00 PM</option><option value="4 PM">4:00 PM</option><option value="5 PM">5:00 PM</option><option value="6 PM">6:00 PM</option><option value="7 PM">7:00 PM</option><option value="8 PM">8:00 PM</option><option value="9 PM">9:00 PM</option><option value="10 PM">10:00 PM</option><option value="11 PM">11:00 PM</option><option value="12 AM">12:00 AM</option></select></div></div><div class="col-lg-2 select-half-on" id="end_date_wrapper_1"><label for="inputEmail" class="  control-label">*To </label><div class=" "><select name="to-timing['+day+'][]" class="form-control" id="select" style=" height: 2.2em !important;"><option value="">-To-</option><option value="1 AM">1:00 AM</option><option value="2 AM">2:00 AM</option><option value="3 AM">3:00 AM</option><option value="4 AM">4:00 AM</option><option value="5 AM">5:00 AM</option><option value="6 AM">6:00 AM</option><option value="7 AM">7:00 AM</option><option value="8 AM">8:00 AM</option><option value="9 AM">9:00 AM</option><option value="10 AM">10:00 AM</option><option value="11 AM">11:00 AM</option><option value="12 PM">12:00 PM</option><option value="1 PM">1:00 PM</option><option value="2 PM">2:00 PM</option><option value="3 PM">3:00 PM</option><option value="4 PM">4:00 PM</option><option value="5 PM">5:00 PM</option><option value="6 PM">6:00 PM</option><option value="7 PM">7:00 PM</option><option value="8 PM">8:00 PM</option><option value="9 PM">9:00 PM</option><option value="10 PM">10:00 PM</option><option value="11 PM">11:00 PM</option><option value="12 AM">12:00 AM</option></select></div></div><div class="col-lg-4"></div></div>');   
}
             
             
             
             
function readURL(input) {

    if (input.files && input.files[0]) {
        
         var url = input.value;
    var ext = url.substring(url.lastIndexOf('.') + 1).toLowerCase();
        
        
        if (input.files && input.files[0]&& (ext == "gif" || ext == "png" || ext == "jpeg" || ext == "jpg")) {
            
            var reader = new FileReader();
            reader.onload = function (e) {
                $('#pro-pic-id').attr('src', e.target.result);
            }
            reader.readAsDataURL(input.files[0]);
            
        }
        else
        {
            alert("Please upload a valid image file.");
            $("#add-pro-pic").val('');
        }    
        
    }
}

$("#add-pro-pic").change(function(){
    readURL(this);
});

function readCvURL(input) {

    if (input.files && input.files[0]) {
        var url = input.value;
        var ext = url.substring(url.lastIndexOf('.') + 1).toLowerCase();
        
        if (input.files && input.files[0]&& (ext == "pdf" || ext == "doc" || ext == "docx")) {
            
           /* var reader = new FileReader();
            reader.onload = function (e) {
                $('#pro-pic-id').attr('src', e.target.result);
            }
            reader.readAsDataURL(input.files[0]);*/
            
        }
        else
        {
            alert("Only pdf and word files are accepted.");
            $("#cv").val('');
        }    
        
    }
}

$("#cv").change(function(){
    readCvURL(this);
});


function readIdURL(input) {

    if (input.files && input.files[0]) {
        var url = input.value;
        var ext = url.substring(url.lastIndexOf('.') + 1).toLowerCase();
        
        if (input.files && input.files[0]&& (ext == "gif" || ext == "png" || ext == "jpeg" || ext == "jpg")) {
            
           /* var reader = new FileReader();
            reader.onload = function (e) {
                $('#pro-pic-id').attr('src', e.target.result);
            }
            reader.readAsDataURL(input.files[0]);*/
            
        }
        else
        {
            alert("Only image files are accepted in Id Card field.");
            $("#id_card").val('');
        }    
        
    }
}

$("#id_card").change(function(){
    readIdURL(this);
});          
</script>