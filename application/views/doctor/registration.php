<div class="form-register-doctor">
    <div class="container">
        <div class="col-lg-8 col-md-offset-2">
        <h1 style="margin-bottom:30px;"><?php echo isset($page->title) ? $page->title : 'Apply As Doctor' ;?></h1>
        <p><?php echo isset($page->content) ? $page->content : ''; ?></p>
<?php $attributes = array("name" => "doctorinfo" , "id" => "doctorinfo", "enctype" => 'multipart/form-data');
       echo form_open("registerdoctor/applyAsDoctor", $attributes);?>

<?php echo $this->session->flashdata('msg'); ?>
    <div class="personal-detail">
    <h3 style="border-bottom:#0484cf solid 2px; padding-bottom:5px;">Personal Detail</h3>
    <div class="row">
        <div class="col-lg-6">
            <label for="inputEmail" class="  control-label">*Name </label>
            <div class=" ">
                <input name="fname" type="text" value="" class="form-control" required>
            </div>
    </div>
    
    <div class="col-lg-6">
        <label for="inputEmail" class="  control-label">*Surname </label>
        <div class=" ">
            <input name="sname" type="text" value="" class="form-control" required>
        </div>
    </div>
    
    <div class="col-lg-6">
    <label for="inputEmail" class="  control-label">*Email </label>
    <div class=" ">                   
        <input name="email" type="email" value="" class="form-control" required>
    </div>
</div>
    <div class="col-lg-6">
    <label for="inputEmail" class="  control-label">*Phone </label>
    <div class=" ">                                        
        <input name="phone" type="text" value="" class="form-control" required>
    </div>
</div>

<!--div class="col-lg-12">
    <label for="inputEmail" class="  control-label">*Address </label>
    <div class=" ">                                        
         
        <textarea name="address" rows="3"  class="form-control"></textarea>
    </div>
</div-->
    

	</div>
 </div>
 
 
 
   <div class="personal-detail">
    <h3 style="border-bottom:#0484cf solid 2px; padding-bottom:5px;">Academic qualifications</h3>
    <div class="row main-qualification-section">
    <div class="acadmeic-quali-repeater" id="repeatID1">
    <input type="hidden" name="aqcount" id="aqcount" value="2" /> 
    <div class="count-ac col-lg-12"><span>1</span></div>
        <div class="col-lg-4">
    
            <label for="inputEmail" class="  control-label">*Qualification </label>
            <div class=" ">
                <input name="qualification[]" type="text" value="" class="form-control" required>
            </div>
 
        </div>
        <div class="col-lg-4">
    
            <label for="inputEmail" class="  control-label">*Institution </label>
            <div class=" ">
                <input name="institution[]" type="text" value="" class="form-control" required>
            </div>
 
        </div>
        <div class="col-lg-4">
    
            <label for="inputEmail" class="  control-label">*Year  </label>
            <div class=" ">
                <input name="year[]" type="text" value="" class="form-control" required>
            </div>
 
        </div>
         
        </div>
         <div id="aq-repeter"></div>
        <div class="add-academic col-lg-12"><a href="javascript:void(0)" class="add-more-academic-link submit-button pull-right">Add More Academic</a></div>
        
     </div>
    </div>
       <div class="personal-detail">
    <h3 style="border-bottom:#0484cf solid 2px; padding-bottom:5px;">Biography</h3>
    <div class="row">
  
        <div class="col-lg-12">
 
    <label for="inputEmail" class="  control-label">Description <span style="font-size:10px;">(Maximum 150 Words)</span></label>
    <textarea class="form-control" id="content" name="description" ></textarea>
    
</div>


</div>
<div class="form-group">
    <label for="inputEmail" class="  control-label">*Upload CV</label>
    <div class=" ">
        <input name="cv" type="file" value="" id="cv" class="form-control" required>
    </div>
</div>
<div class="form-group">
    <label for="inputEmail" class="  control-label">*Upload Id Card</label>
    <div class=" ">                            
        <input name="id_card" type="file" value="" id="id_card" class="form-control">
    </div>
</div>
</div>


                <div class="personal-detail">
    <h3 style="border-bottom:#0484cf solid 2px; padding-bottom:5px;">Type</h3>
    <div class="row">
  <div class="col-lg-12">
    <label for="inputEmail" class="  control-label">*Area of training </label>
    <div class=" ">
        <input id="type-of-doctor-2" type="radio" name="type_of_doctor" value="2" checked> Psychologist &nbsp;&nbsp;
        <input id="type-of-doctor-1" type="radio" name="type_of_doctor" value="1"> Dietitian &nbsp;&nbsp;
        <input id="type-of-doctor-3" type="radio" name="type_of_doctor" value="3"> Medical Doctor &nbsp;&nbsp;
    </div>
</div>
      <div class="clearfix" style=" height: 10px;
display: inline-block;
width: 100%;"></div>

<div class="col-lg-6" id="only-medical-doctor-1"  >
    <label for="inputEmail" class="  control-label">Profession Number </label>
    <div class=" ">                                        
        <input name="profession_number" type="text" value="" class="form-control">
    </div>
</div>

<div class="col-lg-6" id="only-medical-doctor-2"  >
    <label for="inputEmail" class="  control-label">Practice Number </label>
    <div class=" ">                                        
        <input name="practice_number" type="text" value="" class="form-control">
    </div>
</div>     

</div></div>

       <div class="personal-detail">
    <h3 style="border-bottom:#0484cf solid 2px; padding-bottom:5px;">Working Experience</h3>
    <div class="row">
      <input type="hidden" name="wecount" id="wecount" value="2" /> 
    <div class="count-we col-lg-12"><span>1</span></div>
  <div class="exp-repeat" id="repeatID2">
        <div class="col-lg-3">
 <label for="inputEmail" class="  control-label">*Company </label>
    <div class=" ">
        <input name="company[]" type="text" value="" class="form-control" required>
    </div>
</div>
 <div class="col-lg-3">
 <label for="inputEmail" class="  control-label">*Job Title </label>
    <div class=" ">
        <input name="job_title[]" type="text" value="" class="form-control" required>
    </div>
</div>
 <div class="col-lg-2 select-half-on">
 <label for="inputEmail" class="  control-label">*Start Date </label>
 <div class=" ">
        <select name="start_mm[]" class="form-control" id="select" style=" height: 2.2em !important;">
            <option value="">Month</option>
            <option value="01">Jan</option>
            <option value="02">Feb</option>
            <option value="03">Mar</option>
            <option value="04">Apr</option>
            <option value="05">May</option>
            <option value="06">June</option>
            <option value="07">July</option>
            <option value="08">Aug</option>
            <option value="09">Sep</option>
            <option value="10">Oct</option>
            <option value="11">Nov</option>
            <option value="12">Dec</option>
        </select>
     <select name="start_yy[]" class="" id="select" style=" height: 2.2em !important;">
            <option value="">Year</option>
            <?php for($i=date('Y')-80; $i<=date('Y'); $i++): ?>
                <option value="<?php echo $i; ?>"><?php echo $i; ?></option>
            <?php endfor; ?>
        </select>
</div>
</div>

      <div class="col-lg-2 select-half-on" id="end_date_wrapper_1">
 <label for="inputEmail" class="  control-label">*End Date </label>
 <div class=" ">
        <select name="end_mm[]" class="form-control" id="select" style=" height: 2.2em !important;">
            <option value="">Month</option>
            <option value="01" <?php echo date('m') == '01' ? 'selected' : ''; ?>>Jan</option>
            <option value="02" <?php echo date('m') == '02' ? 'selected' : ''; ?>>Feb</option>
            <option value="03" <?php echo date('m') == '03' ? 'selected' : ''; ?>>Mar</option>
            <option value="04" <?php echo date('m') == '04' ? 'selected' : ''; ?>>Apr</option>
            <option value="05" <?php echo date('m') == '05' ? 'selected' : ''; ?>>May</option>
            <option value="06" <?php echo date('m') == '06' ? 'selected' : ''; ?>>June</option>
            <option value="07" <?php echo date('m') == '07' ? 'selected' : ''; ?>>July</option>
            <option value="08" <?php echo date('m') == '08' ? 'selected' : ''; ?>>Aug</option>
            <option value="09" <?php echo date('m') == '09' ? 'selected' : ''; ?>>Sep</option>
            <option value="10" <?php echo date('m') == '10' ? 'selected' : ''; ?>>Oct</option>
            <option value="11" <?php echo date('m') == '11' ? 'selected' : ''; ?>>Nov</option>
            <option value="12" <?php echo date('m') == '12' ? 'selected' : ''; ?>>Dec</option>
        </select>
        <select name="end_yy[]" class="form-control" id="select" style=" height: 2.2em !important;">
            <option value="">Year</option>
            <?php for($i=date('Y')-80; $i<=date('Y'); $i++): ?>
                <option value="<?php echo $i; ?>" <?php echo $i == date('Y') ? 'selected' : ''; ?>><?php echo $i; ?></option>
            <?php endfor; ?>
        </select>
</div>
</div>
      
      <div class="col-lg-2 select-half-on">
    <label for="inputEmail" class="control-label"> Present here </label>
    <div class=" "> 
            <input 
                name="stay_here[]" 
                type="checkbox" 
                value="1" 
                class="present_here pull-left"
                id="present_here_1"   
                data-indexof="1" 
                onclick="present_here(this.id);" 
             />
    </div>
 </div> 
      
</div>
 <div id="we-repeter"></div>
<div class="add-academic col-lg-12"><a href="javascript:void(0)" class="add-more-exp-link submit-button pull-right">Add More Experience</a></div>
</div></div>
                     
 
<div class="personal-detail">
    <h3 style="border-bottom:#0484cf solid 2px; padding-bottom:5px;">Days & Timings</h3>
    
    <div class="row">
        <input type="hidden" name="timecount" id="daycount" value="1" />
        <div><input type="checkbox" name="dayoftheweek[]" value="1"/><label>&nbsp;Monday</label></div>
        <div id="timings-1" style="display:none;">
        <div class="time-repeat" id="repeatID2">
            <div class="col-lg-6 select-half-on"></div>            
            <div class="col-lg-2 select-half-on">
                <label for="inputEmail" class="  control-label">*From </label>
                <div class=" ">
                <select name="from-timimg[1][]" class="form-control" id="select" style=" height: 2.2em !important;">
                    <option value="">-From-</option>
                    <option value="1 AM">1:00 AM</option>
                    <option value="2 AM">2:00 AM</option>
                    <option value="3 AM">3:00 AM</option>
                    <option value="4 AM">4:00 AM</option>
                    <option value="5 AM">5:00 AM</option>
                    <option value="6 AM">6:00 AM</option>
                    <option value="7 AM">7:00 AM</option>
                    <option value="8 AM">8:00 AM</option>
                    <option value="9 AM">9:00 AM</option>
                    <option value="10 AM">10:00 AM</option>
                    <option value="11 AM">11:00 AM</option>
                    <option value="12 PM">12:00 PM</option>
                    <option value="1 PM">1:00 PM</option>
                    <option value="2 PM">2:00 PM</option>
                    <option value="3 PM">3:00 PM</option>
                    <option value="4 PM">4:00 PM</option>
                    <option value="5 PM">5:00 PM</option>
                    <option value="6 PM">6:00 PM</option>
                    <option value="7 PM">7:00 PM</option>
                    <option value="8 PM">8:00 PM</option>
                    <option value="9 PM">9:00 PM</option>
                    <option value="10 PM">10:00 PM</option>
                    <option value="11 PM">11:00 PM</option>
                    <option value="12 AM">12:00 AM</option>
                </select>
                </div>
            </div>

            <div class="col-lg-2 select-half-on" id="end_date_wrapper_1">
            <label for="inputEmail" class="  control-label">*To </label>
            <div class=" ">
                <select name="to-timing[1][]" class="form-control" id="to-timing-1" style=" height: 2.2em !important;">
                    <option value="">-To-</option>
                </select>
            </div>
            </div>
            <div class="col-lg-4 select-half-on" id="end_date_wrapper_1"></div>  
        </div>
        <div id="time-repeter-1"></div>
        <div class="add-academic col-lg-12"><a href="javascript:void(0);" class="add-more-time-link submit-button pull-right" onClick="repeatTime(1);">Add More Time</a></div>
    </div>
    </div>

    <div class="row">
        <div><input type="checkbox" name="dayoftheweek[]" value="2"/><label>&nbsp;Tuesday</label></div>
        <div id="timings-2" style="display:none;">
        <div class="time-repeat" id="repeatID2">
            <div class="col-lg-6 select-half-on"></div>
            <div class="col-lg-2 select-half-on">
                <label for="inputEmail" class="  control-label">*From </label>
                <div class=" ">
                <select name="from-timimg[2][]" class="form-control" id="select" style=" height: 2.2em !important;">
                    <option value="">-From-</option>
                    <option value="1 AM">1:00 AM</option>
                    <option value="2 AM">2:00 AM</option>
                    <option value="3 AM">3:00 AM</option>
                    <option value="4 AM">4:00 AM</option>
                    <option value="5 AM">5:00 AM</option>
                    <option value="6 AM">6:00 AM</option>
                    <option value="7 AM">7:00 AM</option>
                    <option value="8 AM">8:00 AM</option>
                    <option value="9 AM">9:00 AM</option>
                    <option value="10 AM">10:00 AM</option>
                    <option value="11 AM">11:00 AM</option>
                    <option value="12 PM">12:00 PM</option>
                    <option value="1 PM">1:00 PM</option>
                    <option value="2 PM">2:00 PM</option>
                    <option value="3 PM">3:00 PM</option>
                    <option value="4 PM">4:00 PM</option>
                    <option value="5 PM">5:00 PM</option>
                    <option value="6 PM">6:00 PM</option>
                    <option value="7 PM">7:00 PM</option>
                    <option value="8 PM">8:00 PM</option>
                    <option value="9 PM">9:00 PM</option>
                    <option value="10 PM">10:00 PM</option>
                    <option value="11 PM">11:00 PM</option>
                    <option value="12 AM">12:00 AM</option>
                </select>
                </div>
            </div>

            <div class="col-lg-2 select-half-on" id="end_date_wrapper_1">
            <label for="inputEmail" class="  control-label">*To </label>
            <div class=" ">
                <select name="to-timing[2][]" class="form-control" id="to-timing-2" style=" height: 2.2em !important;">
                    <option value="">-To-</option>
                </select>
            </div>
            </div>
            <div class="col-lg-4 select-half-on" id="end_date_wrapper_1"></div>  
        </div>
        <div id="time-repeter-2"></div>
        <div class="add-academic col-lg-12"><a href="javascript:void(0);" class="add-more-time-link submit-button pull-right" onClick="repeatTime(2);">Add More Time</a></div>
    </div>
    </div>

    <div class="row">
        <input type="hidden" name="timecount" id="daycount" value="3" />
        <div><input type="checkbox" name="dayoftheweek[]" value="3"/><label>&nbsp;Wednesday</label></div>
        <div id="timings-3" style="display:none;">
        <div class="time-repeat" id="repeatID2">
            <div class="col-lg-6 select-half-on"></div>           
            <div class="col-lg-2 select-half-on">
                <label for="inputEmail" class="  control-label">*From </label>
                <div class=" ">
                <select name="from-timimg[3][]" class="form-control" id="select" style=" height: 2.2em !important;">
                    <option value="">-From-</option>
                    <option value="1 AM">1:00 AM</option>
                    <option value="2 AM">2:00 AM</option>
                    <option value="3 AM">3:00 AM</option>
                    <option value="4 AM">4:00 AM</option>
                    <option value="5 AM">5:00 AM</option>
                    <option value="6 AM">6:00 AM</option>
                    <option value="7 AM">7:00 AM</option>
                    <option value="8 AM">8:00 AM</option>
                    <option value="9 AM">9:00 AM</option>
                    <option value="10 AM">10:00 AM</option>
                    <option value="11 AM">11:00 AM</option>
                    <option value="12 PM">12:00 PM</option>
                    <option value="1 PM">1:00 PM</option>
                    <option value="2 PM">2:00 PM</option>
                    <option value="3 PM">3:00 PM</option>
                    <option value="4 PM">4:00 PM</option>
                    <option value="5 PM">5:00 PM</option>
                    <option value="6 PM">6:00 PM</option>
                    <option value="7 PM">7:00 PM</option>
                    <option value="8 PM">8:00 PM</option>
                    <option value="9 PM">9:00 PM</option>
                    <option value="10 PM">10:00 PM</option>
                    <option value="11 PM">11:00 PM</option>
                    <option value="12 AM">12:00 AM</option>
                </select>
                </div>
            </div>

            <div class="col-lg-2 select-half-on" id="end_date_wrapper_1">
            <label for="inputEmail" class="  control-label">*To </label>
            <div class=" ">
                <select name="to-timing[3][]" class="form-control" id="to-timing-3" style=" height: 2.2em !important;">
                    <option value="">-To-</option>
                </select>
            </div>
            </div>
            <div class="col-lg-4 select-half-on" id="end_date_wrapper_1"></div>  
        </div>
        <div id="time-repeter-3"></div>
        <div class="add-academic col-lg-12"><a href="javascript:void(0);" class="add-more-time-link submit-button pull-right" onClick="repeatTime(3);">Add More Time</a></div>
    </div>
    </div>

    <div class="row">
        <input type="hidden" name="timecount" id="daycount" value="4" />
        <div><input type="checkbox" name="dayoftheweek[]" value="4"/><label>&nbsp;Thursday</label></div>
        <div id="timings-4" style="display:none;">
        <div class="time-repeat" id="repeatID2">
            <div class="col-lg-6 select-half-on"></div>           
            <div class="col-lg-2 select-half-on">
                <label for="inputEmail" class="  control-label">*From </label>
                <div class=" ">
                <select name="from-timimg[4][]" class="form-control" id="select" style=" height: 2.2em !important;">
                    <option value="">-From-</option>
                    <option value="1 AM">1:00 AM</option>
                    <option value="2 AM">2:00 AM</option>
                    <option value="3 AM">3:00 AM</option>
                    <option value="4 AM">4:00 AM</option>
                    <option value="5 AM">5:00 AM</option>
                    <option value="6 AM">6:00 AM</option>
                    <option value="7 AM">7:00 AM</option>
                    <option value="8 AM">8:00 AM</option>
                    <option value="9 AM">9:00 AM</option>
                    <option value="10 AM">10:00 AM</option>
                    <option value="11 AM">11:00 AM</option>
                    <option value="12 PM">12:00 PM</option>
                    <option value="1 PM">1:00 PM</option>
                    <option value="2 PM">2:00 PM</option>
                    <option value="3 PM">3:00 PM</option>
                    <option value="4 PM">4:00 PM</option>
                    <option value="5 PM">5:00 PM</option>
                    <option value="6 PM">6:00 PM</option>
                    <option value="7 PM">7:00 PM</option>
                    <option value="8 PM">8:00 PM</option>
                    <option value="9 PM">9:00 PM</option>
                    <option value="10 PM">10:00 PM</option>
                    <option value="11 PM">11:00 PM</option>
                    <option value="12 AM">12:00 AM</option>
                </select>
                </div>
            </div>

            <div class="col-lg-2 select-half-on" id="end_date_wrapper_1">
            <label for="inputEmail" class="  control-label">*To </label>
            <div class=" ">
                <select name="to-timing[4][]" class="form-control" id="to-timing-4" style=" height: 2.2em !important;">
                    <option value="">-To-</option>
                </select>
            </div>
            </div>
            <div class="col-lg-4 select-half-on" id="end_date_wrapper_1"></div>  
        </div>
        <div id="time-repeter-4"></div>
        <div class="add-academic col-lg-12"><a href="javascript:void(0);" class="add-more-time-link submit-button pull-right" onClick="repeatTime(4);">Add More Time</a></div>
    </div>
    </div>

    <div class="row">
        <div><input type="checkbox" name="dayoftheweek[]" value="5"/><label>&nbsp;Friday</label></div>
        <div id="timings-5" style="display:none;">
        <div class="time-repeat" id="repeatID2">
            <div class="col-lg-6 select-half-on"></div>           
            <div class="col-lg-2 select-half-on">
                <label for="inputEmail" class="  control-label">*From </label>
                <div class=" ">
                <select name="from-timimg[5][]" class="form-control" id="select" style=" height: 2.2em !important;">
                    <option value="">-From-</option>
                    <option value="1 AM">1:00 AM</option>
                    <option value="2 AM">2:00 AM</option>
                    <option value="3 AM">3:00 AM</option>
                    <option value="4 AM">4:00 AM</option>
                    <option value="5 AM">5:00 AM</option>
                    <option value="6 AM">6:00 AM</option>
                    <option value="7 AM">7:00 AM</option>
                    <option value="8 AM">8:00 AM</option>
                    <option value="9 AM">9:00 AM</option>
                    <option value="10 AM">10:00 AM</option>
                    <option value="11 AM">11:00 AM</option>
                    <option value="12 PM">12:00 PM</option>
                    <option value="1 PM">1:00 PM</option>
                    <option value="2 PM">2:00 PM</option>
                    <option value="3 PM">3:00 PM</option>
                    <option value="4 PM">4:00 PM</option>
                    <option value="5 PM">5:00 PM</option>
                    <option value="6 PM">6:00 PM</option>
                    <option value="7 PM">7:00 PM</option>
                    <option value="8 PM">8:00 PM</option>
                    <option value="9 PM">9:00 PM</option>
                    <option value="10 PM">10:00 PM</option>
                    <option value="11 PM">11:00 PM</option>
                    <option value="12 AM">12:00 AM</option>
                </select>
                </div>
            </div>

            <div class="col-lg-2 select-half-on" id="end_date_wrapper_1">
            <label for="inputEmail" class="  control-label">*To </label>
            <div class=" ">
                <select name="to-timing[5][]" class="form-control" id="to-timing-5" style=" height: 2.2em !important;">
                    <option value="">-To-</option>
                </select>
            </div>
            </div>
            <div class="col-lg-4 select-half-on" id="end_date_wrapper_1"></div>  
        </div>
        <div id="time-repeter-5"></div>
        <div class="add-academic col-lg-12"><a href="javascript:void(0);" class="add-more-time-link submit-button pull-right" onClick="repeatTime(5);">Add More Time</a></div>
    </div>
    </div>

    <div class="row">
        <div><input type="checkbox" name="dayoftheweek[]" value="6"/><label>&nbsp;Saturday</label></div>
        <div id="timings-6" style="display:none;">
        <div class="time-repeat" id="repeatID2">
            <div class="col-lg-6 select-half-on"></div>           
            <div class="col-lg-2 select-half-on">
                <label for="inputEmail" class="  control-label">*From </label>
                <div class=" ">
                <select name="from-timimg[6][]" class="form-control" id="select" style=" height: 2.2em !important;">
                    <option value="">-From-</option>
                    <option value="1 AM">1:00 AM</option>
                    <option value="2 AM">2:00 AM</option>
                    <option value="3 AM">3:00 AM</option>
                    <option value="4 AM">4:00 AM</option>
                    <option value="5 AM">5:00 AM</option>
                    <option value="6 AM">6:00 AM</option>
                    <option value="7 AM">7:00 AM</option>
                    <option value="8 AM">8:00 AM</option>
                    <option value="9 AM">9:00 AM</option>
                    <option value="10 AM">10:00 AM</option>
                    <option value="11 AM">11:00 AM</option>
                    <option value="12 PM">12:00 PM</option>
                    <option value="1 PM">1:00 PM</option>
                    <option value="2 PM">2:00 PM</option>
                    <option value="3 PM">3:00 PM</option>
                    <option value="4 PM">4:00 PM</option>
                    <option value="5 PM">5:00 PM</option>
                    <option value="6 PM">6:00 PM</option>
                    <option value="7 PM">7:00 PM</option>
                    <option value="8 PM">8:00 PM</option>
                    <option value="9 PM">9:00 PM</option>
                    <option value="10 PM">10:00 PM</option>
                    <option value="11 PM">11:00 PM</option>
                    <option value="12 AM">12:00 AM</option>
                </select>
                </div>
            </div>

            <div class="col-lg-2 select-half-on" id="end_date_wrapper_1">
            <label for="inputEmail" class="  control-label">*To </label>
            <div class=" ">
                <select name="to-timing[6][]" class="form-control" id="to-timing-6" style=" height: 2.2em !important;">
                    <option value="">-To-</option>
                </select>
            </div>
            </div>
            <div class="col-lg-4 select-half-on" id="end_date_wrapper_1"></div>  
        </div>
        <div id="time-repeter-6"></div>
        <div class="add-academic col-lg-12"><a href="javascript:void(0);" class="add-more-time-link submit-button pull-right" onClick="repeatTime(6);">Add More Time</a></div>
    </div>
    </div>

    <div class="row">
        <div><input type="checkbox" name="dayoftheweek[]" value="7"/><label>&nbsp;Sunday</label></div>
        <div id="timings-7" style="display:none;">
        <div class="time-repeat" id="repeatID2">
            <div class="col-lg-6 select-half-on"></div>           
            <div class="col-lg-2 select-half-on">
                <label for="inputEmail" class="  control-label">*From </label>
                <div class=" ">
                <select name="from-timimg[7][]" class="form-control" id="select" style=" height: 2.2em !important;">
                    <option value="">-From-</option>
                    <option value="1 AM">1:00 AM</option>
                    <option value="2 AM">2:00 AM</option>
                    <option value="3 AM">3:00 AM</option>
                    <option value="4 AM">4:00 AM</option>
                    <option value="5 AM">5:00 AM</option>
                    <option value="6 AM">6:00 AM</option>
                    <option value="7 AM">7:00 AM</option>
                    <option value="8 AM">8:00 AM</option>
                    <option value="9 AM">9:00 AM</option>
                    <option value="10 AM">10:00 AM</option>
                    <option value="11 AM">11:00 AM</option>
                    <option value="12 PM">12:00 PM</option>
                    <option value="1 PM">1:00 PM</option>
                    <option value="2 PM">2:00 PM</option>
                    <option value="3 PM">3:00 PM</option>
                    <option value="4 PM">4:00 PM</option>
                    <option value="5 PM">5:00 PM</option>
                    <option value="6 PM">6:00 PM</option>
                    <option value="7 PM">7:00 PM</option>
                    <option value="8 PM">8:00 PM</option>
                    <option value="9 PM">9:00 PM</option>
                    <option value="10 PM">10:00 PM</option>
                    <option value="11 PM">11:00 PM</option>
                    <option value="12 AM">12:00 AM</option>
                </select>
                </div>
            </div>

            <div class="col-lg-2 select-half-on" id="end_date_wrapper_1">
            <label for="inputEmail" class="  control-label">*To </label>
            <div class=" ">
                <select name="to-timing[7][]" class="form-control" id="to-timing-7" style=" height: 2.2em !important;">
                    <option value="">-To-</option>
                </select>
            </div>
            </div>
            <div class="col-lg-4 select-half-on" id="end_date_wrapper_1"></div>  
        </div>
        <div id="time-repeter-7"></div>
        <div class="add-academic col-lg-12"><a href="javascript:void(0);" class="add-more-time-link submit-button pull-right" onClick="repeatTime(7);">Add More Time</a></div>
    </div>
    </div>

</div>

<button type="submit" class="btn btn-default submit-button">Submit</button>
<?php echo form_close(); ?>
        </div>
    </div>
   </div>
<script type="text/javascript">
$(document).on('click', '.Doctor_infoSubmit, select', function(){
  var total_error = checkExprience();
   if(total_error > 0){
       return false;
   }
});



function checkExprience(){ // validate Experience 
   var count = 0;
   var total_error = 0;
   
   $( ".exp-repeat" ).each(function() {

        var startClass = 'start' + count; // classname define
        var endClass = 'end' + count;// classname define
        
        $('.'+endClass+'end_yy').remove(); // remove exist classname
        $('.'+startClass+'start_yy').remove(); // remove exist classname
        
        var error = 0;
        count++;  // use for classname unique define
        
        $(this).addClass(startClass); // add class
        
        var display_property = $('.'+ startClass +' select[name="end_mm[]"] ').parent().parent().css('display'); // check property of end date div for validation
        
        
        var start_month = $('.'+ startClass +' select[name="start_mm[]"] ').val();
        var start_year = $('.'+ startClass +' select[name="start_yy[]"] ').val();
        
        if(display_property == 'block'){ // start and end date compare
            var end_month = $('.'+ startClass +' select[name="end_mm[]"] ').val();
            var end_year = $('.'+ startClass +' select[name="end_yy[]"] ').val();


            if(end_year == '' || end_month == ''){
                $('.'+ startClass +' select[name="end_yy[]"] ').after('<label id="end_yy-error" class="error '+endClass+'end_yy" for="end_yy">Please select end date.</label>');
                error++;
            }
            
            if(start_year == '' || start_month == ''){
                $('.'+ startClass +' select[name="start_yy[]"] ').after('<label id="start_yy-error" class="error '+startClass+'start_yy" for="end_yy">Please select start date.</label>');
                error++;
            }
        
            if(error == 0){
                if(start_year > end_year || (start_year == end_year && start_month > end_month)){
                   $('.'+ startClass +' select[name="end_yy[]"] ').after('<label id="end_yy-error" class="error '+endClass+'end_yy" for="end_yy">End date Should be greater than Start date.</label>');
                   error++;
                }
            }
            
        }else if(start_year == '' || start_month == ''){ // check present data
            $('.'+ startClass +' select[name="start_yy[]"] ').after('<label id="start_yy-error" class="error '+startClass+'start_yy" for="end_yy">Please select start date.</label>');
            error++;
        }
    
        total_error = total_error + error; // count errors
    
   });

   
        return total_error;
   
}

    $("#doctorinfo").validate({rules: {
            'dayoftheweek[]': {
                required: true
            }
            
        },
        
        messages: {
            'dayoftheweek[]': {
                required: "You must check at least 1 box",
                //maxlength: "Check no more than {0} boxes"
            }
        }
    });
</script>
 
<script type="text/javascript">
$(document).ready(function() {
    $('#Rank').bind('change', function() {
        var elements = $('div.day-container').children().hide(); // hide all the elements
        var value = $(this).val();

        if (value.length) { // if somethings' selected
            elements.filter('.' + value).show(); // show the ones we want
        }
    }).trigger('change');
    
     $('timings').bind('change', function() {
        var elements = $('div.timing-container').children().hide(); // hide all the elements
        var value = $(this).val();

        if (value.length) { // if somethings' selected
            elements.filter('.' + value).show(); // show the ones we want
        }
    }).trigger('change');
    
});

/*$(".dropdown dt a").on('click', function() {
  $(".dropdown dd ul").slideToggle('fast');
});

$(".dropdown dd ul li a").on('click', function() {
  $(".dropdown dd ul").hide();
});
*/
function getSelectedValue(id) {
  return $("#" + id).find("dt a span.value").html();
}

$('select').on('change', function() {
  var item = this.name;
  var remains = item.substr(12);
  key = remains.substr(0,1);
  value = this.value;

  var result = value.split(' ');
  var mystring = [];
  start = result[0];
  start++;
  var changed = 'FALSE';
  // start time is 12 PM make next time 1 PM 
  if(start == 13 && result[1] == 'PM'){
    start = 1;
  }
  //start time is 11 AM make next time 12 PM
  if(start == 12)
  {
    if(result[1] == 'AM')
    {
        result[1] = 'PM';
        changed = 'TRUE';
    }
    else //start time is 11 PM make next time 12 PM
    {
        result[1] = 'AM';
    }
  }

  for (var i = start; i <= 12; i++) 
  {
    
    mystring.push('<option value="'+i+' '+result[1]+'">'+i+':00 '+result[1]+'</option>');
    if(i =='11')
    {
        if(result[1] == 'AM')
        {
            mystring.push('<option value="12 PM">12:00 PM</option>');
            i=0; result[1]= 'PM';
        }
        else
        {
            mystring.push('<option value="12 AM">12:00 AM</option>');
            i=13;   
        }

    }
    if(i == '12')
    {
        if(result[1] == 'PM' && changed == 'TRUE'){
            i = 0;
        }
        else {
           //mystring.push('<option value="12 AM">12:00 AM</option>');
        }
    }
 }
 $('#to-timing-'+key).html(mystring);
});

$('input[type="checkbox"]').on('click', function() {
  if ($(this).is(':checked')) {
    $('#timings-'+this.value).show();
  } else {
    $('#timings-'+this.value).hide();
  }
});

  function delete_md() {
    document.getElementById("del_md").submit();
  }
  
function repeatTime(day){
$('#time-repeter-'+day).append('<div class="time-repeat" id="repeatID2"><div class="col-lg-2 select-half-on"></div><div class="col-lg-2 select-half-on" id="timings-1"><label for="inputEmail" class="  control-label">*From </label><div class=" "><select name="from-timimg['+day+'][]" class="form-control" id="select" style=" height: 2.2em !important;"><option value="1 AM">1:00 AM</option><option value="2 AM">2:00 AM</option><option value="3 AM">3:00 AM</option><option value="4 AM">4:00 AM</option><option value="5 AM">5:00 AM</option><option value="6 AM">6:00 AM</option><option value="7 AM">7:00 AM</option><option value="8 AM">8:00 AM</option><option value="9 AM">9:00 AM</option><option value="10 AM">10:00 AM</option><option value="11 AM">11:00 AM</option><option value="12 PM">12:00 PM</option><option value="1 PM">1:00 PM</option><option value="2 PM">2:00 PM</option><option value="3 PM">3:00 PM</option><option value="4 PM">4:00 PM</option><option value="5 PM">5:00 PM</option><option value="6 PM">6:00 PM</option><option value="7 PM">7:00 PM</option><option value="8 PM">8:00 PM</option><option value="9 PM">9:00 PM</option><option value="10 PM">10:00 PM</option><option value="11 PM">11:00 PM</option><option value="12 AM">12:00 AM</option></select></div></div><div class="col-lg-2 select-half-on" id="end_date_wrapper_1"><label for="inputEmail" class="  control-label">*To </label><div class=" "><select name="to-timing['+day+'][]" class="form-control" id="select" style=" height: 2.2em !important;"><option value="">To</option><option value="1 AM">1:00 AM</option><option value="2 AM">2:00 AM</option><option value="3 AM">3:00 AM</option><option value="4 AM">4:00 AM</option><option value="5 AM">5:00 AM</option><option value="6 AM">6:00 AM</option><option value="7 AM">7:00 AM</option><option value="8 AM">8:00 AM</option><option value="9 AM">9:00 AM</option><option value="10 AM">10:00 AM</option><option value="11 AM">11:00 AM</option><option value="12 PM">12:00 PM</option><option value="1 PM">1:00 PM</option><option value="2 PM">2:00 PM</option><option value="3 PM">3:00 PM</option><option value="4 PM">4:00 PM</option><option value="5 PM">5:00 PM</option><option value="6 PM">6:00 PM</option><option value="7 PM">7:00 PM</option><option value="8 PM">8:00 PM</option><option value="9 PM">9:00 PM</option><option value="10 PM">10:00 PM</option><option value="11 PM">11:00 PM</option><option value="12 AM">12:00 AM</option></select></div></div><div class="col-lg-4"></div></div>');   
}

function readCvURL(input) {

    if (input.files && input.files[0]) {
        var url = input.value;
        var ext = url.substring(url.lastIndexOf('.') + 1).toLowerCase();
        
        if (input.files && input.files[0]&& (ext == "pdf" || ext == "doc" || ext == "docx")) {
            
           /* var reader = new FileReader();
            reader.onload = function (e) {
                $('#pro-pic-id').attr('src', e.target.result);
            }
            reader.readAsDataURL(input.files[0]);*/
            
        }
        else
        {
            alert("Only pdf and word files are accepted.");
            $("#cv").val('');
        }    
        
    }
}

$("#cv").change(function(){
    readCvURL(this);
});

function readIdURL(input) {

    if (input.files && input.files[0]) {
        var url = input.value;
        var ext = url.substring(url.lastIndexOf('.') + 1).toLowerCase();
        
        if (input.files && input.files[0]&& (ext == "gif" || ext == "png" || ext == "jpeg" || ext == "jpg")) {
            
           /* var reader = new FileReader();
            reader.onload = function (e) {
                $('#pro-pic-id').attr('src', e.target.result);
            }
            reader.readAsDataURL(input.files[0]);*/
            
        }
        else
        {
            alert("Only image files are accepted in Id Upload Field.");
            $("#id_card").val('');
        }    
        
    }
}

$("#id_card").change(function(){
    readIdURL(this);
});
</script>