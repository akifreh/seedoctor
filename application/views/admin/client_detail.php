 <div class="content-wrapper">
    <section class="content-header">
      <h1>
        Client Detail
      </h1>
      <ol class="breadcrumb">
        <li><a href="<?php echo base_url();?>admin"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="<?php echo base_url();?>admin/ourClients">Clients</a></li>
        <li class="active">Client Detail</li>
      </ol>
    </section>

    <section class="invoice">
      <div class="row">
        <div class="col-xs-12">
          <h2 class="page-header">
            <i class="fa fa-user-circle-o"></i> <?php echo isset($row->title) ? ucfirst($row->title) : ''; ?>
          </h2>
        </div>
      </div>
      
      <div class="row invoice-info">
        
        <div class="col-sm-4 invoice-col pad_btm_50" >
            <img src="<?php echo $row->logo ? $row->logo : ''; ?>" alt="" class="img-responsive">
        </div>  
          
        <div class="col-sm-4 invoice-col font_16">
            <address>
            <h2><strong>General Info</strong></h2>
            
            <p><strong>Name : </strong><?php echo isset($row->title) ? ucfirst($row->title) : ''; ?></p>
            <p><strong>Website : </strong><a href="<?php echo isset($row->website) ? ucfirst($row->website) : ''; ?>" target="_blank"><?php echo isset($row->website) ? ucfirst($row->website) : ''; ?></a></p>
            <p><strong>Company Size : </strong><?php echo isset($row->company_size) ? $row->company_size : ''; ?></p>
            <p><strong>Email Address : </strong><a href="mailto:<?php echo $row ? $row->email : ''; ?>"><?php echo $row ? $row->email : ''; ?></a></p>
          </address>
        </div>
          
        <div class="col-sm-4 invoice-col font_16">
        
        <address>
            <h2><strong>Details</strong></h2>
            <p><strong>Description : </strong><?php echo isset($row->description) ? $row->description : ''; ?></p>
          </address>
        </div>
      </div>
      <!-- /.row -->

      <!-- Table row -->
      
       <div class="row">
        <?php if(!empty($userOtherProfile)) : ?>

           <div class="col-xs-12 table-responsive pad_btm_40" >
                  <table class="table table-striped">
                      <caption class="table_caption"><strong>Statutory</strong></caption>
                    <thead>
                    <tr>
                      <th>Job Title</th>
                      <th>Company Employed</th>
                      <th>Next of Keen</th>
                      <th>Medical Aid</th>
                    </tr>
                    </thead>

                    <tbody>
                      <?php foreach ($userOtherProfile as $row2) : ?>
                            <tr>
                              <td><?php echo isset($row->job_title) ? $row->job_title : '' ;?></td>
                              <td><?php echo isset($row->company_employed) ? $row->company_employed : '';?></td>
                              <td><?php echo isset($row->next_of_kin) ? $row->next_of_kin : '';?></td>
                              <td><?php echo isset($row->medical_aid) ? $row->medical_aid : ''; ?></td>
                            </tr>
                      <?php endforeach; ?>
                    </tbody>

                  </table>
                </div>
                <!-- /.col -->
              
        <?php endif; ?>
      
      <?php if(!empty($userOtherProfile)) : ?>
              <div class="col-xs-12 table-responsive">
                <table class="table table-striped">
                    <caption class="table_caption"><strong>Other Profile</strong></caption>
                  <thead>
                  <tr>
                    <th>Name</th>
                    <th>Relationship</th>
                    <th>Date of Birth</th>
                  </tr>
                  </thead>

                  <tbody>
                    <?php foreach ($userOtherProfile as $row2) : ?>
                          <tr>
                            <td><?php echo ucfirst($row2->name); ?></td>
                            <td><?php echo ucfirst($row2->relationship); ?></td>
                            <td><?php echo date('d M Y', strtotime($row2->dob)); ?></td>
                          </tr>
                    <?php endforeach; ?>
                  </tbody>

                </table>
              </div>
              <!-- /.col -->
            
      <?php endif; ?>
       </div>      
      
<!--        <div class="row">
         accepted payments column 
        <div class="col-xs-6">
          <p class="lead">Statutory:</p>
          <p class="text-muted well well-sm no-shadow" style="margin-top: 10px;">
            Job Title : <?php echo isset($row->job_title) ? $row->job_title : '' ;?>
            dopplr jibjab, movity jajah plickers sifteo edmodo ifttt zimbra.
          </p>
        </div>
       
      </div>-->
      <!-- /.row -->
    </section>
    <!-- /.content -->
    <div class="clearfix"></div>
  </div>