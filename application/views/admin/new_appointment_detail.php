 <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        User Detail
<!--        <small>#007612</small>-->
      </h1>
      <ol class="breadcrumb">
        <li><a href="<?php echo base_url();?>"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="<?php echo base_url();?>admin/userlist">Admin Dashboard</a></li>
        <li class="active">User Detail</li>
      </ol>
    </section>

<!--    <div class="pad margin no-print">
      <div class="callout callout-info" style="margin-bottom: 0!important;">
        <h4><i class="fa fa-info"></i> Note:</h4>
        This page has been enhanced for printing. Click the print button at the bottom of the invoice to test.
      </div>
    </div>-->

    <!-- Main content -->
    <section class="invoice">
      <!-- title row -->
      <div class="row">
        <div class="col-xs-12">
          <h2 class="page-header">
            <i class="fa fa-user-circle-o"></i> <?php echo ucfirst($patient->fname) . ' '. ucfirst($patient->sname);?> 
            <!--<small class="pull-right">Date: 2/10/2014</small>-->
          </h2>
        </div>
        <!-- /.col -->
      </div>
      
      
      
      <!-- info row -->
      <div class="row invoice-info">
        
          
        <div class="col-sm-3 invoice-col pad_btm_50" >
            <img src="<?php echo base_url()?>uploads/profile/thumbnail/<?php echo $patient->profile_pic ?>" class="img-responsive"/>
        </div>  
          
        <div class="col-sm-3 invoice-col font_16">
            <address>
                <h2><strong>&nbsp;</strong></h2>
            
            <p><strong>Name :</strong> <?php echo ucfirst($patient->fname) . ' '. ucfirst($patient->sname);?> </p>
            <p><strong>Gender : </strong> <?php echo ucfirst($patient->gender);?> </p>
            <p><strong>Age :</strong> <?php echo date_diff(date_create($patient->dob), date_create('today'))->y;?></p>
            <p> <strong>Patient :</strong> <?php echo ucfirst($record->patient); ?></p>
          </address>
        </div>
          
        <div class="col-sm-3 invoice-col font_16">
          
        <address>
            <h2><strong>&nbsp;</strong></h2>
            <p> <strong>Taking Medicine :</strong> <?php echo ucfirst($record->med_taken); ?></p>
            <p><strong>Day :</strong> <?php echo date('d M Y', strtotime($record->start_time)); ?></p>
            <p><strong> Time :</strong> <?php echo date('g:i a', strtotime($record->start_time)) . ' - ' . date('g:i a', strtotime($record->end_time)); ?></p>
            <p> <strong>Brief Description :</strong> <?php echo ucfirst($record->brief_description); ?></p>
          </address>
        </div>
          
        <div class="col-sm-3 invoice-col font_16"></div>  
        
        <!-- /.col -->
      </div>
      <!-- /.row -->

      <div class="row">
            <div class="col-xs-12 pad_btm_20">
                    <a class="complete-profile-btn" href="javascript:history.back()">
                        <button type="button" class="btn btn-a btn-success" >Go Back</button>
                    </a>
                    
                    <?php if($this->session->userdata['logged_in']['role'] == 3) : ?>
                        <a class="complete-profile-btn" href="<?php echo base_url() . 'doctordashboard/userProfile/' . $patient->id; ?>">
                            <button type="button" class="btn btn-a btn-info" >Medical History</button>
                        </a>
                    <?php endif; ?>   
            </div>
          
            <?php if($record->symptom) : ?>
                <div class="col-xs-12">
                  <p class="lead">SYMPTOMS</p>
                  <?php foreach ($record->symptom as $key => $value) : ?>
                        <div class="text-muted well well-sm no-shadow font_18"><strong><?php echo ucfirst($key); ?></strong>
                            <div class="row font_16 pad_top_15">
                                <?php foreach ($value as $val) : ?>
                                      <div class="col-sm-3">
                                          <i class="fa fa-check-circle" aria-hidden="true"></i>&nbsp;&nbsp; <?php echo $val; ?>
                                      </div>
                                <?php endforeach; ?>
                            </div>
                        </div>
                  <?php endforeach; ?>
                </div>
             <?php endif; ?> 
      </div>
      
           
      <!-- Table row -->
      
      <!-- /.row -->
    </section>
    <!-- /.content -->
    <div class="clearfix"></div>
  </div>