


 <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Appointments
        <!--<small>advanced tables</small>-->
      </h1>
      <ol class="breadcrumb">
        <li><a href="<?php echo base_url();?>admin"><i class="fa fa-dashboard"></i> Home</a></li>
        <!--<li><a href="#">Tables</a></li>-->
        <li class="active"><?php echo ucfirst($this->uri->segment(3));?> Appointments</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-xs-12">
          
          <?php if($this->uri->segment(3) == 'upcoming'): ?>  
             <div class="box">
            <div class="box-header">
              <h3 class="box-title">Upcoming Appointments</h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body table-responsive">
              <table id="upcoming_appointment_table" class="table table-bordered table-striped">
                <thead>
                <tr>
                  <th>#</th>
                  <th>Patient</th>
                  <th>Doctor</th>
                  <th>Field</th>
                  <th>Type</th>
                  <th>Date</th>
                  <th>Time</th>
                  <th>&nbsp;</th>
                </tr>
                </thead>
                <tbody>
                    <?php if($upcoming_appointments) :
                         $count = 1; 
                         foreach ($upcoming_appointments as $row) :?>
                    
                        <tr>
                            <td><?php echo $count++; ?></td>
                            <td><?php echo $row->patientInfo->fname; ?></td>
                            <td><?php echo $row->doctorInfo->fname; ?></td>
                            <td><?php echo ucfirst($row->doctorInfo->type_of_doctor); ?></td>
                            <td><?php echo ucfirst($row->type); ?></td>
                            <td><?php echo date('d M Y', strtotime($row->start_time)); ?></td>
                            <td><?php echo date('h:i a', strtotime($row->start_time)) . ' - ' . date('h:i a', strtotime($row->end_time)); ?></td>
                            <td><a href="<?php echo base_url() . 'appointment/detail/'. $row->id; ?>">
                                     <button type="button" class="btn btn-a btn-info" >details</button>
                                </a>
                            </td>
                         
                        </tr>
                         <?php  endforeach; 
                                endif; ?>
                </tbody>
              </table>
            </div>
            <!-- /.box-body -->
          </div>
          <?php endif; ?> 
            
          <?php if($this->uri->segment(3) == 'finished'): ?>
            <div class="box">
            <div class="box-header">
              <h3 class="box-title">Finished Appointments</h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body table-responsive">
              <table id="finish_appointment_table" class="table table-bordered table-striped">
                <thead>
                <tr>
                  <th>#</th>
                  <th>Patient</th>
                  <th>Doctor</th>
                  <th>Field</th>
                  <th>Type</th>
                  <th>Date</th>
                  <th>Time</th>
                  <!--th>User Location MOVED TO DETAIL PAGE</th-->
                  <th>&nbsp;</th>
                </tr>
                </thead>
                <tbody>
                    <?php if($finished_appointments) : 
                         $count = 1; 
                         foreach ($finished_appointments as $row) :?>
                        <tr>
                            <td><?php echo $count++; ?></td>
                            <td><?php echo $row->patientInfo->fname; ?></td>
                            <td><?php echo $row->doctorInfo->fname; ?></td>
                            <td><?php echo ucfirst($row->doctorInfo->type_of_doctor); ?></td>
                            <td><?php echo ucfirst($row->type); ?></td>
                            <td><?php echo date('d M Y', strtotime($row->start_time)); ?></td>
                            <td><?php echo date('h:i a', strtotime($row->start_time)) . ' - ' . date('h:i a', strtotime($row->end_time)); ?></td>
                            <td><a href="<?php echo base_url() . 'appointment/detail/'. $row->id; ?>">
                                    <button type="button" class="btn btn-a btn-info" >details</button>
                                </a>
                            </td>
                        </tr>
                         <?php  endforeach; 
                                endif; ?>
                </tbody>
              </table>
            </div>
            <!-- /.box-body -->
          </div>
          <?php endif; ?>  
          
          <?php if($this->uri->segment(3) == 'missed'): ?>
            <div class="box">
            <div class="box-header">
              <h3 class="box-title">Missed Appointments</h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body table-responsive">
              <table id="missed_appointment_table" class="table table-bordered table-striped">
                <thead>
                <tr>
                  <th>#</th>
                  <th>Patient</th>
                  <th>Doctor</th>
                  <th>Field</th>
                  <th>Type</th>
                  <th>Date</th>
                  <th>Time</th>
                  <th>&nbsp;</th>
                </tr>
                </thead>
                <tbody>
                    <?php if($missed_appointments) :
                         $count = 1; 
                         foreach ($missed_appointments as $row) :?>
                        <tr>
                            <td><?php echo $count++; ?></td>
                            <td><?php echo $row->patientInfo->fname; ?></td>
                            <td><?php echo $row->doctorInfo->fname; ?></td>
                            <td><?php echo ucfirst($row->doctorInfo->type_of_doctor); ?></td>
                            <td><?php echo ucfirst($row->type); ?></td>
                            <td><?php echo date('d M Y', strtotime($row->start_time)); ?></td>
                            <td><?php echo date('h:i a', strtotime($row->start_time)) . ' - ' . date('h:i a', strtotime($row->end_time)); ?></td>
                            <td><a href="<?php echo base_url() . 'appointment/detail/'. $row->id; ?>">
                                    <button type="button" class="btn btn-a btn-info" >details</button>
                                </a>
                            </td>
                        </tr>
                         <?php  endforeach; 
                                endif; ?>
                </tbody>
              </table>
            </div>
            <!-- /.box-body -->
          </div>
          <?php endif; ?>
            
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
  </div>






  
