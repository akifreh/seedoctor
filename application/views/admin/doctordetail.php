<div class="medical-pro-section">
<div class="container">
    <div class="row">
<?php echo $this->session->flashdata('message'); ?>

                    <div class="medical-history-detail">
                <div class="row">
                <div class="col-lg-3 col-md-4 col-sm-4">
                  <div class="user-pro-pic-main" style="    height: 260px; width: 90%;"> <img src="<?php echo $row ? $row->profile_pic : ''?>"/></div>
                </div>
                <div class="col-lg-3 col-md-4 col-sm-8 ">
                <h3 style="border-bottom:#0484cf solid 2px; padding-bottom:5px;">Personal Detail</h3>
                 
                <div class="doc-day-time-profile"> <div class="left-col"><strong>Name :</strong></div> <div class="right-col"><?php echo $row ? ucfirst($row->fname) . ' '. ucfirst($row->sname) : '';?> </div></div>

                  <div class="doc-day-time-profile"> <div class="left-col"> <strong> Email :</strong> </div> <div class="right-col"><?php echo $row ? $row->email : '';?></div></div>
                   <div class="doc-day-time-profile"> <div class="left-col"><strong>Phone :</strong> </div> <div class="right-col"><?php echo $row ? $row->phone : '';?></div></div>
                 <div class="doc-day-time-profile"> <div class="left-col"><strong> Address : </strong> </div> <div class="right-col"><?php echo $row ? $row->address : '';?> </div></div>
                 <div class="doc-day-time-profile"> <div class="left-col"><strong> City : </strong>  </div><div class="right-col"><?php echo $row ? $row->city : '';?> </div></div>
                  <div class="doc-day-time-profile"> <div class="left-col"><strong> Province : </strong> </div> <div class="right-col"><?php echo $row ? $row->province : '';?> </div></div>
                 </div>
                  
                  
                  <div class="col-lg-3 col-md-4 ">
                    <h3 style="border-bottom:#0484cf solid 2px; padding-bottom:5px;">Day & Timings</h3>
                 
                    <!--<div><strong> Speciality :</strong> <?php //echo $row ? $row->speciality : '';?> </div>-->
                    <div> 
                        <?php if($doctorTimings) : ?>
                            
                            <?php foreach($doctorTimings as $key => $v) : ?>
                                <div class="doc-day-time-profile"> 
                                <div class="left-col">
                                <?php switch ($key) {
                                case '1':
                                  echo '<strong>Monday :</strong>';
                                  break;
                                case '2':
                                  echo '<strong>Tuesday :</strong>';
                                  break;
                                case '3':
                                  echo '<strong>Wednesday :</strong>';
                                  break;
                                case '4':
                                  echo '<strong>Thursday :</strong>';
                                  break;
                                case '5':
                                  echo '<strong>Friday :</strong>';
                                  break;
                                case '6':
                                  echo '<strong>Saturday :</strong>';
                                  break;
                                case '7':
                                  echo '<strong>Sunday :</strong>';
                                  break;
                              }?>  
                              </div>
                              <div class="right-col"> 
                              <?php foreach ($v as $key => $doctorTiming) : ?>
                                <span><?php echo date('g:i a', strtotime($doctorTiming->start_time)) . ' - ' .  date('g:i a', strtotime($doctorTiming->end_time)); ?></span>  
                                <?php endforeach; ?>
                              </div>
                              </div>
                            <?php endforeach;?> 
                            
                            <?php endif; ?></div>
                  
                 </div>
                 
                 <div class="col-lg-3 ">
                    <h3 style="border-bottom:#0484cf solid 2px; padding-bottom:5px;">Other</h3>
                   <div><strong>Type of Practitioner : </strong><?php echo $row ? ucfirst($row->type_of_doctor) : '';?> </div>
                    <?php if($row->cv) : ?>
                    <div><strong> Cv :</strong> <a href="<?php echo base_url() ?>uploads/cv/<?php echo $row->cv;?>" download>Download cv </a> </div>
                   <?php endif; ?>
                   <?php if($row->id_card) : ?>
                    <div class="user-pro-pic-main" style="    height: 260px; width: 90%;"> <img src="<?php echo $row ? $row->id_card : ''?>"/>    </div>
                    <?php endif; ?>
                   <div><a href="<?php echo base_url() . 'user/changePassword'; ?>">Change Password</a></div>
                 </div>
                
                <div class="col-lg-12">
                  <h3 style="border-bottom:#0484cf solid 2px; padding-bottom:5px;">Biography</h3>
                 	 <p><?php echo $row->description; ?></p>
                 </div>
                 <div class="col-lg-12">
                 <h3 style="border-bottom:#0484cf solid 2px; padding-bottom:5px;">Academic qualifications</h3>
                 	 <div ><ol class="acadmic-info-ol"><?php if($row->academics) : foreach($row->academics as $value) :?>
<li class="acadmic-info">
                 <h4> <?php echo $value->qualification ? $value->qualification : '';?></h4>
                  <div  class="institution-info"><?php echo $value->institution ? $value->institution : '';?></div>
                 <div  class="date-info"> <?php echo $value->year ? $value->year : '';?><br/></div>
                  </li>
                  <?php endforeach; endif; ?></ol></div>
                 </div>
                 <div class="col-lg-12">
                 <h3 style="border-bottom:#0484cf solid 2px; padding-bottom:5px;">Working Experience</h3>
                 	 
                    <div ><?php if($row->experience) : foreach($row->experience as $value) :
                        
                        
                        
                       //echo $value->present_here.'<br/>';
                        
                        ?>
                    <div class="experience-info">
                      <h4>  <?php echo $value->job_title ? $value->job_title : '';?></h4>
                      <div class="company-info">  <?php echo $value->company ? $value->company : '';?></div>
                      <div class="date-info"><?php echo $value->start_date ? date('M Y', strtotime($value->start_date)) : '';?> - <?php if($value->present_here == 1) {echo "Present"; } else { echo $value->end_date ? date('M Y', strtotime($value->end_date)) . '<br/>' : ''; } ?></div>
                    </div>
                  <?php endforeach; endif; ?></div>
                 </div>
                 </div>
                 
                      <?php if($doctorStatus == 0) : ?>
                        <a class="complete-profile" href="<?php echo base_url() ?>admin/getDoctorEmail/<?php echo $row->user_id ?>">Approve</a>
                     <?php endif; ?>

                     <?php if(isset($editProfile) && ($editProfile)) : ?>
                        <a class="complete-profile" href="<?php echo base_url() ?>doctordashboard/editProfile">Edit Profile</a>
                     <?php endif; ?>
                    </div>
</div>
    </div>
</div>
</div>