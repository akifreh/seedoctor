<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Update Doctor Detail      
      </h1>
      <ol class="breadcrumb">
        <li><a href="<?php echo base_url();?>admin"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="<?php echo base_url();?>admin/userlist/doctor">Doctors List</a></li>
        <li class="active">Update Doctor Detail</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">

     <?php $days = ['Mon' => '1', 'Tue' => '2', 'Wed' => '3',  'Thu' => '4', 'Fri' => '5', 'Sat' => '6', 'Sun' => '7'];
     
//     echo '<pre>'; print_r(count($doctorAvailableDays));die;
//      $days = ['Mon' => 'monday', 'Tue' => 'tuesday', 'Wed' => 'wednesday',  'Thu' => 'thursday', 'Fri' => 'friday', 'Sat' => 'saturday', 'Sun' => 'sunday']
     ?>
     <?php $attributes = array("name" => "doctorinfo" , "id" => "doctorinfo", "enctype" => 'multipart/form-data');
       echo form_open("admin/Savedoctorprofile/$id", $attributes);?>
       
        
  
       <style type="text/css">
         .add-pro-pic-link {
    width: 100%;
    padding: 8%;
    background: rgba(4,132,207, 0.8);
    color: #fff;
    position: absolute;
    bottom: 0px;
    font-size: 16px;
    cursor: pointer; 
    left: 0px;
}
.user-pro-pic-main {
    height: 210px;
    overflow: hidden;
    width: auto;
    display: inline-block;
    position: relative;
}
.user-pro-pic-main img {
    height: 100%;
    width: auto;
    top: 50%;
    left: 50%;
    position: relative;
    transform: translateX(-50%) translateY(-50%);
    -ms-transform: translateX(-50%) translateY(-50%);
    -webkit-transform: translateX(-50%) translateY(-50%);
    -moz-transform: translateX(-50%) translateY(-50%);
    -o-transform: translateX(-50%) translateY(-50%);
}
#add-pro-pic {
    display: none;
}
 
       </style>
       
             <script type="text/javascript">
                $(document).ready(function(){

   $(".add-pro-pic-link").click(function() {
    $("#add-pro-pic").trigger('click');
  }); 
    });
             </script>
<!--        Personal  details  -->
      <div class="box box-default ">
        <div class="box-header with-border" data-widget="collapse">
          <h3 class="box-title">Personal Details</h3>

          <div class="box-tools pull-right">
            <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
            <!--<button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-remove"></i></button>-->
          </div>
        </div>
         <!--/.box-header--> 
        <div class="box-body">
          <div class="row">
             <!--/.col--> 
             <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12  padding-right-0">
                <div class="user-pro-pic-main">
                    <img src="<?php echo isset($record->profile_pic) ? $record->profile_pic : ''; ?>" id="pro-pic-id" alt="Profile-Image"  >
                          <input type="file" name="profile_pic" id="add-pro-pic" class="add-profile-pic"> 
                          <span class="add-pro-pic-link"><i class="fa fa-camera" aria-hidden="true"></i> Edit Photo</span>
                      
                       <!--<div class="prfoile-pic-update" style="display:none;" >Please Click Update Button to Change your Profile Picture</div>-->
                </div>
            </div>

            <div class="col-md-6">
                <div class="form-group">
                    <label for="">Name<span>*</span></label>
                  <input placeholder="Enter Name" name="fname" type="text" value="<?php echo isset($record->fname) ? $record->fname : ''; ?>" class="form-control" >
                </div>
                
                <div class="form-group">
                  <label for="">Email<span>*</span></label>
                  <input placeholder="Enter Email" name="email" type="text" value="<?php echo isset($record->email) ? $record->email : ''; ?>" class="form-control" >
                </div>
                
                <div class="form-group">
                  <label for="">Address<span>*</span></label>
                  <input placeholder="Enter Address" type="text" name="address" class="form-control" value="<?php echo isset($record->address) ? $record->address : ''; ?>">
                </div>
                
                <div class="form-group">
                  <label for="">Province</label>
                  <input placeholder="Enter Province" type="text" name="province" class="form-control" value="<?php echo isset($record->province) ? $record->province : ''; ?>">
                </div>
                
                <div class="form-group">
                  <label for="">Upload CV</label>
                  <input name="cv" type="file" value="" id="cv" class="form-control">
                </div>
               <!--/.form-group--> 
            </div>
            
            <div class="col-md-6">
                <div class="form-group">
                  <label for="">Surname<span>*</span></label>
                  <input placeholder="Enter Surname" name="sname" type="text" value="<?php echo isset($record->sname) ? $record->sname : ''; ?>" class="form-control" >
                </div>
                
                <div class="form-group">
                  <label for="">Phone<span>*</span></label>
                  <input placeholder="Enter Phone" name="phone" type="text" value="<?php echo isset($record->phone) ? $record->phone : ''; ?>" class="form-control" >
                </div>
                
                <div class="form-group">
                  <label for="">City</label>
                  <input placeholder="Enter City" type="text" name="city" class="form-control" value="<?php echo isset($record->city) ? $record->city : ''; ?>">
                </div>
                
                <div class="form-group">
                  <label for="">Gender<span>*</span></label>
                  <div>
                      <input type="radio" class="minimal gender" name="gender" value="male" <?php echo isset($record->gender) && ($record->gender == 'male') ? 'checked' : ''; ?>> Male
                      <input type="radio" class="minimal gender" name="gender" value="female" <?php echo isset($record->gender) && ($record->gender == 'female') ? 'checked' : ''; ?>> Female
                  </div>
                </div>   
               <!--/.form-group--> 
            </div>
            
            
             <!--/.col--> 
          </div>
           <!--/.row--> 
        </div>
         <!--/.box-body--> 
        
      </div>
       <!--/.box--> 
      
         <!--Biography EXAMPLE--> 
      <div class="box box-default ">
        <div class="box-header with-border" data-widget="collapse">
          <h3 class="box-title">Biography</h3>

          <div class="box-tools pull-right">
            <button type="button" class="btn btn-box-tool" ><i class="fa fa-minus"></i></button>
            <!--<button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-remove"></i></button>-->
          </div>
        </div>
         <!--/.box-header--> 
        <div class="box-body">
          <div class="row">
             <!--/.col--> 
            <div class="col-md-12">
                <div class="form-group">
                    <label>Description </label><span> (Maximum 150 Words)</span>
                    <textarea rows="5" placeholder="Enter Description ..." class="form-control" id="content" name="description" ><?php echo isset($record->description) ? $record->description : ''; ?></textarea>
                </div>
               <!--/.form-group--> 
            </div>
             <!--/.col--> 
          </div>
           <!--/.row--> 
        </div>
         <!--/.box-body--> 
        
      </div>
       
         <!--Type EXAMPLE--> 
      <div class="box box-default">
        <div class="box-header with-border" data-widget="collapse">
          <h3 class="box-title">Type</h3>

          <div class="box-tools pull-right">
            <button type="button" class="btn btn-box-tool" ><i class="fa fa-minus"></i></button>
            <!--<button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-remove"></i></button>-->
          </div>
        </div>
         <!--/.box-header--> 
        <div class="box-body">
          <div class="row">
             <!--/.col--> 
            <div class="col-md-6">
                
                 <div class="form-group">
                  <label for="">Profession Number</label>
                  <input placeholder="Enter Profession Number" name="profession_number" type="text" value="<?php echo isset($record->profession_number) ? $record->profession_number : ''; ?>" class="form-control">
                </div>
                
                <div class="form-group">
                  <label for="">Practice Number</label>
                  <input placeholder="Enter Practice Number" name="practice_number" type="text" value="<?php echo isset($record->practice_number) ? $record->practice_number : ''; ?>" class="form-control">
                </div>
               <!--/.form-group--> 
            </div>
            
            
            
            <div class="col-md-6">
                <div class="form-group">
                  <label for=""> Speciality </label>
                  <input placeholder="Enter Speciality" name="speciality" type="text" value="<?php echo isset($record->speciality) ? $record->speciality : ''; ?>" class="form-control">
                </div>
                
                <div class="form-group">
                  <label for="">Area of training<span>*</span></label>
                  <div>
                        <input class="minimal" id="type-of-doctor-2" type="radio" name="type_of_doctor" value="2" <?php echo isset($record->type_of_doctor) && ($record->type_of_doctor == 'psychologist') ? 'checked' : '' ;?>> Psychologist  
                        <input class="minimal" id="type-of-doctor-1" type="radio" name="type_of_doctor" value="1" <?php echo isset($record->type_of_doctor) && ($record->type_of_doctor == 'dietitian') ? 'checked' : '' ;?>> Dietitian 
                        <input class="minimal" id="type-of-doctor-3" type="radio" name="type_of_doctor" value="3" <?php echo isset($record->type_of_doctor) && ($record->type_of_doctor == 'medical-doctor') ? 'checked' : '' ;?>> Medical Doctor
                  </div>
                </div>  
                
               <!--/.form-group--> 
            </div>
            
            
             <!--/.col--> 
          </div>
           <!--/.row--> 
        </div>
         <!--/.box-body--> 
        
      </div>  
       
          
           <!--Academic qualifications--> 
      <div class="box box-default ">
        <div class="box-header with-border" data-widget="collapse">
          <h3 class="box-title">Academic qualifications</h3>

          <div class="box-tools pull-right">
            <button type="button" class="btn btn-box-tool" ><i class="fa fa-minus"></i></button>
<!--            <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-remove"></i></button>-->
          </div>
        </div>
         <!--/.box-header--> 
        <div class="box-body">
          <div class="row">
             <!--/.col--> 
            
            <?php if($record->academics) : 
                    $count = 1;
                    foreach($record->academics as $key => $row) : 
            ?>
             <div>
                <input type="hidden" name="aqcount" id="aqcount" value="<?php echo count($record->academics) +1; ?>" />
                <div class="col-lg-12"><span><?php $count; $count++;?></span></div>
                <div class="col-md-4">
                   <div class="form-group">
                      <label for="">Qualification<span>*</span></label>
                      <input type="text" placeholder="Enter Qualification" name="qualification[]" value="<?php echo isset($row->qualification) ? $row->qualification : ''; ?>" class="form-control" >
                   </div> 
                </div>                 

                <div class="col-md-4">
                   <div class="form-group">
                      <label for="">Institution<span>*</span></label>
                      <input placeholder="Enter Institution" name="institution[]" type="text" value="<?php echo isset($row->institution) ? $row->institution : ''; ?>" class="form-control" >
                   </div> 
                </div>                 

                <div class="col-md-3">
                     <div class="bootstrap-timepicker">
                        <div class="form-group">
                            <label>Year<span>*</span></label>
                            <div class="input-group">
                              <div class="input-group-addon">
                                <i class="fa fa-calendar"></i>
                              </div>
                                <input type="text" name="year[]"  class="form-control pull-right" id="academic_year" value="<?php echo isset($row->year) ? $row->year : ''; ?>">
                            </div>
                             <!--/.input group--> 
                          </div>
                      </div>

                </div>  

                <div class="col-md-1">
                    <div class="top-space"></div>
                    <div class="doctor-academic"><i class="fa fa-remove"></i></div>
                </div> 
            </div>
                <?php endforeach; ?>
                <?php else : ?>
             
             <div>
                    <input type="hidden" name="aqcount" id="aqcount" value="2" /> 
            
                    <div class="col-md-4">
                       <div class="form-group">
                          <label for="">Qualification<span>*</span></label>
                          <input type="text"  placeholder="Enter Qualification" name="qualification[]" value="<?php echo isset($row->qualification) ? $row->qualification : ''; ?>" class="form-control" >
                       </div> 
                    </div>                 

                    
                    <div class="col-md-4">
                       <div class="form-group">
                          <label for="">Institution<span>*</span></label>
                          <input placeholder="Enter Institution" name="institution[]" type="text" value="<?php echo isset($row->institution) ? $row->institution : ''; ?>" class="form-control" >
                       </div> 
                    </div>                 

                    <div class="col-md-3">
                         <div class="bootstrap-timepicker">
                            <div class="form-group">
                                <label>Year<span>*</span></label>
                                <div class="input-group">
                                  <div class="input-group-addon">
                                    <i class="fa fa-calendar"></i>
                                  </div>
                                    <input type="text" name="year[]" class="form-control pull-right" id="academic_year">
                                </div>
                                 <!--/.input group--> 
                              </div>
                          </div>

                    </div>  
                    
                    <div class="col-md-1">
                    <div class="top-space"></div>
                    <div class="doctor-academic"><i class="fa fa-remove"></i></div>
                </div> 
                    
             </div>    
                <?php endif; ?>
            
            <div id="aq-repeter"></div>
            
            <div class="add-academic col-lg-12">
                <a href="javascript:void(0)" class="pull-right add-more-academic-link btn btn-success">
                    Add More Academic
                </a>
            </div>
            
             <!--/.col--> 
          </div>
           <!--/.row--> 
        </div>
<!--         /.box-body -->
        
      </div>
       <!--/.box-->  
         
        <!--Working Experience--> 
      <div class="box box-default">
        <div class="box-header with-border" data-widget="collapse">
          <h3 class="box-title">Working Experience</h3>

          <div class="box-tools pull-right">
            <button type="button" class="btn btn-box-tool" ><i class="fa fa-minus"></i></button>
            <!--<button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-remove"></i></button>-->
          </div>
        </div>
         <!--/.box-header--> 
        <div class="box-body">
          <div class="row">
             <!--/.col--> 
            
             <?php
               
                if($record->experience) :
                    $count=1;
                    $checkUniq = 1;
                    $linkButtonStatus = '';
            
                    foreach($record->experience as $key => $row) : ?>
                     
                    <div>
                        <input type="hidden" name="wecount" id="wecount" value="<?php echo count($record->experience) + 1;?>" /> 
                        <div class="count-we col-lg-12"><span><?php $count; $count += 1;?></span></div>

                        <div class="col-md-3">
                           <div class="form-group">
                              <label for="">Company<span>*</span></label>
                              <input placeholder="Enter Company" name="company[]" type="text" value="<?php echo isset($row->company) ? $row->company : ''; ?>" class="form-control" >
                           </div> 
                        </div>                 

                        <div class="col-md-2">
                           <div class="form-group">
                              <label for="">Job Title<span>*</span></label>
                              <input placeholder="Enter Job Title" name="job_title[]" type="text" value="<?php echo isset($row->job_title) ? $row->job_title : ''; ?>" class="form-control" >
                           </div> 
                        </div>                 

                        <div class="col-md-2">
                             <div class="bootstrap-timepicker">
                                <div class="form-group">
                                    <label>Start Date<span>*</span></label>
                                    <div class="input-group">
                                      <div class="input-group-addon">
                                        <i class="fa fa-calendar"></i>
                                      </div>
                                        <input  name="work_from_mm_yy[]" type="text" class="form-control work_from_mm_yy" value="<?php echo ($row->start_date != '0000-00-00') ? date('Y-m', strtotime($row->start_date)) : ''; ?>">
                                    </div>
                                     <!--/.input group--> 
                                  </div>
                              </div>

                            </div>  


                        <div class="col-md-2">
                         <div class="bootstrap-timepicker" id="end_date_wrapper_<?php echo $checkUniq; ?>" style="display:<?php echo ($row->present_here == 1) ? 'none' : 'block'; ?>">
                            <div class="form-group">
                                <label>End Date<span>*</span></label>

                                <div class="input-group">
                                  <div class="input-group-addon">
                                    <i class="fa fa-calendar"></i>
                                  </div>
                                  <input type="text" name="work_to_mm_yy[]" class="form-control work_to_mm_yy" value="<?php echo ($row->end_date != '0000-00-00') ? date('Y-m', strtotime($row->end_date)) : ''; ?>">
                                </div>
                                 <!--/.input group--> 
                              </div>
                          </div>
                        </div>        


                        <div class="col-md-2">
                           <div class="form-group">
                               <div class="top-space"></div> 
                              <label for="">Present</label>
                              <input name="stay_here[]" type="checkbox" value="<?php echo $checkUniq; ?>" class="pull-left present_here " id="present_here_<?php echo $checkUniq; ?>" <?php echo ($row->present_here == 1) ? 'checked' : ''; ?>  
                                        data-indexof="<?php echo $checkUniq; ?>" onclick="present_here(this.id);" <?php   if(!$row->present_here): echo  "disabled='disabled'";   endif; ?> />
                           </div> 
                        </div>

                        <div class="col-md-1">
                        <div class="top-space"></div>
                        <div class="doctor-academic"><i class="fa fa-remove"></i></div>
                        </div>

                    </div>
            
               <?php $checkUniq++; endforeach; ?>
                <?php else : ?>
             
                <div>
                    <input type="hidden" name="wecount" id="wecount" value="2" /> 
                    <div class="col-md-3">
                        <div class="form-group">
                           <label for="">Company<span>*</span></label>
                           <input placeholder="Enter Company" name="company[]" type="text" value="<?php echo isset($row->company) ? $row->company : ''; ?>" class="form-control" >
                        </div> 
                     </div>                 

                     <div class="col-md-2">
                        <div class="form-group">
                           <label for="">Job Title<span>*</span></label>
                           <input placeholder="Enter Job Title" name="job_title[]" type="text" value="<?php echo isset($row->job_title) ? $row->job_title : ''; ?>" class="form-control" >
                        </div> 
                     </div>                 

                     <div class="col-md-2">
                          <div class="bootstrap-timepicker">
                             <div class="form-group">
                                 <label>Start Date<span>*</span></label>
                                 <div class="input-group">
                                   <div class="input-group-addon">
                                     <i class="fa fa-calendar"></i>
                                   </div>
                                     <input type="text" class="form-control work_from_mm_yy" name="work_from_mm_yy[]" >
                                 </div>
                                  <!--/.input group--> 
                               </div>
                           </div>

                         </div>  

                     <div class="col-md-2">
                      <div class="bootstrap-timepicker">
                         <div class="form-group">
                             <label>End Date<span>*</span></label>

                             <div class="input-group">
                               <div class="input-group-addon">
                                 <i class="fa fa-calendar"></i>
                               </div>
                               <input  type="text" name="work_to_mm_yy[]" class="form-control work_to_mm_yy" >
                             </div>
                              <!--/.input group--> 
                           </div>
                       </div>

                     </div>  

                     <div class="col-md-2">
                        <div class="form-group">
                            <div class="top-space"></div> 
                           <label for="">Present</label>
                           <input  name="stay_here[]" type="checkbox" value="1"  class="present_here pull-left " id="present_here_1" data-indexof="1" onclick="present_here(this.id);" />
                        </div> 
                     </div>
                    
                    <div class="col-md-1">
                        <div class="top-space"></div>
                        <div class="doctor-academic"><i class="fa fa-remove"></i></div></div>
                        
                 </div>   
                <?php endif; ?>
             
            <div id="we-repeter"></div>
            
            <div class="add-academic col-lg-12">
                <a href="javascript:void(0)" class="pull-right add-more-exp-link btn btn-success">
                    Add More Experience
                </a>
            </div>
            
             <!--/.col--> 
          </div>
           <!--/.row--> 
        </div>
         <!--/.box-body--> 
       
      </div>
       <!--/.box--> 
       
    <div style="border-top-color: #d2d6de;position: relative; border-radius: 3px; background: #ffffff; border-top: 3px solid #d2d6de; margin-bottom: 20px; width: 100%; box-shadow: 0 1px 1px rgba(0,0,0,0.1);">
      <div class="box-header with-border" data-widget="collapse">
        <h3 class="box-title">Days & Timing</h3>
      
    <div class="row">
        <?php $hasTimings = FALSE; if(isset($doctorTimings[1])) $hasTimings = TRUE; ?>
        <div class="col-sm-2"><input type="checkbox" name="dayoftheweek[]" class="mytime" value="1" <?php echo $hasTimings ? 'checked' : ''; ?>/><label>&nbsp;Monday</label></div>
        <div id="timings-1" style="<?php echo $hasTimings ? '' :  'display:none;'; ?>">
        <?php if(!$hasTimings) : ?>
            <?php $doctorTimings[1] = array('abc'); $value = NULL; ?>
        <?php endif; ?>
        <?php $count=0 ; foreach ($doctorTimings[1] as $key => $value) : ?>
        <div class="time-repeat" id="repeatID2">
            <?php $count++; ?>
            <?php if($count == 1) : ?>
            <div class="col-lg-4 select-half-on"></div>
            <?php else : ?>
            <div class="col-lg-2 select-half-on"></div>
            <?php endif; ?>                        
            <div class="col-lg-2 select-half-on">
                <label for="inputEmail" class="  control-label">*From </label>
                <div class=" ">
                <select name="from-timimg[1][]" class="form-control" id="select" style=" height: 2.2em !important;">
                    <option value="">-From-</option>
                    <option value="1 AM"<?php echo ($value && $value->start_time == '01:00:00') ? 'selected' : ''; ?>>1:00 AM</option>
                    <option value="2 AM"<?php echo ($value && $value->start_time == '02:00:00') ? 'selected' : ''; ?>>2:00 AM</option>
                    <option value="3 AM"<?php echo ($value && $value->start_time == '03:00:00') ? 'selected' : ''; ?>>3:00 AM</option>
                    <option value="4 AM"<?php echo ($value && $value->start_time == '04:00:00') ? 'selected' : ''; ?>>4:00 AM</option>
                    <option value="5 AM"<?php echo ($value && $value->start_time == '05:00:00') ? 'selected' : ''; ?>>5:00 AM</option>
                    <option value="6 AM"<?php echo ($value && $value->start_time == '06:00:00') ? 'selected' : ''; ?>>6:00 AM</option>
                    <option value="7 AM"<?php echo ($value && $value->start_time == '07:00:00') ? 'selected' : ''; ?>>7:00 AM</option>
                    <option value="8 AM"<?php echo ($value && $value->start_time == '08:00:00') ? 'selected' : ''; ?>>8:00 AM</option>
                    <option value="9 AM"<?php echo ($value && $value->start_time == '09:00:00') ? 'selected' : ''; ?>>9:00 AM</option>
                    <option value="10 AM"<?php echo ($value && $value->start_time == '10:00:00') ? 'selected' : ''; ?>>10:00 AM</option>
                    <option value="11 AM"<?php echo ($value && $value->start_time == '11:00:00') ? 'selected' : ''; ?>>11:00 AM</option>
                    <option value="12 PM"<?php echo ($value && $value->start_time == '12:00:00') ? 'selected' : ''; ?>>12:00 PM</option>
                    <option value="1 PM"<?php echo ($value && $value->start_time == '13:00:00') ? 'selected' : ''; ?>>1:00 PM</option>
                    <option value="2 PM"<?php echo ($value && $value->start_time == '14:00:00') ? 'selected' : ''; ?>>2:00 PM</option>
                    <option value="3 PM"<?php echo ($value && $value->start_time == '15:00:00') ? 'selected' : ''; ?>>3:00 PM</option>
                    <option value="4 PM"<?php echo ($value && $value->start_time == '16:00:00') ? 'selected' : ''; ?>>4:00 PM</option>
                    <option value="5 PM"<?php echo ($value && $value->start_time == '17:00:00') ? 'selected' : ''; ?>>5:00 PM</option>
                    <option value="6 PM"<?php echo ($value && $value->start_time == '18:00:00') ? 'selected' : ''; ?>>6:00 PM</option>
                    <option value="7 PM"<?php echo ($value && $value->start_time == '19:00:00') ? 'selected' : ''; ?>>7:00 PM</option>
                    <option value="8 PM"<?php echo ($value && $value->start_time == '20:00:00') ? 'selected' : ''; ?>>8:00 PM</option>
                    <option value="9 PM"<?php echo ($value && $value->start_time == '21:00:00') ? 'selected' : ''; ?>>9:00 PM</option>
                    <option value="10 PM"<?php echo ($value && $value->start_time == '22:00:00') ? 'selected' : ''; ?>>10:00 PM</option>
                    <option value="11 PM"<?php echo ($value && $value->start_time == '23:00:00') ? 'selected' : ''; ?>>11:00 PM</option>
                    <option value="12 AM"<?php echo ($value && ($value->start_time == '24:00:00' || $value->start_time=='00:00:00')) ? 'selected' : ''; ?>>12:00 PM</option>
                </select>
                </div>
            </div>

            <div class="col-lg-2 select-half-on" id="end_date_wrapper_1">
            <label for="inputEmail" class="  control-label">*To </label>
            <div class=" ">
                <select name="to-timing[1][]" class="form-control" id="select" style=" height: 2.2em !important;">
                    <option value="">-To-</option>
                    <option value="1 AM"<?php echo ($value && $value->end_time =='01:00:00') ? 'selected' : ''; ?>>1:00 AM</option>
                    <option value="2 AM"<?php echo ($value && $value->end_time =='02:00:00') ? 'selected' : ''; ?>>2:00 AM</option>
                    <option value="3 AM"<?php echo ($value && $value->end_time =='03:00:00') ? 'selected' : ''; ?>>3:00 AM</option>
                    <option value="4 AM"<?php echo ($value && $value->end_time =='04:00:00') ? 'selected' : ''; ?>>4:00 AM</option>
                    <option value="5 AM"<?php echo ($value && $value->end_time =='05:00:00') ? 'selected' : ''; ?>>5:00 AM</option>
                    <option value="6 AM"<?php echo ($value && $value->end_time =='06:00:00') ? 'selected' : ''; ?>>6:00 AM</option>
                    <option value="7 AM"<?php echo ($value && $value->end_time =='07:00:00') ? 'selected' : ''; ?>>7:00 AM</option>
                    <option value="8 AM"<?php echo ($value && $value->end_time =='08:00:00') ? 'selected' : ''; ?>>8:00 AM</option>
                    <option value="9 AM"<?php echo ($value && $value->end_time =='09:00:00') ? 'selected' : ''; ?>>9:00 AM</option>
                    <option value="10 AM"<?php echo ($value && $value->end_time =='10:00:00') ? 'selected' : ''; ?>>10:00 AM</option>
                    <option value="11 AM"<?php echo ($value && $value->end_time =='11:00:00') ? 'selected' : ''; ?>>11:00 AM</option>
                    <option value="12 PM"<?php echo ($value && $value->end_time =='12:00:00') ? 'selected' : ''; ?>>12:00 PM</option>
                    <option value="1 PM"<?php echo ($value && $value->end_time =='13:00:00') ? 'selected' : ''; ?>>1:00 PM</option>
                    <option value="2 PM"<?php echo ($value && $value->end_time =='14:00:00') ? 'selected' : ''; ?>>2:00 PM</option>
                    <option value="3 PM"<?php echo ($value && $value->end_time =='15:00:00') ? 'selected' : ''; ?>>3:00 PM</option>
                    <option value="4 PM"<?php echo ($value && $value->end_time =='16:00:00') ? 'selected' : ''; ?>>4:00 PM</option>
                    <option value="5 PM"<?php echo ($value && $value->end_time =='17:00:00') ? 'selected' : ''; ?>>5:00 PM</option>
                    <option value="6 PM"<?php echo ($value && $value->end_time =='18:00:00') ? 'selected' : ''; ?>>6:00 PM</option>
                    <option value="7 PM"<?php echo ($value && $value->end_time =='19:00:00') ? 'selected' : ''; ?>>7:00 PM</option>
                    <option value="8 PM"<?php echo ($value && $value->end_time =='20:00:00') ? 'selected' : ''; ?>>8:00 PM</option>
                    <option value="9 PM"<?php echo ($value && $value->end_time =='21:00:00') ? 'selected' : ''; ?>>9:00 PM</option>
                    <option value="10 PM"<?php echo ($value && $value->end_time =='22:00:00') ? 'selected' : ''; ?>>10:00 PM</option>
                    <option value="11 PM"<?php echo ($value && $value->end_time =='23:00:00') ? 'selected' : ''; ?>>11:00 PM</option>
                    <option value="12 AM"<?php echo ($value && ($value->end_time == '24:00:00' || $value->end_time=='00:00:00')) ? 'selected' : ''; ?>>12:00 PM</option>
                </select>
            </div>
            </div>
            <div class="col-lg-4 select-half-on" id="end_date_wrapper_1"></div>  
        </div>
    <?php endforeach; ?>
        <div id="time-repeter-1"></div>
        <div class="add-academic col-lg-12"><a href="javascript:void(0);" class="pull-right add-more-academic-link btn btn-success" onClick="repeatTime(1);">Add More Time</a></div>
    </div>
    </div>

    <div class="row">
    <?php $hasTimings = FALSE; if(isset($doctorTimings[2])) $hasTimings = TRUE; ?>
        <div class="col-sm-2"><input type="checkbox" name="dayoftheweek[]" class="mytime" value="2" <?php echo $hasTimings ? 'checked' : ''; ?>/><label>&nbsp;Tuesday</label></div>
        <div id="timings-2" style="<?php echo $hasTimings ? '' :  'display:none;'; ?>">
    <?php if(!$hasTimings) : ?>
        <?php $doctorTimings[2] = array('abc'); $value = NULL; ?>
        <?php endif; ?>
        <?php $count=0 ; foreach ($doctorTimings[2] as $key => $value) : ?>
        <div class="time-repeat" id="repeatID2">
            <?php $count++; ?>
            <?php if($count == 1) : ?>
            <div class="col-lg-4 select-half-on"></div>
            <?php else : ?>
            <div class="col-lg-2 select-half-on"></div>
            <?php endif; ?>                        
            <div class="col-lg-2 select-half-on">
                <label for="inputEmail" class="  control-label">*From </label>
                <div class=" ">
                <select name="from-timimg[2][]" class="form-control" id="select" style=" height: 2.2em !important;">
                    <option value="">-From-</option>
                    <option value="1 AM"<?php echo ($value && $value->start_time == '01:00:00') ? 'selected' : ''; ?>>1:00 AM</option>
                    <option value="2 AM"<?php echo ($value && $value->start_time == '02:00:00') ? 'selected' : ''; ?>>2:00 AM</option>
                    <option value="3 AM"<?php echo ($value && $value->start_time == '03:00:00') ? 'selected' : ''; ?>>3:00 AM</option>
                    <option value="4 AM"<?php echo ($value && $value->start_time == '04:00:00') ? 'selected' : ''; ?>>4:00 AM</option>
                    <option value="5 AM"<?php echo ($value && $value->start_time == '05:00:00') ? 'selected' : ''; ?>>5:00 AM</option>
                    <option value="6 AM"<?php echo ($value && $value->start_time == '06:00:00') ? 'selected' : ''; ?>>6:00 AM</option>
                    <option value="7 AM"<?php echo ($value && $value->start_time == '07:00:00') ? 'selected' : ''; ?>>7:00 AM</option>
                    <option value="8 AM"<?php echo ($value && $value->start_time == '08:00:00') ? 'selected' : ''; ?>>8:00 AM</option>
                    <option value="9 AM"<?php echo ($value && $value->start_time == '09:00:00') ? 'selected' : ''; ?>>9:00 AM</option>
                    <option value="10 AM"<?php echo ($value && $value->start_time == '10:00:00') ? 'selected' : ''; ?>>10:00 AM</option>
                    <option value="11 AM"<?php echo ($value && $value->start_time == '11:00:00') ? 'selected' : ''; ?>>11:00 AM</option>
                    <option value="12 PM"<?php echo ($value && $value->start_time == '12:00:00') ? 'selected' : ''; ?>>12:00 PM</option>
                    <option value="1 PM"<?php echo ($value && $value->start_time == '13:00:00') ? 'selected' : ''; ?>>1:00 PM</option>
                    <option value="2 PM"<?php echo ($value && $value->start_time == '14:00:00') ? 'selected' : ''; ?>>2:00 PM</option>
                    <option value="3 PM"<?php echo ($value && $value->start_time == '15:00:00') ? 'selected' : ''; ?>>3:00 PM</option>
                    <option value="4 PM"<?php echo ($value && $value->start_time == '16:00:00') ? 'selected' : ''; ?>>4:00 PM</option>
                    <option value="5 PM"<?php echo ($value && $value->start_time == '17:00:00') ? 'selected' : ''; ?>>5:00 PM</option>
                    <option value="6 PM"<?php echo ($value && $value->start_time == '18:00:00') ? 'selected' : ''; ?>>6:00 PM</option>
                    <option value="7 PM"<?php echo ($value && $value->start_time == '19:00:00') ? 'selected' : ''; ?>>7:00 PM</option>
                    <option value="8 PM"<?php echo ($value && $value->start_time == '20:00:00') ? 'selected' : ''; ?>>8:00 PM</option>
                    <option value="9 PM"<?php echo ($value && $value->start_time == '21:00:00') ? 'selected' : ''; ?>>9:00 PM</option>
                    <option value="10 PM"<?php echo ($value && $value->start_time == '22:00:00') ? 'selected' : ''; ?>>10:00 PM</option>
                    <option value="11 PM"<?php echo ($value && $value->start_time == '23:00:00') ? 'selected' : ''; ?>>11:00 PM</option>
                    <option value="12 AM"<?php echo ($value && ($value->start_time == '24:00:00' || $value->start_time=='00:00:00')) ? 'selected' : ''; ?>>12:00 PM</option>
                </select>
                </div>
            </div>

            <div class="col-lg-2 select-half-on" id="end_date_wrapper_1">
            <label for="inputEmail" class="  control-label">*To </label>
            <div class=" ">
                <select name="to-timing[2][]" class="form-control" id="select" style=" height: 2.2em !important;">
                    <option value="">-To-</option>
                    <option value="1 AM"<?php echo ($value && $value->end_time =='01:00:00') ? 'selected' : ''; ?>>1:00 AM</option>
                    <option value="2 AM"<?php echo ($value && $value->end_time =='02:00:00') ? 'selected' : ''; ?>>2:00 AM</option>
                    <option value="3 AM"<?php echo ($value && $value->end_time =='03:00:00') ? 'selected' : ''; ?>>3:00 AM</option>
                    <option value="4 AM"<?php echo ($value && $value->end_time =='04:00:00') ? 'selected' : ''; ?>>4:00 AM</option>
                    <option value="5 AM"<?php echo ($value && $value->end_time =='05:00:00') ? 'selected' : ''; ?>>5:00 AM</option>
                    <option value="6 AM"<?php echo ($value && $value->end_time =='06:00:00') ? 'selected' : ''; ?>>6:00 AM</option>
                    <option value="7 AM"<?php echo ($value && $value->end_time =='07:00:00') ? 'selected' : ''; ?>>7:00 AM</option>
                    <option value="8 AM"<?php echo ($value && $value->end_time =='08:00:00') ? 'selected' : ''; ?>>8:00 AM</option>
                    <option value="9 AM"<?php echo ($value && $value->end_time =='09:00:00') ? 'selected' : ''; ?>>9:00 AM</option>
                    <option value="10 AM"<?php echo ($value && $value->end_time =='10:00:00') ? 'selected' : ''; ?>>10:00 AM</option>
                    <option value="11 AM"<?php echo ($value && $value->end_time =='11:00:00') ? 'selected' : ''; ?>>11:00 AM</option>
                    <option value="12 PM"<?php echo ($value && $value->end_time =='12:00:00') ? 'selected' : ''; ?>>12:00 PM</option>
                    <option value="1 PM"<?php echo ($value && $value->end_time =='13:00:00') ? 'selected' : ''; ?>>1:00 PM</option>
                    <option value="2 PM"<?php echo ($value && $value->end_time =='14:00:00') ? 'selected' : ''; ?>>2:00 PM</option>
                    <option value="3 PM"<?php echo ($value && $value->end_time =='15:00:00') ? 'selected' : ''; ?>>3:00 PM</option>
                    <option value="4 PM"<?php echo ($value && $value->end_time =='16:00:00') ? 'selected' : ''; ?>>4:00 PM</option>
                    <option value="5 PM"<?php echo ($value && $value->end_time =='17:00:00') ? 'selected' : ''; ?>>5:00 PM</option>
                    <option value="6 PM"<?php echo ($value && $value->end_time =='18:00:00') ? 'selected' : ''; ?>>6:00 PM</option>
                    <option value="7 PM"<?php echo ($value && $value->end_time =='19:00:00') ? 'selected' : ''; ?>>7:00 PM</option>
                    <option value="8 PM"<?php echo ($value && $value->end_time =='20:00:00') ? 'selected' : ''; ?>>8:00 PM</option>
                    <option value="9 PM"<?php echo ($value && $value->end_time =='21:00:00') ? 'selected' : ''; ?>>9:00 PM</option>
                    <option value="10 PM"<?php echo ($value && $value->end_time =='22:00:00') ? 'selected' : ''; ?>>10:00 PM</option>
                    <option value="11 PM"<?php echo ($value && $value->end_time =='23:00:00') ? 'selected' : ''; ?>>11:00 PM</option>
                    <option value="12 AM"<?php echo ($value && ($value->end_time == '24:00:00' || $value->end_time=='00:00:00')) ? 'selected' : ''; ?>>12:00 PM</option>
                </select>
            </div>
            </div>
            <div class="col-lg-4 select-half-on" id="end_date_wrapper_1"></div>  
        </div>
        <?php endforeach; ?>
        <div id="time-repeter-2"></div>
        <div class="add-academic col-lg-12"><a href="javascript:void(0);" class="pull-right add-more-academic-link btn btn-success" onClick="repeatTime(2);">Add More Time</a></div>
    </div>
    </div>

    <div class="row">
        <?php $hasTimings = FALSE; if(isset($doctorTimings[3])) $hasTimings = TRUE; ?>
        <div class="col-sm-2"><input type="checkbox" name="dayoftheweek[]" class="mytime" value="3" <?php echo $hasTimings ? 'checked' : ''; ?>/><label>&nbsp;Wednesday</label></div>
        <div id="timings-3" style="<?php echo $hasTimings ? '' :  'display:none;'; ?>">
        <?php if(!$hasTimings) : ?>
            <?php $doctorTimings[3] = array('abc'); $value = NULL; ?>
        <?php endif; ?>
        <?php $count=0 ; foreach ($doctorTimings[3] as $key => $value) : ?>
        <div class="time-repeat" id="repeatID2">
            <?php $count++; ?>
            <?php if($count == 1) : ?>
            <div class="col-lg-4 select-half-on"></div>
            <?php else : ?>
            <div class="col-lg-2 select-half-on"></div>
            <?php endif; ?>                        
            <div class="col-lg-2 select-half-on">
                <label for="inputEmail" class="  control-label">*From </label>
                <div class=" ">
                <select name="from-timimg[3][]" class="form-control" id="select" style=" height: 2.2em !important;">
                    <option value="">-From-</option>
                    <option value="1 AM"<?php echo ($value && $value->start_time == '01:00:00') ? 'selected' : ''; ?>>1:00 AM</option>
                    <option value="2 AM"<?php echo ($value && $value->start_time == '02:00:00') ? 'selected' : ''; ?>>2:00 AM</option>
                    <option value="3 AM"<?php echo ($value && $value->start_time == '03:00:00') ? 'selected' : ''; ?>>3:00 AM</option>
                    <option value="4 AM"<?php echo ($value && $value->start_time == '04:00:00') ? 'selected' : ''; ?>>4:00 AM</option>
                    <option value="5 AM"<?php echo ($value && $value->start_time == '05:00:00') ? 'selected' : ''; ?>>5:00 AM</option>
                    <option value="6 AM"<?php echo ($value && $value->start_time == '06:00:00') ? 'selected' : ''; ?>>6:00 AM</option>
                    <option value="7 AM"<?php echo ($value && $value->start_time == '07:00:00') ? 'selected' : ''; ?>>7:00 AM</option>
                    <option value="8 AM"<?php echo ($value && $value->start_time == '08:00:00') ? 'selected' : ''; ?>>8:00 AM</option>
                    <option value="9 AM"<?php echo ($value && $value->start_time == '09:00:00') ? 'selected' : ''; ?>>9:00 AM</option>
                    <option value="10 AM"<?php echo ($value && $value->start_time == '10:00:00') ? 'selected' : ''; ?>>10:00 AM</option>
                    <option value="11 AM"<?php echo ($value && $value->start_time == '11:00:00') ? 'selected' : ''; ?>>11:00 AM</option>
                    <option value="12 PM"<?php echo ($value && $value->start_time == '12:00:00') ? 'selected' : ''; ?>>12:00 PM</option>
                    <option value="1 PM"<?php echo ($value && $value->start_time == '13:00:00') ? 'selected' : ''; ?>>1:00 PM</option>
                    <option value="2 PM"<?php echo ($value && $value->start_time == '14:00:00') ? 'selected' : ''; ?>>2:00 PM</option>
                    <option value="3 PM"<?php echo ($value && $value->start_time == '15:00:00') ? 'selected' : ''; ?>>3:00 PM</option>
                    <option value="4 PM"<?php echo ($value && $value->start_time == '16:00:00') ? 'selected' : ''; ?>>4:00 PM</option>
                    <option value="5 PM"<?php echo ($value && $value->start_time == '17:00:00') ? 'selected' : ''; ?>>5:00 PM</option>
                    <option value="6 PM"<?php echo ($value && $value->start_time == '18:00:00') ? 'selected' : ''; ?>>6:00 PM</option>
                    <option value="7 PM"<?php echo ($value && $value->start_time == '19:00:00') ? 'selected' : ''; ?>>7:00 PM</option>
                    <option value="8 PM"<?php echo ($value && $value->start_time == '20:00:00') ? 'selected' : ''; ?>>8:00 PM</option>
                    <option value="9 PM"<?php echo ($value && $value->start_time == '21:00:00') ? 'selected' : ''; ?>>9:00 PM</option>
                    <option value="10 PM"<?php echo ($value && $value->start_time == '22:00:00') ? 'selected' : ''; ?>>10:00 PM</option>
                    <option value="11 PM"<?php echo ($value && $value->start_time == '23:00:00') ? 'selected' : ''; ?>>11:00 PM</option>
                    <option value="12 AM"<?php echo ($value && ($value->start_time == '24:00:00' || $value->start_time=='00:00:00')) ? 'selected' : ''; ?>>12:00 PM</option>
                </select>
                </div>
            </div>

            <div class="col-lg-2 select-half-on" id="end_date_wrapper_1">
            <label for="inputEmail" class="  control-label">*To </label>
            <div class=" ">
                <select name="to-timing[3][]" class="form-control" id="select" style=" height: 2.2em !important;">
                    <option value="">-To-</option>
                    <option value="1 AM"<?php echo ($value && $value->end_time =='01:00:00') ? 'selected' : ''; ?>>1:00 AM</option>
                    <option value="2 AM"<?php echo ($value && $value->end_time =='02:00:00') ? 'selected' : ''; ?>>2:00 AM</option>
                    <option value="3 AM"<?php echo ($value && $value->end_time =='03:00:00') ? 'selected' : ''; ?>>3:00 AM</option>
                    <option value="4 AM"<?php echo ($value && $value->end_time =='04:00:00') ? 'selected' : ''; ?>>4:00 AM</option>
                    <option value="5 AM"<?php echo ($value && $value->end_time =='05:00:00') ? 'selected' : ''; ?>>5:00 AM</option>
                    <option value="6 AM"<?php echo ($value && $value->end_time =='06:00:00') ? 'selected' : ''; ?>>6:00 AM</option>
                    <option value="7 AM"<?php echo ($value && $value->end_time =='07:00:00') ? 'selected' : ''; ?>>7:00 AM</option>
                    <option value="8 AM"<?php echo ($value && $value->end_time =='08:00:00') ? 'selected' : ''; ?>>8:00 AM</option>
                    <option value="9 AM"<?php echo ($value && $value->end_time =='09:00:00') ? 'selected' : ''; ?>>9:00 AM</option>
                    <option value="10 AM"<?php echo ($value && $value->end_time =='10:00:00') ? 'selected' : ''; ?>>10:00 AM</option>
                    <option value="11 AM"<?php echo ($value && $value->end_time =='11:00:00') ? 'selected' : ''; ?>>11:00 AM</option>
                    <option value="12 PM"<?php echo ($value && $value->end_time =='12:00:00') ? 'selected' : ''; ?>>12:00 PM</option>
                    <option value="1 PM"<?php echo ($value && $value->end_time =='13:00:00') ? 'selected' : ''; ?>>1:00 PM</option>
                    <option value="2 PM"<?php echo ($value && $value->end_time =='14:00:00') ? 'selected' : ''; ?>>2:00 PM</option>
                    <option value="3 PM"<?php echo ($value && $value->end_time =='15:00:00') ? 'selected' : ''; ?>>3:00 PM</option>
                    <option value="4 PM"<?php echo ($value && $value->end_time =='16:00:00') ? 'selected' : ''; ?>>4:00 PM</option>
                    <option value="5 PM"<?php echo ($value && $value->end_time =='17:00:00') ? 'selected' : ''; ?>>5:00 PM</option>
                    <option value="6 PM"<?php echo ($value && $value->end_time =='18:00:00') ? 'selected' : ''; ?>>6:00 PM</option>
                    <option value="7 PM"<?php echo ($value && $value->end_time =='19:00:00') ? 'selected' : ''; ?>>7:00 PM</option>
                    <option value="8 PM"<?php echo ($value && $value->end_time =='20:00:00') ? 'selected' : ''; ?>>8:00 PM</option>
                    <option value="9 PM"<?php echo ($value && $value->end_time =='21:00:00') ? 'selected' : ''; ?>>9:00 PM</option>
                    <option value="10 PM"<?php echo ($value && $value->end_time =='22:00:00') ? 'selected' : ''; ?>>10:00 PM</option>
                    <option value="11 PM"<?php echo ($value && $value->end_time =='23:00:00') ? 'selected' : ''; ?>>11:00 PM</option>
                    <option value="12 AM"<?php echo ($value && ($value->end_time == '24:00:00' || $value->end_time=='00:00:00')) ? 'selected' : ''; ?>>12:00 PM</option>
                </select>
            </div>
            </div>
            <div class="col-lg-4 select-half-on" id="end_date_wrapper_1"></div>  
        </div>
        <?php endforeach; ?>
        <div id="time-repeter-3"></div>
        <div class="add-academic col-lg-12"><a href="javascript:void(0);" class="pull-right add-more-academic-link btn btn-success" onClick="repeatTime(3);">Add More Time</a></div>
    </div>
    </div>

            <div class="row">
        <?php $hasTimings = FALSE; if(isset($doctorTimings[4])) $hasTimings = TRUE; ?>
        <div class="col-sm-2"><input type="checkbox" name="dayoftheweek[]" value="4" class="mytime" <?php echo $hasTimings ? 'checked' : ''; ?>/><label>&nbsp;Thursday</label></div>
        <div id="timings-4" style="<?php echo $hasTimings ? '' :  'display:none;'; ?>">
        <?php if(!$hasTimings) : ?>
            <?php $doctorTimings[4] = array('abc'); $value = NULL; ?>
        <?php endif; ?>
        <?php $count=0 ; foreach ($doctorTimings[4] as $key => $value) : ?>
        <div class="time-repeat" id="repeatID2">
            <?php $count++; ?>
            <?php if($count == 1) : ?>
            <div class="col-lg-4 select-half-on"></div>
            <?php else : ?>
            <div class="col-lg-2 select-half-on"></div>
            <?php endif; ?>                        
            <div class="col-lg-2 select-half-on">
                <label for="inputEmail" class="  control-label">*From </label>
                <div class=" ">
                <select name="from-timimg[4][]" class="form-control" id="select" style=" height: 2.2em !important;">
                    <option value="">-From-</option>
                    <option value="1 AM"<?php echo ($value && $value->start_time == '01:00:00') ? 'selected' : ''; ?>>1:00 AM</option>
                    <option value="2 AM"<?php echo ($value && $value->start_time == '02:00:00') ? 'selected' : ''; ?>>2:00 AM</option>
                    <option value="3 AM"<?php echo ($value && $value->start_time == '03:00:00') ? 'selected' : ''; ?>>3:00 AM</option>
                    <option value="4 AM"<?php echo ($value && $value->start_time == '04:00:00') ? 'selected' : ''; ?>>4:00 AM</option>
                    <option value="5 AM"<?php echo ($value && $value->start_time == '05:00:00') ? 'selected' : ''; ?>>5:00 AM</option>
                    <option value="6 AM"<?php echo ($value && $value->start_time == '06:00:00') ? 'selected' : ''; ?>>6:00 AM</option>
                    <option value="7 AM"<?php echo ($value && $value->start_time == '07:00:00') ? 'selected' : ''; ?>>7:00 AM</option>
                    <option value="8 AM"<?php echo ($value && $value->start_time == '08:00:00') ? 'selected' : ''; ?>>8:00 AM</option>
                    <option value="9 AM"<?php echo ($value && $value->start_time == '09:00:00') ? 'selected' : ''; ?>>9:00 AM</option>
                    <option value="10 AM"<?php echo ($value && $value->start_time == '10:00:00') ? 'selected' : ''; ?>>10:00 AM</option>
                    <option value="11 AM"<?php echo ($value && $value->start_time == '11:00:00') ? 'selected' : ''; ?>>11:00 AM</option>
                    <option value="12 PM"<?php echo ($value && $value->start_time == '12:00:00') ? 'selected' : ''; ?>>12:00 PM</option>
                    <option value="1 PM"<?php echo ($value && $value->start_time == '13:00:00') ? 'selected' : ''; ?>>1:00 PM</option>
                    <option value="2 PM"<?php echo ($value && $value->start_time == '14:00:00') ? 'selected' : ''; ?>>2:00 PM</option>
                    <option value="3 PM"<?php echo ($value && $value->start_time == '15:00:00') ? 'selected' : ''; ?>>3:00 PM</option>
                    <option value="4 PM"<?php echo ($value && $value->start_time == '16:00:00') ? 'selected' : ''; ?>>4:00 PM</option>
                    <option value="5 PM"<?php echo ($value && $value->start_time == '17:00:00') ? 'selected' : ''; ?>>5:00 PM</option>
                    <option value="6 PM"<?php echo ($value && $value->start_time == '18:00:00') ? 'selected' : ''; ?>>6:00 PM</option>
                    <option value="7 PM"<?php echo ($value && $value->start_time == '19:00:00') ? 'selected' : ''; ?>>7:00 PM</option>
                    <option value="8 PM"<?php echo ($value && $value->start_time == '20:00:00') ? 'selected' : ''; ?>>8:00 PM</option>
                    <option value="9 PM"<?php echo ($value && $value->start_time == '21:00:00') ? 'selected' : ''; ?>>9:00 PM</option>
                    <option value="10 PM"<?php echo ($value && $value->start_time == '22:00:00') ? 'selected' : ''; ?>>10:00 PM</option>
                    <option value="11 PM"<?php echo ($value && $value->start_time == '23:00:00') ? 'selected' : ''; ?>>11:00 PM</option>
                    <option value="12 AM"<?php echo ($value && ($value->start_time == '24:00:00' || $value->start_time=='00:00:00')) ? 'selected' : ''; ?>>12:00 PM</option>
                </select>
                </div>
            </div>

            <div class="col-lg-2 select-half-on" id="end_date_wrapper_1">
            <label for="inputEmail" class="  control-label">*To </label>
            <div class=" ">
                <select name="to-timing[4][]" class="form-control" id="select" style=" height: 2.2em !important;">
                    <option value="">-To-</option>
                    <option value="1 AM"<?php echo ($value && $value->end_time =='01:00:00') ? 'selected' : ''; ?>>1:00 AM</option>
                    <option value="2 AM"<?php echo ($value && $value->end_time =='02:00:00') ? 'selected' : ''; ?>>2:00 AM</option>
                    <option value="3 AM"<?php echo ($value && $value->end_time =='03:00:00') ? 'selected' : ''; ?>>3:00 AM</option>
                    <option value="4 AM"<?php echo ($value && $value->end_time =='04:00:00') ? 'selected' : ''; ?>>4:00 AM</option>
                    <option value="5 AM"<?php echo ($value && $value->end_time =='05:00:00') ? 'selected' : ''; ?>>5:00 AM</option>
                    <option value="6 AM"<?php echo ($value && $value->end_time =='06:00:00') ? 'selected' : ''; ?>>6:00 AM</option>
                    <option value="7 AM"<?php echo ($value && $value->end_time =='07:00:00') ? 'selected' : ''; ?>>7:00 AM</option>
                    <option value="8 AM"<?php echo ($value && $value->end_time =='08:00:00') ? 'selected' : ''; ?>>8:00 AM</option>
                    <option value="9 AM"<?php echo ($value && $value->end_time =='09:00:00') ? 'selected' : ''; ?>>9:00 AM</option>
                    <option value="10 AM"<?php echo ($value && $value->end_time =='10:00:00') ? 'selected' : ''; ?>>10:00 AM</option>
                    <option value="11 AM"<?php echo ($value && $value->end_time =='11:00:00') ? 'selected' : ''; ?>>11:00 AM</option>
                    <option value="12 PM"<?php echo ($value && $value->end_time =='12:00:00') ? 'selected' : ''; ?>>12:00 PM</option>
                    <option value="1 PM"<?php echo ($value && $value->end_time =='13:00:00') ? 'selected' : ''; ?>>1:00 PM</option>
                    <option value="2 PM"<?php echo ($value && $value->end_time =='14:00:00') ? 'selected' : ''; ?>>2:00 PM</option>
                    <option value="3 PM"<?php echo ($value && $value->end_time =='15:00:00') ? 'selected' : ''; ?>>3:00 PM</option>
                    <option value="4 PM"<?php echo ($value && $value->end_time =='16:00:00') ? 'selected' : ''; ?>>4:00 PM</option>
                    <option value="5 PM"<?php echo ($value && $value->end_time =='17:00:00') ? 'selected' : ''; ?>>5:00 PM</option>
                    <option value="6 PM"<?php echo ($value && $value->end_time =='18:00:00') ? 'selected' : ''; ?>>6:00 PM</option>
                    <option value="7 PM"<?php echo ($value && $value->end_time =='19:00:00') ? 'selected' : ''; ?>>7:00 PM</option>
                    <option value="8 PM"<?php echo ($value && $value->end_time =='20:00:00') ? 'selected' : ''; ?>>8:00 PM</option>
                    <option value="9 PM"<?php echo ($value && $value->end_time =='21:00:00') ? 'selected' : ''; ?>>9:00 PM</option>
                    <option value="10 PM"<?php echo ($value && $value->end_time =='22:00:00') ? 'selected' : ''; ?>>10:00 PM</option>
                    <option value="11 PM"<?php echo ($value && $value->end_time =='23:00:00') ? 'selected' : ''; ?>>11:00 PM</option>
                    <option value="12 AM"<?php echo ($value && ($value->end_time == '24:00:00' || $value->end_time=='00:00:00')) ? 'selected' : ''; ?>>12:00 PM</option>
                </select>
            </div>
            </div>
            <div class="col-lg-4 select-half-on" id="end_date_wrapper_1"></div>  
        </div>
        <?php endforeach; ?>
        <div id="time-repeter-4"></div>
        <div class="add-academic col-lg-12"><a href="javascript:void(0);" class="pull-right add-more-academic-link btn btn-success" onClick="repeatTime(4);">Add More Time</a></div>
    </div>
    </div>

            <div class="row">
        <?php $hasTimings = FALSE; if(isset($doctorTimings[5])) $hasTimings = TRUE; ?>
        <div class="col-sm-2"><input type="checkbox" name="dayoftheweek[]" value="5" class="mytime" <?php echo $hasTimings ? 'checked' : ''; ?>/><label>&nbsp;Friday</label></div>
        <div id="timings-5" style="<?php echo $hasTimings ? '' :  'display:none;'; ?>">
        <?php if(!$hasTimings) : ?>
            <?php $doctorTimings[5] = array('abc'); $value = NULL; ?>
        <?php endif; ?>
        <?php $count=0 ; foreach ($doctorTimings[5] as $key => $value) : ?>
        <div class="time-repeat" id="repeatID2">
            <?php $count++; ?>
            <?php if($count == 1) : ?>
            <div class="col-lg-4 select-half-on"></div>
            <?php else : ?>
            <div class="col-lg-2 select-half-on"></div>
            <?php endif; ?>                        
            <div class="col-lg-2 select-half-on">
                <label for="inputEmail" class="  control-label">*From </label>
                <div class=" ">
                <select name="from-timimg[5][]" class="form-control" id="select" style=" height: 2.2em !important;">
                    <option value="">-From-</option>
                    <option value="1 AM"<?php echo ($value && $value->start_time == '01:00:00') ? 'selected' : ''; ?>>1:00 AM</option>
                    <option value="2 AM"<?php echo ($value && $value->start_time == '02:00:00') ? 'selected' : ''; ?>>2:00 AM</option>
                    <option value="3 AM"<?php echo ($value && $value->start_time == '03:00:00') ? 'selected' : ''; ?>>3:00 AM</option>
                    <option value="4 AM"<?php echo ($value && $value->start_time == '04:00:00') ? 'selected' : ''; ?>>4:00 AM</option>
                    <option value="5 AM"<?php echo ($value && $value->start_time == '05:00:00') ? 'selected' : ''; ?>>5:00 AM</option>
                    <option value="6 AM"<?php echo ($value && $value->start_time == '06:00:00') ? 'selected' : ''; ?>>6:00 AM</option>
                    <option value="7 AM"<?php echo ($value && $value->start_time == '07:00:00') ? 'selected' : ''; ?>>7:00 AM</option>
                    <option value="8 AM"<?php echo ($value && $value->start_time == '08:00:00') ? 'selected' : ''; ?>>8:00 AM</option>
                    <option value="9 AM"<?php echo ($value && $value->start_time == '09:00:00') ? 'selected' : ''; ?>>9:00 AM</option>
                    <option value="10 AM"<?php echo ($value && $value->start_time == '10:00:00') ? 'selected' : ''; ?>>10:00 AM</option>
                    <option value="11 AM"<?php echo ($value && $value->start_time == '11:00:00') ? 'selected' : ''; ?>>11:00 AM</option>
                    <option value="12 PM"<?php echo ($value && $value->start_time == '12:00:00') ? 'selected' : ''; ?>>12:00 PM</option>
                    <option value="1 PM"<?php echo ($value && $value->start_time == '13:00:00') ? 'selected' : ''; ?>>1:00 PM</option>
                    <option value="2 PM"<?php echo ($value && $value->start_time == '14:00:00') ? 'selected' : ''; ?>>2:00 PM</option>
                    <option value="3 PM"<?php echo ($value && $value->start_time == '15:00:00') ? 'selected' : ''; ?>>3:00 PM</option>
                    <option value="4 PM"<?php echo ($value && $value->start_time == '16:00:00') ? 'selected' : ''; ?>>4:00 PM</option>
                    <option value="5 PM"<?php echo ($value && $value->start_time == '17:00:00') ? 'selected' : ''; ?>>5:00 PM</option>
                    <option value="6 PM"<?php echo ($value && $value->start_time == '18:00:00') ? 'selected' : ''; ?>>6:00 PM</option>
                    <option value="7 PM"<?php echo ($value && $value->start_time == '19:00:00') ? 'selected' : ''; ?>>7:00 PM</option>
                    <option value="8 PM"<?php echo ($value && $value->start_time == '20:00:00') ? 'selected' : ''; ?>>8:00 PM</option>
                    <option value="9 PM"<?php echo ($value && $value->start_time == '21:00:00') ? 'selected' : ''; ?>>9:00 PM</option>
                    <option value="10 PM"<?php echo ($value && $value->start_time == '22:00:00') ? 'selected' : ''; ?>>10:00 PM</option>
                    <option value="11 PM"<?php echo ($value && $value->start_time == '23:00:00') ? 'selected' : ''; ?>>11:00 PM</option>
                    <option value="12 AM"<?php echo ($value && ($value->start_time == '24:00:00' || $value->start_time=='00:00:00')) ? 'selected' : ''; ?>>12:00 PM</option>
                </select>
                </div>
            </div>

            <div class="col-lg-2 select-half-on" id="end_date_wrapper_1">
            <label for="inputEmail" class="  control-label">*To </label>
            <div class=" ">
                <select name="to-timing[5][]" class="form-control" id="select" style=" height: 2.2em !important;">
                    <option value="">-To-</option>
                    <option value="1 AM"<?php echo ($value && $value->end_time =='01:00:00') ? 'selected' : ''; ?>>1:00 AM</option>
                    <option value="2 AM"<?php echo ($value && $value->end_time =='02:00:00') ? 'selected' : ''; ?>>2:00 AM</option>
                    <option value="3 AM"<?php echo ($value && $value->end_time =='03:00:00') ? 'selected' : ''; ?>>3:00 AM</option>
                    <option value="4 AM"<?php echo ($value && $value->end_time =='04:00:00') ? 'selected' : ''; ?>>4:00 AM</option>
                    <option value="5 AM"<?php echo ($value && $value->end_time =='05:00:00') ? 'selected' : ''; ?>>5:00 AM</option>
                    <option value="6 AM"<?php echo ($value && $value->end_time =='06:00:00') ? 'selected' : ''; ?>>6:00 AM</option>
                    <option value="7 AM"<?php echo ($value && $value->end_time =='07:00:00') ? 'selected' : ''; ?>>7:00 AM</option>
                    <option value="8 AM"<?php echo ($value && $value->end_time =='08:00:00') ? 'selected' : ''; ?>>8:00 AM</option>
                    <option value="9 AM"<?php echo ($value && $value->end_time =='09:00:00') ? 'selected' : ''; ?>>9:00 AM</option>
                    <option value="10 AM"<?php echo ($value && $value->end_time =='10:00:00') ? 'selected' : ''; ?>>10:00 AM</option>
                    <option value="11 AM"<?php echo ($value && $value->end_time =='11:00:00') ? 'selected' : ''; ?>>11:00 AM</option>
                    <option value="12 PM"<?php echo ($value && $value->end_time =='12:00:00') ? 'selected' : ''; ?>>12:00 PM</option>
                    <option value="1 PM"<?php echo ($value && $value->end_time =='13:00:00') ? 'selected' : ''; ?>>1:00 PM</option>
                    <option value="2 PM"<?php echo ($value && $value->end_time =='14:00:00') ? 'selected' : ''; ?>>2:00 PM</option>
                    <option value="3 PM"<?php echo ($value && $value->end_time =='15:00:00') ? 'selected' : ''; ?>>3:00 PM</option>
                    <option value="4 PM"<?php echo ($value && $value->end_time =='16:00:00') ? 'selected' : ''; ?>>4:00 PM</option>
                    <option value="5 PM"<?php echo ($value && $value->end_time =='17:00:00') ? 'selected' : ''; ?>>5:00 PM</option>
                    <option value="6 PM"<?php echo ($value && $value->end_time =='18:00:00') ? 'selected' : ''; ?>>6:00 PM</option>
                    <option value="7 PM"<?php echo ($value && $value->end_time =='19:00:00') ? 'selected' : ''; ?>>7:00 PM</option>
                    <option value="8 PM"<?php echo ($value && $value->end_time =='20:00:00') ? 'selected' : ''; ?>>8:00 PM</option>
                    <option value="9 PM"<?php echo ($value && $value->end_time =='21:00:00') ? 'selected' : ''; ?>>9:00 PM</option>
                    <option value="10 PM"<?php echo ($value && $value->end_time =='22:00:00') ? 'selected' : ''; ?>>10:00 PM</option>
                    <option value="11 PM"<?php echo ($value && $value->end_time =='23:00:00') ? 'selected' : ''; ?>>11:00 PM</option>
                    <option value="12 AM"<?php echo ($value && ($value->end_time == '24:00:00' || $value->end_time=='00:00:00')) ? 'selected' : ''; ?>>12:00 PM</option>
                </select>
            </div>
            </div>
            <div class="col-lg-4 select-half-on" id="end_date_wrapper_1"></div>  
        </div>
    <?php endforeach; ?>
        <div id="time-repeter-5"></div>
        <div class="add-academic col-lg-12"><a href="javascript:void(0);" class="pull-right add-more-academic-link btn btn-success" onClick="repeatTime(5);">Add More Time</a></div>
    </div>
    </div>

        <div class="row">
        <?php $hasTimings = FALSE; if(isset($doctorTimings[6])) $hasTimings = TRUE; ?>
        <div class="col-sm-2"><input type="checkbox" name="dayoftheweek[]" value="6" class="mytime" <?php echo $hasTimings ? 'checked' : ''; ?>/><label>&nbsp;Saturday</label></div>
        <div id="timings-6" style="<?php echo $hasTimings ? '' :  'display:none;'; ?>">
        <?php if(!$hasTimings) : ?>
            <?php $doctorTimings[6] = array('abc'); $value = NULL; ?>
        <?php endif; ?>
        <?php $count=0 ; foreach ($doctorTimings[6] as $key => $value) : ?>
        <div class="time-repeat" id="repeatID2">
            <?php $count++; ?>
            <?php if($count == 1) : ?>
            <div class="col-lg-4 select-half-on"></div>
            <?php else : ?>
            <div class="col-lg-2 select-half-on"></div>
            <?php endif; ?>                        
            <div class="col-lg-2 select-half-on">
                <label for="inputEmail" class="  control-label">*From </label>
                <div class=" ">
                <select name="from-timimg[6][]" class="form-control" id="select" style=" height: 2.2em !important;">
                    <option value="">-From-</option>
                    <option value="1 AM"<?php echo ($value && $value->start_time == '01:00:00') ? 'selected' : ''; ?>>1:00 AM</option>
                    <option value="2 AM"<?php echo ($value && $value->start_time == '02:00:00') ? 'selected' : ''; ?>>2:00 AM</option>
                    <option value="3 AM"<?php echo ($value && $value->start_time == '03:00:00') ? 'selected' : ''; ?>>3:00 AM</option>
                    <option value="4 AM"<?php echo ($value && $value->start_time == '04:00:00') ? 'selected' : ''; ?>>4:00 AM</option>
                    <option value="5 AM"<?php echo ($value && $value->start_time == '05:00:00') ? 'selected' : ''; ?>>5:00 AM</option>
                    <option value="6 AM"<?php echo ($value && $value->start_time == '06:00:00') ? 'selected' : ''; ?>>6:00 AM</option>
                    <option value="7 AM"<?php echo ($value && $value->start_time == '07:00:00') ? 'selected' : ''; ?>>7:00 AM</option>
                    <option value="8 AM"<?php echo ($value && $value->start_time == '08:00:00') ? 'selected' : ''; ?>>8:00 AM</option>
                    <option value="9 AM"<?php echo ($value && $value->start_time == '09:00:00') ? 'selected' : ''; ?>>9:00 AM</option>
                    <option value="10 AM"<?php echo ($value && $value->start_time == '10:00:00') ? 'selected' : ''; ?>>10:00 AM</option>
                    <option value="11 AM"<?php echo ($value && $value->start_time == '11:00:00') ? 'selected' : ''; ?>>11:00 AM</option>
                    <option value="12 PM"<?php echo ($value && $value->start_time == '12:00:00') ? 'selected' : ''; ?>>12:00 PM</option>
                    <option value="1 PM"<?php echo ($value && $value->start_time == '13:00:00') ? 'selected' : ''; ?>>1:00 PM</option>
                    <option value="2 PM"<?php echo ($value && $value->start_time == '14:00:00') ? 'selected' : ''; ?>>2:00 PM</option>
                    <option value="3 PM"<?php echo ($value && $value->start_time == '15:00:00') ? 'selected' : ''; ?>>3:00 PM</option>
                    <option value="4 PM"<?php echo ($value && $value->start_time == '16:00:00') ? 'selected' : ''; ?>>4:00 PM</option>
                    <option value="5 PM"<?php echo ($value && $value->start_time == '17:00:00') ? 'selected' : ''; ?>>5:00 PM</option>
                    <option value="6 PM"<?php echo ($value && $value->start_time == '18:00:00') ? 'selected' : ''; ?>>6:00 PM</option>
                    <option value="7 PM"<?php echo ($value && $value->start_time == '19:00:00') ? 'selected' : ''; ?>>7:00 PM</option>
                    <option value="8 PM"<?php echo ($value && $value->start_time == '20:00:00') ? 'selected' : ''; ?>>8:00 PM</option>
                    <option value="9 PM"<?php echo ($value && $value->start_time == '21:00:00') ? 'selected' : ''; ?>>9:00 PM</option>
                    <option value="10 PM"<?php echo ($value && $value->start_time == '22:00:00') ? 'selected' : ''; ?>>10:00 PM</option>
                    <option value="11 PM"<?php echo ($value && $value->start_time == '23:00:00') ? 'selected' : ''; ?>>11:00 PM</option>
                    <option value="12 AM"<?php echo ($value && ($value->start_time == '24:00:00' || $value->start_time=='00:00:00')) ? 'selected' : ''; ?>>12:00 PM</option>
                </select>
                </div>
            </div>

            <div class="col-lg-2 select-half-on" id="end_date_wrapper_1">
            <label for="inputEmail" class="  control-label">*To </label>
            <div class=" ">
                <select name="to-timing[6][]" class="form-control" id="select" style=" height: 2.2em !important;">
                    <option value="">-To-</option>
                    <option value="1 AM"<?php echo ($value && $value->end_time =='01:00:00') ? 'selected' : ''; ?>>1:00 AM</option>
                    <option value="2 AM"<?php echo ($value && $value->end_time =='02:00:00') ? 'selected' : ''; ?>>2:00 AM</option>
                    <option value="3 AM"<?php echo ($value && $value->end_time =='03:00:00') ? 'selected' : ''; ?>>3:00 AM</option>
                    <option value="4 AM"<?php echo ($value && $value->end_time =='04:00:00') ? 'selected' : ''; ?>>4:00 AM</option>
                    <option value="5 AM"<?php echo ($value && $value->end_time =='05:00:00') ? 'selected' : ''; ?>>5:00 AM</option>
                    <option value="6 AM"<?php echo ($value && $value->end_time =='06:00:00') ? 'selected' : ''; ?>>6:00 AM</option>
                    <option value="7 AM"<?php echo ($value && $value->end_time =='07:00:00') ? 'selected' : ''; ?>>7:00 AM</option>
                    <option value="8 AM"<?php echo ($value && $value->end_time =='08:00:00') ? 'selected' : ''; ?>>8:00 AM</option>
                    <option value="9 AM"<?php echo ($value && $value->end_time =='09:00:00') ? 'selected' : ''; ?>>9:00 AM</option>
                    <option value="10 AM"<?php echo ($value && $value->end_time =='10:00:00') ? 'selected' : ''; ?>>10:00 AM</option>
                    <option value="11 AM"<?php echo ($value && $value->end_time =='11:00:00') ? 'selected' : ''; ?>>11:00 AM</option>
                    <option value="12 PM"<?php echo ($value && $value->end_time =='12:00:00') ? 'selected' : ''; ?>>12:00 PM</option>
                    <option value="1 PM"<?php echo ($value && $value->end_time =='13:00:00') ? 'selected' : ''; ?>>1:00 PM</option>
                    <option value="2 PM"<?php echo ($value && $value->end_time =='14:00:00') ? 'selected' : ''; ?>>2:00 PM</option>
                    <option value="3 PM"<?php echo ($value && $value->end_time =='15:00:00') ? 'selected' : ''; ?>>3:00 PM</option>
                    <option value="4 PM"<?php echo ($value && $value->end_time =='16:00:00') ? 'selected' : ''; ?>>4:00 PM</option>
                    <option value="5 PM"<?php echo ($value && $value->end_time =='17:00:00') ? 'selected' : ''; ?>>5:00 PM</option>
                    <option value="6 PM"<?php echo ($value && $value->end_time =='18:00:00') ? 'selected' : ''; ?>>6:00 PM</option>
                    <option value="7 PM"<?php echo ($value && $value->end_time =='19:00:00') ? 'selected' : ''; ?>>7:00 PM</option>
                    <option value="8 PM"<?php echo ($value && $value->end_time =='20:00:00') ? 'selected' : ''; ?>>8:00 PM</option>
                    <option value="9 PM"<?php echo ($value && $value->end_time =='21:00:00') ? 'selected' : ''; ?>>9:00 PM</option>
                    <option value="10 PM"<?php echo ($value && $value->end_time =='22:00:00') ? 'selected' : ''; ?>>10:00 PM</option>
                    <option value="11 PM"<?php echo ($value && $value->end_time =='23:00:00') ? 'selected' : ''; ?>>11:00 PM</option>
                    <option value="12 AM"<?php echo ($value && ($value->end_time == '24:00:00' || $value->end_time=='00:00:00')) ? 'selected' : ''; ?>>12:00 PM</option>
                </select>
            </div>
            </div>
            <div class="col-lg-4 select-half-on" id="end_date_wrapper_1"></div>  
        </div>
        <?php endforeach; ?>
        <div id="time-repeter-6"></div>
        <div class="add-academic col-lg-12"><a href="javascript:void(0);" class="pull-right add-more-academic-link btn btn-success" onClick="repeatTime(6);">Add More Time</a></div>
    </div>
    </div>

    <div class="row">
        <?php $hasTimings = FALSE; if(isset($doctorTimings[7])) $hasTimings = TRUE; ?>
        <div class="col-sm-2"><input type="checkbox" name="dayoftheweek[]" value="7" class="mytime" <?php echo $hasTimings ? 'checked' : ''; ?>/><label>&nbsp;Sunday</label></div>
        <div id="timings-7" style="<?php echo $hasTimings ? '' :  'display:none;'; ?>">
        <?php if(!$hasTimings) : ?>
            <?php $doctorTimings[7] = array('abc'); $value = NULL; ?>
        <?php endif; ?>
        <?php $count=0 ; foreach ($doctorTimings[7] as $key => $value) : ?>
        <div class="time-repeat" id="repeatID2">
            <?php $count++; ?>
            <?php if($count == 1) : ?>
            <div class="col-lg-4 select-half-on"></div>
            <?php else : ?>
            <div class="col-lg-2 select-half-on"></div>
            <?php endif; ?>                        
            <div class="col-lg-2 select-half-on">
                <label for="inputEmail" class="  control-label">*From </label>
                <div class=" ">
                <select name="from-timimg[7][]" class="form-control" id="select" style=" height: 2.2em !important;">
                    <option value="">-From-</option>
                    <option value="1 AM"<?php echo ($value && $value->start_time == '01:00:00') ? 'selected' : ''; ?>>1:00 AM</option>
                    <option value="2 AM"<?php echo ($value && $value->start_time == '02:00:00') ? 'selected' : ''; ?>>2:00 AM</option>
                    <option value="3 AM"<?php echo ($value && $value->start_time == '03:00:00') ? 'selected' : ''; ?>>3:00 AM</option>
                    <option value="4 AM"<?php echo ($value && $value->start_time == '04:00:00') ? 'selected' : ''; ?>>4:00 AM</option>
                    <option value="5 AM"<?php echo ($value && $value->start_time == '05:00:00') ? 'selected' : ''; ?>>5:00 AM</option>
                    <option value="6 AM"<?php echo ($value && $value->start_time == '06:00:00') ? 'selected' : ''; ?>>6:00 AM</option>
                    <option value="7 AM"<?php echo ($value && $value->start_time == '07:00:00') ? 'selected' : ''; ?>>7:00 AM</option>
                    <option value="8 AM"<?php echo ($value && $value->start_time == '08:00:00') ? 'selected' : ''; ?>>8:00 AM</option>
                    <option value="9 AM"<?php echo ($value && $value->start_time == '09:00:00') ? 'selected' : ''; ?>>9:00 AM</option>
                    <option value="10 AM"<?php echo ($value && $value->start_time == '10:00:00') ? 'selected' : ''; ?>>10:00 AM</option>
                    <option value="11 AM"<?php echo ($value && $value->start_time == '11:00:00') ? 'selected' : ''; ?>>11:00 AM</option>
                    <option value="12 PM"<?php echo ($value && $value->start_time == '12:00:00') ? 'selected' : ''; ?>>12:00 PM</option>
                    <option value="1 PM"<?php echo ($value && $value->start_time == '13:00:00') ? 'selected' : ''; ?>>1:00 PM</option>
                    <option value="2 PM"<?php echo ($value && $value->start_time == '14:00:00') ? 'selected' : ''; ?>>2:00 PM</option>
                    <option value="3 PM"<?php echo ($value && $value->start_time == '15:00:00') ? 'selected' : ''; ?>>3:00 PM</option>
                    <option value="4 PM"<?php echo ($value && $value->start_time == '16:00:00') ? 'selected' : ''; ?>>4:00 PM</option>
                    <option value="5 PM"<?php echo ($value && $value->start_time == '17:00:00') ? 'selected' : ''; ?>>5:00 PM</option>
                    <option value="6 PM"<?php echo ($value && $value->start_time == '18:00:00') ? 'selected' : ''; ?>>6:00 PM</option>
                    <option value="7 PM"<?php echo ($value && $value->start_time == '19:00:00') ? 'selected' : ''; ?>>7:00 PM</option>
                    <option value="8 PM"<?php echo ($value && $value->start_time == '20:00:00') ? 'selected' : ''; ?>>8:00 PM</option>
                    <option value="9 PM"<?php echo ($value && $value->start_time == '21:00:00') ? 'selected' : ''; ?>>9:00 PM</option>
                    <option value="10 PM"<?php echo ($value && $value->start_time == '22:00:00') ? 'selected' : ''; ?>>10:00 PM</option>
                    <option value="11 PM"<?php echo ($value && $value->start_time == '23:00:00') ? 'selected' : ''; ?>>11:00 PM</option>
                    <option value="12 AM"<?php echo ($value && ($value->start_time == '24:00:00' || $value->start_time=='00:00:00')) ? 'selected' : ''; ?>>12:00 PM</option>
                </select>
                </div>
            </div>

            <div class="col-lg-2 select-half-on" id="end_date_wrapper_1">
            <label for="inputEmail" class="  control-label">*To </label>
            <div class=" ">
                <select name="to-timing[7][]" class="form-control" id="select" style=" height: 2.2em !important;">
                    <option value="">-To-</option>
                    <option value="1 AM"<?php echo ($value && $value->end_time =='01:00:00') ? 'selected' : ''; ?>>1:00 AM</option>
                    <option value="2 AM"<?php echo ($value && $value->end_time =='02:00:00') ? 'selected' : ''; ?>>2:00 AM</option>
                    <option value="3 AM"<?php echo ($value && $value->end_time =='03:00:00') ? 'selected' : ''; ?>>3:00 AM</option>
                    <option value="4 AM"<?php echo ($value && $value->end_time =='04:00:00') ? 'selected' : ''; ?>>4:00 AM</option>
                    <option value="5 AM"<?php echo ($value && $value->end_time =='05:00:00') ? 'selected' : ''; ?>>5:00 AM</option>
                    <option value="6 AM"<?php echo ($value && $value->end_time =='06:00:00') ? 'selected' : ''; ?>>6:00 AM</option>
                    <option value="7 AM"<?php echo ($value && $value->end_time =='07:00:00') ? 'selected' : ''; ?>>7:00 AM</option>
                    <option value="8 AM"<?php echo ($value && $value->end_time =='08:00:00') ? 'selected' : ''; ?>>8:00 AM</option>
                    <option value="9 AM"<?php echo ($value && $value->end_time =='09:00:00') ? 'selected' : ''; ?>>9:00 AM</option>
                    <option value="10 AM"<?php echo ($value && $value->end_time =='10:00:00') ? 'selected' : ''; ?>>10:00 AM</option>
                    <option value="11 AM"<?php echo ($value && $value->end_time =='11:00:00') ? 'selected' : ''; ?>>11:00 AM</option>
                    <option value="12 PM"<?php echo ($value && $value->end_time =='12:00:00') ? 'selected' : ''; ?>>12:00 PM</option>
                    <option value="1 PM"<?php echo ($value && $value->end_time =='13:00:00') ? 'selected' : ''; ?>>1:00 PM</option>
                    <option value="2 PM"<?php echo ($value && $value->end_time =='14:00:00') ? 'selected' : ''; ?>>2:00 PM</option>
                    <option value="3 PM"<?php echo ($value && $value->end_time =='15:00:00') ? 'selected' : ''; ?>>3:00 PM</option>
                    <option value="4 PM"<?php echo ($value && $value->end_time =='16:00:00') ? 'selected' : ''; ?>>4:00 PM</option>
                    <option value="5 PM"<?php echo ($value && $value->end_time =='17:00:00') ? 'selected' : ''; ?>>5:00 PM</option>
                    <option value="6 PM"<?php echo ($value && $value->end_time =='18:00:00') ? 'selected' : ''; ?>>6:00 PM</option>
                    <option value="7 PM"<?php echo ($value && $value->end_time =='19:00:00') ? 'selected' : ''; ?>>7:00 PM</option>
                    <option value="8 PM"<?php echo ($value && $value->end_time =='20:00:00') ? 'selected' : ''; ?>>8:00 PM</option>
                    <option value="9 PM"<?php echo ($value && $value->end_time =='21:00:00') ? 'selected' : ''; ?>>9:00 PM</option>
                    <option value="10 PM"<?php echo ($value && $value->end_time =='22:00:00') ? 'selected' : ''; ?>>10:00 PM</option>
                    <option value="11 PM"<?php echo ($value && $value->end_time =='23:00:00') ? 'selected' : ''; ?>>11:00 PM</option>
                    <option value="12 AM"<?php echo ($value && ($value->end_time == '24:00:00' || $value->end_time=='00:00:00')) ? 'selected' : ''; ?>>12:00 PM</option>
                </select>
            </div>
            </div>
            <div class="col-lg-4 select-half-on" id="end_date_wrapper_1"></div>  
        </div>
        <?php endforeach; ?>
        <div id="time-repeter-7"></div>
        <div class="add-academic col-lg-12"><a href="javascript:void(0);" class="pull-right add-more-academic-link btn btn-success" onClick="repeatTime(7);">Add More Time</a></div>
    </div>
    </div>
  </div>
</div>

<div class="box-footer">
            <button class="btn btn-success submit-button" style=" float: right;" type="submit">Update</button>
        </div> 
      
      <?php echo form_close(); ?>

      <?php /*
        <!-- Days EXAMPLE -->
      <div class="box box-default">
        <div class="box-header with-border" data-widget="collapse">
          <h3 class="box-title">Days & Timing</h3>

          <div class="box-tools pull-right">
            <button type="button" class="btn btn-box-tool" ><i class="fa fa-minus"></i></button>
            <!--<button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-remove"></i></button>-->
          </div>
        </div>
        <!-- /.box-header -->
        <div class="box-body">
          <div class="row">
              
            <div class="col-md-12 day-checkboxes">
                
                <?php
                foreach($days as $key => $value):
                    $checked = '';
                    $timing = $fixed_from = $fixed_to = $flexible_from_time = $flexible_to_time = '';
                        if(count($doctorAvailableDays) > 0):
                            foreach($doctorAvailableDays as $k => $v):
                                if($value == $v->day_id){
                                    $checked = 'checked';
                                    $timing = $v->timing;
                                    $fixed_from = $v->fixed_from_time;
                                    $fixed_to = $v->fixed_to_time;
                                    $flexible_from_time = $v->flexible_from_time;
                                    $flexible_to_time = $v->flexible_to_time;
                                }
                            endforeach;
                        endif;    
                    ?>
                    <?php  ?>
                
                    <div class="row">
                    <div class="col-sm-2">
                        <div class="form-group">
                            <div class="top-space"></div>
                            <input class="<?php echo $key; ?> " type="checkbox" name="days[dayoftheweek][]" value="<?php echo $value; ?>" <?php echo $checked;?> />
                            <label> <?php echo $key; ?></label>
                        </div>
                    </div>   
                    
                    <div id="<?php echo $key; ?>">
                        
                        <div class="col-sm-2">
                            <div class="form-group">
                              <label>Timing</label>
                                <select size="1" id="timings" title="" name="days[timings][]" class="form-control select2" style="width: 100%;">
                                    <option value="">Select your timing</option>
                                    <option value="fixed" <?php echo ($timing == 1) ? 'selected' : '' ;?>>Fixed</option>
                                    <option value="flexible" <?php echo ($timing == 2) ? 'selected' : '' ;?>>Flexible</option>
                                </select>
                            </div>
                        </div>

                        <div class="col-sm-2">
                          <div class="bootstrap-timepicker">
                            <div class="form-group">
                              <label>From</label>

                              <div class="input-group">
                                  <input name="days[fixed_from][]" type="text" class="form-control timepicker" value="<?php echo $fixed_from; ?>">

                                <div class="input-group-addon">
                                  <i class="fa fa-clock-o"></i>
                                </div>
                              </div>
                            </div>
                          </div>

                        </div>

                        <div class="col-sm-2">
                          <div class="bootstrap-timepicker">
                            <div class="form-group">
                              <label>To</label>

                              <div class="input-group">
                                <input name="days[fixed_to][]" type="text" class="form-control timepicker" value="<?php echo $fixed_to; ?>">

                                <div class="input-group-addon">
                                  <i class="fa fa-clock-o"></i>
                                </div>
                              </div>
                            </div>
                          </div>

                        </div>

                        <!-- felxible -->
                        <div style="<?php echo ($timing == 1) ? 'display:none;' : 'display:block;' ;?>" class="flexible-timing <?php echo ($timing == 1) ? 'col-sm-5' : '' ;?>">
                        
                        <div class="col-sm-2 ">
                            <div class="bootstrap-timepicker" style="">
                            <div class="form-group">
                              <label>From</label>

                              <div class="input-group">
                                <input name="days[flexible_from][]" type="text" class="form-control timepicker" value="<?php echo $flexible_from_time; ?>">

                                <div class="input-group-addon">
                                  <i class="fa fa-clock-o"></i>
                                </div>
                              </div>
                            </div>
                          </div>

                        </div>

                        <div class="col-sm-2">
                          <div class="bootstrap-timepicker">
                            <div class="form-group">
                              <label>To</label>
                              <div class="input-group">
                                <input name="days[flexible_to][]" type="text" class="form-control timepicker" value="<?php echo $flexible_to_time; ?>">
                                <div class="input-group-addon">
                                  <i class="fa fa-clock-o"></i>
                                </div>
                              </div>
                            </div>
                          </div>
                        </div>    
                    </div>
                    
                    </div>
                </div>
                
                <?php 
                    endforeach;
                ?>
            </div>   
            
            <!-- /.col -->
          </div>
          
         
            
          <!-- /.row -->
        </div>
        <!-- /.box-body -->
        
      </div> */ ?>
      <!-- /.box -->
     
      

    </section>
    <!-- /.content -->
  </div>

<script>
 //Bootstrap Date picker
    $(document).on('click', '#academic_year', function(){
        $(this).datepicker({
            autoclose: true,
            format: " yyyy", // Notice the Extra space at the beginning
            viewMode: "years", 
            minViewMode: "years",
            endDate: new Date(),
            
        }).focus();
     });
    
    
    $(document).on('click', '.doctor-academic', function(){ // onclick
          $(this).parent().parent().remove();    
    });
    
    function readURL(input) {

    if (input.files && input.files[0]) {
        
         var url = input.value;
    var ext = url.substring(url.lastIndexOf('.') + 1).toLowerCase();
        
        
        if (input.files && input.files[0]&& (ext == "gif" || ext == "png" || ext == "jpeg" || ext == "jpg")) {
            
            var reader = new FileReader();
            reader.onload = function (e) {
                $('#pro-pic-id').attr('src', e.target.result);
            }
            reader.readAsDataURL(input.files[0]);
            
        }
        else
        {
            alert("Please upload a valid image file.");
            $("#add-pro-pic").val('');
        }    
        
      }
    }

    $("#add-pro-pic").change(function(){
        readURL(this);
    });

     $('.add-more-academic-link').click(function(){
        var aqval = $("#aqcount").val();
        
        var html = '<div><div class="acadmeic-quali-repeater" id="repeatID1"><div class="count-ac col-lg-12"><span ></span></div>' +

                '<div class="col-md-4"><div class="form-group"><label for="">Qualification<span>*</span></label><input type="text"  placeholder="Enter Qualification" name="qualification[]" class="form-control" ></div></div>' +                 


                '<div class="col-md-4"><div class="form-group"><label for="">Institution<span>*</span></label><input placeholder="Enter Institution" name="institution[]" type="text"  class="form-control" ></div></div>' +

                '<div class="col-md-3"><div class="bootstrap-timepicker"><div class="form-group"><label>Year<span>*</span></label><div class="input-group">' +
                                                  '<div class="input-group-addon"><i class="fa fa-calendar"></i></div><input type="text" name="year[]" class="form-control pull-right" id="academic_year"></div></div></div></div>' +
                                                  
                                                  
                '<div class="col-md-1"><div class="top-space"></div><div class="doctor-academic"><i class="fa fa-remove"></i></div></div> ' +

                '</div></div>';
        
          $('#aq-repeter').append(html);
      aqval = parseInt(aqval) + 1;
      $("#aqcount").val(aqval);
         
    
        });
        
        
        $(document).on('click', '.work_from_mm_yy, .work_to_mm_yy', function(){ // onclick
            $(this).datepicker({
                autoclose: true,
                format: "yyyy-mm",
                viewMode: "months", 
                minViewMode: "months",
                endDate: new Date(),
            }).focus();
        });
        
$(".mytime").on('click', function() {
  if($(this).is(':checked')){
    $('#timings-'+this.value).show(); 
  }else{
    $('#timings-'+this.value).hide();
  }
});

        $(document).on('click', '.add-more-exp-link', function(){
            
            var weval = $("#wecount").val();
            var html = '<div><div class="exp-repeat" id="repeatID2"><div class="count-we col-lg-12"></div>' + 
                    '<input type="hidden" name="wecount" id="wecount" value="2" />' +  
                    '<div class="col-md-3">' +  
                        '<div class="form-group">' +  
                           '<label for="">Company<span>*</span></label>' +  
                           '<input placeholder="Enter Company" name="company[]" type="text" class="form-control" >' +  
                        '</div> ' +  
                     '</div>' +  

                     '<div class="col-md-2">' +
                        '<div class="form-group">' +
                           '<label for="">Job Title<span>*</span></label>' +
                           '<input placeholder="Enter Job Title" name="job_title[]" type="text" class="form-control" >' +
                        '</div> ' +
                     '</div>    ' +             

                    ' <div class="col-md-2">' +
                     ' <div class="bootstrap-timepicker">' +
                      '  <div class="form-group">' +
                       '<label>Start Date<span>*</span></label>' +
                        '<div class="input-group">' +
                         ' <div class="input-group-addon">' +
                          ' <i class="fa fa-calendar"></i>' +
                           ' </div>' +
                            '  <input name="work_from_mm_yy[]" type="text" class="form-control work_from_mm_yy">' +
                             ' </div>' +
                              ' </div>' +
                           '</div>' +
                         '</div>  ' +

                     '<div class="col-md-2" >' +
                      '<div class="bootstrap-timepicker" id="end_date_wrapper_'+weval+'">' +
                         '<div class="form-group">' +
                          '   <label>End Date<span>*</span></label>' +

                           '  <div class="input-group">' +
                            '   <div class="input-group-addon">' +
                            '     <i class="fa fa-calendar"></i>' +
                           '    </div>' +
                              ' <input  type="text" name="work_to_mm_yy[]" class="form-control work_to_mm_yy" >' +
                            ' </div>' +
                             
                          ' </div>' +
                       '</div>' +

                     '</div>  ' +

                     '<div class="col-md-2">' +
                       ' <div class="form-group">' +
                          '  <div class="top-space"></div>' + 
                          ' <label for="">Present</label>' +
                        '   <input  name="stay_here[]" value="'+weval+'" class="pull-left present_here" onclick="present_here(this.id);" id="present_here_'+weval+'" data-indexof="'+weval+'" type="checkbox" />' +
                        '</div> ' +
                      '</div> ' +  
                      
                      '<div class="col-md-1"><div class="top-space"></div><div class="doctor-academic"><i class="fa fa-remove"></i></div></div>' +
                     '</div><div>';

                    $('#we-repeter').append(html);   


                    var atLeastOneIsChecked = false;
                    $('.present_here').each(function () {

                      if ($(this).is(':checked')) {
                        atLeastOneIsChecked = true;
                        // Stop .each from processing any more items
                        return false;
                      }
                    });

                    if(atLeastOneIsChecked)
                    {
                        $("#present_here_"+weval).attr('disabled','disabled');
                    }
                    
                    weval = parseInt(weval) + 1;
                    $("#wecount").val(weval);
                });
                
                // hide and show based on checkbox checked
                present_here = function(id)
                {
                    var indexOf     =  $("#"+id).data('indexof');
                    var endwrapper  = "end_date_wrapper_"+indexOf;
                    var checkstatus = $("#"+id).is(':checked') ? 1 : 0;
                    if(checkstatus == 1){
                        $(".present_here").attr('disabled','disabled');
                       // $(".present_here").checked(false);
                        $("#"+id).removeAttr('disabled');
                       // $("#"+id).checked(true);    
                        $("#"+endwrapper).css('display','none');
                        $("#"+endwrapper).siblings().css('display','none');
                        
                      //  $(".add-more-exp-link").css('display','none'); // hide add more link
                    } else {

                        $(".present_here").removeAttr('disabled');

                        $(".present_here").css('display','block');
                        $("#"+endwrapper).css('display','block');
                     //   $(".add-more-exp-link").css('display','block');
                    }  
                }

/*
$(".day-checkboxes input:checkbox").each(function(){ // when page load check status of checkbox 
    
    var id = $(this).attr('class'); 
    
    var isChecked = $(this).prop('checked');
    
    if(isChecked){
        $("#" + id + " :input").prop("disabled", false); 
    }else{
        $("#" + id + " :input").prop("disabled", true); 
    }
    
});

$(document).on('click', '.day-checkboxes input:checkbox', function(){ // when click on checkbox disabled/enabled input fields
    
    var id = $(this).attr('class');

    //var isDisabled = $("#" + id + " :input").prop('disabled');
    var isChecked = $(this).prop('checked');
    
    if(isChecked){
        $("#" + id + " :input").prop("disabled", false); 
    }else{
        $("#" + id + " :input").prop("disabled", true); 
    }
    
});


$(document).on('change', '.select2', function(){ // select timing fixed/flexible
    
    var id = $(this).parent().parent().parent().prop("id"); // parent id of div 
    
    var timing = $(this).val(); 
    
    if(timing == 'fixed'){
        $("#" + id + " :input[type='text']").parent().parent().parent().css("display", 'block'); // input field show
        $('#' + id).children('.flexible-timing').css('display', 'none'); // flexible timer hide
        $('#' + id).children('.flexible-timing').addClass('col-sm-5'); // flexible class remove
//        $("#" + id + " :input[type='text']").prop("disabled", false); // all input text field enabled
//        $('#' + id).children('.flexible-timing').prop("disabled", true); // flexible-timing input field disabled
        
    }else if(timing == 'flexible'){
        $("#" + id + " :input[type='text']").parent().parent().parent().css("display", 'block'); // input field hide
        $('#' + id).children('.flexible-timing').css('display', 'block'); // flexible timer show
        $('#' + id).children('.flexible-timing').removeClass('col-sm-5'); // flexible class remove
//        $("#" + id + " :input[type='text']").prop("disabled", false); // all input text field enabled

    }else{
        $("#" + id + " :input[type='text']").parent().parent().parent().css("display", 'none'); // input field hide
//        $("#" + id + " :input[type='text']").prop("disabled", true); // all text input field disabled
    }
});
*/

</script>
<script src="https://cdn.jsdelivr.net/jquery.validation/1.16.0/jquery.validate.min.js"></script>
    <script>
$(document).on('blur', 'input[type="text"]' ,function(){
    var value = $(this).attr('name');
    var input_validate_field_name  = ['type_of_doctor', 'fname', 'sname', 'email', 'phone', 'address', 'qualification[]', 'institution[]', 'year[]', 'company[]', 'job_title[]', 'work_from_mm_yy[]', 'work_to_mm_yy[]'];//, 
    if ($.inArray(value, input_validate_field_name) != '-1') {
        var count = 0;
        $('input[name="'+ value + '"]').each(function() {
              var input_field_name = value;
              if (input_field_name.indexOf('[]') > -1) {
                    input_field_name = input_field_name.replace('[]','');
              }
              var ErrorId = input_field_name + '-error-' + count;
              count++;
              $('#' + ErrorId).remove();
              var values = $.trim($(this).val());
              if(values == ''){
                  $(this).after('<label id="'+ErrorId+'" class="error" for="'+value+'">This field is required.</label>');
              }else if (value == 'email'){
                 if (!(/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/.test(values))){
                      $(this).after('<label id="'+ErrorId+'" class="error" for="'+value+'">You have entered an invalid email address!.</label>');
                 }
             }
        });
    }
});

function repeatTime(day){
$('#time-repeter-'+day).append('<div class="time-repeat" id="repeatID2"><div class="col-lg-2 select-half-on"></div><div class="col-lg-2 select-half-on" id="timings-1"><label for="inputEmail" class="  control-label"></label><div class=" "><select name="from-timimg['+day+'][]" class="form-control" id="select" style=" height: 2.2em !important;"><option value="">-From-</option><option value="1 AM">1:00 AM</option><option value="2 AM">2:00 AM</option><option value="3 AM">3:00 AM</option><option value="4 AM">4:00 AM</option><option value="5 AM">5:00 AM</option><option value="6 AM">6:00 AM</option><option value="7 AM">7:00 AM</option><option value="8 AM">8:00 AM</option><option value="9 AM">9:00 AM</option><option value="10 AM">10:00 AM</option><option value="11 AM">11:00 AM</option><option value="12 PM">12:00 PM</option><option value="1 PM">1:00 PM</option><option value="2 PM">2:00 PM</option><option value="3 PM">3:00 PM</option><option value="4 PM">4:00 PM</option><option value="5 PM">5:00 PM</option><option value="6 PM">6:00 PM</option><option value="7 PM">7:00 PM</option><option value="8 PM">8:00 PM</option><option value="9 PM">9:00 PM</option><option value="10 PM">10:00 PM</option><option value="11 PM">11:00 PM</option><option value="12 AM">12:00 AM</option></select></div></div><div class="col-lg-2 select-half-on" id="end_date_wrapper_1"><label for="inputEmail" class="  control-label"></label><div class=" "><select name="to-timing['+day+'][]" class="form-control" id="select" style=" height: 2.2em !important;"><option value="">-To-</option><option value="1 AM">1:00 AM</option><option value="2 AM">2:00 AM</option><option value="3 AM">3:00 AM</option><option value="4 AM">4:00 AM</option><option value="5 AM">5:00 AM</option><option value="6 AM">6:00 AM</option><option value="7 AM">7:00 AM</option><option value="8 AM">8:00 AM</option><option value="9 AM">9:00 AM</option><option value="10 AM">10:00 AM</option><option value="11 AM">11:00 AM</option><option value="12 PM">12:00 PM</option><option value="1 PM">1:00 PM</option><option value="2 PM">2:00 PM</option><option value="3 PM">3:00 PM</option><option value="4 PM">4:00 PM</option><option value="5 PM">5:00 PM</option><option value="6 PM">6:00 PM</option><option value="7 PM">7:00 PM</option><option value="8 PM">8:00 PM</option><option value="9 PM">9:00 PM</option><option value="10 PM">10:00 PM</option><option value="11 PM">11:00 PM</option><option value="12 AM">12:00 AM</option></select></div></div><div class="col-lg-4"></div></div>');   
}
$('#doctorinfo').on('submit', function(event) {
            var input_validate_field_name  = ['type_of_doctor', 'gender', 'fname', 'sname', 'email', 'phone', 'address', 'qualification[]', 'institution[]', 'year[]', 'company[]', 'job_title[]', 'work_from_mm_yy[]', 'work_to_mm_yy[]'];//, ,
            var error = 0;
            $('.error').remove();
            $.each(input_validate_field_name, function( index, value ) {
                var input_type = $('input[name="'+ value + '"]').attr('type');
               /* if(input_type == 'checkbox'){
                    $('#' + ErrorId).remove();
                    $('input[name="'+ value + '"]').each(function() {
                        var ErrorId = 'checkbox' + '-error';
                        var is_checked = $(this).prop('checked');
                            if(is_checked){
                                var id = $(this).attr('class');
                                var timing = $('#'+id+' select').val();

                                var fixed_from = $('#'+id+' input[name="days[fixed_from][]"]').val();
                                var fixed_to = $('#'+id+' input[name="days[fixed_to][]"]').val();
                                
                                var flexible_from = $('#'+id+' input[name="days[flexible_from][]"]').val();
                                var flexible_to = $('#'+id+' input[name="days[flexible_to][]"]').val();

                                var dtStarts = new Date("1/1/2007 " + fixed_from);
                                var dtEnds = new Date("1/1/2007 " + fixed_to);
                                var difference_in_milliseconds1 = dtEnds - dtStarts;
                                if (difference_in_milliseconds1 < 0){
                                     $('#'+id+' input[name="days[fixed_to][]"]').parent().parent().parent().after('<label id="'+ErrorId+'" class="error" for="'+value+'" style="display: block;">End time should be greater than start time.</label>');
                                     error++;
                                }
                                
                                if(timing == 'flexible'){
                                    var dtStart = new Date("1/1/2007 " + flexible_from);
                                    var dtEnd = new Date("1/1/2007 " + flexible_to);
                                    var difference_in_milliseconds = dtEnd - dtStart;
                                    if (difference_in_milliseconds < 0){
                                         $('#'+id+' input[name="days[flexible_to][]"]').parent().parent().parent().after('<label id="'+ErrorId+'" class="error" for="'+value+'" style="display: block;">End time should be greater than start time.</label>');
                                         error++;
                                    }
                                }
                                
                            }
                      });
                }else */
                if(input_type == 'radio'){
                    var input_field_name = value;
                    var ErrorId = input_field_name + '-error';
                    $('#' + ErrorId).remove();
                    if (!$('input[name='+ value + ']:checked').val() ) {          
                        $('input[name='+ value + ']').parent().parent().append('<label id="'+ErrorId+'" class="error" for="'+value+'">This field is required.</label>');
                    } 
                }else if(value == 'work_to_mm_yy[]'){    
                   
                    var count = 0;
                    $('input[name="'+ value + '"]').each(function() {
                        var tmpclass = 'temp-error';
                        $(this).parent().parent().parent().parent().parent().addClass(tmpclass);
                        var present_here = $('.'+tmpclass + ' .present_here' ).prop('checked');
                            
                        if(present_here == false){    
                            
                            var input_field_name = value;
                            if (input_field_name.indexOf('[]') > -1) {
                                    input_field_name = input_field_name.replace('[]','');
                            }
                            
                            var ErrorId = input_field_name + '-error-' + count;
                            $('#' + ErrorId).remove();
                             
                            var to_year = $('.'+tmpclass + ' .work_to_mm_yy' ).val();
                            var from_year = $('.'+tmpclass + ' .work_from_mm_yy' ).val();
                            var arr_from_year = from_year.split("-");
                            var arr_to_year = to_year.split("-");

                            from_year = '01/' + arr_from_year["1"] + '/' + arr_from_year["0"];
                            to_year = '01/' + arr_to_year["1"] + '/' + arr_to_year["0"];

                            var dtStarts = new Date(from_year);
                            var dtEnds = new Date(to_year);
                            var difference_in_milliseconds1 = dtEnds - dtStarts;
                            if (difference_in_milliseconds1 < 0){
                                 $('.'+tmpclass + ' .work_to_mm_yy' ).parent().parent().parent().after('<label id="'+ErrorId+'" class="error" for="'+value+'" style="display: block;">End date should be greater than start date.</label>');
                                 error++;
                                }
                           }
                           
                           $(this).parent().parent().parent().parent().parent().removeClass(tmpclass);
                           count++;
                        });
                }else{
                    count = 0;
                    $('input[name="'+ value + '"]').each(function() {
                          var input_field_name = value;
                          if (input_field_name.indexOf('[]') > -1) {
                                input_field_name = input_field_name.replace('[]','');
                          }
                          var ErrorId = input_field_name + '-error-' + count;
                          count++;
                          $('#' + ErrorId).remove();
                          var qualification = $.trim($(this).val());
                          if(qualification == ''){
                              $(this).after('<label id="'+ErrorId+'" class="error" for="'+value+'">This field is required.</label>');
                              error++;
                          }
                      });
                }
            });
            
            if(error == 0){
                 return true;
            }else{
                //$('.error:first-child').scrollTo();
                 $('html, body').animate({
                    scrollTop: $(".error:first").offset().top
                }, 2000);
                 return false;
            }
            
     });
  

        
    function readCvURL(input) {

    if (input.files && input.files[0]) {
        var url = input.value;
        var ext = url.substring(url.lastIndexOf('.') + 1).toLowerCase();
        
        if (input.files && input.files[0] && (ext == "pdf" || ext == "doc" || ext == "docx")) {
        }
        else
        {
            alert("Only pdf and word files are accepted in CV.");
            $("#cv").val('');
        }    
        
    }
}

$("#cv").change(function(){
    readCvURL(this);
});

    </script>
<style>
    .error{
        color: red !important;
    }
    .doctor-academic .fa-remove{
        color: red !important;
    }
</style>