<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Advanced Form Elements
        <small>Preview</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="#">Forms</a></li>
        <li class="active">Advanced Elements</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">

      <!-- SELECT2 EXAMPLE -->
      <div class="box box-default collapsed-box">
        <div class="box-header with-border">
          <h3 class="box-title">Personal Details</h3>

          <div class="box-tools pull-right">
            <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-plus"></i></button>
            <!--<button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-remove"></i></button>-->
          </div>
        </div>
        <!-- /.box-header -->
        <div class="box-body">
          <div class="row">
            <!-- /.col -->
            <div class="col-md-6">
                <div class="form-group">
                  <label for="">Name</label>
                  <input placeholder="Enter Name" name="fname" type="text" value="<?php echo isset($record->fname) ? $record->fname : ''; ?>" class="form-control" required>
                </div>
                
                <div class="form-group">
                  <label for="">Email</label>
                  <input placeholder="Enter Email" name="email" type="email" value="<?php echo isset($record->email) ? $record->email : ''; ?>" class="form-control" required>
                </div>
                
                <div class="form-group">
                  <label for="">Address</label>
                  <input placeholder="Enter Address" type="text" name="address" class="form-control" value="<?php echo isset($record->address) ? $record->address : ''; ?>">
                </div>
                
                <div class="form-group">
                  <label for="">Province</label>
                  <input placeholder="Enter Province" type="text" name="province" class="form-control" value="<?php echo isset($record->province) ? $record->province : ''; ?>">
                </div>
                
                <div class="form-group">
                  <label for="">Upload CV</label>
                  <input name="cv" type="file" value="" class="form-control">
                </div>
              <!-- /.form-group -->
            </div>
            
            <div class="col-md-6">
                <div class="form-group">
                  <label for="">Surname</label>
                  <input placeholder="Enter Surname" name="sname" type="text" value="<?php echo isset($record->sname) ? $record->sname : ''; ?>" class="form-control" required>
                </div>
                
                <div class="form-group">
                  <label for="">Phone</label>
                  <input placeholder="Enter Phone" name="phone" type="text" value="<?php echo isset($record->phone) ? $record->phone : ''; ?>" class="form-control" required>
                </div>
                
                <div class="form-group">
                  <label for="">City</label>
                  <input placeholder="Enter City" type="text" name="city" class="form-control" value="<?php echo isset($record->city) ? $record->city : ''; ?>">
                </div>
                
                <div class="form-group">
                  <label for="">Gender</label>
                  <div>
                      <input type="radio" class="minimal" name="gender" value="male" <?php echo isset($record->gender) && ($record->gender == 'male') ? 'checked' : ''; ?>> Male
                      <input type="radio" class="minimal1" name="gender" value="female" <?php echo isset($record->gender) && ($record->gender == 'female') ? 'checked' : ''; ?>> Female
                  </div>
                </div>   
              <!-- /.form-group -->
            </div>
            
            
            <!-- /.col -->
          </div>
          <!-- /.row -->
        </div>
        <!-- /.box-body -->
        <div class="box-footer">
          Visit <a href="https://select2.github.io/">Select2 documentation</a> for more examples and information about
          the plugin.
        </div>
      </div>
      <!-- /.box -->
      
      
      <!-- Biography EXAMPLE -->
      <div class="box box-default collapsed-box">
        <div class="box-header with-border">
          <h3 class="box-title">Biography</h3>

          <div class="box-tools pull-right">
            <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-plus"></i></button>
            <!--<button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-remove"></i></button>-->
          </div>
        </div>
        <!-- /.box-header -->
        <div class="box-body">
          <div class="row">
            <!-- /.col -->
            <div class="col-md-12">
                <div class="form-group">
                    <label>Description </label><span> (Maximum 150 Words)</span>
                    <textarea rows="5" placeholder="Enter Description ..." class="form-control" id="content" name="description" ><?php echo isset($record->description) ? $record->description : ''; ?></textarea>
                </div>
              <!-- /.form-group -->
            </div>
            
        
            
            
            <!-- /.col -->
          </div>
          <!-- /.row -->
        </div>
        <!-- /.box-body -->
        <div class="box-footer">
          Visit <a href="https://select2.github.io/">Select2 documentation</a> for more examples and information about
          the plugin.
        </div>
      </div>
      
      
     
      
      
       <!-- Type EXAMPLE -->
      <div class="box box-default collapsed-box">
        <div class="box-header with-border">
          <h3 class="box-title">Type</h3>

          <div class="box-tools pull-right">
            <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-plus"></i></button>
            <!--<button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-remove"></i></button>-->
          </div>
        </div>
        <!-- /.box-header -->
        <div class="box-body">
          <div class="row">
            <!-- /.col -->
            <div class="col-md-6">
                
                 <div class="form-group">
                  <label for="">Profession Number</label>
                  <input placeholder="Enter Profession Number" name="profession_number" type="text" value="<?php echo isset($record->profession_number) ? $record->profession_number : ''; ?>" class="form-control">
                </div>
                
                <div class="form-group">
                  <label for="">Practice Number</label>
                  <input placeholder="Enter Practice Number" name="practice_number" type="text" value="<?php echo isset($record->practice_number) ? $record->practice_number : ''; ?>" class="form-control">
                </div>
              <!-- /.form-group -->
            </div>
            
            
            
            <div class="col-md-6">
                <div class="form-group">
                  <label for=""> Speciality </label>
                  <input placeholder="Enter Speciality" name="speciality" type="text" value="<?php echo isset($record->speciality) ? $record->speciality : ''; ?>" class="form-control">
                </div>
                
                
                
                <div class="form-group">
                  <label for="">Area of training</label>
                  <div>
                        <input class="minimal1" id="type-of-doctor-2" type="radio" name="type_of_doctor" value="2" <?php echo isset($record->type_of_doctor) && ($record->type_of_doctor == 'psychologist') ? 'checked' : '' ;?>> Psychologist  
                        <input class="minimal1" id="type-of-doctor-1" type="radio" name="type_of_doctor" value="1" <?php echo isset($record->type_of_doctor) && ($record->type_of_doctor == 'dietitian') ? 'checked' : '' ;?>> Dietitian 
                        <input class="minimal1" id="type-of-doctor-3" type="radio" name="type_of_doctor" value="3" <?php echo isset($record->type_of_doctor) && ($record->type_of_doctor == 'medical-doctor') ? 'checked' : '' ;?>> Medical Doctor
                  </div>
                </div>  
                
              <!-- /.form-group -->
            </div>
            
            
            <!-- /.col -->
          </div>
          <!-- /.row -->
        </div>
        <!-- /.box-body -->
        <div class="box-footer">
          Visit <a href="https://select2.github.io/">Select2 documentation</a> for more examples and information about
          the plugin.
        </div>
      </div>
      
       
       
      
      
         <!-- Days EXAMPLE -->
      <div class="box box-default collapsed-box">
        <div class="box-header with-border">
          <h3 class="box-title">Days & Timing</h3>

          <div class="box-tools pull-right">
            <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-plus"></i></button>
            <!--<button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-remove"></i></button>-->
          </div>
        </div>
        <!-- /.box-header -->
        <div class="box-body">
          <div class="row">
            <!-- /.col -->
            <div class="col-md-6">
                
                <div class="row">

                    <div class="col-sm-3">
                        <div class="form-group">
                            <input class="minimal1" type="checkbox" name="dayoftheweek[]" value="1" <?php echo (isset($doctorDays) && in_array('monday', $doctorDays)) ? 'checked' : ''; ?>/>
                            <label> Monday</label>
                        </div>
                    </div>
                    
                    <div class="col-sm-3">
                        <div class="form-group">
                            <input class="minimal1" type="checkbox" name="dayoftheweek[]" value="2" <?php echo (isset($doctorDays) && in_array('tuesday', $doctorDays)) ? 'checked' : ''; ?>/>
                            <label> Tuesday</label>
                        </div>
                    </div>
                    
                    <div class="col-sm-3">
                        <div class="form-group">
                            <input class="minimal1" type="checkbox" name="dayoftheweek[]" value="3" <?php echo (isset($doctorDays) && in_array('wednesday', $doctorDays)) ? 'checked' : ''; ?>/>
                            <label> Wednesday</label>
                        </div>
                    </div>
                    
                    <div class="col-sm-3">
                        <div class="form-group">
                            <input class="minimal1" type="checkbox" name="dayoftheweek[]" value="4" <?php echo (isset($doctorDays) && in_array('thursday', $doctorDays)) ? 'checked' : ''; ?>/>
                            <label> Thursday</label>
                        </div>
                    </div>
                    
                    <div class="col-sm-3">
                        <div class="form-group">
                            <input class="minimal1" type="checkbox" name="dayoftheweek[]" value="5" <?php echo (isset($doctorDays) && in_array('friday', $doctorDays)) ? 'checked' : ''; ?>/>
                            <label> Friday</label>
                        </div>
                    </div>
                    
                    <div class="col-sm-3">
                        <div class="form-group">
                            <input class="minimal1" type="checkbox" name="dayoftheweek[]" value="6" <?php echo (isset($doctorDays) && in_array('saturday', $doctorDays)) ? 'checked' : ''; ?>/>
                            <label> Saturday</label>
                        </div>
                    </div>
                    
                    <div class="col-sm-3">
                        <div class="form-group">
                            <input class="minimal1" type="checkbox" name="dayoftheweek[]" value="7" <?php echo (isset($doctorDays) && in_array('sunday', $doctorDays)) ? 'checked' : ''; ?>/>
                            <label> Sunday</label>
                        </div>
                    </div>
                    
                </div>    
                
             
              <!-- /.form-group -->
            </div>
            
            
            
            <div class="col-md-6">
                <div class="form-group">
                  <label>Timing</label>
                    <select size="1" id="timings" title="" name="timings" class="form-control select2" style="width: 100%;">
                        <option value="">Select your timing</option>
                        <option value="fixed" <?php echo (count($doctorTimings) == 1) ? 'selected' : '' ;?>>Fixed</option>
                        <option value="flexible" <?php echo (count($doctorTimings) == 2) ? 'selected' : '' ;?>>Flexible</option>
                    </select>
                </div>
              <!-- /.form-group -->
            </div>
            
            <div class="col-md-12">
                
                <div class="row">
                    <div class="col-sm-6">
                      <div class="bootstrap-timepicker">
                        <div class="form-group">
                          <label>Time picker:</label>

                          <div class="input-group">
                            <input type="text" class="form-control timepicker">

                            <div class="input-group-addon">
                              <i class="fa fa-clock-o"></i>
                            </div>
                          </div>
                        </div>
                      </div>

                    </div>
                    
                    <div class="col-sm-6">
                      <div class="bootstrap-timepicker">
                        <div class="form-group">
                          <label>Time picker:</label>

                          <div class="input-group">
                            <input type="text" class="form-control timepicker">

                            <div class="input-group-addon">
                              <i class="fa fa-clock-o"></i>
                            </div>
                          </div>
                        </div>
                      </div>

                    </div>
                </div>
                
                
            </div>
            
            <div class="col-md-12">
                
                <div class="row">
                    <div class="col-sm-6">
                      <div class="bootstrap-timepicker">
                        <div class="form-group">
                          <label>Time picker:</label>

                          <div class="input-group">
                            <input type="text" class="form-control timepicker">

                            <div class="input-group-addon">
                              <i class="fa fa-clock-o"></i>
                            </div>
                          </div>
                        </div>
                      </div>

                    </div>
                    
                    <div class="col-sm-6">
                      <div class="bootstrap-timepicker">
                        <div class="form-group">
                          <label>Time picker:</label>

                          <div class="input-group">
                            <input type="text" class="form-control timepicker">

                            <div class="input-group-addon">
                              <i class="fa fa-clock-o"></i>
                            </div>
                          </div>
                        </div>
                      </div>

                    </div>
                </div>
                
                
            </div>
            
                
                
            
            
            <!-- /.col -->
          </div>
          <!-- /.row -->
        </div>
        <!-- /.box-body -->
        <div class="box-footer">
          Visit <a href="https://select2.github.io/">Select2 documentation</a> for more examples and information about
          the plugin.
        </div>
      </div>
      <!-- /.box -->
      
       <!-- Working Experience -->
      <div class="box box-default collapsed-box">
        <div class="box-header with-border">
          <h3 class="box-title">Working Experience</h3>

          <div class="box-tools pull-right">
            <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-plus"></i></button>
            <!--<button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-remove"></i></button>-->
          </div>
        </div>
        <!-- /.box-header -->
        <div class="box-body">
          <div class="row">
            <!-- /.col -->
            
            <div class="col-md-3">
               <div class="form-group">
                  <label for="">Profession Number</label>
                  <input placeholder="Enter Profession Number" name="profession_number" type="text" value="<?php echo isset($record->profession_number) ? $record->profession_number : ''; ?>" class="form-control">
               </div> 
            </div>                 
            
            <div class="col-md-3">
               <div class="form-group">
                  <label for="">Profession Number</label>
                  <input placeholder="Enter Profession Number" name="profession_number" type="text" value="<?php echo isset($record->profession_number) ? $record->profession_number : ''; ?>" class="form-control">
               </div> 
            </div>                 
            
            <div class="col-md-2">
                 <div class="bootstrap-timepicker">
                    <div class="form-group">
                        <label>Start Date</label>
                        <div class="input-group">
                          <div class="input-group-addon">
                            <i class="fa fa-calendar"></i>
                          </div>
                          <input type="text" class="form-control" data-inputmask="'alias': 'dd/mm/yyyy'" data-mask>
                        </div>
                        <!-- /.input group -->
                      </div>
                  </div>

                </div>  
            
            <div class="col-md-2">
             <div class="bootstrap-timepicker">
                <div class="form-group">
                    <label>End Date:</label>

                    <div class="input-group">
                      <div class="input-group-addon">
                        <i class="fa fa-calendar"></i>
                      </div>
                      <input type="text" class="form-control" data-inputmask="'alias': 'dd/mm/yyyy'" data-mask>
                    </div>
                    <!-- /.input group -->
                  </div>
              </div>

            </div>  
            
            <div class="col-md-2">
               <div class="form-group">
                   <div class="top-space"></div> 
                  <label for="">Present</label>
                  <input placeholder="Enter Profession Number" name="profession_number" type="checkbox" value="<?php echo isset($record->profession_number) ? $record->profession_number : ''; ?>" class="">
               </div> 
            </div>
            
            <div class="add-academic col-lg-12">
                <a href="javascript:void(0)" class="pull-right">
                    <button class="btn btn-success submit-button">Add More Academic </button>
                </a>
            </div>
            
            <!-- /.col -->
          </div>
          <!-- /.row -->
        </div>
        <!-- /.box-body -->
        <div class="box-footer">
          Visit <a href="https://select2.github.io/">Select2 documentation</a> for more examples and information about
          the plugin.
        </div>
      </div>
      <!-- /.box -->
      
      
       <!-- Academic qualifications -->
      <div class="box box-default ">
        <div class="box-header with-border">
          <h3 class="box-title">Academic qualifications</h3>

          <div class="box-tools pull-right">
            <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
            <!--<button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-remove"></i></button>-->
          </div>
        </div>
        <!-- /.box-header -->
        <div class="box-body">
          <div class="row">
            <!-- /.col -->
            
            <div class="col-md-4">
               <div class="form-group">
                  <label for="">Qualification</label>
                  <input placeholder="Enter Profession Number" name="profession_number" type="text" value="<?php echo isset($record->profession_number) ? $record->profession_number : ''; ?>" class="form-control">
               </div> 
            </div>                 
            
            <div class="col-md-4">
               <div class="form-group">
                  <label for="">Institution</label>
                  <input placeholder="Enter Profession Number" name="profession_number" type="text" value="<?php echo isset($record->profession_number) ? $record->profession_number : ''; ?>" class="form-control">
               </div> 
            </div>                 
            
            <div class="col-md-4">
                 <div class="bootstrap-timepicker">
                    <div class="form-group">
                        <label>Year</label>
                        <div class="input-group">
                          <div class="input-group-addon">
                            <i class="fa fa-calendar"></i>
                          </div>
                          <input type="text" class="form-control" data-inputmask="'alias': 'dd/mm/yyyy'" data-mask>
                        </div>
                        <!-- /.input group -->
                      </div>
                  </div>

                </div>  
            
            <div class="add-academic col-lg-12">
                <a href="javascript:void(0)" class="pull-right">
                    <button class="btn btn-success submit-button">Add More Academic </button>
                </a>
            </div>
            
            <!-- /.col -->
          </div>
          <!-- /.row -->
        </div>
        <!-- /.box-body -->
        <div class="box-footer">
          Visit <a href="https://select2.github.io/">Select2 documentation</a> for more examples and information about
          the plugin.
        </div>
      </div>
      <!-- /.box -->
      
      <!-- /.box -->

      <div class="row">
        <div class="col-md-6">

          <div class="box box-danger">
            <div class="box-header">
              <h3 class="box-title">Input masks</h3>
            </div>
            <div class="box-body">
              <!-- Date dd/mm/yyyy -->
              <div class="form-group">
                <label>Date masks:</label>

                <div class="input-group">
                  <div class="input-group-addon">
                    <i class="fa fa-calendar"></i>
                  </div>
                  <input type="text" class="form-control" data-inputmask="'alias': 'dd/mm/yyyy'" data-mask>
                </div>
                <!-- /.input group -->
              </div>
              <!-- /.form group -->

              <!-- Date mm/dd/yyyy -->
              <div class="form-group">
                <div class="input-group">
                  <div class="input-group-addon">
                    <i class="fa fa-calendar"></i>
                  </div>
                  <input type="text" class="form-control" data-inputmask="'alias': 'mm/dd/yyyy'" data-mask>
                </div>
                <!-- /.input group -->
              </div>
              <!-- /.form group -->

              <!-- phone mask -->
              <div class="form-group">
                <label>US phone mask:</label>

                <div class="input-group">
                  <div class="input-group-addon">
                    <i class="fa fa-phone"></i>
                  </div>
                  <input type="text" class="form-control" data-inputmask='"mask": "(999) 999-9999"' data-mask>
                </div>
                <!-- /.input group -->
              </div>
              <!-- /.form group -->

              <!-- phone mask -->
              <div class="form-group">
                <label>Intl US phone mask:</label>

                <div class="input-group">
                  <div class="input-group-addon">
                    <i class="fa fa-phone"></i>
                  </div>
                  <input type="text" class="form-control"
                         data-inputmask="'mask': ['999-999-9999 [x99999]', '+099 99 99 9999[9]-9999']" data-mask>
                </div>
                <!-- /.input group -->
              </div>
              <!-- /.form group -->

              <!-- IP mask -->
              <div class="form-group">
                <label>IP mask:</label>

                <div class="input-group">
                  <div class="input-group-addon">
                    <i class="fa fa-laptop"></i>
                  </div>
                  <input type="text" class="form-control" data-inputmask="'alias': 'ip'" data-mask>
                </div>
                <!-- /.input group -->
              </div>
              <!-- /.form group -->

            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->

          <div class="box box-info">
            <div class="box-header">
              <h3 class="box-title">Color & Time Picker</h3>
            </div>
            <div class="box-body">
              <!-- Color Picker -->
              <div class="form-group">
                <label>Color picker:</label>
                <input type="text" class="form-control my-colorpicker1">
              </div>
              <!-- /.form group -->

              <!-- Color Picker -->
              <div class="form-group">
                <label>Color picker with addon:</label>

                <div class="input-group my-colorpicker2">
                  <input type="text" class="form-control">

                  <div class="input-group-addon">
                    <i></i>
                  </div>
                </div>
                <!-- /.input group -->
              </div>
              <!-- /.form group -->

              <!-- time Picker -->
              <div class="bootstrap-timepicker">
                <div class="form-group">
                  <label>Time picker:</label>

                  <div class="input-group">
                    <input type="text" class="form-control timepicker">

                    <div class="input-group-addon">
                      <i class="fa fa-clock-o"></i>
                    </div>
                  </div>
                  <!-- /.input group -->
                </div>
                <!-- /.form group -->
              </div>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->

        </div>
        <!-- /.col (left) -->
        <div class="col-md-6">
          <div class="box box-primary">
            <div class="box-header">
              <h3 class="box-title">Date picker</h3>
            </div>
            <div class="box-body">
              <!-- Date -->
              <div class="form-group">
                <label>Date:</label>

                <div class="input-group date">
                  <div class="input-group-addon">
                    <i class="fa fa-calendar"></i>
                  </div>
                  <input type="text" class="form-control pull-right" id="datepicker">
                </div>
                <!-- /.input group -->
              </div>
              <!-- /.form group -->

              <!-- Date range -->
              <div class="form-group">
                <label>Date range:</label>

                <div class="input-group">
                  <div class="input-group-addon">
                    <i class="fa fa-calendar"></i>
                  </div>
                  <input type="text" class="form-control pull-right" id="reservation">
                </div>
                <!-- /.input group -->
              </div>
              <!-- /.form group -->

              <!-- Date and time range -->
              <div class="form-group">
                <label>Date and time range:</label>

                <div class="input-group">
                  <div class="input-group-addon">
                    <i class="fa fa-clock-o"></i>
                  </div>
                  <input type="text" class="form-control pull-right" id="reservationtime">
                </div>
                <!-- /.input group -->
              </div>
              <!-- /.form group -->

              <!-- Date and time range -->
              <div class="form-group">
                <label>Date range button:</label>

                <div class="input-group">
                  <button type="button" class="btn btn-default pull-right" id="daterange-btn">
                    <span>
                      <i class="fa fa-calendar"></i> Date range picker
                    </span>
                    <i class="fa fa-caret-down"></i>
                  </button>
                </div>
              </div>
              <!-- /.form group -->

            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->

          <!-- iCheck -->
          <div class="box box-success">
            <div class="box-header">
              <h3 class="box-title">iCheck - Checkbox &amp; Radio Inputs</h3>
            </div>
            <div class="box-body">
              <!-- Minimal style -->

              <!-- checkbox -->
              <div class="form-group">
                <label>
                  <input type="checkbox" class="minimal" checked>
                </label>
                <label>
                  <input type="checkbox" class="minimal">
                </label>
                <label>
                  <input type="checkbox" class="minimal" disabled>
                  Minimal skin checkbox
                </label>
              </div>

              <!-- radio -->
              <div class="form-group">
                <label>
                  <input type="radio" name="r1" class="minimal" checked>
                </label>
                <label>
                  <input type="radio" name="r1" class="minimal">
                </label>
                <label>
                  <input type="radio" name="r1" class="minimal" disabled>
                  Minimal skin radio
                </label>
              </div>

              <!-- Minimal red style -->

              <!-- checkbox -->
              <div class="form-group">
                <label>
                  <input type="checkbox" class="minimal-red" checked>
                </label>
                <label>
                  <input type="checkbox" class="minimal-red">
                </label>
                <label>
                  <input type="checkbox" class="minimal-red" disabled>
                  Minimal red skin checkbox
                </label>
              </div>

              <!-- radio -->
              <div class="form-group">
                <label>
                  <input type="radio" name="r2" class="minimal-red" checked>
                </label>
                <label>
                  <input type="radio" name="r2" class="minimal-red">
                </label>
                <label>
                  <input type="radio" name="r2" class="minimal-red" disabled>
                  Minimal red skin radio
                </label>
              </div>

              <!-- Minimal red style -->

              <!-- checkbox -->
              <div class="form-group">
                <label>
                  <input type="checkbox" class="flat-red" checked>
                </label>
                <label>
                  <input type="checkbox" class="flat-red">
                </label>
                <label>
                  <input type="checkbox" class="flat-red" disabled>
                  Flat green skin checkbox
                </label>
              </div>

              <!-- radio -->
              <div class="form-group">
                <label>
                  <input type="radio" name="r3" class="flat-red" checked>
                </label>
                <label>
                  <input type="radio" name="r3" class="flat-red">
                </label>
                <label>
                  <input type="radio" name="r3" class="flat-red" disabled>
                  Flat green skin radio
                </label>
              </div>
            </div>
            <!-- /.box-body -->
            <div class="box-footer">
              Many more skins available. <a href="http://fronteed.com/iCheck/">Documentation</a>
            </div>
          </div>
          <!-- /.box -->
        </div>
        <!-- /.col (right) -->
      </div>
      <!-- /.row -->

    </section>
    <!-- /.content -->
  </div>