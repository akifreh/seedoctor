 <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Add Our Client
<!--        <small>#007612</small>-->
      </h1>
      <ol class="breadcrumb">
        <li><a href="<?php echo base_url();?>admin"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="<?php echo base_url();?>admin/userlist/clients">Our Clients</a></li>
        <li class="active">Add Client</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="invoice">
      <?php $attributes = array("name" => "" , "class" => "form-horizontal", "id" => "", "enctype" => 'multipart/form-data');
       echo form_open("admin/addClient", $attributes);?>

              <div class="box-body">
                
                <div class="form-group">
                  <label class="col-sm-2 control-label">Logo</label>
                  <div class="col-sm-10">
                    <img src="<?php echo isset($record->profile_pic) ? $record->profile_pic : ''; ?>" id="pro-pic-id" alt="Profile-Image" style="width:40%;height: 210px;" />
                    <input type="file" name="profile_pic" id="add-pro-pic" class="add-profile-pic"> 
                    <span class="add-pro-pic-link"><i class="fa fa-camera" aria-hidden="true"></i> Add Photo</span>
                  </div>
                </div>

                <div class="form-group">
                  <label for="Link" class="col-sm-2 control-label">Website</label>

                  <div class="col-sm-10">
                    <input type="text" class="form-control" placeholder="Company Website" id="website" name="website">
                  </div>
                </div>
                
                <div class="form-group">
                  <label for="inputPassword3" class="col-sm-2 control-label">Company Name</label>

                  <div class="col-sm-10">
                    <input type="text" class="form-control" placeholder="Company Name" id="companyname" name="company_name">
                  </div>
                </div>

                <div class="form-group">
                  <label for="inputPassword3" class="col-sm-2 control-label">Email</label>

                  <div class="col-sm-10">
                    <input type="text" class="form-control" placeholder="Email Address" id="email" name="email">
                  </div>
                </div>

                <div class="form-group">
                  <label for="inputPassword3" class="col-sm-2 control-label">Company Size</label>

                  <div class="col-sm-10">
                    <input type="text" class="form-control" placeholder="Company Size" id="CompanySize" name="company_size">
                  </div>
                </div>
                <div class="form-group">
                	<label for="inputPassword3" class="col-sm-2 control-label">Description</label>

                  	<div class="col-sm-10">
                  	<textarea name="description" class="form-control" id="description" placeholder="description"></textarea>
                	</div>
            	</div>
              </div>
              <!-- /.box-body -->
              <div class="box-footer">
                <!--button type="submit" class="btn btn-default">Cancel</button-->
                <button type="submit" class="btn btn-info pull-right">Save</button>
              </div>
              <!-- /.box-footer -->
            </form>
	</section>

</div>

<script type="text/javascript">
function readURL(input) {

    if (input.files && input.files[0]) {
        
         var url = input.value;
    var ext = url.substring(url.lastIndexOf('.') + 1).toLowerCase();
        
        
        if (input.files && input.files[0]&& (ext == "gif" || ext == "png" || ext == "jpeg" || ext == "jpg")) {
            
            var reader = new FileReader();
            reader.onload = function (e) {
                $('#pro-pic-id').attr('src', e.target.result);
            }
            reader.readAsDataURL(input.files[0]);
            
        }
        else
        {
            alert("Please upload a valid image file.");
            $("#add-pro-pic").val('');
        }    
        
    }
}

$("#add-pro-pic").change(function(){
    readURL(this);
});
</script>