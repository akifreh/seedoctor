 <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Patient Detail
<!--        <small>#007612</small>-->
      </h1>
      <ol class="breadcrumb">
        <li><a href="<?php echo base_url();?>admin"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="<?php echo base_url();?>admin/userlist/patient">Patients</a></li>
        <li class="active">Patient Detail</li>
      </ol>
    </section>

<!--    <div class="pad margin no-print">
      <div class="callout callout-info" style="margin-bottom: 0!important;">
        <h4><i class="fa fa-info"></i> Note:</h4>
        This page has been enhanced for printing. Click the print button at the bottom of the invoice to test.
      </div>
    </div>-->

    <!-- Main content -->
    <section class="invoice">
      <!-- title row -->
      <div class="row">
        <div class="col-xs-12">
          <h2 class="page-header">
            <i class="fa fa-user-circle-o"></i> <?php echo isset($row->fname) ? ucfirst($row->fname).' ' : ''; ?><?php echo isset($row->sname) ? ucfirst($row->sname) : ''; ?>
            <!--<small class="pull-right">Date: 2/10/2014</small>-->
          </h2>
        </div>
        <!-- /.col -->
      </div>
      
      
      
      <!-- info row -->
      <div class="row invoice-info">
        
          
        <div class="col-sm-3 invoice-col pad_btm_50" >
            <img src="<?php echo $row ? $row->profile_pic : ''; ?>" alt="" class="img-responsive">
        </div>  
          
        <div class="col-sm-3 invoice-col font_16">
            <address>
            <h2><strong>Personal Details</strong></h2>
            
            <p><strong>Name : </strong><?php echo isset($row->fname) ? ucfirst($row->fname) : ''; ?></p>
            <p><strong>Surname : </strong><?php echo isset($row->sname) ? ucfirst($row->sname) : ''; ?></p>
            <p><strong>Date of birth : </strong><?php echo isset($row->dob) ? date('d M Y', strtotime($row->dob)) : ''; ?></p>
          </address>
        </div>
          
        <div class="col-sm-3 invoice-col font_16">
          
        <address>
            <h2><strong>Contact Details</strong></h2>
            <p><strong class="grey" >Residental Address</strong></p>
            <p><strong>Address : </strong><?php echo isset($row->address) ? $row->address : '';  ?></p>
            <p><strong>City : </strong><?php echo isset($row->city) ? $row->city : ''; ?></p>
            <p><strong>Province : </strong><?php echo isset($row->province) ? $row->province : ''; ?></p>
            <p><strong>Contact Number : </strong><?php echo isset($row->phone) ? $row->phone : ''; ?></p>
            <p><strong>Cell Number : </strong><?php echo isset($row->cell) ? $row->cell : ''; ?></p>
          </address>
        </div>
          
        <div class="col-sm-3 invoice-col font_16">
          
        <address>
            <h2><strong>Postal Address</strong></h2>
            
            <p><strong>Address : </strong><?php echo isset($row->p_address) ? $row->p_address : ''; ?></p>
            <p><strong>City : </strong><?php echo isset($row->p_city) ? $row->p_city : ''; ?></p>
            <p><strong>Province : </strong><?php echo isset($row->p_province) ? $row->p_province : '';  ?></p>
            <p><strong>Email Address : </strong><a href="mailto:<?php echo $row ? $row->email : ''; ?>"><?php echo $row ? $row->email : ''; ?></a></p>
          </address>
        </div>  
        <!-- /.col -->
      </div>
      <!-- /.row -->

      <!-- Table row -->
      
       <div class="row">
        <?php if(!empty($userOtherProfile)) : ?>

           <div class="col-xs-12 table-responsive pad_btm_40" >
                  <table class="table table-striped">
                      <caption class="table_caption"><strong>Statutory</strong></caption>
                    <thead>
                    <tr>
                      <th>Job Title</th>
                      <th>Company Employed</th>
                      <th>Next of Keen</th>
                      <th>Medical Aid</th>
                    </tr>
                    </thead>

                    <tbody>
                      <?php foreach ($userOtherProfile as $row2) : ?>
                            <tr>
                              <td><?php echo isset($row->job_title) ? $row->job_title : '' ;?></td>
                              <td><?php echo isset($row->company_employed) ? $row->company_employed : '';?></td>
                              <td><?php echo isset($row->next_of_kin) ? $row->next_of_kin : '';?></td>
                              <td><?php echo isset($row->medical_aid) ? $row->medical_aid : ''; ?></td>
                            </tr>
                      <?php endforeach; ?>
                    </tbody>

                  </table>
                </div>
                <!-- /.col -->
              
        <?php endif; ?>
      
      <?php if(!empty($userOtherProfile)) : ?>
              <div class="col-xs-12 table-responsive">
                <table class="table table-striped">
                    <caption class="table_caption"><strong>Other Profile</strong></caption>
                  <thead>
                  <tr>
                    <th>Name</th>
                    <th>Relationship</th>
                    <th>Date of Birth</th>
                  </tr>
                  </thead>

                  <tbody>
                    <?php foreach ($userOtherProfile as $row2) : ?>
                          <tr>
                            <td><?php echo ucfirst($row2->name); ?></td>
                            <td><?php echo ucfirst($row2->relationship); ?></td>
                            <td><?php echo date('d M Y', strtotime($row2->dob)); ?></td>
                          </tr>
                    <?php endforeach; ?>
                  </tbody>

                </table>
              </div>
              <!-- /.col -->
            
      <?php endif; ?>
       </div>      
      
<!--        <div class="row">
         accepted payments column 
        <div class="col-xs-6">
          <p class="lead">Statutory:</p>
          <p class="text-muted well well-sm no-shadow" style="margin-top: 10px;">
            Job Title : <?php echo isset($row->job_title) ? $row->job_title : '' ;?>
            dopplr jibjab, movity jajah plickers sifteo edmodo ifttt zimbra.
          </p>
        </div>
       
      </div>-->
      <!-- /.row -->
    </section>
    <!-- /.content -->
    <div class="clearfix"></div>
  </div>