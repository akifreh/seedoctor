<div class="content-wrapper">
  <section class="content-header">
    <h1>Our Clients</h1>
      <ol class="breadcrumb">
        <li><a href="<?php echo base_url();?>admin"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active"><?php echo 'our-clients';?> </li>
      </ol>
    </section>

    <section class="content">
      <div class="row">
        <div class="col-xs-12">            
          <?php echo $this->session->flashdata('message');?>                
          <div class="box" > 
            <div class="box-header">
              <h3 class="box-title">Our Client List</h3>
              <a href="<?php echo base_url(); ?>admin/addClient"> <button class="btn btn-success add-doctor-btn submit-button" style=" float: right;">Add a Client </button></a>
            </div>
            <!-- /.box-header -->
            <div class="box-body ">
              <table id="patient_table" class="table table-bordered table-striped">
                <thead>
                <tr>
                  <th>Title</th>
                  <th>Logo</th>
                  <th>Details</th>
                  <th>Status</th>
                  <th>Action</th>
                </tr>
                </thead>
                <tbody>
                <?php if($records) :
                  foreach ($records as $row) :?>
                    <tr>
                      <td><?php echo $row->title; ?></td>
                      <td><a href="<?php echo base_url() . 'uploads/clients/'. $row->logo;?>" target="_blank"><img src="<?php echo base_url() . 'uploads/clients/'. $row->logo; ?>" height="30" width="50"/></a></td>
                      <td>
                          <a href="<?php echo base_url() .'admin/client_detail/'. $row->id; ?>">
                            <button type="button" class="btn btn-a btn-info" >View</button>
                          </a>
                      </td>
                      <td><?php echo ($row->status == 1) ? 'Active' : 'Inactive'; ?></td>
                      <td><?php if($row->status == 1){ ?>
                          <a href="<?php echo base_url() .'admin/updateClientStatus/'. $row->id . '/2'; ?>" class="fa fa-eye-slash suspend-icon" title="Un Active" ></a>
                          <?php } ?>

                          <?php if($row->status == 2){ ?>
                          <a href="<?php echo base_url() .'admin/updateClientStatus/'. $row->id . '/1'; ?>" class="fa fa-eye active-icon" title="Active" ></a>
                          <?php } ?>
                          <a href="<?php echo base_url() .'admin/deleteClient/'. $row->id; ?>" onClick="return confirm('Are you absolutely sure you want to delete?')" class="fa fa-trash" title="Delete"></a>
                      </td>  
                    </tr>
                   <?php endforeach; 
                  endif; ?>
                </tbody>
              </table>
            </div>
          </div>
        </div>
      </div>
    </section>
            
