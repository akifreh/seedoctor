
<?php// include_once('header.php')?>

 <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Payments
        <!--<small>advanced tables</small>-->
      </h1>
      <ol class="breadcrumb">
        <li><a href="<?php echo base_url();?>admin"><i class="fa fa-dashboard"></i> Home</a></li>
        <!--<li><a href="#">Tables</a></li>-->
        <li class="active">Payments</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-xs-12">
          <div class="box">
            <div class="box-header">
              <h3 class="box-title">Payments List</h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body ">
              <table id="payment_table" class="table table-bordered table-striped">
                <thead>
                <tr>
                  <th>#</th>
                  <th>Appointment</th>
                  <th>Patient</th>
                  <th>Doctor</th>
                  <th>Duration</th>
                  <th>Charged</th>
                  <th>Price</th>
                  <th>Status</th>
                  <th>Transaction Id</th>
                  <th>Capture Date</th>
                  
                </tr>
                </thead>
                <tbody>
                    <?php if($records) :
                        $count = 1;
                        foreach ($records as $row) :?>
                        <tr>
                          <td><?php echo $count++; ?></td>
                          <td><a href="<?php echo base_url() . 'appointment/detail/' . $row->appointment_id;?>"><?php echo $row->appointment_id; ?></a></td>
                          <td><a href="<?php echo base_url() . 'admin/viewUserDetail/' . $row->patientInfo->id; ?>"><?php echo $row->patientInfo->fname; ?></a></td>
                          <td><a href="<?php echo base_url() . 'admin/viewDoctorDetail/' . $row->doctorInfo->user_id; ?>"><?php echo $row->doctorInfo->fname; ?></a></td>
                          <td><?php echo date('i:s ' , strtotime($row->actual_duration)); ?></td>
                          <td><?php echo date('i ' , strtotime($row->charged_duration)) . ' @' . round($row->rate * 15); ?></td>
                          <td><?php echo $row->price; ?></td>
                          <td><?php echo $row->status ? 'Captured' : 'Error'; ?></td>
                          <td><?php echo $row->transaction_id ? $row->transaction_id : '-------'; ?></td>
                          <td><?php echo $row->created_at ? date('d M Y - h:i a', strtotime($row->created_at)) : '------'; ?></td>
                         
                        </tr>
                         <?php  endforeach; 
                                endif; ?>
                </tbody>
              </table>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
  </div>






  
