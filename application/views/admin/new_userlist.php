
<?php if($this->uri->segment(3) == 'patient'):
        $navigation = 'Patient Management';

        elseif($this->uri->segment(3) == 'pages'):
        $navigation = 'Pages Management';    
            
        elseif($this->uri->segment(3) == 'doctor'):
        $navigation = 'Doctor List';   
        
        elseif($this->uri->segment(3) == 'doctor_rates'):
        $navigation = 'Doctor Rates';   
        
        endif;
    
?>

 <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
      <?= $navigation?>
        <!--<small>advanced tables</small>-->
      </h1>
      <ol class="breadcrumb">
        <li><a href="<?php echo base_url();?>admin"><i class="fa fa-dashboard"></i> Home</a></li>
        <!--<li><a href="#">Tables</a></li>-->
        <li class="active"><?php echo $navigation;?> </li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-xs-12">
            <!--<div class="row">-->
            
                <?php echo $this->session->flashdata('msg_suspend');?>
            
                <?php if($this->uri->segment(3) == 'patient'): ?>
                
                    <div class="box" > 
            <div class="box-header">
              <h3 class="box-title">Patients List</h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body ">
              <table id="patient_table" class="table table-bordered table-striped">
                <thead>
                <tr>
                  <th>#</th>
                  <th>Name</th>
                  <th>Email</th>
                  <th>Gender</th>
                  <th>Details</th>
                  <th>&nbsp;</th>
                </tr>
                </thead>
                <tbody>
                    <?php if($userslist) :
                        $count = 1;
                        foreach ($userslist as $row) :?>
                        <tr>
                            <td><?php echo $count++; ?></td>
                            <td><?php echo $row->fname; ?></td>
                            <td><?php echo $row->email; ?></td>
                            <td><?php echo ucfirst($row->gender); ?></td>
                            <td>
                                <a href="<?php echo base_url() .'admin/viewUserDetail/'. $row->id; ?>">
                                    <button type="button" class="btn btn-a btn-info" >View</button>
                                </a>
                            </td>
                            <td><?php if($row->status == 1){ ?>
                                <a href="<?php echo base_url() .'admin/usersuspend/patient/'. $row->id; ?>" class="fa fa-eye-slash suspend-icon" title="Suspend" ></a>
                                <?php } ?>

                                <?php if($row->status == 3){ ?>
                                <a href="<?php echo base_url() .'admin/useractive/patient/'. $row->id; ?>" class="fa fa-eye active-icon" title="Active" ></a>
                                <?php } ?>
                                <a href="<?php echo base_url() .'admin/deleteUser/'. $row->id; ?>" onClick="return confirm('Are you absolutely sure you want to delete?')" class="fa fa-trash" title="Delete"></a>
                            </td>
                            
                        </tr>
                         <?php  endforeach; 
                                endif; ?>
                </tbody>
              </table>
            </div>
            <!-- /.box-body -->
          </div>
            
                    <?php endif;?>
                
                    
                <?php if($this->uri->segment(3) == 'doctor'): ?>
                    <div class="box">
                    <div class="box-header">
                      <h3 class="box-title">Doctors List</h3>
                      <button class="btn btn-success add-doctor-btn submit-button" data-toggle="modal" data-target=".add-doctor" style=" float: right;"> Add a Doctor </button>
                    </div>
                    <!-- /.box-header -->
                    <div class="box-body">
                      <table id="doctor_table" class="table table-bordered table-striped">
                        <thead>
                        <tr>
                          <th>#</th>
                          <th>Name</th>
                          <th>Email</th>
                          <th>Gender</th>
                          <th>Details</th>
                          <th>&nbsp;</th>
                        </tr>
                        </thead>
                        <tbody>
                            <?php if($doctorProfile) :
                                $count = 1;
                                foreach ($doctorProfile as $row) :?>
                                <tr>
                                    <td><?php echo $count++; ?></td>
                                    <td><?php echo ucfirst($row->fname); ?></td>
                                    <td><?php echo $row->email; ?></td>
                                    <td><?php echo ucfirst($row->gender); ?></td>
                                    <td>
                                        <a href="<?php echo base_url(). 'admin/viewDoctorDetail/'.$row->user_id; ?>">
                                            <button type="button" class="btn btn-a btn-info" >View</button>
                                        </a>
                                    </td>
                                    <td>
                                        <?php if($row->status == 1){ ?>
                                        <a href="<?php echo base_url() .'admin/usersuspend/doctor/'. $row->user_id; ?>" class="fa fa-eye-slash suspend-icon" title="Suspend" ></a>
                                        <?php } ?>

                                        <?php if($row->status == 3){ ?> 
                                        <a href="<?php echo base_url() .'admin/useractive/doctor/'. $row->user_id; ?>" class="fa fa-eye active-icon" title="Active" ></a>
                                        <?php } ?>
                                        <a href="<?php echo base_url() .'admin/UpdateDoctorDetail/'. $row->user_id; ?>" class="fa fa-edit" title="Edit"></a>
                                        <a href="<?php echo base_url() .'admin/deleteDoctor/'. $row->user_id; ?>" onClick="return confirm('Are you absolutely sure you want to delete?')" class="fa fa-trash" title="Delete" ></a>
                                    </td>

                                </tr>
                                 <?php  endforeach; 
                                        endif; ?>
                        </tbody>
                      </table>
                    </div>
                    <!-- /.box-body -->
                  </div>
                <?php endif;?>  

            
                <?php if($this->uri->segment(3) == 'doctor_rates'): ?>
                    <div class="box">
                    <div class="box-header">
                      <h3 class="box-title">Doctor's Rates</h3>
                    </div>
                    <!-- /.box-header -->
                    <div class="box-body ">
                      <table id="dr_rates_table" class="table table-bordered table-striped">
                        <thead>
                        <tr>
                          <th>#</th>
                          <th>Doctor</th>
                          <th>Charges /15 min</th>
                          <th>Edit</th>
                        </tr>
                        </thead>
                        <tbody>
                            <?php if($doctorRates) :
                                $count = 1;
                                foreach ($doctorRates as $row2) :?>
                                <tr>
                                    <td><?php echo $count++; ?></td>
                                    <td><?php echo $row2->doctor_type; ?></td>
                                    <td>R <?php echo $row2->rates; ?></td>
                                    <td><a href="" data-id="<?php echo $row2->id; ?>" onclick="abc($(this).attr('data-id'))" data-toggle="modal" data-target=".edit-doctor-rates" class="fa fa-pencil-square-o submit-button edit-icon" title="Edit">
                                            <!--Edit-->
                                        </a>
                                    </td>

                                </tr>
                                 <?php  endforeach; 
                                        endif; ?>
                        </tbody>
                      </table>
                    </div>
                    <!-- /.box-body -->
                  </div>

                <?php endif;?>          


                <?php if($this->uri->segment(3) == 'pages'): ?>
                    <div class="box">
                    <div class="box-header">
                      <h3 class="box-title">Pages</h3>
                    </div>
                    <!-- /.box-header -->
                    <div class="box-body ">
                      <table id="pages_table" class="table table-bordered table-striped">
                        <thead>
                        <tr>
                          <th>#</th>
                          <th>Name</th>
                          <th>View</th>
                          <th>Edit</th>
                        </tr>
                        </thead>
                        <tbody>
                            <?php if($pages) :
                                $count = 1;
                                foreach ($pages as $key => $row2) :?>
                                <tr>
                                    <td><?php echo $count++; ?></td>
                                    <td><?php echo $row2; ?></td>
                                    <td><a target="_blank" href="<?php echo ($key != 'fees') ? base_url() . 'page/show/' . $key : base_url() . '#our-fees';?>" class="btn submit-button btn-info">Show</a></td>
                                    <td><a target="_blank" href="<?php echo base_url() . 'page/save/' . $key;?>" class="fa fa-pencil-square-o submit-button edit-icon"  title="Edit"></a></td>
                                </tr>
                                 <?php  endforeach; 
                                        endif; ?>
                        </tbody>
                      </table>
                    </div>
                    <!-- /.box-body -->
                  </div>
                <?php endif;?>      
                
                
            
                    
            <!--</div>-->    
                
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
  </div>
 
 <script type="text/javascript">
    function abc(x){
      // alert("hello hi"+x);
              $("#rateid").val(x);
    }
</script>

<script type="text/javascript">
    function form_submit() {
      document.getElementById("rateform").submit();
     }    
</script>

<!-- Doctor rates Update -->
  
  <div class="modal modal-info fade edit-doctor-rates sad-modal" id="modal-info">
          <div class="modal-dialog">
            <div class="modal-content">
              
              <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Edit Rates</h4>
              </div>
                
              <div class="modal-body">
                    <div class="appointment-form">
                        <?php $attributes = array("name" => "rateform", "id"=>"rateform"); echo form_open("admin/doctorRates", $attributes);?>
                            
                        <div class="form-group">
                            <input type="hidden" name="rateid" id="rateid" value="">
                            <input style="color: #000;" type="text" name="rates">
                        </div>
                    </div>
              </div>
                
              <div class="modal-footer">
                <button type="button" class="btn btn-outline pull-left" data-dismiss="modal">Close</button>
                <button onclick="form_submit()" type="submit" class="sad-button btn btn-outline"  data-dismiss="modal" aria-hidden="true" data-toggle="modal" data-target=".request-a-doctor" > Submit </button>
              </div>
                
                    <?php echo form_close(); ?>
                    <?php echo $this->session->flashdata('msg'); ?>
            </div>
<!--             .modal-content -->
          </div>
           <!--.modal-dialog--> 
        </div>


<!-- Add Doctor -->

<div class="modal modal-info fade add-doctor sad-modal" id="modal-info">
          <div class="modal-dialog">
            <div class="modal-content">
              
              <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Add a Doctor</h4>
              </div>
                
              <div class="modal-body">
                    <!--<div class="box-body">-->
                        <?php $attributes = array("name" => "add-doctor", "id"=>"add-doctor"); echo form_open("admin/addDoctor", $attributes);?>
                          <!-- text input -->
                          <div class="form-group">
                            <input name="fname" type="text" class="form-control" placeholder="First name">
                          </div>
                          <div class="form-group">
                            <input name="sname" type="text" class="form-control" placeholder="Surname">
                          </div>
                          <div class="form-group">
                            <input id="email" name="email" type="text" class="form-control" placeholder="Email address">
                          </div>
                          <!--div class="form-group">
                            <input name="password" type="password" class="form-control" placeholder="Password">
                          </div-->
                          <div class="form-group">
                            <input name="phone" type="text" class="form-control" placeholder="Phone number">
                          </div>
                          
                          <div class="form-group">
                            <div class="radio">
                              <label>
                                <input type="radio" name="gender" value="male" checked>
                                Male
                              </label>
                              <label>
                                <input type="radio" name="gender" value="female">
                                Female
                              </label>  
                            </div>
                          </div>
                          
                          <div class="form-group">
                            <input name="degree" type="text" class="form-control" placeholder="Degree">
                          </div>
                          <!-- textarea -->
                          <div class="form-group">
                            <textarea class="form-control" rows="3" name="experience" placeholder="Experience"></textarea>
                          </div>
                          <div class="form-group">
                            <textarea class="form-control" rows="3" name="speciality" placeholder="Speciality"></textarea>
                          </div>

                          <!-- select -->
                          <div class="form-group">
                            <select class="form-control"  name="doctor_type">
                                <option value="1">Dietitian</option>
                                <option value="2">Psychologist</option>
                                <option value="3">Medical Doctor</option>
                            </select>
                          </div>
                        </form>
                      <!--</div>-->
              </div>
                
              <div class="modal-footer">
                <button type="button" class="btn btn-outline pull-left" data-dismiss="modal">Close</button>
                <button onclick="add_doctor()" type="submit" class="sad-button btn btn-outline"  data-dismiss="modal" aria-hidden="true" data-toggle="modal" data-target=".add-doctor" > Submit </button>
              </div>
                
                    <?php echo form_close(); ?>
                    <?php echo $this->session->flashdata('msg'); ?>
            </div>
            <!-- /.modal-content -->
          </div>
          <!-- /.modal-dialog -->
        </div>


<script type="text/javascript">
            
    function add_doctor() {
        document.getElementById("add-doctor").submit();
    }
</script>