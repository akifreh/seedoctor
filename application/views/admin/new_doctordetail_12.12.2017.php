 <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        User Detail
<!--        <small>#007612</small>-->
      </h1>
      <ol class="breadcrumb">
        <li><a href="<?php echo base_url();?>"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="<?php echo base_url();?>admin/userlist/doctor">Doctor List</a></li>
        <li class="active">Doctor Detail</li>
      </ol>
    </section>

<!--    <div class="pad margin no-print">
      <div class="callout callout-info" style="margin-bottom: 0!important;">
        <h4><i class="fa fa-info"></i> Note:</h4>
        This page has been enhanced for printing. Click the print button at the bottom of the invoice to test.
      </div>
    </div>-->

    <!-- Main content -->
    <section class="invoice">
      <!-- title row -->
      <div class="row">
          
          <?php echo $this->session->flashdata('email_success'); ?>
          <?php echo $this->session->flashdata('email_fail'); ?>
          
        <div class="col-xs-12">
          <h2 class="page-header">
            <i class="fa fa-user-circle-o"></i> <?php echo isset($row->fname) ? ucfirst($row->fname).' ' : ''; ?><?php echo isset($row->sname) ? ucfirst($row->sname) : ''; ?>
            <!--<small class="pull-right">Date: 2/10/2014</small>-->
          </h2>
        </div>
        <!-- /.col -->
      </div>
      
      
      
      <!-- info row -->
      <div class="row invoice-info">
        
          
        <div class="col-sm-3 invoice-col pad_btm_50" >
            <img src="<?php echo $row ? $row->profile_pic : ''; ?>" alt="" class="img-responsive">
        </div>  
          
        <div class="col-sm-3 invoice-col font_16">
            <address>
            <h2><strong>Personal Details</strong></h2>
            
            <p><strong>Name : </strong><?php echo $row ? $row->fname . ' '. $row->sname : '';?></p>
            <p><strong>Email : </strong><?php echo $row ? $row->email : '';?></p>
            <p><strong>Phone : </strong><?php echo $row ? $row->phone : '';?></p>
            <p><strong>Address : </strong><?php echo $row ? $row->address : '';?></p>
            <p><strong>City : </strong><?php echo $row ? $row->city : '';?></p>
            <p><strong>Province : </strong><?php echo $row ? $row->province : '';?> </p>
          </address>
        </div>
        
        
         <?php 
                if($doctorTimings) :
                    $timing = '';
                    foreach($doctorTimings as $doctorTiming) :  
                        $timing .= '<p>'.date('g:i a', strtotime($doctorTiming->start_time)) . ' - ' .  date('g:i a', strtotime($doctorTiming->end_time)).'</p>';  
                    endforeach; 
                endif;
                
                if($doctorAvailableDays) : 
                    $days = '';
                    foreach($doctorAvailableDays as $doctorAvailableDay) :
                         $days .= ucfirst($doctorAvailableDay).', ';
                    endforeach; 
                    $days = rtrim($days, ', ');
                endif;
            ?> 
          
        <div class="col-sm-3 invoice-col font_16">
        <address>
            <h2><strong>&nbsp;</strong></h2>
            <p><strong>Timings :</strong></p><?php echo $timing; ?>
            <p><strong>Days : </strong><?php echo $days;  ?></p>
            <p><strong>Cv : </strong> <a href="<?php echo base_url() ?>uploads/cv/<?php echo $row ? $row->cv : ''?>" download>Download cv </a> </p>
            
          </address>
        </div>
          
        <div class="col-sm-3 invoice-col font_16">
          
        <address>
            <h2><strong>&nbsp;</strong></h2>
            <p><strong>Type of Practitioner : </strong><?php echo $row ? ucfirst($row->type_of_doctor) : '';?></p>
            <p><strong></strong><a href="<?php echo base_url() . 'user/changePassword'; ?>">Change Password</a></p>
          </address>
        </div>  
        <!-- /.col -->
      </div>
      <!-- /.row -->
      
      <div class="row">
            <div class="col-xs-12">
              <p class="lead">Biography</p>
              <p class="text-muted well well-sm no-shadow"><?php echo $row->description; ?></p>
            </div>
            
            <div class="col-xs-12">
              <p class="lead">Academic qualifications</p>
              <p class="text-muted well well-sm no-shadow"><?php //if($row->academics) : foreach($row->academics as $value) :?></p>
            </div>
      </div>
      
      <!-- Table row -->
      
       <div class="row">

           <div class="col-xs-12 table-responsive pad_btm_40" >
                  <table class="table table-striped">
                      <caption class="table_caption"><strong>Academic qualifications</strong></caption>
<!--                    <thead>
                    <tr>
                      <th>Degree</th>
                      <th>University</th>
                      <th>Year of passing</th>
                    </tr>
                    </thead>-->

                    <tbody>
                      <?php if($row->academics) : foreach($row->academics as $value) :?>
                            <tr>
                              <td><?php echo $value->qualification ? $value->qualification : '';?></td>
                              <td><?php echo $value->institution ? $value->institution : '';?></td>
                              <td> <?php echo $value->year ? $value->year : '';?></td>
                            </tr>
                      <?php endforeach; endif; ?>
                    </tbody>

                  </table>
                </div>
                <!-- /.col -->
              
        
      
      
              <div class="col-xs-12 table-responsive">
                <table class="table table-striped">
                    <caption class="table_caption"><strong>Working Experience</strong></caption>
<!--                  <thead>
                  <tr>
                    <th>Designation</th>
                    <th>Company</th>
                    <th>Exprience</th>
                  </tr>
                  </thead>-->

                  <tbody>
                    <?php if($row->experience) : foreach($row->experience as $value) : ?>
                          <tr>
                            <td><?php echo $value->job_title ? $value->job_title : '';?></td>
                            <td><?php echo $value->company ? $value->company : '';?></td>
                            <td><?php echo $value->start_date ? date('M Y', strtotime($value->start_date)) : '';?> - <?php if($value->present_here == 1) {echo "Present"; } else { echo $value->end_date ? date('M Y', strtotime($value->end_date)) . '<br/>' : ''; } ?></td>
                          </tr>
                    <?php endforeach; endif; ?>
                  </tbody>

                </table>
              </div>
              <!-- /.col -->
       </div>

       <div class="row">
            <div class="col-xs-12">
                
              <?php if($doctorStatus == 0) : ?>
                    <a class="complete-profile" href="<?php echo base_url() ?>admin/getDoctorEmail/<?php echo $row->user_id ?>">
                        <button type="button" class="btn btn-a btn-success" >Approve</button>
                    </a>
              <?php endif; ?>
                    
              <?php if(isset($editProfile) && ($editProfile)) : ?>
                    <a class="complete-profile" href="<?php echo base_url() ?>doctordashboard/editProfile">
                        <button type="button" class="btn btn-a btn-info" >Edit Profile</button>
                    </a>
              <?php endif; ?>      
            </div>
            
      </div>
      
<!--        <div class="row">
         accepted payments column 
        <div class="col-xs-6">
          <p class="lead">Statutory:</p>
          <p class="text-muted well well-sm no-shadow" style="margin-top: 10px;">
            Job Title : <?php echo isset($row->job_title) ? $row->job_title : '' ;?>
            dopplr jibjab, movity jajah plickers sifteo edmodo ifttt zimbra.
          </p>
        </div>
       
      </div>-->
      <!-- /.row -->
    </section>
    <!-- /.content -->
    <div class="clearfix"></div>
  </div>