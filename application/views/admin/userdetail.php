<?php error_reporting(0);?>

 <div class="signup-form-step user-profile-section">
	 <div class="container">
		<div class="personal-info">
		<div class="row">
			<div class="col-lg-2 col-md-2 padding-right-0">
                            <img src="<?php echo $row ? $row->profile_pic : ''; ?>" alt="" style="width:100%;">
			</div>
			<div class="col-lg-3 col-md-3">
			<div class="col-lg-12 col-md-12"><h2>Personal Details</h2></div>
				<div class="form-group">
					<label for="inputEmail" class="col-lg-2 control-label">Name</label>
					<div class="col-lg-12">
						<?php echo isset($row->fname) ? ucfirst($row->fname) : ''; ?>
					</div>
				</div>
				<div class="form-group">
					<label for="inputEmail" class="col-lg-2 control-label">Surname</label>
					<div class="col-lg-12">
						<?php echo isset($row->sname) ? ucfirst($row->sname) : ''; ?>
					</div>
				</div>
				<div class="form-group">
					<label for="inputEmail" class="col-lg-2 control-label">Date of birth</label>
						<div class="col-lg-12 col-md-12">
			 <?php echo isset($row->dob) ? date('d M Y', strtotime($row->dob)) : ''; ?>
				</div>
				</div>
			</div>
			<div class="col-lg-4 col-md-4">
			<div class="col-lg-12 col-md-12"><h2>Contact Details</h2></div>
				<div class="form-group">
      <label for="textArea" class="col-lg-2 control-label">Residential Address</label>
      <div class="col-lg-12">
        
   Address : <?php echo isset($row->address) ? $row->address : ''; ?>
  
      </div>
      <div class="col-lg-12">
        
   City : <?php echo isset($row->city) ? $row->city : ''; ?>
  
      </div>
      <div class="col-lg-12">
        
   Province : <?php echo isset($row->province) ? $row->province : ''; ?>
  
      </div>
    </div>
	 <div class="form-group">
      <label for="textArea" class="col-lg-2 control-label">Contact Number</label>
      <div class="col-lg-12">
        
  <?php echo isset($row->phone) ? $row->phone : ''; ?>
  
      </div>
    </div>
	<div class="form-group">
					<label for="inputEmail" class="col-lg-2 control-label">Cell Number </label>
					<div class="col-lg-12">
						<?php echo isset($row->cell) ? $row->cell : ''; ?>
					</div>
				</div>
			</div>
			<div class="col-lg-3 col-md-3 contact-detail-last">
			
			
				<div class="form-group">
      <label for="textArea" class="col-lg-2 control-label">Postal Address</label>
      
      <div class="col-lg-12">
        
   Address : <?php echo isset($row->p_address) ? $row->p_address : ''; ?>
  
      </div>
      <div class="col-lg-12">
        
   City : <?php echo isset($row->p_city) ? $row->p_city : ''; ?>
  
      </div>
      <div class="col-lg-12">
        
   Province : <?php echo isset($row->p_province) ? $row->p_province : ''; ?>
  
      </div>
    </div>
				
				<div class="form-group">
					<label for="inputEmail" class="col-lg-2 control-label">Email Address </label>
					<div class="col-lg-12">
						<a href="mailto:<?php echo $row ? $row->email : ''; ?>"><?php echo $row ? $row->email : ''; ?></a>
					</div>
				</div>
			</div>
		</div>
		</div>
	 </div>
 </div>

<div class="statutory-section user-profile-section">
	<div class="container">
		<div class="statutory-section-inner">
		<div class="statutory-section-heading col-lg-12">
			<h2>Statutory</h2>
		</div>

		<div class="row">
			<div class="col-lg-8 col-md-8">
			<div class="row">
				<div class="col-lg-6 col-md-6">
					<div class="form-group">
					<label for="inputEmail" class="col-lg-12 control-label">Job Title </label>
					<div class="col-lg-12">
						<?php echo isset($row->job_title) ? $row->job_title : '' ;?>
					</div>
				</div>
				<div class="form-group">
					<label for="inputEmail" class="col-lg-12 control-label">Company Employed</label>
					<div class="col-lg-12">
						<?php echo isset($row->company_employed) ? $row->company_employed : '';?>
					</div>
				</div>
				 
				</div>
				<div class="col-lg-6 col-md-6">
					<div class="form-group">
					<label for="inputEmail" class="col-lg-12 control-label">Next of Keen </label>
					<div class="col-lg-12">
						<?php echo isset($row->next_of_kin) ? $row->next_of_kin : '';?>
					</div>
				</div>
				<div class="form-group">
					<label for="inputEmail" class="col-lg-12 control-label">Medical Aid </label>
					<div class="col-lg-12">
						<?php echo isset($row->medical_aid) ? $row->medical_aid : ''; ?>
					</div>
				</div>
				 
				</div>
				</div>
			</div>
			 
			</div>
		</div>
	</div>
 </div>

 <div class="other-profile-section user-profile-section">
	<div class="container">
	<div class="row">
             <?php if(!empty($userOtherProfile)) : ?>
		<div class="col-lg-12 col-md-12">
			<div class="pro-heading"><h2>Other Profiles</h2></div>
		</div>
	</div>
		<div class="row">
			 
			<div class="col-lg-12 col-md-12 table-responsive-res">
                           
				<div class="profile-table table-responsive">
                                    
					<div class="table-header name-tab col-lg-4">Name</div>
                                        <div class="table-header name-tab col-lg-4">Relationship</div>
					<div class="table-header col-lg-4">Date of Birth</div>
                                      <?php foreach ($userOtherProfile as $row2) : ?>
					<div class="table-data border-right col-lg-4"><?php echo ucfirst($row2->name); ?></div>
                                        <div class="table-data border-right col-lg-4"><?php echo ucfirst($row2->relationship); ?></div>
					<div class="table-data col-lg-4"><?php echo date('d M Y', strtotime($row2->dob)); ?></div>
                                      <?php endforeach; ?>
                                    
				</div>
                         
			</div>
		</div>
               <?php endif; ?>
	</div>
 </div>