  <!-- Latest compiled and minified CSS -->
<link rel="stylesheet" href="<?php echo base_url(); ?>js/star/jquery.rateyo.min.css">
<!-- Latest compiled and minified JavaScript -->
<script src="<?php echo base_url(); ?>js/star/jquery.rateyo.min.js"></script>
<style>
.jq-ry-container{
	display:inline-block;
}</style>
	</style>
<div class="forget-password">
<div class="container">
<div class="col-lg-12 col-md-12">
	<div class="alert alert-success text-center" style="margin: 0 auto;"> <h4>Your call has been ended successfully. Please take time to rate the doctor and the system.</h4></div>
	<h1>Give Feedback</h1>
	<?php if(isset($message)): ?>
		<div class="alert alert-success text-center"><?php echo $message; ?></div>
	<?php endif; ?>
	<?php if(isset($error)) : ?>
		<div class="alert alert-danger text-center"><?php echo $error; ?></div>
	<?php endif; ?>
	<p>Please let us know about your experience in using SeeADoctor.</p>
	<p></p>
	<div class="signup-form">
    
    <?php $attributes = array("name" => "ratingForm", "id" => "ratingForm");
            echo form_open("user/rateDoctor", $attributes);?>
		<div class="row">
			<div class="col-lg-4 col-md-4">
				<label for="inputEmail" class="  control-label">Rate Dr <?php echo ucfirst($doctor->fname); ?> </label>
                <div id="rateYo"></div>
                <div class="counter"></div>
                <input id="rating" name="rating" type="hidden" class="form-control" placeholder="Rating" value="<?php echo $rating; ?>">
                <input name="doctorId" type="hidden" value=<?php echo $doctor->id; ?>>
                <input name="appointmentId" type="hidden" value=<?php echo $appointmentId; ?>>
			</div>
			<div class="col-lg-4 col-md-4">
				<label for="inputEmail" class="  control-label">Rate our system</label>
                <div id="system-rateYo"></div>
                <div class="counter"></div>
                <input id="system-rating" name="system_rating" type="hidden" class="form-control" placeholder="System Rating" value="">
			</div>
		</div>
		<div class="row">
			<div class="col-lg-6 col-md-6">
				<label for="inputEmail" class="  control-label">Comments </label>
    			<textarea  rows="4" cols="50" class="form-control" id="content" name="comments" ></textarea>
			</div>
		</div>
		<div class="row">
			<div class="col-lg-12 col-md-12">
                <button name="submit" type="submit" class="btn btn-default submit-button">Submit</button>
		  	</div> 
		</div>
		<?php echo form_close(); ?>
	</div>
</div>
</div>
</div>

 
<script type="text/javascript">
	 
//Make sure that the dom is ready
$(function () {
 
  	$("#rateYo").rateYo({
    	rating: '<?php echo $rating; ?>',
    	halfStar: true,

    	onChange: function (rating, rateYoInstance) {
      		$("#rating").val(rating);
    	}
  	});

  	$("#system-rateYo").rateYo({
    	rating: 5,
    	halfStar: true,

    	onChange: function (rating, rateYoInstance) {
    		console.log(rating);
      		$("#system-rating").val(rating);
    	}
  	});  	

});
</script>