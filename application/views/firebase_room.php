<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8" />

    <!-- jQuery -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.1.0/jquery.min.js"></script>

    <!-- Firebase -->
    <script src="https://www.gstatic.com/firebasejs/3.4.0/firebase.js"></script>

    <!-- Firechat -->
    <link rel="stylesheet" href="https://cdn.firebase.com/libs/firechat/3.0.1/firechat.min.css" />
    <script src="https://cdn.firebase.com/libs/firechat/3.0.1/firechat.min.js"></script>
    <script src="https://apis.google.com/js/client.js" type="text/javascript"></script>

    <!-- Custom CSS -->
    <style>
      #firechat-wrapper {
        height: 475px;
        max-width: 325px;
        padding: 10px;
        border: 1px solid #ccc;
        background-color: #fff;
        margin: 50px auto;
        text-align: center;
        -webkit-border-radius: 4px;
        -moz-border-radius: 4px;
        border-radius: 4px;
        -webkit-box-shadow: 0 5px 25px #666;
        -moz-box-shadow: 0 5px 25px #666;
        box-shadow: 0 5px 25px #666;
      }
    </style>
  </head>

  <!--
    Example: Anonymous Authentication

    This example uses Firebase Simple Login to create "anonymous" user sessions in Firebase,
    meaning that user credentials are not required, though a user has a valid Firebase
    authentication token and security rules still apply.

    Requirements: in order to use this example with your own Firebase, you'll need to do the following:
      1. Apply the security rules at https://github.com/firebase/firechat/blob/master/rules.json
      2. Enable the "Anonymous" authentication provider in Forge
      3. Update the URL below to reference your Firebase
      4. Update the room id for auto-entry with a public room you have created
   -->
  <body>
    <div id="firechat-wrapper"></div>
    <input type="file" id="input" onchange="handleFiles(this.files)">
    <input type="hidden" name="roomId" id="roomId"/>
    <input type="hidden" name="user" id="user"/>

    <script>
        // Initialize Firebase
        var config = {
        apiKey: "AIzaSyAa5v90ME39AacY7BUSCOSJlGXFFfqewgs",
        authDomain: "seeadoctor-3a03d.firebaseapp.com",
        databaseURL: "https://seeadoctor-3a03d.firebaseio.com",
        storageBucket: "seeadoctor-3a03d.appspot.com",
        messagingSenderId: "789741748368"
      };
      firebase.initializeApp(config);
        var dbName = '<?php echo $db_name; ?>';
        var chatRef = firebase.database().ref(dbName);
        var chatRoomExists = 'FALSE';
        var chatTitle = '<?php echo $room_name; ?>';
        var roomId = '';
        var storage = firebase.storage();

    // Listen for authentication state changes
      firebase.auth().onAuthStateChanged(function(user) {
        if (user) {
          roomChatSetup(user);
        } else {
         //  If the user is not logged in, sign them in anonymously
         firebase.auth().signInAnonymously().catch(function(error) {
            console.log("Error signing user in anonymously:", error);
          });
        }
      });
        
        // Get a reference to the storage service, which is used to create references in your storage bucket
        var storage = firebase.storage();

        // Create a storage reference from our storage service
        var storageRef = storage.ref();

      function roomChatSetup(user) {

        var chat = new Firechat(chatRef);
        var chatUser = user;

        // If room already present get reference
        chat.getRoomList(function (roomList) { 
          for (var key in roomList) 
          { 
            var title = roomList[key]['name'];

            if(title == chatTitle) {
              chatRoomExists = 'TRUE';
              roomId = roomList[key]['id'];
              break;
            }
          }
          if(chatRoomExists == 'FALSE') {

            chat.createRoom(chatTitle, "private", function(roomId) {
                chat.setUser(chatUser.uid, "<?php echo $username;?>", function(user) {
                  chat.enterRoom(roomId);
                });
              location.reload();
            });
            
          } else {
            chat.setUser(chatUser.uid, "<?php echo $username;?>", function(user) {
                document.getElementById('roomId').value = roomId;
                document.getElementById('user').value = chatUser.uid;
                chat.enterRoom(roomId);

            });
            initChat(user);
          }  
        });
      }

      function initChat(user) {
        var chat = new FirechatUI(chatRef, document.getElementById("firechat-wrapper"));
        chat.setUser(user.uid, "<?php echo $username;?>");
      }

       function sendMessage(message) 
       {
          var chat = new Firechat(chatRef);  
          var roomId = document.getElementById("roomId").value;
          var chatUserId = document.getElementById("user").value;
          chat.setUser(chatUserId, "<?php echo $username;?>", function(user) 
          {
              chat.sendMessage(roomId, message, messageType='default');
          });
      }

    function uploadFile(file)
    {
       // File or Blob named mountains.jpg
        var file = file;
        var dbName = '<?php echo $db_name; ?>';
        var room = '<?php echo $room_name; ?>';

        // Create the file metadata
        var metadata = {
          contentType: file.type
        };
        // Upload file and metadata to the object 'images/room_name/mountains.jpg'
        var uploadTask = storageRef.child('uploads/'+dbName+'/'+room+'/' + file.name).put(file, metadata);

        // Listen for state changes, errors, and completion of the upload.
        uploadTask.on(firebase.storage.TaskEvent.STATE_CHANGED, // or 'state_changed'
          function(snapshot) {
            // Get task progress, including the number of bytes uploaded and the total number of bytes to be uploaded
            var progress = (snapshot.bytesTransferred / snapshot.totalBytes) * 100;
            console.log('Upload is ' + progress + '% done');
            switch (snapshot.state) {
              case firebase.storage.TaskState.PAUSED: // or 'paused'
                console.log('Upload is paused');
                break;
              case firebase.storage.TaskState.RUNNING: // or 'running'
                console.log('Upload is running');
                break;
            }
          }, function(error) {
          switch (error.code) {
            case 'storage/unauthorized':
              console.log('User doesn\'t have permission to access the object'); // User doesn't have permission to access the object
              break;

            case 'storage/canceled':
              // User canceled the upload
              break;

            case 'storage/unknown':
            console.log(error.serverResponse);
              // Unknown error occurred, inspect error.serverResponse
              break;
          }
        }, function() {
          // Upload completed successfully, now we can get the download URL
          var downloadURL = uploadTask.snapshot.downloadURL;
          generateShortUrl(downloadURL);
        });

    }

    function handleFiles()
    {
      var selectedFile = document.getElementById('input').files[0];
      uploadFile(selectedFile);
    }

    function generateShortUrl(url) 
    {
      var apikey = '<?php echo $mapKey; ?>'
      gapi.client.setApiKey(apikey);
      gapi.client.load('urlshortener', 'v1', function() { 
      var Url = url;
      var request = gapi.client.urlshortener.url.insert({
        'resource': {
        'longUrl': Url
         }
      });
      request.execute(function(response) {

            if (response.id != null) {
                message = "My shared file is at " + response.id;
                sendMessage(message);
            }
            else {
                alert("Error: creating short url \n" + response.error);
                console.log(response.error);
            }
        });
    });
    }
    </script>
  </body>
</html>