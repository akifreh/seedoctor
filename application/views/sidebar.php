
  
  
 
<!-- Left side column. contains the logo and sidebar -->
  <aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
      <!-- Sidebar user panel -->

      <!-- search form -->
<!--      <form action="#" method="get" class="sidebar-form">
        <div class="input-group">
          <input type="text" name="q" class="form-control" placeholder="Search...">
          <span class="input-group-btn">
                <button type="submit" name="search" id="search-btn" class="btn btn-flat"><i class="fa fa-search"></i>
                </button>
              </span>
        </div>
      </form>-->
      <!-- /.search form -->
      <!-- sidebar menu: : style can be found in sidebar.less -->
      <ul class="sidebar-menu" data-widget="tree">
        <!--<li class="header">MAIN NAVIGATION</li>-->
        
        
        <?php if(isset($this->session->userdata['logged_in'])) : ?>
            <?php if($this->session->userdata['logged_in']['role'] == 2) : ?> 
              
              <li <?php if(isset($breadcrumbs['appointment'])) echo 'class="active"'; ?>>
                <a href="<?php echo base_url(); ?>appointment">
                  <i class="fa fa-dashboard"></i> <span>SEE A DOCTOR</span>
                </a>
              </li>

              <li <?php if(isset($breadcrumbs['medical_profile'])) echo 'class="active"'; ?>>
                <a href="<?php echo base_url()?>medical-profile">
                  <i class="fa fa-paypal"></i> <span>MEDICAL FILE</span>
                </a>
              </li>

              <li <?php if(isset($breadcrumbs['user_profile'])) echo 'class="active"'; ?>>
                <a href="<?php echo base_url()?>home/complete-profile">
                  <i class="fa fa-calendar"></i> <span>MY PROFILE</span>
                </a>
              </li>
              
            <?php elseif($this->session->userdata['logged_in']['role'] == 1): ?> 
              
              <li <?php if(isset($breadcrumbs['admin_dashboard'])) echo 'class="active"'; ?>>
                <a href="<?php echo base_url(); ?>admin">
                  <i class="fa fa-dashboard"></i> <span>DASHBOARD</span>
                </a>
              </li>
              
              <li <?php if(isset($breadcrumbs['admin_payment'])) echo 'class="active"'; ?>>
                <a href="<?php echo base_url(); ?>admin/payments" >
                  <i class="fa fa-paypal"></i> <span>PAYMENTS</span>
                </a>
              </li>

              <li class="treeview <?php if(isset($breadcrumbs['admin_appointment']) || isset($breadcrumbs['admin_finished']) || isset($breadcrumbs['admin_missed'])) echo 'active'; ?>">
                <a href="#">
                  <i class="fa fa fa-calendar"></i> <span>APPOINTMENTS</span>
                  <span class="pull-right-container">
                        <i class="fa fa-angle-left pull-right"></i>
                      </span>
                </a>
                <ul class="treeview-menu">
                    <li class="<?php if(isset($breadcrumbs['admin_appointment'])) echo 'active'; ?>"><a href="<?php echo base_url(); ?>admin/appointments/upcoming"><i class="fa fa-indent"></i>UPCOMING APPOINTMENTS</a></li>
                    <li class="<?php if(isset($breadcrumbs['admin_finished'])) echo 'active'; ?>"><a href="<?php echo base_url(); ?>admin/appointments/finished"><i class="fa fa-calendar-plus-o"></i>FINISHED APPOINTMENTS</a></li>
                    <li class="<?php if(isset($breadcrumbs['admin_missed'])) echo 'active'; ?>"><a href="<?php echo base_url(); ?>admin/appointments/missed"><i class="fa fa-calendar-times-o"></i>MISSED APPOINTMENTS</a></li>
                </ul>
              </li>
              
              <li class="treeview <?php if(isset($breadcrumbs['admin_doctor']) || isset($breadcrumbs['admin_doctor_rates'])) echo 'active'; ?>">
                <a href="#">
                  <i class="fa fa-medkit"></i> <span>DOCTOR MANAGEMENT</span>
                  <span class="pull-right-container">
                        <i class="fa fa-angle-left pull-right"></i>
                      </span>
                </a>
                <ul class="treeview-menu">
                    <li class="<?php if(isset($breadcrumbs['admin_doctor'])) echo 'active'; ?>"><a href="<?php echo base_url(); ?>admin/userlist/doctor"><i class="fa fa-user-md"></i>DOCTOR LIST</a></li>
                    <li class="<?php if(isset($breadcrumbs['admin_doctor_rates'])) echo 'active'; ?>"><a href="<?php echo base_url(); ?>admin/userlist/doctor_rates"><i class="fa fa-stethoscope"></i>DOCTOR RATES</a></li>
                </ul>
              </li>
        
              <li <?php if(isset($breadcrumbs['admin_patient'])) echo 'class="active"'; ?>>
                <a href="<?php echo base_url(); ?>admin/userlist/patient" >
                  <i class="fa fa-wheelchair"></i> <span>PATIENT MANAGEMENT</span>
                </a>
              </li>
              
              <li <?php if(isset($breadcrumbs['admin_pages'])) echo 'class="active"'; ?>>
                <a href="<?php echo base_url(); ?>admin/userlist/pages" >
                  <i class="fa fa-list-alt"></i> <span>PAGES MANAGEMENT</span>
                </a>
              </li>

              <li <?php if(isset($breadcrumbs['admin_pages'])) echo 'class="active"'; ?>>
                <a href="<?php echo base_url(); ?>admin/ourClients" >
                  <i class="fa fa-list-alt"></i> <span>OUR CLIENTS</span>
                </a>
              </li>
              
            <?php elseif($this->session->userdata['logged_in']['role'] == 3): ?> 
              
              <li <?php if(isset($breadcrumbs['doctor_dashboard'])) echo 'class="active"'; ?>>
                <a href="<?php echo base_url(); ?>doctordashboard/dashboard">
                  <i class="fa fa-dashboard"></i> <span>DASHBOARD</span>
                </a>
              </li>

              <li class="treeview <?php if(isset($breadcrumbs['doctor_appointment']) || isset($breadcrumbs['doctor_finished']) || isset($breadcrumbs['doctor_missed'])) echo 'active'; ?>">
                <a href="#">
                  <i class="fa fa fa-calendar"></i> <span>APPOINTMENTS</span>
                  <span class="pull-right-container">
                        <i class="fa fa-angle-left pull-right"></i>
                      </span>
                </a>
                <ul class="treeview-menu">
                    <li class="<?php if(isset($breadcrumbs['doctor_appointment'])) echo 'active'; ?>"><a href="<?php echo base_url(); ?>doctordashboard/index/upcoming"><i class="fa fa-indent"></i>UPCOMING APPOINTMENTS</a></li>
                    <li class="<?php if(isset($breadcrumbs['doctor_finished'])) echo 'active'; ?>"><a href="<?php echo base_url(); ?>doctordashboard/index/finished"><i class="fa fa-calendar-plus-o"></i>FINISHED APPOINTMENTS</a></li>
                    <li class="<?php if(isset($breadcrumbs['doctor_missed'])) echo 'active'; ?>"><a href="<?php echo base_url(); ?>doctordashboard/index/missed"><i class="fa fa-calendar-times-o"></i>MISSED APPOINTMENTS</a></li>
                </ul>
              </li>

              <li <?php if(isset($breadcrumbs['doctor_profile'])) echo 'class="active"'; ?>>
                <a href="<?php echo base_url(); ?>doctordashboard/profile">
                  <i class="fa fa-user"></i> <span>MY PROFILE</span>
                </a>
              </li>

              <li <?php if(isset($breadcrumbs['doctor_profile'])) echo 'class="active"'; ?>>
                <a href="<?php echo base_url(); ?>doctordashboard/profile">
                  <i class="fa fa-paypal"></i> <span>PAYMENTS</span>
                </a>
              </li>
              
            <?php endif; ?>
              
            <?php else :?>
            
             <li <?php if(isset($breadcrumbs['services'])) echo 'class="active"'; ?>>
                <a href="<?php echo base_url(); ?>#services-sad">
                  <i class="fa fa-dashboard"></i> <span>OUR SERVICES</span>
                </a>
              </li>

              <li <?php if(isset($breadcrumbs['treat'])) echo 'class="active"'; ?>>
                <a href="<?php echo base_url(); ?>#ww-treat-sad">
                  <i class="fa fa-paypal"></i> <span>WHAT WE TREAT</span>
                </a>
              </li>

              <li <?php if(isset($breadcrumbs['team'])) echo 'class="active"'; ?>>
                <a href="<?php echo base_url(); ?>#team-sad">
                  <i class="fa fa-calendar"></i> <span>OUR TEAM</span>
                </a>
              </li>
              
              <li <?php if(isset($breadcrumbs['apply_doctor'])) echo 'class="active"'; ?>>
                <a href="<?php echo base_url(); ?>registerdoctor">
                  <i class="fa fa-calendar"></i> <span>FOR DOCTORS</span>
                </a>
              </li>
              
          <?php endif; ?> 
            
        
        
<!--        <li class="treeview">
          <a href="#">
            <i class="fa fa-dashboard"></i> <span>Dashboard</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li><a href="../../index.html"><i class="fa fa-circle-o"></i> Dashboard v1</a></li>
            <li><a href="../../index2.html"><i class="fa fa-circle-o"></i> Dashboard v2</a></li>
          </ul>
        </li>
        <li class="treeview">
          <a href="#">
            <i class="fa fa-files-o"></i>
            <span>Layout Options</span>
            <span class="pull-right-container">
              <span class="label label-primary pull-right">4</span>
            </span>
          </a>
          <ul class="treeview-menu">
            <li><a href="../layout/top-nav.html"><i class="fa fa-circle-o"></i> Top Navigation</a></li>
            <li><a href="../layout/boxed.html"><i class="fa fa-circle-o"></i> Boxed</a></li>
            <li><a href="../layout/fixed.html"><i class="fa fa-circle-o"></i> Fixed</a></li>
            <li><a href="../layout/collapsed-sidebar.html"><i class="fa fa-circle-o"></i> Collapsed Sidebar</a></li>
          </ul>
        </li>
        <li>
          <a href="../widgets.html">
            <i class="fa fa-th"></i> <span>Widgets</span>
            <span class="pull-right-container">
              <small class="label pull-right bg-green">new</small>
            </span>
          </a>
        </li>
        <li class="treeview">
          <a href="#">
            <i class="fa fa-pie-chart"></i>
            <span>Charts</span>
            <span class="pull-right-container">
                  <i class="fa fa-angle-left pull-right"></i>
                </span>
          </a>
          <ul class="treeview-menu">
            <li><a href="../charts/chartjs.html"><i class="fa fa-circle-o"></i> ChartJS</a></li>
            <li><a href="../charts/morris.html"><i class="fa fa-circle-o"></i> Morris</a></li>
            <li><a href="../charts/flot.html"><i class="fa fa-circle-o"></i> Flot</a></li>
            <li><a href="../charts/inline.html"><i class="fa fa-circle-o"></i> Inline charts</a></li>
          </ul>
        </li>
        <li class="treeview">
          <a href="#">
            <i class="fa fa-laptop"></i>
            <span>UI Elements</span>
            <span class="pull-right-container">
                  <i class="fa fa-angle-left pull-right"></i>
                </span>
          </a>
          <ul class="treeview-menu">
            <li><a href="../UI/general.html"><i class="fa fa-circle-o"></i> General</a></li>
            <li><a href="../UI/icons.html"><i class="fa fa-circle-o"></i> Icons</a></li>
            <li><a href="../UI/buttons.html"><i class="fa fa-circle-o"></i> Buttons</a></li>
            <li><a href="../UI/sliders.html"><i class="fa fa-circle-o"></i> Sliders</a></li>
            <li><a href="../UI/timeline.html"><i class="fa fa-circle-o"></i> Timeline</a></li>
            <li><a href="../UI/modals.html"><i class="fa fa-circle-o"></i> Modals</a></li>
          </ul>
        </li>
        <li class="treeview">
          <a href="#">
            <i class="fa fa-edit"></i> <span>Forms</span>
            <span class="pull-right-container">
                  <i class="fa fa-angle-left pull-right"></i>
                </span>
          </a>
          <ul class="treeview-menu">
            <li><a href="../forms/general.html"><i class="fa fa-circle-o"></i> General Elements</a></li>
            <li><a href="../forms/advanced.html"><i class="fa fa-circle-o"></i> Advanced Elements</a></li>
            <li><a href="../forms/editors.html"><i class="fa fa-circle-o"></i> Editors</a></li>
          </ul>
        </li>
        <li class="treeview active">
          <a href="#">
            <i class="fa fa-table"></i> <span>Tables</span>
            <span class="pull-right-container">
                  <i class="fa fa-angle-left pull-right"></i>
                </span>
          </a>
          <ul class="treeview-menu">
            <li><a href="simple.html"><i class="fa fa-circle-o"></i> Simple tables</a></li>
            <li class="active"><a href="data.html"><i class="fa fa-circle-o"></i> Data tables</a></li>
          </ul>
        </li>
        <li>
          <a href="../calendar.html">
            <i class="fa fa-calendar"></i> <span>Calendar</span>
            <span class="pull-right-container">
              <small class="label pull-right bg-red">3</small>
              <small class="label pull-right bg-blue">17</small>
            </span>
          </a>
        </li>
        <li>
          <a href="../mailbox/mailbox.html">
            <i class="fa fa-envelope"></i> <span>Mailbox</span>
            <span class="pull-right-container">
              <small class="label pull-right bg-yellow">12</small>
              <small class="label pull-right bg-green">16</small>
              <small class="label pull-right bg-red">5</small>
            </span>
          </a>
        </li>
        <li class="treeview">
          <a href="#">
            <i class="fa fa-folder"></i> <span>Examples</span>
            <span class="pull-right-container">
                  <i class="fa fa-angle-left pull-right"></i>
                </span>
          </a>
          <ul class="treeview-menu">
            <li><a href="../examples/invoice.html"><i class="fa fa-circle-o"></i> Invoice</a></li>
            <li><a href="../examples/profile.html"><i class="fa fa-circle-o"></i> Profile</a></li>
            <li><a href="../examples/login.html"><i class="fa fa-circle-o"></i> Login</a></li>
            <li><a href="../examples/register.html"><i class="fa fa-circle-o"></i> Register</a></li>
            <li><a href="../examples/lockscreen.html"><i class="fa fa-circle-o"></i> Lockscreen</a></li>
            <li><a href="../examples/404.html"><i class="fa fa-circle-o"></i> 404 Error</a></li>
            <li><a href="../examples/500.html"><i class="fa fa-circle-o"></i> 500 Error</a></li>
            <li><a href="../examples/blank.html"><i class="fa fa-circle-o"></i> Blank Page</a></li>
            <li><a href="../examples/pace.html"><i class="fa fa-circle-o"></i> Pace Page</a></li>
          </ul>
        </li>
        <li class="treeview">
          <a href="#">
            <i class="fa fa-share"></i> <span>Multilevel</span>
            <span class="pull-right-container">
                  <i class="fa fa-angle-left pull-right"></i>
                </span>
          </a>
          <ul class="treeview-menu">
            <li><a href="#"><i class="fa fa-circle-o"></i> Level One</a></li>
            <li class="treeview">
              <a href="#"><i class="fa fa-circle-o"></i> Level One
                <span class="pull-right-container">
                  <i class="fa fa-angle-left pull-right"></i>
                </span>
              </a>
              <ul class="treeview-menu">
                <li><a href="#"><i class="fa fa-circle-o"></i> Level Two</a></li>
                <li class="treeview">
                  <a href="#"><i class="fa fa-circle-o"></i> Level Two
                    <span class="pull-right-container">
                      <i class="fa fa-angle-left pull-right"></i>
                    </span>
                  </a>
                  <ul class="treeview-menu">
                    <li><a href="#"><i class="fa fa-circle-o"></i> Level Three</a></li>
                    <li><a href="#"><i class="fa fa-circle-o"></i> Level Three</a></li>
                  </ul>
                </li>
              </ul>
            </li>
            <li><a href="#"><i class="fa fa-circle-o"></i> Level One</a></li>
          </ul>
        </li>
        <li><a href="https://adminlte.io/docs"><i class="fa fa-book"></i> <span>Documentation</span></a></li>
        <li class="header">LABELS</li>
        <li><a href="#"><i class="fa fa-circle-o text-red"></i> <span>Important</span></a></li>
        <li><a href="#"><i class="fa fa-circle-o text-yellow"></i> <span>Warning</span></a></li>
        <li><a href="#"><i class="fa fa-circle-o text-aqua"></i> <span>Information</span></a></li>-->
      </ul>
    </section>
    <!-- /.sidebar -->
  </aside>