<!DOCTYPE html>
<html>
  <head>
  </head>
  <body>
    <input type="text" id="lat" name="lat" value="">
    <input type="text" id="lng" name="lng" value="">
    
    <?php $script = 
    '<script>
      function initMap() {

        // Try HTML5 geolocation.
        if (navigator.geolocation) {
          navigator.geolocation.getCurrentPosition(function(position) {
            var pos = {
              lat: position.coords.latitude,
              lng: position.coords.longitude
            };
            console.log(pos);
             document.getElementById(\'lat\').value=pos.lat;
             document.getElementById(\'lng\').value=pos.lng;
          }, function() {
            handleLocationError(true);
          });
        } else {
          handleLocationError(false);
        }
      }

      function handleLocationError(browserHasGeolocation) {
        console.log(browserHasGeolocation ? \'Error: The Geolocation service failed.\' : \'Error: Your browser doesn\'t support geolocation.\');
      }
    </script>
    <script async defer
    src="https://maps.googleapis.com/maps/api/js?key=' . $mapKey . '&callback=initMap">
    </script>';

    if( isset($this->session->userdata('logged_in')['role']) && $this->session->userdata('logged_in')['role'] == 2) :
      echo $script;
    endif;
    ?>

  </body>
</html>