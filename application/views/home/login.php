 
<div class="forget-password">
<div class="container">
<div class="col-lg-6 col-md-6">
  <h1>Sign In</h1>
   <?php echo $this->session->flashdata('verify_msg'); ?>
  <div class="signup-form">
    <?php $attributes = array("name" => "registrationform", "id" => "login-form" , "class" => " ");
                echo form_open("home/user_login_process", $attributes);?>
    <div class="row">
      <div class="col-lg-6 col-md-6">
               <input name="email" type="email" class="form-control login-email" placeholder="Email" required>
      </div>
    </div>
      <div class="row">
      <div class="col-lg-6 col-md-6">
               <input name="password" type="password" class="form-control" placeholder="Password" required> 
      </div>
    </div>
    <div class="row">
      <div class="col-lg-12 col-md-12">
                <button type="submit" class="btn btn-default submit-button">Login</button>
        </div> 
    </div>
    <div class="row">
      <div class="col-lg-12 col-md-12">
                <?php echo form_close(); ?>
          <a href="<?php echo base_url(); ?>user/forgetPassword" class="forgot-password">Forgot Password?</a>    
          <script type="text/javascript">
              $("#login-form").validate();
          </script>
          <?php if($this->session->flashdata('error')) : ?>
            <div class="new-error">
              <?php echo $this->session->flashdata('error'); //echo $this->session->flashdata('admin_message'); ?> 
            </div>
          <?php endif; ?>
        </div> 
         <?php echo $this->session->flashdata('error_message'); ?>
    </div> 
  </div>
</div>
</div>
</div>

<style type="text/css">
  .forgot-password {
    float: none !important;  
   color: #0484cf !important;    
    margin-top: 15px;
    display: inline-block;
    margin-right: 0px;
}

header .padding-0-full{
  float: right;
}
@media screen and (max-width: 768px) {
header .padding-0-full {
    float: none;
}
}
</style>
