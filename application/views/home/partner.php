<div class="partners-section">

 

    <div class="container">
        <div class="partners-inner">
            <?php echo $this->session->flashdata('message');?> 
            <h1>Partner</h1>
           <p>Delivering reliable healthcare consultation to your employees, at the touch of a button.</p>

<p>For employers that need to improve productivity and workplace efficiency See-A-Doctor is an online tele-medicine platform, that provides seamless and affordable healthcare to your employees.</p>

<p>Dazzle your employees. No more driving to the doctor’s office and waiting in long queues. </p>


<div class="our-client-sction">
<h3>Our Clients</h3>
        <div  id="our-partners" class="owl-carousel our-partners-logos">
            <?php if($our_clients) : ?>
          <?php foreach ($our_clients as $key => $value) : ?>
                        <div class="our-partners-box item">
                          <a href="<?php echo $value->website; ?>" target="_blank"><img src="<?php echo $value->logo; ?>"></a>
                        </div>
          <?php endforeach; ?>
          <?php endif; ?>
                 </div>
</div>

<div class="row">
<div class="col-md-8">
    <div class="partner-form-background">
      <?php $attributes = array("name" => "partnersform" , "class" => "partnersdetailsl", "id" => "partnersform");
       echo form_open("home/add_client", $attributes);?>
        <div class="partner-form-half">
          <div class="partner-form-field-half"><input type="text" placeholder="First Name" id="name" name="name" required></div>
          <div class="partner-form-field-half"><input type="text" placeholder="Last Name" id="surname" name="surname" required></div>
          <div class="partner-form-field"><input type="email" placeholder="Email Address" id="email" name="email" required></div>
         <div class="partner-form-field-half"><input type="text" placeholder="Company Name" id="companyname" name="companyname" required></div> <div class="partner-form-field-half"><input type="text" placeholder="Company Size" id="CompanySize" name="CompanySize"></div>
</div>
<div class="partner-form-half">
<div class="partner-form-field"><textarea name="message" id="message" placeholder="Message" required></textarea></div>
</div>
<div class="partner-form-full">

<div class="partner-form-field">
<input value="Submit" class="btn btn-primary partnersbtn" id="partnersprocess" style="font-weight:bold; color:#fff;" type="submit" name="submit2">
</div>
</div>
      </form>
      </div></div>

</div>




        </div>
    </div>    
</div>

 <script>

    $(document).ready(function($) {
      $(".our-partners-logos").owlCarousel({
                items : 6,
    startPosition:0,
                nav: true,
                loop: true,
                dots:false,
                
                responsive: {
                  0: {
                    items: 1
                  },
                  600: {
                    items: 3
                  },
                  1000: {
                    items: 6
                  }
                },
     afterAction: function(el){
   //remove class active
   this
   .$owlItems
   .removeClass('active')

   //add class active
   this
   .$owlItems //owl internal $ object containing items
   .eq(this.currentItem + 1)
   .addClass('active')    
    } 
   
       
      });
      owl = $("#ourteam").data('owlCarousel');
    owl.jumpTo(1);
    });
  
 
    </script>