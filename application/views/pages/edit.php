<script src="<?php echo base_url() . 'js/ckeditor/ckeditor.js';?>"></script>

<div class="pages-editor">
<div class="container">
<div class="col-lg-12 col-md-12">
	<h2><?php echo $type_title; ?> Page</h2>
	<?php if(isset($message)): ?>
		<div class="alert alert-success text-center"><?php echo $message; ?></div>
	<?php endif; ?>
	<?php if(isset($error)) : ?>
		<div class="alert alert-danger text-center"><?php echo $error; ?></div>
	<?php endif; ?>

	<div class=" ">
    <?php echo form_open("page/save"); ?>
 <div>
               <input type="text" name="title" placeholder="Enter Title" value="<?php echo $title; ?>" class="form-control"/>
		</div>
		<div>
                <textarea name="content" id="content" ><p><?php echo isset($content) ? $content : ''; ?></p></textarea>
		</div>
		<input type="hidden" name="type" value="<?php echo $type; ?>"></input>
		<div class="row">
			<div class="col-lg-12 col-md-12">
                <button name="submit" type="submit" class="btn btn-default pull-right submit-button">Submit</button>
		  	</div> 
		</div>
		<?php echo form_close(); ?>
	</div>
</div>
</div>
</div>

<script type="text/javascript">
	CKEDITOR.replace( 'content' );
</script>