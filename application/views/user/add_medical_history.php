<?php $attributes = array("name" => "profileform");
echo form_open("userprofile/addUserInfo", $attributes);
if($profilepic) :
foreach ($profilepic as $pp) : 
endforeach; 
endif; ?>
<div class="user-profile-section">
	<div class="container">
		<div class="user-pro-inner">
 <div class="row">
     <?php echo $this->session->flashdata('msg'); ?>
     
			<div class="col-lg-2 col-md-2 padding-right-0">
				<img src="<?php echo isset($pp->profile_pic) ? $pp->profile_pic : ''; ?>" alt="" style="width:100%;">
			</div>
     
			<div class="col-lg-3 col-md-3">
				<div class="form-group">
					<label for="inputEmail" class="col-lg-12 control-label">Sports activity</label>
						<div class="col-lg-12 col-md-12">
                            <input name="sport_activity" type="text" class="form-control" id="inputEmail">
						</div>
				</div>
                <div class="form-group">
					<label for="inputEmail" class="col-lg-12 control-label">Height</label>
					<div class="col-lg-12">
                                            <input name="height" type="text" class="form-control" id="inputEmail">
					</div>
				</div>
				<div class="form-group">
					<label for="inputEmail" class="col-lg-12 control-label">Weight</label>
					<div class="col-lg-12">
                                            <input name="weight" type="text" class="form-control" id="inputEmail">
					</div>
				</div>
			</div>
			<div class="col-lg-4 col-md-4">
			 
		<div class="form-group">
					<label for="inputEmail" class="col-lg-12 control-label">Special diet</label>
					<div class="col-lg-12">
                                            <input name="diet" type="text" class="form-control" id="inputEmail">
					</div>
				</div>
				<div class="form-group">
					<label for="inputEmail" class="col-lg-12 control-label">Drink alcohol</label>
					<div class="col-lg-12">
						<div class="radio">
          <label>
              <input name="alcohol" type="radio" id="optionsRadios1" value="yes">
            Yes
          </label>
		   <label>
		  <input name="alcohol" type="radio" id="optionsRadios1" value="no" checked>
            No
          </label>
        </div>
					</div>
				</div>
				<div class="form-group">
					<label for="inputEmail" class="col-lg-12 control-label">Smoke </label>
						<div class="col-lg-12 col-md-12">
					<div class="radio">
          <label>
            <input type="radio" name="smoke" id="optionsRadios1" value="yes" >
            Yes
          </label>
		   <label>
		  <input type="radio" name="smoke" id="optionsRadios1" value="no" checked>
            No
          </label>
        </div>
				</div>
				</div>
				<div class="form-group">
					<label for="inputEmail" class="col-lg-12 control-label">Other</label>
						<div class="col-lg-12 col-md-12">
                                                    <input name="other" type="text" class="form-control" id="inputEmail">
				</div>
				</div>
			</div>
			<div class="col-lg-3 col-md-3 contact-detail-last">
			
			&nbsp;
				 
			</div>
       
            
     
		</div>
		</div>
	</div>
  </div>

  <div class="user-profile">
	<div class="container">
		<div class="user-pro-inner">
			<ul class="nav nav-tabs">
<!--<li class="active"><a href="#medical-history" data-toggle="tab" aria-expanded="true">Medical History </a></li>-->
<li class="tabLinkClass active" id="myAllergiesTabLink"><a href="javascript:void(0);" onclick="toggleCondTab('myAllergiesTabLink', 'my-allergies');" data-toggle="tab" aria-expanded="true">My Allergies</a></li>
<li class="tabLinkClass" id="myExistingTabLink"><a href="javascript:void(0);" onclick="toggleCondTab('myExistingTabLink', 'existing-condition');" data-toggle="tab" aria-expanded="false">Pre-Existing Condition</a></li>
<li class="tabLinkClass" id="myPersnalTabLink"><a href="javascript:void(0);" onclick="toggleCondTab('myPersnalTabLink', 'personal-updates');" data-toggle="tab" aria-expanded="false">Personal Updates</a></li>
<li class="tabLinkClass" id="myFamilyTabLink"><a href="javascript:void(0);" onclick="toggleCondTab('myFamilyTabLink', 'family-history');" data-toggle="tab" aria-expanded="false">Family History</a></li>
<li class="tabLinkClass" id="myDoctorTabLink"><a href="javascript:void(0);" onclick="toggleCondTab('myDoctorTabLink', 'my-doctors');" data-toggle="tab" aria-expanded="false" style="margin-right: 0px;">My Doctors</a></li>
			</ul>
<div id="myTabContent" class="tab-content">
<!--  <div class="tab-pane fade active in other-profile-section" id="medical-history">
 
    <div class="row">
			<div class="col-lg-3 col-md-3">
			 
				<div class="form-group">
					
					 
						<label for="inputEmail" class="  control-label">Date of Visit</label>
                                                <select name="v_dd[]" class="form-control" id="select">
          <?php
                                        for($i=1; $i<=31; $i++){
                                            $selected = '';
                                        //if ($cardExpDate[2] == $i) //$selected = ' selected="selected"';
                                        print('<option value="'.str_pad($i, 2, "0", STR_PAD_LEFT).'"'.$selected.'>'.str_pad($i, 2, "0", STR_PAD_LEFT).'</option>'."\n"); 
                                        }
                                        ?>
                                                </select>
                                                <select name="v_mm[]" class="form-control" id="select">
          <?php
                                        for($i=1; $i<=12; $i++){
                                            $selected = '';
                                       // if ($cardExpDate[1] == $i) //$selected = ' selected="selected"';
                                        print('<option value="'.str_pad($i, 2, "0", STR_PAD_LEFT).'"'.$selected.'>'.str_pad($i, 2, "0", STR_PAD_LEFT).'</option>'."\n"); 
                                        }
                                        ?>
        </select>
                                                <select name="v_yy[]" class="form-control" id="select">
          <?php
                                        for($i=date('Y'); $i>1899; $i--) {
                                        $selected = '';
                                        //if ($cardExpDate[0] == $i) //$selected = ' selected="selected"';
                                        print('<option value="'.$i.'"'.$selected.'>'.$i.'</option>'."\n");
                                    }
                                    ?>
        </select>
				</div>
				<div class="form-group">
					<label for="inputEmail" class="control-label">Healthcare Visit </label>
					<div class=" ">
                                            <input name="health" type="text" class="form-control" id="inputEmail">
					</div>
				</div>
				<div class="form-group">
					<label for="inputEmail" class="control-label">Practitioner name </label>
					<div class=" ">
						<input type="text" class="form-control" id="inputEmail">
					</div>
				</div>
				<div class="form-group">
					<label for="inputEmail" class="control-label">Doctors Comments </label>
					<div class=" ">
						<textarea class="form-control" rows="3" id="textArea"></textarea>
					</div>
				</div>
				<div class="form-group">
					<label for="inputEmail" class="control-label">Prescription </label>
					<div class=" ">
						<textarea class="form-control" rows="3" id="textArea"></textarea>
					</div>
				</div>
			 
				 
				 
				<div class="button-submit">
					<a href="#" class="add-more">Add more</a>
				</div>
				
				 
				
			</div>
			<div class="col-lg-9 col-md-9">
			 <div class="medical-history-table table-design">
				<div class="table-header">
					<div class="table-head-col">Date of Visit</div>
					<div class="table-head-col">Healthcare Visit</div>
					<div class="table-head-col">Practitioner Name</div>
					<div class="table-head-col">Doctors Comments</div>
					<div class="table-head-col">Prescription</div>
				</div>
				<div class="table-body">
					<div class="table-row">
						<div class="table-col">Date of Visit</div>
						<div class="table-col">Healthcare Visit</div>
						<div class="table-col">Practitioner Name</div>
						<div class="table-col">Doctors Comments</div>
						<div class="table-col">Prescription</div>
					</div>
					<div class="table-row">
						<div class="table-col">Date of Visit</div>
						<div class="table-col">Healthcare Visit</div>
						<div class="table-col">Practitioner Name</div>
						<div class="table-col">Doctors Comments</div>
						<div class="table-col">Prescription</div>
					</div>
					<div class="table-row">
						<div class="table-col">Date of Visit</div>
						<div class="table-col">Healthcare Visit</div>
						<div class="table-col">Practitioner Name</div>
						<div class="table-col">Doctors Comments</div>
						<div class="table-col">Prescription</div>
					</div>
					
					 
				</div>
			 
			 </div>
			</div>
		</div>
  </div>-->
  <!-- My Allergies -->
  <div class="tab-pane active in fade" id="my-allergies">
 
    <div class="row">
			<div class="col-lg-3 col-md-3">
			 
				 
			 
				<div class="form-group">
					<label for="inputEmail" class="control-label">Resistant </label>
					<div class=" ">
                                            <textarea name="resistant[]" class="form-control" rows="3" id="textArea"></textarea>
					</div>
				</div>
				<div class="form-group">
					<label for="inputEmail" class="control-label">Reaction </label>
					<div class=" ">
                                            <textarea name="reaction[]" class="form-control" rows="3" id="textArea"></textarea>
					</div>
				</div>
			 	
				<div class='more-form'></div> 
				 
				<div class="button-submit">
                                    <a href="javascript:void(0)" id="allergy-addmorebtn" class="add-more">Add more</a>
				</div>
                            
                            <script type="text/javascript" src="https://code.jquery.com/jquery.min.js"></script>     
                               <script type="text/javascript">
                                   
                                    $(document).ready(function(){
                                        $("#allergy-addmorebtn").on("click", function(){
                                            $(".more-form").append("\
                                    \
<div class='form-group'>\
<label for='inputEmail' class='control-label'> Resistant </label>\n\
<div class=''> \n\
<textarea name='resistant[]' class='form-control' rows='3' id='textArea'></textarea> \n\
</div>\n\
</div> \n\
<div class='form-group'>\n\
<label for='inputEmail' class='control-label'>Relationship</label>\n\
    <div class=''>  \n\
    <textarea name='reaction[]' class='form-control' rows='3' id='textArea'></textarea> </div> \n\
</div>\n\
"); 
                                        });
                                        $(document).on("click", "a.remove" , function() {
                                            $(this).parent().remove();
                                        });
                                    });
                                </script>
                                
                                
				
				 
				
			</div>
        
			<div class="col-lg-9 col-md-9">
			 <div class="medical-history-table table-design">
				<div class="table-header">
					<div class="table-head-col col-lg-4 col-md-4 col-sm-4 col-xs-4">Resistant</div>
					<div class="table-head-col col-lg-4 col-md-4 col-sm-4 col-xs-4">Reaction</div>
					<div class="table-head-col col-lg-4 col-md-4 col-sm-4 col-xs-4">Status</div>
					
				</div>
				<?php if($getallergies) : ?>
                <?php foreach ($getallergies as $row): ?>
				<div class="table-body">
					<div class="table-col col-lg-4 col-md-4 col-sm-4 col-xs-4"><?php echo $row->resistant; ?></div>
					<div class="table-col col-lg-4 col-md-4 col-sm-4 col-xs-4"><?php echo $row->reaction; ?></div>
					<div class="table-col col-lg-4 col-md-4 col-sm-4 col-xs-4">No</div>
				 
				</div>
                <?php endforeach; ?>
            	<?php endif; ?>
			 </div>
			</div>
		</div>
  </div>
  
  
  <!-- Existing Condition -->
   <div class="tab-pane fade in" id="existing-condition">
    
    <div class="row">
			<div class="col-lg-3 col-md-3">
			 
				<div class="form-group">
					<label for="inputEmail" class="control-label">Pre-Existing Conditions: <span style="width:11px">(Multi selection field)</span> </label>
					<div class=" ">
                                              <select style="width:160px;" name="feeling[]" multiple>
                                                <option value="Hypertension">Hypertension</option>
                                                <option value="Hypotension">Hypotension</option>
                                                <option value="Diabetes">Diabetes</option>
                                                <option value="Arthritis">Arthritis</option>
                                                <option value="Asthma">Asthma</option>
                                                <option value="Cancer">Cancer</option>
                                                <option value="Migraine">Migraine</option>
                                                <option value="Anxiety">Anxiety</option>
                                                <option value="Depression">Depression</option>
                                                <option value="OCD">OCD</option>                                                
                                              </select>
					</div>
				</div>
				
				 
				
			</div>
			<div class="col-lg-9 col-md-9">
			 <div class="medical-history-table table-design">
				<div class="table-header">
					<div class="table-head-col col-lg-12">How are you feeling?</div>
					 
				</div>
                <?php if($getPreExisting) : ?>
                <?php foreach ($getPreExisting as $row2): ?>
				<div class="table-body">
					<div class="table-col col-lg-12"><?php echo $row2->feeling; ?></div>
					 
				</div>
                <?php endforeach; ?>
            	<?php endif; ?>
			 </div>
			</div>
		</div>
   </div>
   <div class="tab-pane fade in" id="personal-updates">
  
    <div class="row">
			<div class="col-lg-3 col-md-3">
			 
				<div class="form-group">
					<label for="inputEmail" class="control-label">How are you feeling? </label>
					<div class=" ">
                                            <textarea name="pu_feeling" class="form-control" rows="3" id="textArea"></textarea>
					</div>
				</div>
				<div class="form-group">
					<label for="inputEmail" class="control-label">Symptoms </label>
					<div class=" ">
                                            <textarea name="symptom"class="form-control" rows="3" id="textArea"></textarea>
					</div>
				</div>
				<div class="form-group">
					<label for="inputEmail" class="control-label">Medication taken </label>
					<div class=" ">
						<div class="radio">
          <label>
            <input type="radio" name="med_taken" id="optionsRadios1" value="yes" checked="">
            Yes
          </label>
		   <label>
		  <input type="radio" name="med_taken" id="optionsRadios1" value="no" checked="">
            No
          </label>
        </div>
					</div>
				</div>
				<div class="form-group">
					<label for="inputEmail" class="control-label">Describe the meds </label>
					<div class=" ">
                                            <textarea name="describe_med" class="form-control" rows="3" id="textArea"></textarea>
					</div>
				</div>
				 
			 <div class="form-group">
					<label for="inputEmail" class="control-label">Date </label>
					<div class=" ">
                                            <select name="pu_dd" class="form-control" id="select">
          <?php
                                        for($i=1; $i<=31; $i++){
                                            $selected = '';
                                        //if ($cardExpDate[2] == $i) //$selected = ' selected="selected"';
                                        print('<option value="'.str_pad($i, 2, "0", STR_PAD_LEFT).'"'.$selected.'>'.str_pad($i, 2, "0", STR_PAD_LEFT).'</option>'."\n"); 
                                        }
                                        ?>
        </select>
                                            <select name="pu_mm" class="form-control" id="select">
          <?php
                                        for($i=1; $i<=12; $i++){
                                            $selected = '';
                                       // if ($cardExpDate[1] == $i) //$selected = ' selected="selected"';
                                        print('<option value="'.str_pad($i, 2, "0", STR_PAD_LEFT).'"'.$selected.'>'.str_pad($i, 2, "0", STR_PAD_LEFT).'</option>'."\n"); 
                                        }
                                        ?>
        </select>
                                            <select name="pu_yy" class="form-control" id="select">
          <?php

                                        for($i=date('Y'); $i>1899; $i--) {
                                        $selected = '';
                                        //if ($cardExpDate[0] == $i) //$selected = ' selected="selected"';
                                        print('<option value="'.$i.'"'.$selected.'>'.$i.'</option>'."\n");
                                    }
                                    ?>
        </select>
					</div>
				</div>
			 
		
				 
				
			</div>
			<div class="col-lg-9 col-md-9">
			 <div class="medical-history-table table-design">
				<div class="table-header">
					<div class="table-head-col col-lg-3 col-md-3 col-sm-3 col-xs-3">How are you feeling?</div>
					<div class="table-head-col col-lg-2 col-md-2 col-sm-2 col-xs-2">Symptoms</div>
					<div class="table-head-col col-lg-2 col-md-2 col-sm-2 col-xs-2">Medication</div>
					<div class="table-head-col col-lg-3 col-md-3 col-sm-3 col-xs-3">Describe the meds</div>
					<div class="table-head-col col-lg-2 col-md-2 col-sm-2 col-xs-2">Date</div>
				</div>
                <?php if($getPersonalUpdate) : ?>
                <?php foreach ($getPersonalUpdate as $row3):?>
				<div class="table-body">
					<div class="table-col col-lg-3 col-md-3 col-sm-3 col-xs-3"><?php echo $row3->feeling; ?></div>
					<div class="table-col col-lg-2 col-md-2 col-sm-2 col-xs-2"><?php echo $row3->symptom; ?></div>
					<div class="table-col col-lg-2 col-md-2 col-sm-2 col-xs-2"><?php echo $row3->med_taken; ?></div>
					<div class="table-col col-lg-3 col-md-3 col-sm-3 col-xs-3"><?php echo $row3->describe_med; ?></div>
					<div class="table-col col-lg-2 col-md-2 col-sm-2 col-xs-2"><?php echo $row3->date; ?></div>
					
				</div>
                <?php endforeach; ?>
            <?php endif; ?>
			 </div>
			</div>
		</div>
   </div>
   <div class="tab-pane fade in" id="family-history">
    
     <div class="row">
			<div class="col-lg-3 col-md-3">
			 
				<div class="form-group">
					<label for="inputEmail" class="control-label">Condition </label>
					<div class=" ">
                                            <textarea name="condition[]" class="form-control" rows="3" id="textArea"></textarea>
					</div>
				</div>
				 
			
				<div class="form-group">
					<label for="inputEmail" class="control-label">Relationship </label>
					<div class=" ">
                                            <input name="fh_relationship[]" type="text" class="form-control" id="inputEmail">
					</div>
				</div>
                            
                            <div class="family-more-form"></div>
	
				<div class="button-submit">
                                    <a href="javascript:void(0)" id="family-addmorebtn" class="add-more">Add more</a>
				</div>
				
                            <script type="text/javascript">
                                   
                                    $(document).ready(function(){
                                        $("#family-addmorebtn").on("click", function(){
                                            $(".family-more-form").append("\
                                    \
<div class='form-group'>\
<label for='inputEmail' class='control-label'> Condition </label>\n\
<div class=''> \n\
<textarea name='condition[]' class='form-control' rows='3' id='textArea'></textarea> \n\
</div>\n\
</div> \n\
<div class='form-group'>\n\
<label for='inputEmail' class='control-label'>Relationship</label>\n\
    <div class=''>  \n\
    <input name='fh_relationship[]' type='text' class='form-control' id='inputEmail'> </div> \n\
</div>\n\
"); 
                                        });
                                        $(document).on("click", "a.remove" , function() {
                                            $(this).parent().remove();
                                        });
                                    });
                                </script>
                                
                                
				 
				
			</div>
			<div class="col-lg-9 col-md-9">
			 <div class="medical-history-table table-design">
				<div class="table-header">
					<div class="table-head-col col-lg-4 col-md-4 col-sm-4 col-xs-4">Condition</div>
					<div class="table-head-col col-lg-4 col-md-4 col-sm-4 col-xs-4">Status</div>
					<div class="table-head-col col-lg-4 col-md-4 col-sm-4 col-xs-4">Relationship</div>
					 
				</div>
				<?php if($familyHistory) : ?>
                             <?php foreach ($familyHistory as $row4): ?>
				<div class="table-body">
					<div class="table-col col-lg-4 col-md-4 col-sm-4 col-xs-4"><?php echo $row4->condition; ?></div>
					<div class="table-col col-lg-4 col-md-4 col-sm-4 col-xs-4">Healthcare Visit</div>
					<div class="table-col col-lg-4 col-md-4 col-sm-4 col-xs-4"><?php echo $row4->relationship; ?></div>
			 
				</div>
                             <?php endforeach; ?>
                         <?php endif; ?>
			 </div>
			</div>
		</div>
   </div>
   <div class="tab-pane fade in" id="my-doctors">
    <div class="row">
			<div class="col-lg-3 col-md-3">
			 
			 
				<div class="form-group">
					<label for="inputEmail" class="control-label">Name </label>
					<div class=" ">
                                            <input name="md_name[]" type="text" class="form-control" id="inputEmail">
					</div>
				</div>
				<div class="form-group">
					<label for="inputEmail" class="control-label">Surname </label>
					<div class=" ">
                                            <input name="md_sname[]" type="text" class="form-control" id="inputEmail">
					</div>
				</div>
				<div class="form-group">
					<label for="inputEmail" class="control-label">Contact number </label>
					<div class=" ">
                                            <input name="md_number[]" type="text" class="form-control" id="inputEmail">
					</div>
				</div>
				<div class="form-group">
					<label for="inputEmail" class="control-label">Email address </label>
					<div class=" ">
                                            <input name="md_email[]" type="text" class="form-control" id="inputEmail">
					</div>
				</div>
			 
				<div class="mydoctor-more-form"></div> 
				 
				<div class="button-submit">
                                    <a href="javascript:void(0)" id="mydoctor-addmorebtn" class="add-more">Add more</a>
				</div>
				
                            
                            <script type="text/javascript">
                                   
                                    $(document).ready(function(){
                                        $("#mydoctor-addmorebtn").on("click", function(){
                                            $(".mydoctor-more-form").append("\
                                    \
<div class='form-group'>\
<label for='inputEmail' class='control-label'> Name </label>\n\
<div class=''> \n\
 <input name='md_name[]' type='text' class='form-control' id='inputEmail'> \n\
</div>\n\
</div> \n\
<div class='form-group'>\n\
<label for='inputEmail' class='control-label'>Surname</label>\n\
    <div class=''>  \n\
     <input name='md_sname[]' type='text' class='form-control' id='inputEmail'> </div> \n\
</div>\n\
<div class='form-group'>\n\
<label for='inputEmail' class='control-label'>Contact number</label>\n\
    <div class=''>  \n\
    <input name='md_number[]' type='text' class='form-control' id='inputEmail'>  </div> \n\
</div>\n\
<div class='form-group'>\n\
<label for='inputEmail' class='control-label'>Email address</label>\n\
    <div class=''>  \n\
    <input name='md_email[]' type='text' class='form-control' id='inputEmail'>  </div> \n\
</div>\n\
"); 
                                        });
                                        $(document).on("click", "a.remove" , function() {
                                            $(this).parent().remove();
                                        });
                                    });
                                </script>
                                
                                
				 
				
			</div>
			<div class="col-lg-9 col-md-9">
			 <div class="medical-history-table table-design">
				<div class="table-header">
					<div class="table-head-col col-lg-3 col-md-3 col-sm-3 col-xs-3">Name</div>
					<div class="table-head-col col-lg-3 col-md-3 col-sm-3 col-xs-3">Surname</div>
					<div class="table-head-col col-lg-3 col-md-3 col-sm-3 col-xs-3">Contact number</div>
					<div class="table-head-col col-lg-3 col-md-3 col-sm-3 col-xs-3">Email address</div> 
				</div>
                             <?php if($myDoctor): ?>
                             <?php foreach ($myDoctor as $row5): ?>
				<div class="table-body">
					<div class="table-col col-lg-3 col-md-3 col-sm-3 col-xs-3"><?php echo $row5->name;?></div>
					<div class="table-col col-lg-3 col-md-3 col-sm-3 col-xs-3"><?php echo $row5->sname;?></div>
					<div class="table-col col-lg-3 col-md-3 col-sm-3 col-xs-3"><?php echo $row5->contact_number;?></div>
					<div class="table-col col-lg-3 col-md-3 col-sm-3 col-xs-3"><?php echo $row5->email;?></div> 
					 
				</div>
                            <?php endforeach; ?>
                        <?php endif; ?>
			 </div>
			</div>
		</div>  
  </div>
</div>
		</div>
           <div class="button-submit">
		<button type="submit" class="btn btn-default submit-button">Submit</button>
	   </div>
            
	</div>
  </div>
  
  
  <script type="text/javascript">
                                   
			function toggleCondTab(lid,did) {
				$('.tabLinkClass').removeClass('active');
				
				$('#my-allergies').hide();
				$('#existing-condition').hide();
				$('#personal-updates').hide();
				$('#family-history').hide();
				$('#my-doctors').hide();
				
				$('#'+lid).addClass('active');
				$('#'+did).show();
				
				
			}
   
   </script>


     <?php echo form_close(); ?>