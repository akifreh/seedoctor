<!DOCTYPE html>
<html>
<head>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Payments</title>
    <link href="<?php echo base_url("/css/bootstrap.css"); ?>" rel="stylesheet" type="text/css" />
</head>
<body>
<?php echo $this->session->flashdata('msg'); ?>
 <div class="appointment-table pending-payments-table">
    <div class="container">
      <h2>Pending Payments</h2>
      <div class="table-design">
      <div class="appointment-table-inner pending-payments-table-inner ">
          <div class="table-header name-tab">Healthcare Person</div>
          <div class="table-header  name-tab">Date</div>
          <div class="table-header  name-tab">Time</div>
          <div class="table-header tb-10 name-tab">Duration</div>
          <div class="table-header tb-10 name-tab">Payment</div>
          <div class="table-header">Action</div>
          <?php if($records) : ?>
          <?php foreach ($records as $value): ?>
          <div class="table-data border-right"><?php echo $value->doctorname; ?></div>
          <div class="table-data   border-right"><?php echo date('d M Y', strtotime($value->start_time)); ?></div>
          <div class="table-data   border-right"><?php echo date('h:i a', strtotime($value->start_time)) . ' - ' . date('h:i a', strtotime($value->end_time)); ?></div>
          <div class="table-data tb-10 border-right"><?php echo $value->charged_duration; ?></div>
          <div class="table-data tb-10 border-right"><?php echo $value->price; ?></div>
          <div class="table-data text-center" style="text-align: center;padding-left: 10px;">
            <a href="<?php echo base_url().'appointment/detail/'.$value->appointment_id;?>" class="">Details</a>&nbsp;&nbsp;
            <a href="<?php echo base_url().'userprofile/payPendingAmount/'.$value->appointment_id;?>">Pay Now</a>
          </div>                         
          <?php endforeach;?>
          <?php endif; ?>
        </div>
        </div>
    </div>
   </div>
</body>