  <!-- CANCEL APPOINTMENT -->
  <?php $attributes = array("name" => "cancel-appointment-form", "id"=> "cancel-appointment-form");
  echo form_open("appointment/cancelAppoinment", $attributes); ?>
 
<div class="modal cancel-appointment sad-modal" id="cancel-modal">
  <div class="modal-dialog">
    <div class="modal-content">
      <button type="button" id="cross-modal" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
      <div class="modal-body">
        <div class="schedule-appointment">
          <h2>Cancel Appointment</h2>
          <div class="appointment-form">
            <input type="hidden" name="appointment_id" id="modal-appointment-id" />
            <div class="form-group">
              <label for="select" class=" control-label">Briefly describe the reason for cancelling your appointment</label>
              <textarea name="reason" class="form-control" rows="3" id="cancel-textArea"></textarea>
            </div>
            
            <button class="sad-button cancel-appointment-submit" aria-hidden="true" data-toggle="modal" data-target=".cancel-appointment" >Submit</button>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<?php echo form_close(); ?>
 
<script type="text/javascript">   
  $(document).on('click', '#cross-modal', function(){
    $('#cancel-modal').hide();  
  });

  $(document).on('click', '.cancel-appointment-submit', function(e){
    e.preventDefault();
    var validated = validateCancelForm();                
    if(!validated){
      return false;
    }else{
      $('.ajaxloader').show();
      $('#cancel-appointment-form').submit();
    }            
  });
    
  function validateCancelForm(){
      var errorID = 'reason-error';
      $('#'+errorID).remove();
      var reason = $.trim( $('#cancel-textArea').val()); 
      if(reason == ''){
        $('#cancel-textArea').after('<label id="'+errorID+'" class="error" for="reason">This field is required.</label>');
        return false;
      }
      return true;
  } 
</script>
