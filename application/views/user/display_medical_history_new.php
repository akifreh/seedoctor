<style type="text/css">
    #PureChatWidget,header,footer{
        display: none !important;
    }
  .table-responsive {
    width: 100% !important;
}
</style>
<div class="medical-pro-section">
<div class="container">
    <div class="row">
     	<?php if($userinfo) :
     			foreach ($userinfo as $row):   
        		endforeach; 
        	endif; ?>
        
        <?php if($userprofile) : 
        		foreach ($userprofile as $row1) : 
        		endforeach; 
        	endif; ?>

    <h2>Medical Profile</h2>
    <?php if(!isset($dontShowUpdate)) :?>
    	<a class="btn btn-default submit-btn" href ="<?php echo base_url() ?>userprofile/updateMedicalHistory">Add or update your medical history</a>
	<?php endif; ?>
<br/>

<div class="medical-history-detail">
<div class="row">
<div class="col-lg-4 ">
  <strong>Name :</strong> <?php echo isset($row->fname) ? ucfirst($row->fname) . ' ' . ucfirst($row->sname) : '';?> 
  </div>
<?php if(!isset($dontShowUpdate)) :?>
<div class="col-lg-4 ">
<strong> Email :</strong> <?php echo isset($row) ? $row->email : '';?>
 </div>
  <div class="col-lg-4 ">
 <strong>Job Title :</strong> <?php echo isset($row) ? ucfirst($row->job_title) : '';?>
 </div>
 <?php endif; ?>
 </div>
 <div class="row">
<div class="col-lg-4 ">
 <strong>Gender : </strong><?php echo isset($row) ? ucfirst($row->gender) : '';?>
 </div>
<div class="col-lg-4 ">
<strong> Address : </strong>
</strong><?php echo isset($row) ? ucfirst($row->address) : '';?>
 </div>

 <div class="col-lg-4 ">
<strong> Next of Kin :</strong> <?php echo isset($row) ? ucfirst($row->next_of_kin) : '';?>
 </div>
 </div>
  <div class="row">
<div class="col-lg-4 ">

 <strong>Diet :</strong> <?php echo isset($row1) ? ucfirst($row1->diet) : '';?>
 </div>
 
 <div class="col-lg-4 ">
<strong> Height (cm) :</strong> <?php echo isset($row1) ? $row1->height : '' ;?>
 </div>
 
 <div class="col-lg-4 ">
 <strong>Weight (kg) : </strong><?php echo isset($row1) ? $row1->weight : '';?>
 </div>
      
      
 <div class="col-lg-4 ">
 <strong>BMI : </strong><?php echo isset($row1) ? $row1->bmi : '';?>
 </div>     
      
  <div class="col-lg-4 ">
 <strong>BMR : </strong><?php echo isset($row1) ? $row1->bmr : '';?>
 </div>     
      
       <?php $active_level = $this->config->item('active_level'); ?>
      <div class="col-lg-4">
 <strong>Activity Level : </strong> <?php echo isset($row1) ? $active_level[$row1->activity_level]: '';?>
 </div>
 
      
  </div>
  <div class="row">
 <div class="col-lg-4 ">
 <strong>Sport Activity :</strong> <?php echo isset($row1) ? ucfirst($row1->sport_activity) : '';?>
  </div>
 
 <div class="col-lg-4 ">
 <strong>Alcohol :</strong> <?php echo isset($row1) ? ucfirst($row1->alcohol) : '';?>
  </div>
 
 <div class="col-lg-4 ">
<strong> Smoke :</strong> <?php echo isset($row1) ? ucfirst($row1->smoke) : '';?>
  </div>
 
 <div class="col-lg-4">
 <strong>Other Pursuits : </strong> <?php echo isset($row1) ? ucfirst($row1->other) : '';?>
 </div>

  
  </div>
</div>

  <div class="panel panel-default">
    <div class="panel-heading">
      <h3 class="panel-title">Panel containing responsive tabs</h3>
    </div>
    <div class="panel-body">
      <ul class="nav nav-tabs responsive hidden-xs hidden-sm" id="tabs-1">
        <li class="active"><a href="#medical-history">Medical History</a></li>
        <li class=""><a href="#allergies">Allergies</a></li>
        <li class=""><a href="#pre-existing-condition">Pre Existing Condition</a></li>
        <li><a href="#my-updates">My Updates</a></li>
        <li><a href="#family-history">Family History</a></li>
        <li><a href="#my-doctors">My Doctors</a></li>
      </ul>
        
        
        
        <div class="tab-content responsive hidden-xs hidden-sm">
            <div class="tab-pane active" id="medical-history">
                <div class=" table-responsive" >
                    <table id="medical-history-list" class="table table-striped table-bordered" cellspacing="0" width="100%">
                        <thead>
                            <tr>
                                <th>Date of Visit</th>
                                <th>Location</th>
                                <th>Practitioner Name</th>
                                <th>Doctor comments</th>
                                <th>Prescription</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php if ($previousHistory) :
                                    foreach ($previousHistory as $row5):
                                        ?>
                                        <tr>
                                            <td><?php echo date('d M Y', strtotime($row5->start_time)); ?></td>
                                            <td><?php echo 'Site'; ?></td>
                                            <td><?php echo 'Dr ' . $row5->fname; ?></td>
                                            <td><?php echo isset($row5->comments) ? formulate_multiline_text($row5->comments) : ''; ?></td>
                                            <td><?php echo isset($row5->pres_detail) ? $row5->pres_detail : ''; ?></td>
                                        </tr>
                                    <?php endforeach;
                                endif;
                                ?>
                        </tbody>
                    </table>

                </div>
            </div>
            <div class="tab-pane" id="allergies">
                <div class=" table-responsive" >
                    <table id="allergies-list" class="table table-striped table-bordered  table-responsive" cellspacing="0" width="100%">
                        <thead>
                            <tr>
                                <th>Resistant</th>
                                <th>Reaction</th>
                            </tr>
                            <tr>
                                <td>Resistant: e.g Nuts ... </td>
                                <td>Reaction: e.g Swelling ...</td>
                            </tr>
                        </thead>
                        <tbody>
                            
                            <?php if ($userallergies) :
                                    foreach ($userallergies as $userallergy):
                                        ?>
                                        <tr>
                                            <td><?php echo ucfirst($userallergy->resistant); ?></td>
                                            <td><?php echo ucfirst($userallergy->reaction); ?></td>
                                        </tr>
                                    <?php endforeach;
                                endif;
                                ?>
                        </tbody>
                    </table>

                </div>
            </div>

            <div class="tab-pane " id="pre-existing-condition">
                  <div class=" table-responsive" >
                    <table id="existing-condition-list" class="table table-striped table-bordered  table-responsive" cellspacing="0" width="100%">
                        <thead>
                            <tr>
                                <th>How are you feeling?</th>
                            </tr>
                        </thead>
                        <tbody>
                            
                            <?php if ($userpecs) :
                                    foreach ($userpecs as $userpec):
                                        ?>
                                        <tr>
                                            <td><?php echo ucfirst($userpec->feeling); ?></td>
                                        </tr>
                                    <?php endforeach;
                                endif;
                                ?>
                        </tbody>
                    </table>

                </div>
            </div>

            <div class="tab-pane" id="my-updates">
                 <div class=" table-responsive" >
                    <table id="my-updates-list" class="table table-striped table-bordered  table-responsive" cellspacing="0" width="100%">
                        <thead>
                            <tr>
                                <th>How are you feeling?</th>
                                <th>Symptoms</th>
                                <th>Medication taken</th>
                                <th>Medicine Description</th>
                                <th>Date</th>
                                
                            </tr>
                        </thead>
                        <tbody>
                            
                            <?php if ($userpersonalupdates) :
                                    foreach ($userpersonalupdates as $userpersonalupdate):
                                        ?>
                                        <tr>
                                            <td><?php echo ucfirst($userpersonalupdate->feeling); ?></td>
                                            <td><?php echo ucfirst($userpersonalupdate->symptom); ?></td>
                                            <td><?php echo ucfirst($userpersonalupdate->med_taken); ?></td>
                                            <td><?php echo ucfirst($userpersonalupdate->describe_med); ?></td>
                                            <td><?php echo date('d M Y', strtotime($userpersonalupdate->date)); ?></td>
                                        </tr>
                                    <?php endforeach;
                                endif;
                                ?>
                        </tbody>
                    </table>
                </div>
            </div>

            <div class="tab-pane" id="family-history">
                <div class=" table-responsive" >
                    <table id="family-history-list" class="table table-striped table-bordered  table-responsive" cellspacing="0" width="100%">
                        <thead>
                            <tr>
                                <th>Condition</th>
                                <th>Relationship</th>
                            </tr>
                        </thead>
                        <tbody>
                            
                            <?php if ($userfamilyhistory) :
                                    foreach ($userfamilyhistory as $row4):
                                        ?>
                                        <tr>
                                            <td><?php echo ucfirst($row4->condition); ?></td>
                                            <td><?php echo ucfirst($row4->relationship); ?></td>
                                        </tr>
                                    <?php endforeach;
                                endif;
                                ?>
                        </tbody>
                    </table>
                </div>
            </div>

            <div class="tab-pane" id="my-doctors">
                <div class=" table-responsive" >
                    <table id="my-doctor-list" class="table table-striped table-bordered  table-responsive" cellspacing="0" width="100%">
                            <thead>
                                <tr>
                                    <th>Name</th>
                                    <th>Surname</th>
                                    <th>Contact number</th>
                                    <th>Email address</th>
                                </tr>
                            </thead>
                            <tbody>

                                <?php if ($usermydoctor) :
                                        foreach ($usermydoctor as $row5):
                                            ?>
                                            <tr>
                                                <td><?php echo ucfirst($row5->name);?></td>
                                                <td><?php echo ucfirst($row5->sname);?></td>
                                                <td><?php echo $row5->contact_number;?></td>
                                                <td><?php echo $row5->email;?></td>
                                            </tr>
                                        <?php endforeach;
                                    endif;
                                    ?>
                            </tbody>
                        </table>
                    </div>
            </div>

        </div>
      
    </div>
  </div>



</div>
</div>
</div>

<script>

$(document).ready(function() {
    $('#medical-history-list').DataTable();
});

$(document).ready(function() {
    $('#allergies-list').DataTable();
});
$(document).ready(function() {
    $('#existing-condition-list').DataTable();
});
$(document).ready(function() {
    $('#my-updates-list').DataTable();
});
$(document).ready(function() {
    $('#family-history-list').DataTable();
});
$(document).ready(function() {
    $('#my-doctor-list').DataTable();
});

</script>

<style>
    
    .table-responsive table thead tr:nth-child(1){
        background: #0484cf !important;
        font-size: 14px;
        color: #fff;
    }
    
</style>