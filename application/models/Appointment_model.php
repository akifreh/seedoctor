<?php
class Appointment_model extends CI_Model
{
    function __construct()
    {
        // Call the Model constructor
        parent::__construct();
    }
    
    function readNutritionists()
    {
        $condition = 'doctor_cv.type_of_doctor =1 AND users.status = 1';
        $this->db->select('*')
            ->from('users')
            ->join('doctor_cv', 'users.id = doctor_cv.user_id');
//          ->join('available_doctor_days', 'users.id = available_doctor_days.user_id')
//          ->join('timing', 'users.id = timing.user_id');     
        
        $this->db->where($condition);
        $query = $this->db->get();
        
        if ($query->num_rows() >= 1)
        {
            $result = $query->result();
            foreach($result as $row)
            {
                $row->profile_pic = get_image_url($row->profile_pic);
            }
            return $result;
        }
        else
            return false;
    }
    
    function readPsychologists()
    {
        $condition = 'type_of_doctor = 2 AND users.status = 1';
        $this->db->select('*')
            ->from('users')
            ->join('doctor_cv', 'users.id = doctor_cv.user_id')
            ->join('doctor_online', 'users.id = doctor_online.user_id');
        $this->db->order_by('rating', 'DESC');
        $this->db->order_by('no_of_patient', 'DESC');
        $this->db->group_by('doctor_online.user_id');
        $this->db->where($condition);
        $query = $this->db->get();

        if ($query->num_rows() >= 1)
            return $query->result();
        else
            return false;
    }
    
     function readAllPsychologists()
     {
        $condition = 'doctor_cv.type_of_doctor = 2 AND users.status = 1';
        $this->db->select('*')
            ->from('users')
            ->join('doctor_cv', 'users.id = doctor_cv.user_id');
//         ->join('available_doctor_days', 'users.id = available_doctor_days.user_id')
//         ->join('timing', 'users.id = timing.user_id');     
        $this->db->where($condition);
        $query = $this->db->get();
        
        if ($query->num_rows() >= 1)
        {
            $result = $query->result();
            foreach($result as $row)
            {
                $row->profile_pic = get_image_url($row->profile_pic);
            }
            return $result;
        }
        else
            return false;
    }
    
    function readMedicalDoctor()
    {
        $condition = 'type_of_doctor = 3 AND users.status = 1';
        $this->db->select('*')
            ->from('users')
            ->join('doctor_cv', 'users.id = doctor_cv.user_id')
            ->join('doctor_online', 'users.id = doctor_online.user_id');
        $this->db->order_by('rating', 'DESC');
        $this->db->order_by('no_of_patient', 'DESC');
        $this->db->group_by('doctor_online.user_id');
        $this->db->where($condition);
        $query = $this->db->get();

        if ($query->num_rows() >= 1)
            return $query->result();
        else
            return false;
    }
    
    function readAllMedicalDoctor()
    {
       $condition = 'doctor_cv.type_of_doctor = 3 AND users.status = 1';
        $this->db->select('*')
            ->from('users')
            ->join('doctor_cv', 'users.id = doctor_cv.user_id');
//         ->join('available_doctor_days', 'users.id = available_doctor_days.user_id')
//         ->join('timing', 'users.id = timing.user_id');     
        $this->db->where($condition);
        $query = $this->db->get();

        if ($query->num_rows() >= 1)
        {
            $result = $query->result();
            foreach($result as $row)
            {
                $row->profile_pic = get_image_url($row->profile_pic);
            }
            return $result;
        }
        else
            return false; 
    }
    
    function readOneMedicalDoctor($doctorId)
    {
        //$condition = 'type_of_doctor = medical-doctor';
        $this->db->select('*')
            ->from('users')
            //->join('doctor_cv', 'users.id = doctor_cv.user_id')
            ->join('available_doctor_days', 'users.id = available_doctor_days.user_id')
            ->join('days', 'available_doctor_days.day_id = days.id');
       
         //->join('timing', 'users.id = timing.user_id');   
        $this->db->where('available_doctor_days.user_id', $doctorId);
        
        $query = $this->db->get();
        
        if ($query->num_rows() >= 1)
            return $query->result();
        else
            return false;
    }
    
    function readMedicalDoctorTiming($userId)
    {
        $condition = 'user_id =' . $userId;
        $this->db->select('*')
            ->from('users')
            ->join('timing', 'users.id = timing.user_id');
        $this->db->where($condition);
        $query = $this->db->get();
        
        if ($query->num_rows() >= 1)
            return $query->result();
        else
            return false;
    }
    
    function readDoctorDays($dayId, $user_Id)
    {
        $condition = 'day_id='. $dayId . ' AND user_id='. $user_Id;
        $this->db->select('*')
            ->from('days')
            ->join('available_doctor_days', 'days.id = available_doctor_days.day_id');
        $this->db->where($condition);
        $query = $this->db->get();
            
        if ($query->num_rows() >= 1)
            return $query->result();
        else
            return false;
    }
                
    function readDoctorTime($doctorId, $day = NULL)
    {
        $condition = 'user_id='. $doctorId;
     
        $this->db->select('*')
            ->from('timing');
        $this->db->where($condition);
        
        if($day)
        {
            $day_id = $this->get_day_id($day);
            $this->db->where('day_id', $day_id);
            $this->db->order_by('start_time', 'ASC');
        }
        
        $query = $this->db->get();
            
        if ($query->num_rows() >= 1)
            return $query->result();
        else
            return;
    }
    
    function readNutriTime($doctorId, $day = NULL)
    {
        $condition = 'user_id='. $doctorId;
        
        $this->db->select('*')
            ->from('timing');
        $this->db->where($condition);

        if($day)
        {
            $day_id = $this->get_day_id($day);
            $this->db->where('day_id', $day_id);
            $this->db->order_by('start_time', 'ASC');
        }

        $query = $this->db->get();
        
        if ($query->num_rows() >= 1)
            return $query->result();
        else
            return false; 
    }
    
    function readPsychoTime($doctorId, $day = NULL)
    {
        $condition = 'user_id='. $doctorId;
     
        $this->db->select('*')->from('timing');
        $this->db->where($condition);
        if($day)
        {
            $day_id = $this->get_day_id($day);
            $this->db->where('day_id', $day_id);
            $this->db->order_by('start_time', 'ASC');
        }

        $query = $this->db->get();
        if ($query->num_rows() >= 1)
            return $query->result();
        else
            return false; 
    }
    
    function checkDeleteAppointmentNow($id)
    {
        $seconds = $this->config->item('timer_seconds') . ' seconds';
        $time = new DateTime('- ' . $seconds);
        $this->db->where('p_id', $id);
        $this->db->where('created_at >', $time->format('Y-m-d H:i:s'));
        $query = $this->db->get('appointment_now');
        $result = $query->row();
        if($result)
            return array(TRUE, $result->created_at);

        $rows_deleted = FALSE;
        $this->db->where('p_id', $id);
        $query = $this->db->get('appointment_now');
        if($query->num_rows())
        {
            $rows_deleted = TRUE;

            $this->db->where('p_id', $id);
            $result = $this->db->delete('appointment_now');
        } 
        
        return array(FALSE, $rows_deleted);
    }

    function getAppointmentNow($id)
    {
        $this->db->where('p_id', $id);
        $query = $this->db->get('appointment_now');
        $result = $query->result();
        
        return $result ? $result : NULL;
    }

    function get_day_id($day = NULL)
    {
        switch ($day) {
            case 'Mon':
                $day_id = 1;
                break;
            case 'Tue':
                $day_id = 2;
                break;
            case 'Wed':
                $day_id = 3;
                break;
            case 'Thu':
                $day_id = 4;
                break;
            case 'Fri':
                $day_id = 5;
                break;
            case 'Sat':
                $day_id = 6;
                break;
            case 'Sun':
                $day_id = 7;
                break;
        }
        return $day_id;
    }

    function readDoctorAvailableTime($doctorId)
    {
        $condition = 'd_id ='. $doctorId;
        $this->db->select('*')
            ->from('appointments');
        $this->db->where($condition);
        $query = $this->db->get();
        
        if ($query->num_rows() >= 1)
            return $query->result();
        else
            return false;
    }
    
    function readPsychoAvailableTime($doctorId)
    {
        $condition = 'd_id ='. $doctorId;
        $this->db->select('*')
            ->from('appointments');
        $this->db->where($condition);
        $query = $this->db->get();
        
        if ($query->num_rows() >= 1)
            return $query->result();
        else
            return false; 
    }
    
    function readNutriAvailableTime($doctorId)
    {
        $condition = 'd_id ='. $doctorId;
        $this->db->select('*')
            ->from('appointments');
        $this->db->where($condition);
        $query = $this->db->get();
        
        if ($query->num_rows() >= 1)
            return $query->result();
        else
            return false;  
    }

    function insertSymptoms($data)
    {
        $this->db->insert('appointment_symptoms', $data);
    }

    function insertNutritionistAppointment($data)
    {
        $data['created_at'] = date('Y-m-d H:i:s', time());
        $this->db->insert('appointments', $data);
        return $this->db->insert_id(); // Getting last inserted id  
    }
    
    function insertPsychologistAppointment($data)
    {
        $data['created_at'] = date('Y-m-d H:i:s', time());
        $this->db->insert('appointments', $data);
        return $this->db->insert_id(); // Getting last inserted id  
    }
    
    function insertMedicalDoctorAppointment($data)
    {
        $data['created_at'] = date('Y-m-d H:i:s', time());
        $this->db->insert('appointments', $data);
        return $this->db->insert_id(); // Getting last inserted id 
    }
    
    function readPatientAppointment($pID)
    {
        $condition = 'appointments.p_id =' . $pID;
        $this->db->select('*')
            ->from('users')
            ->join('appointments', 'users.id = appointments.p_id');
        $this->db->where($condition);
        $query = $this->db->get();
        
        if ($query->num_rows() >= 1)
            return $query->result();
        else
            return false;               
    }
    
    function insertNowAppointments($data)
    {
        $data['meeting_approval'] = 0;
       // $data['created_at'] = date('Y-m-d H:i:s', time());
        $this->db->insert('appointment_now', $data);
        return $this->db->insert_id(); // Getting last inserted id 
        
    }
    
    
    function insertNotification($data)
    {
        $data['created_date'] = date('Y-m-d H:i:s', time());
        $this->db->insert('notification', $data);
        return $this->db->insert_id(); // Getting last inserted id 
    }
    
    
    function sendEmailToDoctor($to_email, $doctorName, $randomString, $action = 'approve')
    {
        if($action == 'approve')
        {
            $data = array(
                'to_email' => $to_email, 
                'subject' => 'Appointment Now Link',
                'content' => 'Please click on the link below to start your call with the patient.',
                'hasButton' => TRUE,
                'buttonLabel' => 'Start Call',
                'buttonLink' => base_url() .'call?sessionkey=' . $randomString,
                'username' => 'Dr '. $doctorName
             );
        }
        else
        {
            $data = array(
                'to_email' => $to_email, 
                'subject' => 'Appointment Denied',
                'content' => 'You have recently denied an appoinment request.<br /><br />If it wasn\'t you please login and change your password.',
                'hasButton' => TRUE,
                'buttonLabel' => 'Login',
                'buttonLink' => base_url(),
                'username' => $doctorName
             );
        }

        send_email($data);
    }
    
    function sendEmailToPatient($to_email, $patientName, $str, $action = 'approve', $doctorName = NULL)
    {
        if($action == 'approve')
        {
            $data = array(
                'to_email' => $to_email, 
                'subject' => 'Appointment Approved',
                'content' => 'Dr ' . $doctorName . ' has approved your appointment request.<br /><br />Please click on the following link to start your call.',
                'hasButton' => TRUE,
                'buttonLabel' => 'Start Call',
                'buttonLink' => base_url() .'call?sessionkey=' . $str,
                'username' => $patientName
             );
         
        }
        else
        {
            $data = array(
                'to_email' => $to_email, 
                'subject' => 'Appointment Denied',
                'content' => 'Dr ' . $doctorName . ' has denied your appointment now request.<br /><br />Please wait until your request is approved by other doctors online or schedule an appoinment.',
                'hasButton' => FALSE,
                'username' => $patientName
             );
        }
        send_email($data);
    }
    
    function sendEmailToPatientForAppointment($to_email, $patientName, $appDate, $appTime)
    {
        $data = array(
            'to_email' => $to_email, 
            'subject' => 'Appointment Schedule',
            'content' => 'Your Appointment has been set on <b>'. $appDate .'</b> at <b>' . $appTime . '</b><br /><br />You will recieve a meeting room url 30 minutes before your meeting time.',
            'hasButton' => FALSE,
            'username' => $patientName
        );
        send_email($data);
    }
    
    function sendEmailToDoctorForAppointment($to_email, $doctorName, $appDate, $appTime)
    {    
        $data = array(
            'to_email' => $to_email, 
            'subject' => 'Appointment Request',
            'content' => 'You have a request for appointment on <b>'. $appDate .'</b> at <b>' . $appTime . '</b><br /><br />You will recieve a meeting room url 30 minutes before your meeting time.',
            'hasButton' => FALSE,
            'username' => 'Dr ' . $doctorName
        );
        send_email($data);
    }
    
    function sendCronEmailToPatientForAppointment($to_email, $patientName, $appDate, $appTime, $str, $doctorName)
    { 
        $data = array(
            'to_email' => $to_email, 
            'subject' => 'Appointment in 30 mins',
            'content' => 'Your appointment with Dr ' . $doctorName . ' on <b>'. $appDate .'</b> at <b>' . $appTime . '</b> will start in 30 minutes.<br /><br />Click below to start your meeting after 30 minutes.',
            'hasButton' => TRUE,
            'buttonLabel' => 'Start Meeting',
            'buttonLink' => base_url() .'call?sessionkey='.$str,
            'username' => $patientName
        );
        send_email($data);
    }
    
    function sendCronEmailToDoctorForAppointment($to_email, $doctorName, $appDate, $appTime, $str, $patientName)
    {
        $data = array(
            'to_email' => $to_email, 
            'subject' => 'Appointment in 30 mins',
            'content' => 'Your meeting with ' . $patientName .' on <b>'. $appDate .'</b> at <b>' . $appTime . '</b> will start in 30 minutes.<br /><br />Click below to start your meeting after 30 minutes.',
            'hasButton' => TRUE,
            'buttonLabel' => 'Start Meeting',
            'buttonLink' => base_url() .'call?sessionkey='.$str,
            'username' => 'Dr ' . $doctorName
        );
        send_email($data);
    }
    
    function readPatientScheduledAppointment($appId)
    {
        $condition = 'id ='. $appId;
        $this->db->select('*')
            ->from('appointments');
        $this->db->where($condition);
        $query = $this->db->get();
        
        if ($query->num_rows() >= 1)
            return $query->result();
        else
            return false;  
    }
    
    function readAllAppointments()
    {
        $time = new DateTime();
        $this->db->where('type', 'schedule');
        $this->db->where('meeting_approval', 0);
        $this->db->where('start_time >', $time->format('Y-m-d H:i:s'));
        $time->modify('+1 day');
        $this->db->where('start_time <', $time->format('Y-m-d 00:00:00'));
        $query = $this->db->get('appointments');
        //echo $this->db->last_query(); die;
        $result = $query->result();
        
        return $result ? $result : NULL;        
    }
    
    function getLastestAppointmentLink($id)
    {
        // 5 mins before start of any call
        $time = new DateTime();
        $condition = 'p_id =' . $id . ' AND meeting_approval = 1';
        $this->db->where($condition);
        $this->db->where('start_time >', $time->format('Y-m-d H:i:s'));
        $time->modify('+5 mins');
        $this->db->where('start_time <', $time->format('Y-m-d H:i:s'));
        $query = $this->db->get('appointments');
        $result = $query->row();
        if($result)
        {
            $this->db->where('appointment_id', $result->id);
            $query = $this->db->get('sessionkey');
            $record = $query->row();
            if($record)
            {
                $array = array('link' => base_url() . 'call?sessionkey=' . $record->session_key,
                                'id' => $result->id,
                                'doctor' => $this->getUserById($record->d_id)
                );
            }
                return $array;
        }
        return NULL;
    }

    function readAllMyAppointments($userId)
    {
        $currentDate = date('Y-m-d H:i:s');
        $condition = 'app.p_id='. $userId . ' AND start_time > \''. $currentDate.'\'';;
        $query =  $this->db->select('app.id, app.start_time, app.end_time, doctor.fname as doctorname, patients.fname as patientname,meeting_approval')
            ->from('appointments as app')
            ->join('users as doctor', 'doctor.id=app.d_id')
            ->join('users as patients', 'patients.id=app.p_id')
            ->where($condition);
        $this->db->order_by('start_time');
        $query = $this->db->get();
        
        if ($query->num_rows() >= 1)
            return $query->result();
        else
            return;     
    }

    function getAppointment($id)
    {
        if(!$id)
          return NULL;

        $this->db->where('id', $id);
        $query = $this->db->get('appointments');
        
        $result = $query->row();
        if($result)
        {
            $this->db->select('category, symptom');
            $this->db->where('appointment_id', $id);
            $query = $this->db->get('appointment_symptoms');
            $symptom = $query->result();
            foreach ($symptom as $value) 
            {
                $array[$value->category][] = $value->symptom; 
            }
            $result->symptom = $array;
        }
        return $result ? $result : NULL;     
    }
    
    function deleteAppoinment($id)
    {
         if(!$id)
          return NULL;

        $this->db->where('id', $id);
        $this->db->delete('appointments');

        $this->db->where('appointment_id', $id);
        $this->db->delete('sessionkey');
        
    }

    function sendCancelAppoinmentEmail($to_email, $doctorName, $patientName, $appointmentDate, $person = 'doctor', $role = 2, $reason)
    {
        if($person == 'doctor')
        {
            if($role == 2)
                $content = $patientName. ' has cancelled following appointment with you<br /><br />Date : '. date('d M Y', strtotime($appointmentDate)) . '&nbsp;&nbsp;&nbsp;Time : '. date('h:i a', strtotime($appointmentDate));
            elseif($role == 3)
                $content = 'You have cancelled following appointment with '. $patientName .'<br /><br />Date : '. date('d M Y', strtotime($appointmentDate)) . '&nbsp;&nbsp;&nbsp;Time : '. date('h:i a', strtotime($appointmentDate));

            $content .= '<br /><br />This is the reason given :<br />' . formulate_multiline_text($reason);
            $data = array(
                'to_email' => $to_email, 
                'subject' => 'Appoinment Canceled',
                'content' => $content,
                'hasButton' => FALSE,
                'username' => 'Dr ' . $doctorName
            );

        }
        else
        {
            if($role == 2)
                $content = 'You have cancelled following appointment with Dr '. $doctorName . '<br /><br />Date : '. date('d M Y', strtotime($appointmentDate)) . '&nbsp;&nbsp;&nbsp;Time : '. date('h:i a', strtotime($appointmentDate)) .'<br /><br />
                    Thank you for letting us know and keep us posted if you need anything else from us.';
            elseif($role == 3)
                $content = 'Dr '. $doctorName . ' has cancelled following appointment with You<br /><br />Date : '. date('d M Y', strtotime($appointmentDate)) . '&nbsp;&nbsp;&nbsp;Time : '. date('h:i a', strtotime($appointmentDate)) .'<br /><br />
                    Please make a new appointment.<br />Sorry for any inconvenience.';
            
            $content .= '<br /><br />This is the reason given :<br />' . formulate_multiline_text($reason);
            $data = array(
                'to_email' => $to_email, 
                'subject' => 'Appoinment Canceled',
                'content' => $content,
                'hasButton' => FALSE,
                'username' => $patientName
            );

        }
        send_email($data);
    }

    function getUserById($id)
    {
        if(!$id)
            return NULL;

        $this->db->where('id', $id);
        $query = $this->db->get('users');
        $result = $query->row();

        return $result ? $result : NULL;

    }

    function sendAppointmentNowNotificationEmail($to_email, $doctorName)
    {
        $data = array(
            'to_email' => $to_email, 
            'subject' => 'Appointment Now Request',
            'content' => 'You have a request for appointment now. Please refresh your dashboard to approve or deny the request or click the link below.',
            'hasButton' => TRUE,
            'buttonLabel' => 'See Request',
            'buttonLink' => base_url() .'doctordashboard',
            'username' => 'Dr ' . $doctorName
        );
        send_email($data); 
    }

    function is_profile_incomplete($id)
    {
        $this->db->from('users');
        $this->db->where('id', $id);
        $query = $this->db->get();
        $result = $query->row();
        $total = 13;
        $current = 5;
        if($result)
        {
            if($result->phone)
                $current++;
            if($result->address)
                $current++;
            if($result->p_address)
                $current++;
            if($result->job_title)
                $current++;
            if($result->company_employed)
                $current++;
            if($result->next_of_kin)
                $current++;
            if($result->medical_aid)
                $current++;
            if($result->profile_pic)
                $current++;

            if($total == $current)
                return FALSE;
            else
                return $current;
        }
        
         return $current;
    }

    function has_card_expired($id)
    {
        $this->db->from('user_registration');
        $this->db->where('user_id', $id);
        $query = $this->db->get();
        $result = $query->row();
        if(!$result || !$result->card_expiry)
            return TRUE;
        
        $now = date(('Y-m-d'), time());
        if($now >= $result->card_expiry)
            return 'expired';
        return FALSE;
    }

    function has_pending_amount($id)
    {
        $this->db->select('appointments.*, payments.*');
        $this->db->from('appointments');
        $this->db->where('appointments.p_id', $id);
        $this->db->where('payments.status', 0);

        $this->db->join('payments', 'appointments.id=payments.appointment_id');
        $query = $this->db->get();
        $result = $query->row();
        if($result)
            return TRUE;
        
        return FALSE;
    }

    function getNextAvailableTime($type = 3)
    {
        $condition = 'doctor_cv.type_of_doctor = ' .$type .' AND users.status = 1';
        $this->db->select('doctor_cv.user_id');
        $this->db->join('doctor_cv', 'users.id = doctor_cv.user_id');     
        $this->db->where($condition);
        $query = $this->db->get('users');
        $records = $query->result();
        
        $user_id = $result = NULL;
		if($records) :
	        foreach ($records as $key => $value) 
	        {
	            $user_id[]= $value->user_id;
	        }
		endif;

        if($user_id)
        {
            $current_time = time();
            $time = date('H:i:s', $current_time);
            $day = date('D', $current_time);
            $day = $this->get_day_id($day);

            $this->db->where('day_id', $day);
            $this->db->where('start_time >',$time);
            $this->db->where_in('user_id', $user_id);
            $this->db->order_by('start_time');
            $query = $this->db->get('timing');
            $result = $query->row();
        }
        
        return $result ? $result->start_time : NULL;
    }
}