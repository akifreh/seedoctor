<?php
class Home_model extends CI_Model
{
    function __construct()
    {
        // Call the Model constructor
        parent::__construct();
    }
    
    function getUserCount($role = 0)
    {
        $this->db->where('role', $role);
        $this->db->where('status', 1);

        $query = $this->db->get('users');
        $result = $query->num_rows();

        return $result ? $result : 0;
    }

     function getServedPatientCount()
    {
        $query = $this->db->get('payments');
        $result = $query->num_rows();

        return $result ? $result : 0;
    }

    function get_clients($status = 1)
    {

        $this->db->where('status', $status);
        $this->db->limit(10);
        $this->db->order_by('id', 'DESC');
        $query = $this->db->get('our_clients');
        $result = $query->result();

        if($result) :
        foreach ($result as $key => $value) 
        {
            $value->logo = $value->logo ? base_url() . 'uploads/clients/' . $value->logo : '';   
        }
        endif;
        return $result;
    }

    function getBestDoctors()
    {
        $this->db->where('users.status', 1);
        $this->db->join('doctor_cv', 'users.id = doctor_cv.user_id');
        $this->db->order_by('rating', 'DESC');
        $this->db->order_by('no_of_patient', 'DESC');
        $this->db->limit(9);
        
        $query = $this->db->get('users');
        $result = $query->result();
        

        foreach($result as $row)
        {
            $row->profile_pic = get_image_url($row->profile_pic);
        }
        return $result ? $result : NULL;
    }

    //send verification email to user's email id
    function sendClientEmailToAdmin($data)
    {   
        $content = 'Name : ' . $data['name'] . ' ' . $data['surname'];
        $content .= '<br />Email : '. $data['email'];
        $content .= '<br />Company Name : '. $data['company_name'];
        $content .= '<br />Company Size : '. $data['company_size'];
        $content .= '<br />Message : '. $data['message'];

        $data = array(
            'to_email' => $this->config->item('site_email'), 
            'subject' => 'Client Request Email',
            'content' => $content,
            'hasButton' => FALSE,
            'username' => 'Admin'
        );
        send_email($data);
        return TRUE;
    }
}