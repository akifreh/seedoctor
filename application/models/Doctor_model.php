<?php
class Doctor_model extends CI_Model
{
    function __construct()
    {
        // Call the Model constructor
        parent::__construct();
    }
    
   function insertDoctor($data)
    {
       $this->db->insert('users', $data);
        return $user_id = $this->db->insert_id(); // Getting last inserted id 
  }
    
    function insertDoctorDays($data2){
       
       for($i =0; $i< count($data2['day']); $i++ ){
           
        $this->db->insert('available_doctor_days', array('day_id'=>$data2['day'][$i], 'user_id'=> $data2['user_id'])); 
       }
    } 
    function insertDoctorInfo($data1){
             
       
       $this->db->insert('doctor_cv', $data1);  
    }
    
    function insertDoctorTiming($data3){
       
       $this->db->insert_batch('timing', $data3); 
    }
    
    function insertAcademics($data){
       
       $this->db->insert('doctor_academics', $data); 
    }
    
    function insertExperience($data){
       $this->db->insert('doctor_experience', $data); 
    }

    function readDoctorNowAppointment($doctorId, $timeCheck = TRUE)
    {
        $time = new DateTime();
        $time->modify('-60 mins');
        
        $condition = 'd_id = '. $doctorId . ' AND meeting_approval='. 0 ;
        $this->db->select('*')->from('appointment_now');
        $this->db->where($condition);
        $this->db->order_by('created_at', 'DESC');
        if($timeCheck)
            $this->db->where('created_at >=', $time->format('Y-m-d g:i:s'));
        $query = $this->db->get();

        if ($query->num_rows() >= 1)
        {
            $result = $query->result();
            $this->db->select('*')->from('appointment_now');
            $condition = 'p_id = '. $result[0]->p_id . ' AND meeting_approval='. 1 ;
            $this->db->where($condition);
            $this->db->where('created_at', $result[0]->created_at);
            $query = $this->db->get();
            if ($query->num_rows() >= 1)
                return false;       
            return $result; 
        }
        else
            return false;
    }
    
    function readPatient($patientId){
        $condition = 'id = '. $patientId;
        $this->db->select('*')
        ->from('users');
    $this->db->where($condition);
    $query = $this->db->get();
            if ($query->num_rows() >= 1) {
            return $query->result();
            } else {
            return;
        }   
    }
    
    function readAllPatient($patientId, $doctorId){
        $condition = 'p_id = '. $patientId . ' AND d_id = '. $doctorId;
        $this->db->select('*')
        ->from('users')
        ->join('appointments', 'users.id = appointments.p_id');
    $this->db->where($condition);
    $query = $this->db->get();
            if ($query->num_rows() >= 1) {
            return $query->result();
            } else {
            return;
        }   
    }
    
    function updateApproveStatus($data){
        
        $this->db->where('id',$data['id']);

        return $this->db->update('appointment_now', $data);    
    }
    
    function readApprovedAppointment($dId, $pId){
        $condition = 'p_id =' . $pId . ' AND d_Id =' . $dId . ' AND meeting_approval = 1';
        $this->db->select('*')
        ->from('appointments');
        $this->db->where($condition);
        $this->db->where('type', 'schedule-');
        $query = $this->db->get();
            if ($query->num_rows() >= 1) {
            return $query->result();
            } else {
            return;
        }   
    }
    
    function readApprovedNowAppointment($dId, $pId){
        $condition = 'p_id =' . $pId . ' AND d_Id =' . $dId . ' AND meeting_approval = 1';
        $this->db->select('*')
        ->from('appointment_now');
        $this->db->where($condition);
        $query = $this->db->get();
            if ($query->num_rows() >= 1) {
            return $query->result();
            } else {
            return;
        }   
    }

    function getLastestAppointmentLink($dId)
    {
        $time = new DateTime();
        $condition = 'd_Id =' . $dId . ' AND meeting_approval = 1';
        $this->db->where($condition);
        $this->db->where('start_time >', $time->format('Y-m-d H:i:s'));
        $time->modify('+5 mins');
        $this->db->where('start_time <', $time->format('Y-m-d H:i:s'));
        $query = $this->db->get('appointments');
        $result = $query->row();
        if($result)
        {
            $this->db->where('appointment_id', $result->id);
            $query = $this->db->get('sessionkey');
            $record = $query->row();
            if($record)
            {
                $array = array('link' => base_url() . 'call?sessionkey=' . $record->session_key,
                                'id' => $result->id,
                                'user' => $this->getUserById($record->p_id));
                return $array;
            }
        }
        return NULL;
    }

    function insertSessionKey($sessInfo){
        $this->db->insert('sessionkey',$sessInfo); 
    }
    
    function readAllScheduledAppointments($doctorId){
      $currentDate = date('Y-m-d H:i:s');  
      $condition = 'd_id='. $doctorId. ' AND start_time >= \''. $currentDate.'\'';
      $this->db->select('*, app.id AS appointment_id')
      ->from('appointments as app')
      ->join('users u', 'u.id=app.p_id');
      $this->db->where($condition);
      $this->db->order_by('start_time', 'ASC');
      //$this->db->where('type', 'schedule');
      
      $query = $this->db->get();
            if ($query->num_rows() >= 1) {
            return $query->result();
            } else {
            return;
        } 
    }
    
    function readAllPreviousAppointments($doctorId, $meeting_approval = 3)
    {
        $currentDate = date('Y-m-d H:i:s');
        $condition = 'd_id='. $doctorId. ' AND start_time < \''. $currentDate.'\'';
        $this->db->select('*, app.id AS appointment_id')->from('appointments as app')->join('users u', 'u.id=app.p_id');
        $this->db->where($condition);
        if($meeting_approval == 3)
            $this->db->where('meeting_approval', $meeting_approval);
        else
             $this->db->where('((meeting_approval = 1 AND type = "now") OR type = "schedule")');
        $this->db->order_by('start_time', 'DESC');
        $query = $this->db->get();
        $result = $query->result();
        if($result && $meeting_approval = 3)
        {
            foreach($result as $row)
            {
                $this->db->where('appointment_id', $row->appointment_id);
                $query = $this->db->get('payments');
                $row->payments = $query->row();
            }
        }
        return $result ? $result : NULL;
    }
    
    function readDoctorRating($doctorId){
        $condition = 'user_id='. $doctorId;
        $this->db->select('rating') 
        ->from('doctor_cv');
      
        $this->db->where($condition);
        $query = $this->db->get();
                    if ($query->num_rows() >= 1) {
                    return $query->result();
                    } else {
                    return;
                }
    }
    
    
    function addAppointment($data)
    {
       $data['created_at'] = date('Y-m-d H:i:s', time());
       $this->db->insert('appointments', $data);
       return $this->db->insert_id(); // Getting last inserted id 
    } 
    
    function deleteAllAppointmentNow($p_id, $created_at)
    {
         if(!$p_id)
          return NULL;

        $this->db->where('p_id', $p_id);
        $this->db->where('created_at', $created_at);
        return $this->db->delete('appointment_now');
    }

    function updateDoctor($data, $id){
        
        $this->db->where('id',$id);
        return $this->db->update('users', $data);    
    }
    
    function updateSymptoms($data, $p_id){
        
        $this->db->where('p_id',$p_id);
        $this->db->where('appointment_id', NULL);
        return $this->db->update('appointment_symptoms', $data);    
    }
    
    function updateDoctorInfo($data, $id){
        
        $this->db->where('user_id', $id);
        return $this->db->update('doctor_cv', $data);    
    }

    function updateDoctorDays($data, $id)
    {
        $this->db->where('user_id', $id);
        $this->db->delete('available_doctor_days');
        foreach($data as $value)
            $this->db->insert('available_doctor_days', array('day_id'=>$value, 'user_id'=> $id));

        return TRUE; 
    }

    function deleteAcademics($id)
    {
        $this->db->where('user_id', $id);
        return $this->db->delete('doctor_academics');  
         
    }

    function deleteExperience($id)
    {
        $this->db->where('user_id', $id);
        return $this->db->delete('doctor_experience');
    }

    function updateDoctorTiming($data, $id)
    {
        $this->db->where('user_id', $id);
        $this->db->delete('timing');

        $this->db->insert_batch('timing', $data);

        return TRUE; 
    } 

    function updateAppointment($data, $id){
        
        $this->db->where('id', $id);
        return $this->db->update('appointments', $data);    
    }

    function sendTimingsChangedEmail($id, $oldTimings, $oldDays)
    {
        if(!$id)
            return NULL;
        $appointments = $this->readAllScheduledAppointments($id);
        if($appointments)
        {
            $content = $this->generateRescheduleMessage($id, $oldTimings, $oldDays);
            $this->load->model('admin_model');
            foreach ($appointments as $key => $value) 
            {
                $patient = $this->admin_model->readUserDetails($value->p_id);
                $data = array(
                    'to_email' => $patient->email, 
                    'subject' => 'Reschedule your appointment',
                    'content' => $content,
                    'hasButton' => FALSE,
                    'username' => ucfirst($patient->fname)
                );
                send_email($data);
            }
        }
    }

    function generateRescheduleMessage($id, $oldTimings, $oldDays)
    {
        $this->load->model('admin_model');
        $newDays = $this->admin_model->readDoctorAvailableDays($id);
        $newTimings = $this->admin_model->readNewDoctorTimings($id);
        $doctor = $this->admin_model->readDoctorDetails($id);
        
        $content = 'Please note that Doctor ' . ucfirst($doctor->fname) . ' has changed his appointment availability.';    
        $content .= '<br /><br /><strong>Previous Appointment Availability :</strong><br />Days & Timings :';
        if($oldTimings) :
            foreach($oldTimings as $key => $v) :                 
                switch ($key) {
                    case '1':
                      $content .= '<br />Monday :';
                      break;
                    case '2':
                      $content .=  '<br />Tuesday :';
                      break;
                    case '3':
                      $content .=  '<br />Wednesday :';
                      break;
                    case '4':
                      $content .=  '<br />Thursday :';
                      break;
                    case '5':
                      $content .=  '<br />Friday :';
                      break;
                    case '6':
                      $content .=  '<br />Saturday :';
                      break;
                    case '7':
                      $content .=  '<br />Sunday :';
                      break;
                }  
                foreach ($v as $key => $teacherTiming) :
                    $content .=  '<br />' . date('g:i a', strtotime($teacherTiming->start_time)) . ' - ' .  date('g:i a', strtotime($teacherTiming->end_time));  
                endforeach;
            endforeach;                     
        endif;

        $content .= '<br /><br /><strong>New Appointment Availability</strong> (applicable from now) :<br />Days & Timings :';
        if($newTimings) :
            foreach($newTimings as $key => $v) :                 
                switch ($key) {
                    case '1':
                      $content .= '<br />Monday :';
                      break;
                    case '2':
                      $content .=  '<br />Tuesday :';
                      break;
                    case '3':
                      $content .=  '<br />Wednesday :';
                      break;
                    case '4':
                      $content .=  '<br />Thursday :';
                      break;
                    case '5':
                      $content .=  '<br />Friday :';
                      break;
                    case '6':
                      $content .=  '<br />Saturday :';
                      break;
                    case '7':
                      $content .=  '<br />Sunday :';
                      break;
                }  
                foreach ($v as $key => $teacherTiming) :
                    $content .=  '<br />' . date('g:i a', strtotime($teacherTiming->start_time)) . ' - ' .  date('g:i a', strtotime($teacherTiming->end_time));  
                endforeach;
            endforeach;                     
        endif;
        $content .= '<br /><br />Please reschedule your appointment with him if these changes have affected your appointment else ignore this email.';
        return $content;
    }

    function getUserById($id)
    {
        if(!$id)
            return NULL;

        $this->db->where('id', $id);
        $query = $this->db->get('users');
        $result = $query->row();

        return $result ? $result : NULL;
    }

    /**
     * Doctor book patient Appointment
     * @param type $doctorId
     * @param type $appointment_id
     * @return boolean
     * 
     */
    
    function DoctorBookAppointment($doctorId, $appointment_id)
    {
        $time = new DateTime();
        $time->modify('-60 mins');
        
        $condition = 'd_id = '. $doctorId . ' AND meeting_approval = 0 AND id = '.$appointment_id  ;
        $this->db->select('*')->from('appointment_now');
        $this->db->where($condition);
        $this->db->order_by('created_at', 'DESC');
        
        if($timeCheck)
            $this->db->where('created_at >=', $time->format('Y-m-d g:i:s'));
        
        $query = $this->db->get();
        
        if ($query->num_rows() == 1)
        { // check Appointment not book by other doctor
            $result = $query->result();
            $this->db->select('*')->from('appointment_now');
            $condition = 'p_id = '. $result[0]->p_id . ' AND meeting_approval='. 1 ;
            $this->db->where($condition);
            $this->db->where('created_at', $result[0]->created_at);
            $query = $this->db->get();
            
            if ($query->num_rows() >= 1){
                return false;       
            }
            return $result; 
            
        }else{
            return false;
        }
    }

    function DoctorSignupEmailSendToAdmin($data)
    {
        
        $content = 'Dr. '. ucfirst($data['fname']) .' '. ucfirst($data['sname']).' has joined our team.<br/><br />Please review his application.';

        $email_data = array(
            'to_email' => $this->config->item('site_email'),
            'subject' => 'New Doctor Joined',
            'content' => $content,
            'hasButton' => FALSE,
            'username' => 'Admin'
        );
        send_email($email_data);
    }

    function DoctorSignupEmailSendToDoctor($data)
    {
        $content = 'Thank you for joining See-A-Doctor team.<br /><br />
                    Your application was successfully submitted. The See-A-Doctor Admin Team will review your application and you will receive an email about the status of your application within two working days.<br /><br />
                    We look forward to working with you in enabling access to healthcare.<br /><br />';

        /*$content = 'Thank you for choosing to join See-A-Doctor team.<br /><br />
                    Your application was submitted successfully. The See-A-Doctor Team will review your application and you will receive an email about the status of your application as soon as possible.<br /><br />
                    We look forward to working with you in enabling access to quality healthcare across Africa. <br /><br />'; */

        $email_data = array(
            'to_email' => $data['email'], 
            'subject' => 'Your Application Received',
            'content' => $content,
            'hasButton' => FALSE,
            'username' => ucfirst($data['fname']) . ' ' . ucfirst($data['sname'])
        );
        send_email($email_data);
    }

    function readAllPayments($id, $status = 1)
    {
        $this->db->select('appointments.*, payments.*');
        $this->db->from('appointments');
        $this->db->where('appointments.d_id', $id);
        $this->db->where('payments.status', $status);

        $this->db->join('payments', 'appointments.id=payments.appointment_id');
        $query = $this->db->get();
        $result = $query->result();
        
        $total = 0;
        foreach ($result as $key => $value) 
        {
            $value->patient_name = $this->get_username_by_id($value->p_id);
            $total += $value->price;
        }

        return $result ? array($result,$total) : array(NULL,NULL);
    }

    function get_username_by_id($id)    
    {
        if(!$id)
          return NULL;

        $this->db->select('fname, sname');
        $this->db->where('id', $id);
        $query = $this->db->get('users');
        $result = $query->row();
        
        return $result ? $result->fname . ' ' . $result->sname : NULL;
    }

        /**** ADDED FOR API SPECIALLY ****/    
    function apiInsertDoctorDays($data)
    {   
       $this->db->insert_batch('available_doctor_days', $data);  
    }

    function apiDeleteAcademic($id)
    {
        $this->db->where('id', $id);
        return $this->db->delete('doctor_academics');      
    }

    function apiDeleteExperience($id)
    {
        $this->db->where('id', $id);
        return $this->db->delete('doctor_experience');
    }

    function apiDeleteTiming($id)
    {
        $this->db->where('id', $id);
        return $this->db->delete('timing');
    }

    function updateAcademic($data, $id)
    {   
        $this->db->where('id', $id);
        return $this->db->update('doctor_academics', $data);    
    }

    function updateExperience($data, $id)
    {   
        $this->db->where('id', $id);
        return $this->db->update('doctor_experience', $data);    
    }

    function updateTiming($data, $id)
    {   
        $this->db->where('id', $id);
        return $this->db->update('timing', $data);    
    }

    function alreadyDayPresent($id, $day_id)
    {
        if(!$id || !$day_id)
            return FALSE;

        $this->db->where('user_id', $id);
        $this->db->where('day_id', $day_id);
        $query = $this->db->get('available_doctor_days');

        $result = $query->num_rows();

        return $result ? TRUE : FALSE;
    }

    /**** END OF API SPECIALLY ****/ 
}
