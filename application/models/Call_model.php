<?php
class Call_model extends CI_Model
{
    function __construct()
    {
        // Call the Model constructor
        parent::__construct();
    }
    
    function readSessionKey($sessionKey, $status_check = TRUE)
    {
        $condition = 'session_key ='. "'$sessionKey'";    
        $this->db->select('*')->from('sessionkey');
        $this->db->where($condition);
        if($status_check)
            $this->db->where('status', 1);
        $query = $this->db->get();
        $result = $query->row();

        return $result ? $result : NULL;
    }
    
    function getUserById($id)
    {
        if(!$id)
            return Null;

        $this->db->where('id', $id);
        $query = $this->db->get('users');
        $result = $query->row();

        return $result ? $result : NULL;
    }
    
    function checkFirstCall($p_id = NULL)
    {
        if(!$p_id)
            return FALSE;

        $this->db->select('id');
        $this->db->where('p_id', $p_id);
        $query = $this->db->get('appointments');
        $result = $query->result();

        if(!$result)
            return TRUE;

        foreach($result as $key => $value) 
        {
            $ids[] = $value->id;
        }

        $this->db->where_in('appointment_id', $ids);
        $query = $this->db->get('payments');
        $result = $query->num_rows();        
    
        return $result ? FALSE : TRUE;   
    }


    function getLatLong($id)
    {
        if(!$id)
            return NULL;

        $this->db->where('id', $id);
        $query = $this->db->get('appointments');
        $result = $query->row();

        return $result ? array('lat' => $result->latitude, 'long' => $result->longitude) : NULL;
    }

    function addPrescription($data = NULL)
    {
        if(!$data)
            return NULL;

        $this->db->insert('prescription', $data);
    }

    function sendPatientInvoiceEmail($to_email, $patient, $doctor, $payment)
    {   
        $record = $this->getPaymentById($payment['payment_id']);
        $rate = $this->getDoctorRate($doctor->id);
        $card = $this->getCardById($patient->id);

        $mins = date('i', strtotime($record->actual_duration));
        $mins = (int) $mins;
        $surplus_time = 0;
        if($mins > 15)
        {
            $surplus = $rate;
            $surplus_time = $mins - 15;
        }
            
        $content =  '<strong>Consultation with Dr ' . $doctor->fname . '</strong><br /><br />';
        $content .= '<strong>Total Time : </strong>' . date('i', strtotime($record->charged_duration)) .' mins<br />';
        $content .= '<strong>Base Time : </strong>15 mins<br />';
        if($surplus_time) :
            $content .= '<strong>Surplus Time : </strong>' . $surplus_time .' mins<br />';
        endif;

        $content .= '<br /><strong>Base Fee : </strong>R ' . $rate;
        if($surplus_time) :
            $content .= '<br /><strong>Surplus Time Fee : </strong>R ';
            $content .=  isset($surplus) ? $record->price - $surplus : 0;
        endif;
        
        $content .= '<br /><strong>Total Fee : </strong>R ' . $record->price;
        $content .= '<br /><br /><strong>Charged : </strong>' . $card->card_brand . ' ' . $card->card_number;
        $content .= '<br /> Thank you for using SeeADoctor.africa';

        $data = array(
            'to_email' => $to_email, 
            'subject' => 'Payment Invoice',
            'content' => $content , 
            'hasButton' => FALSE,
            'username' => $patient->fname
        );
        send_email($data);
    }

    function sendPatientNoInvoiceEmail($to_email, $patient, $doctor, $payment)
    {       
        /******************** START OF - CLOSING THIS FOR NO-CHARGE TEMPORARY *************
        $content = 'This was your first call, so it was not charged.<br />Thank you for using SeeADoctor.africa'; */
        $content =  'Your consultation with <strong>Dr ' . $doctor->fname . '</strong> has finished successfully.<br />';
        $content .= '<br /> Thank you for using SeeADoctor.africa';        
        /********************** END OF - CLOSING THIS FOR NO-CHARGE TEMPORARY *************/
        $data = array(
            'to_email' => $to_email, 
            'subject' => 'First ',
            'content' => $content , 
            'hasButton' => FALSE,
            'username' => $patient->fname
        );
        send_email($data);
    }

    function sendPrescriptionToPatient($to_email, $patient, $doctor, $data)
    {   
        $content = '<strong>Patient : </strong>' . $patient->fname . ' ' . $patient->sname . '<br /><br />';
        $content .= '<strong>Doctor : </strong>' . $doctor->fname . ' ' . $doctor->sname  .'<br /><strong>Profession Number : </strong>'. $doctor->profession_number;
        $content .= $doctor->practice_number ? '<br /><strong>Practice Number : </strong>' . $doctor->practice_number : '';
        $content .= '<br /><strong>Address : </strong>' . $doctor->address;
        $content .= '<br /><strong>Email : </strong>' . $doctor->email;
        $content .= '<br /><strong>Phone : </strong>' . $doctor->phone;
        $content .=  '<h4>Prescribed Medicine</h4>'. $data . '<br />Prescription generated from "http://seeadoctor.africa';

        $data = array(
            'to_email' => $to_email, 
            'subject' => 'Prescription',
            'content' => $content , 
            'hasButton' => FALSE,
            'notPrescription' => FALSE,
            'username' => $patient->fname
        );
        send_email($data);
    }

    function sendPrescriptionToAdmin($to_email, $patient, $doctor, $data)
    {   
        $content = '<strong>Patient : </strong>' . $patient->fname . ' ' . $patient->sname . '<br />';
        $content .= '<strong>Email : </strong>' . $patient->email .'<br /><br />';
        $content .= '<strong>Doctor : </strong>' . $doctor->fname . ' ' . $doctor->sname  .'<br /><strong>Profession Number : </strong>'. $doctor->profession_number;
        $content .= $doctor->practice_number ? '<br /><strong>Practice Number : </strong>' . $doctor->practice_number : '';
        $content .= '<br /><strong>Address : </strong>' . $doctor->address;
        $content .= '<br /><strong>Email : </strong>' . $doctor->email;
        $content .= '<br /><strong>Phone : </strong>' . $doctor->phone;
        $content .=  '<h4>Prescribed Medicine</h4>'. $data . '<br />Prescription generated from "http://seeadoctor.africa';

        $data = array(
            'to_email' => $to_email, 
            'subject' => 'Prescription',
            'content' => $content , 
            'hasButton' => FALSE,
            'username' => 'Admin'
        );
        send_email($data);
    }
    
    function getAppointmentById($id)
    {
        if(!$id)
          return NULL;

        $this->db->where('id', $id);
        $query = $this->db->get('appointments');
        
        $result = $query->row();
        return $result ? $result : NULL;     
    }
    
    function deleteSessionKey($session_key)
    {
         if(!$session_key)
          return NULL;

        $this->db->where('session_key', $session_key);
        return $this->db->delete('sessionkey');
    }

     function updateAppointmentStatus($data)
     {
        
        $this->db->where('id',$data['id']);
        return $this->db->update('appointments', $data);    
    }

    function updateSessionKey($data)
    {
        
        $this->db->where('id', $data['id']);
        return $this->db->update('sessionkey', $data);    
    }

    function updatePatientServed($doctorId){
        
        $this->db->where('user_id', $doctorId);
        $this->db->set('no_of_patient', 'no_of_patient+1', FALSE);
        $this->db->update('doctor_cv');
    }

    function getDoctorRate($id)
    {
        if(!$id)
          return NULL;

        $this->db->select('charges.rates AS Val');
        $this->db->join('charges', 'doctor_cv.type_of_doctor = charges.id');
        $this->db->where('doctor_cv.user_id', $id);
        $query = $this->db->get('doctor_cv');
        
        $result = $query->row();
        return $result ? $result->Val : 0;     
    }

    function insertPayment($data)
    {
        $this->db->insert('payments', $data);
        return $this->db->insert_id();
    }

    function updatePayment($id, $data)
    {
        $this->db->where('id', $id);
        $this->db->update('payments', $data);
    }

    function updateSessionKeyForSessionId($key, $data)
    {
        $this->db->where('session_key', $key);
        $this->db->update('sessionkey', $data);
    }

    function get_user_registration($id)
    {
        if(!$id)
          return NULL;

        $this->db->select('registration_id AS Val');
        $this->db->where('user_id', $id);
        $query = $this->db->get('user_registration');
        
        $result = $query->row();
        return $result ? $result->Val : 0;     
    }

    function getPaymentById($id)
    {
        if(!$id)
            return NULL;
        $this->db->where('id', $id);

        $query = $this->db->get('payments');
        $result = $query->row();

        return $result ? $result : NULL;
    }

    function getPaymentByAppointmentId($id)
    {
        if(!$id)
            return NULL;
        $this->db->where('appointment_id', $id);

        $query = $this->db->get('payments');
        $result = $query->row();

        return $result ? $result : NULL;
    }

    function getCardById($id)
    {
        if(!$id)
            return NULL;
        $this->db->where('user_id', $id);
        $this->db->where('status', 1);

        $query = $this->db->get('user_registration');
        $result = $query->row();

        return $result ? $result : NULL;
    }
}