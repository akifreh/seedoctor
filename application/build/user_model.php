<?php
class user_model extends CI_Model
{
    function __construct()
    {
        // Call the Model constructor
        parent::__construct();
    }
    
    //insert into user table
    function insertUser($data,$data1)
    {
       $this->db->insert('users', $data);
        $user_id = $this->db->insert_id(); // Getting last inserted id 
         $data1['user_id'] = $user_id;
       return $this->db->insert('card_info', $data1);
    }
    
    function updateUser($data)
    {   
        
        $this->db->where('id',$data['id']);
        $this->db->update('users', $data);
        //$this->db->update('card_info', $data1);
        
    }

    function updateCardInfo($data1)
    {   
        
        $this->db->where('user_id',$data1['id']);
        $this->db->update('card_info', $data1);
        //$this->db->update('card_info', $data1);    
    }
    
    //send verification email to user's email id
    function sendEmail($to_email)
    {
        $domain = $this->config->item('email_domain');
        $from_email = $this->config->item('site_email');
        
        $subject = 'Verify Your Email Address';
        $message = 'Dear User,<br /><br />Please click on the below activation link to verify your email address.<br /><br />'. base_url() .'home/verify/' . md5($to_email) . '<br /><br /><br />Thanks<br />Mydomain Team';
        
       //configure email settings
        $config = $this->config->item('email_settings');
        $this->email->initialize($config);
        
        //send mail
        $this->email->from($from_email, $domain);
        $this->email->to($to_email);
        $this->email->subject($subject);
        $this->email->message($message);
        return $this->email->send();
    }
    
    //activate user account
    function verifyEmailID($key)
    {
        $data = array('status' => 1);
        $this->db->where('md5(email)', $key);
        return $this->db->update('users', $data);
    }
    
    function loginUser($data)
    {
        $encPass = md5($data['password']);
       
        $condition = "Email =" . "'" . $data['email'] . "' AND " . "Password =" . "'" . $encPass . "'" . " AND " . 'status =' .'1';
        $this->db->select('*');
        $this->db->from('users');
        $this->db->where($condition);
        $this->db->limit(1);
        $query = $this->db->get();

        if ($query->num_rows() == 1) 
            return true;
        else 
            return false;
        
    }
    
    public function read_user_information($email) 
    {
        $condition = "Email =" . "'" . $email . "'";
        $this->db->select('*');
        $this->db->from('users');
        $this->db->where($condition);
        $this->db->limit(1);
        $query = $this->db->get();

        if ($query->num_rows() == 1)
            return $query->result();
        else
            return false;

    }
        
    public function readUserCardInfo($id)
    {
        $condition = "user_id =" . "'" . $id . "'";
        $this->db->select('*');
        $this->db->from('card_info');
        $this->db->where($condition);
        $this->db->limit(1);
        $query = $this->db->get();
        
        if ($query->num_rows() == 1)
            return $query->result();
        else
            return false;
        
    }
        
}
?>