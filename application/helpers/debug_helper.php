<?php

if ( ! function_exists('admin_url'))
{
	function admin_url($uri = '')
	{
		$CI =& get_instance();
		$uri = $CI->config->item('DEFAULT_PATH_ADMIN') . $uri;
		return $CI->config->site_url($uri);
	}
}

if ( ! function_exists('d'))
{
	function d($obj = NULL, $exit = 1)
	{
		if ($obj)
		{
			print '<pre>';
			print_r ($obj);
			print '</pre>';
		}
		if ($exit == 1)
		{
			exit;
		}
	}
}

if ( ! function_exists('lq'))
{
	function lq($exit = 1)
	{
		$CI =& get_instance();
		echo $CI->db->last_query();
		if ($exit == 1)
		{
			exit;
		}
	}
}

function formulate_substr_words($text = NULL, $length = 30)
{
	if (strlen($text) > $length)
	{
		$text = substr($text, 0, $length);
		$text = strrev($text);
		$text = substr($text, strpos($text, ' '), strlen($text));
		$text = strrev($text) . '...';
	}

	return $text;
}

function formulate_multiline_text($text)
{
	if (!$text)
		return NULL;
	$text = $text ? str_replace("\n", '<br>', $text) : '';
	return $text;
}

function get_timings($day = 1)
{
	$my  = '<div id="timings-'.$day.'"  class="col-lg-2 select-half-on" style="display:none;">';
    $my .= '<select name="from-timimg['.$day.'][]" id="from-timimg[]" class="from-timimg" style="height: 2.2em !important;">';
    $my .= '<option value="">-From-</option>';
    $round=1; $am='AM';
    for($i=1; $i < 13; $i++) :
		$my .= '<option value="' .$i .' '. $am . '">'. $i .':00 '. $am .'</option>';
    	if($i == 11) :
			if($am == 'AM') : $am = 'PM';
            elseif($am == 'PM') : $am = 'AM';
            endif;
        endif;
        if($i == 12 && $round == 1) :
            $i=0; $round=2;
        endif;
    endfor;
    $my .= '</select>';
    $my .= '<select name="to-timimg['. $day .'][]" id="to-timimg[]" class="from-timimg" style=" height: 2.2em !important;">';
    $my .= '<option value="">-To-</option>';
    $round=1; $am='AM';
    for($i=1; $i < 13; $i++) :
		$my .= '<option value="' .$i .' '. $am . '">'. $i .':00 '. $am .'</option>';
    	if($i == 11) :
			if($am == 'AM') : $am = 'PM';
            elseif($am == 'PM') : $am = 'AM';
            endif;
        endif;
        if($i == 12 && $round == 1) :
            $i=0; $round=2;
        endif;
    endfor;
    $my .='</select>';
    $my .='</div>';
   
   /* 
    $my .= '<div class="flexible-'.$day.'" style="">';
    $my .= '<select name="flex-from-timing['.$day .'][]" id="from-timimg[]" class="from-timimg">';
	$my .= '<option value="">-From-</option>';
    $round=1; $am='AM';
    for($i=1; $i < 13; $i++) :
		$my .= '<option value="' .$i .' '. $am . '">'. $i .':00 '. $am .'</option>';
    	if($i == 11) :
			if($am == 'AM') : $am = 'PM';
            elseif($am == 'PM') : $am = 'AM';
            endif;
        endif;
        if($i == 12 && $round == 1) :
            $i=0; $round=2;
        endif;
    endfor;
    $my .= '</select>';
                    
    $my .= '<select name="flex-to-timing['.$day.'][]" id="to-timimg[]" class="from-timimg">';
	$my .= '<option value="">-To-</option>';
    $round=1; $am='AM';
    for($i=1; $i < 13; $i++) :
		$my .= '<option value="' .$i .' '. $am . '">'. $i .':00 '. $am .'</option>';
    	if($i == 11) :
			if($am == 'AM') : $am = 'PM';
            elseif($am == 'PM') : $am = 'AM';
            endif;
        endif;
        if($i == 12 && $round == 1) :
            $i=0; $round=2;
        endif;
    endfor;
    $my .= '</select>';
    $my .= '<div> AND     </div>';
    $my .= '<select name="flex-from-timing['.$day.'][]" id="from-timimg[]" class="from-timimg">';
    $my .= '<option value="">-From-</option>';
    $round=1; $am='AM';
    for($i=1; $i < 13; $i++) :
		$my .= '<option value="' .$i .' '. $am . '">'. $i .':00 '. $am .'</option>';
    	if($i == 11) :
			if($am == 'AM') : $am = 'PM';
            elseif($am == 'PM') : $am = 'AM';
            endif;
        endif;
        if($i == 12 && $round == 1) :
            $i=0; $round=2;
        endif;
    endfor;
    $my .= '</select>';
                    
    $my .= '<select name="flex-to-timing['.$day.'][]" id="to-timimg[]" class="from-timimg">';
                       $my .= '<option value="">-To-</option>';
    $round=1; $am='AM';
    for($i=1; $i < 13; $i++) :
		$my .= '<option value="' .$i .' '. $am . '">'. $i .':00 '. $am .'</option>';
    	if($i == 11) :
			if($am == 'AM') : $am = 'PM';
            elseif($am == 'PM') : $am = 'AM';
            endif;
        endif;
        if($i == 12 && $round == 1) :
            $i=0; $round=2;
        endif;
    endfor;
    $my .='</select>';
    $my .='</div>';
    */
    return $my;
}