var apiKey,
    sessionId,
    token;

    apiKey    = $('#app').attr('apiKey');
    sessionId = $('#app').attr('sessionId');
    token     = $('#app').attr('token');
    
    initializeSession();


function initializeSession() {
  var session = OT.initSession(apiKey, sessionId);
  
  

  // Subscribe to a newly created stream
  session.on('streamCreated', function(event) {
    session.subscribe(event.stream, 'subscriber', {
      insertMode: 'append',
      width: '100%',
      height: '100%'
    });     
  });

// Subscribe to a newly created stream
  session.on('connectionCreated', function(event) {
     if (event.connection.connectionId != session.connection.connectionId) {
        $('input#startCallBut').trigger('click');
  }
      
    
  });
//  session.on("connectionDestroyed", function(event) {
//   console.log('connection destroyed');
//   alert('connection destroyed');
//  });
//  window.addEventListener("beforeunload", function (e) {
//  var confirmationMessage = "\o/";
//
//  (e || window.event).returnValue = confirmationMessage; //Gecko + IE
//  return confirmationMessage;                            //Webkit, Safari, Chrome
//  });
  session.on('sessionDisconnected', function(event) {
          //$('input#endCallBut').trigger('click');
          console.log('You were disconnected from the session.', event.reason);
  });
  
 /* var chatWidget = new OTSolution.TextChat.ChatWidget({
        session: session,
        container: '#chat'
  });*/
  

  // Connect to the session
  session.connect(token, function(error) {
    // If the connection is successful, initialize a publisher and publish to the session
    if (!error) {
      var publisher = OT.initPublisher('publisher', {
        insertMode: 'append',
        width: '100%',
        height: '100%'
      });

      session.publish(publisher);
     

    } else {
      console.log('There was an error connecting to the session: ', error.code, error.message);
    }
  });
}